﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.TileMap.Types;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace NewGame.Test
{
    public class NewPathFinder
    {
        //Our map will consists of booleans, saying, if a tile is passable or not
        public int Size { get; set; }
        public PNode[,] Map { get; set; }

        //Our map has a size of SIZE × SIZE positions, so here |V| is SIZE × SIZE
        public float[][] Dist { get; }
        public char[][] Next { get; }

        protected int TileSize { get; set; }
        protected int GridSizeX, GridSizeY;
        public MapLayer UnWalkMapLayer { get; set; }
        public Vector2 GridWorldSize { get; set; }


        public NewPathFinder(TmxMap pMap, MapLayer pLayer)
        {
            Dist = new float[Size * Size][];
            Next = new char[Size * Size][];

            TileSize = pMap.TileWidth;
            UnWalkMapLayer = pLayer;
            Size = UnWalkMapLayer.Tiles.Count;

            GridWorldSize = new Vector2(pMap.Width * TileSize, pMap.Height * TileSize);
            GridSizeX = pMap.Width;
            GridSizeY = pMap.Height;

            CreateGrid();
        }

        private void CreateGrid()
        {
            Map = new PNode[GridSizeX, GridSizeY];

            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    TileItem tile = UnWalkMapLayer.Tiles[y * GridSizeX + x];

                    Map[x, y] = new PNode(
                        tile.CurrentType != TileItem.Type.Solid,
                        tile.Position, TileSize, x, y);
                }
            }

            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    SortNeighbors(Map[x, y]);
                }
            }
        }

        private void SortNeighbors(PNode pNode)
        {
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    // no diagonals
                    //                    if ((x == -1 || x == 1) && (y == -1 || y == 1))
                    //                        continue;

                    int checkX = pNode.Col + x;
                    int checkY = pNode.Line + y;

                    if (checkX >= 0 && checkX < GridSizeX &&
                        checkY >= 0 && checkY < GridSizeY)
                    {
                        if (x < 0)
                            Map[checkX, checkY].Direction = Direction.Left;
                        else if (x > 0)
                            Map[checkX, checkY].Direction = Direction.Right;
                        else if (y < 0)
                            Map[checkX, checkY].Direction = Direction.Up;
                        else if (y > 0)
                            Map[checkX, checkY].Direction = Direction.Down;

                        Map[checkX, checkY].Dist = 1;

                        //                            Map[y * checkX, y * checkY].Dist = 0;
                    }
                }
            }
        }

    }

    public class PNode
    {
        public bool IsWalkable { get; set; }
        public Direction Direction { get; set; }
        public int TileSize { get; set; }
        public Vector2 Position { get; set; }
        public int Line { get; set; }
        public int Col { get; set; }
        public int Dist { get; set; }

        public PNode(bool isWalkable, Vector2 position, int pTileSize, int pCol, int pLine)
        {
            IsWalkable = isWalkable;
            TileSize = pTileSize;
            Position = position;
            Line = pLine;
            Col = pCol;
        }
    }

}