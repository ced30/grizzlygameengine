﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Test
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example player Input system.
    /// </summary>
    public class SInputControllerPlayer : SBaseInputController
    {
        protected override void ReadGamePad(GameTime pGameTime, IEntity pOwner) { }

        protected override void ReadKeyboard(GameTime pGameTime, IEntity pOwner)
        {
            World.Instance.Input.ReadKeyboard();


            #region Directions

            if (World.Instance.Input.Keyboard.RightPressed)
            {
                pOwner.Systems.Transform.MoveWithDirection(pOwner.Data, Direction.Right);
                pOwner.Data.Properties.IsFlipX = false;
            }

            else if (World.Instance.Input.Keyboard.LeftPressed)
            {
                pOwner.Systems.Transform.MoveWithDirection(pOwner.Data, Direction.Left);
                pOwner.Data.Properties.IsFlipX = true;
            }

            if (World.Instance.Input.Keyboard.UpPressed)
            {
                pOwner.Systems.Transform.MoveWithDirection(pOwner.Data, Direction.Up);
            }

            else if (World.Instance.Input.Keyboard.DownPressed)
            {
                pOwner.Systems.Transform.MoveWithDirection(pOwner.Data, Direction.Down);
            }

            #endregion


            #region Keys

            if (World.Instance.Input.Keyboard.ActionPressed &&
                !World.Instance.Input.Keyboard.OldActionPressed)
            {
                pOwner.PlaySound("Test");
            }

            #endregion


            World.Instance.Input.CloseKeyboard();
        }
    }
}