﻿namespace NewGame.Out.Globals
{
    public static class GameGlobals
    {
        public static string CurrentOutDoorLevelId = "Level_1";
        public static string CurrentOutDoorLevelPath = "Content/Levels/" + CurrentOutDoorLevelId + ".tmx";
        public static string CurrentInDoorLevelPath = "Content/Levels/Dungeons.tmx";
        public static string AtlasGroupsPath = "Content/TmxMapContent/AtlasGroups.tmx";
        public static string StripGroupsPath = "Content/TmxMapContent/StripGroups.tmx";
        public static string StripAnimPath = "Content/TmxMapContent/StripAnims.tmx";
        public static string PngFontsPath = "Content/TmxMapContent/Fonts.tmx";
        public static string TexturesPath = "Content/TmxMapContent/Textures.tmx";
        public static string Seed = "Grizzly";
        public static string InDoorTexturePath = "Levels/Dungeons";
        public static string OutDoorTexturePath = "Levels/Multi_TileSet";
    }
}