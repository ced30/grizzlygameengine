﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Systems.Ai.Enemies
{
    public class AiEnemyCustom : CustomAiController
    {
        public AiEnemyCustom(States pStates) : base(pStates)
        {
            pStates.Add("idle", new SAiStateNone());
            pStates.Add("move", new SAiStateNone());
            pStates.Add("attack", new SAiStateNone());
            pStates.Add("damage", new SAiStateNone());
            pStates.Add("death", new ScrDeathStateDefault());

            pStates.Set("idle");
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            if (pOwner.Data.Conditions.IsExpired || pOwner.Data.States.IsMatching("death")) return;

            Camera.Instance.Shake(ScreenShake.Impact.Mini);
            TakeDamage(pOwner, pCollidedBy);
            pOwner.PlaySound("weaponImpact");
            pOwner.Data.States.Set("damage");
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("none"))
            {
                pOwner.Data.States.Set("idle");
            }

            base.UpdateGameLogic(pGameTime, pOwner);
        }

        protected override void OnStateChange(IEntity pOwner)
        {
            base.OnStateChange(pOwner);

            if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Systems.Shader.Reset();
            }
        }

        protected override void OnAnimationEnd(IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("damage"))
            {
                if (pOwner.Data.Attributes.TotalAttributes.HealthPoints <= 0)
                {
                    pOwner.Data.Attributes.BaseAttributes.HealthPoints = 0;
                    pOwner.PlaySound("weaponImpactSquish");
                    pOwner.Data.States.Set("death");
                }
                else
                {
                    pOwner.Systems.Shader.Reset();
                    pOwner.Data.States.Set("idle");
                }
            }

            base.OnAnimationEnd(pOwner);
        }

        protected override void OnAnimationPlay(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("idle"))
            {
                pOwner.Systems.Renderer.PlayAnimation("idle");
            }
            else if (pOwner.Data.States.IsMatching("damage"))
            {
                pOwner.Systems.Renderer.PlayAnimation("damage");
            }
            else if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Systems.Renderer.PlayAnimation("death");
            }

            base.OnAnimationPlay(pGameTime, pOwner);
        }
    }
}