﻿#region Usings

using System.Diagnostics;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.Spells.DamageOverTime;

#endregion

namespace NewGame.Out.Systems.Ai.Spells
{
    public class AiDamageOverTime : CustomAiController
    {
        private const string TickStr = "tick";
        public AiDamageOverTime(States pStates) : base(pStates)
        {
            pStates.Add("idle", new ScrDotStateCollide());
            pStates.Add("death", new SctDtoStateDeath());

            pStates.Set("idle");

            DictionaryCoolDowns.Add(TickStr, new Cooldown());
            DictionaryCoolDowns[TickStr].Start(1);
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            pOwner.Data.States.Current.Update(pGameTime, pOwner);
            UpdateCoolDowns(pGameTime);
        }
    }
}