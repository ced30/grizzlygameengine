﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace NewGame.Out.Systems.Ai.Spells
{
    public class AiSpellDefault : CustomAiController
    {
        public AiSpellDefault(States pStates) : base(pStates)
        {
            pStates.Add("death", new ScrFadeInRemove());
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            if (!pOwner.Data.States.IsMatching("death"))
                pOwner.Data.States.Set("collide");

            base.OnCollision(pOwner, pCollidedBy);
        }

        protected override void OnAnimationEnd(IEntity pOwner)
        {
            pOwner.Data.States.Set("death");

            base.OnAnimationEnd(pOwner);
        }
    }
}