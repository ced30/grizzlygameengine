﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace NewGame.Out.Systems.Ai
{
    public class CustomAiController : SDefaultAiController
    {
        public CustomAiController(States pStates) : base(pStates)
        {
        }

        protected virtual void TakeDamage(IEntity pOwner, IEntity pCollidedBy)
        {
            pOwner.Data.Attributes.BaseAttributes.HealthPoints -= pCollidedBy.Data.Attributes.TotalAttributes.Damage;
            pOwner.Systems.Shader.SetCurrent("whiteFlash");
        }
    }
}