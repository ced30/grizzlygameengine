﻿
#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Input;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.Player;

#endregion

namespace NewGame.Out.Systems.Ai.Player
{
    public class AiPlayerMinimal : CustomAiController
    {
        /// <summary>
        /// The input wrapper class.
        /// </summary>
        private SBaseAiInputWrapper Input { get; }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public AiPlayerMinimal(States pStates) : base(pStates)
        {
            Input = new SBaseAiInputWrapper();

            pStates.Add("normal", new PlayerStateControlNormal());
        }

        /// <inheritdoc />
        /// <summary>
        /// Executes the Ai gameLogic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            // TODO: add gameLogic logic here

            // We open the input reader.
            Input.ReadInput(pGameTime, pOwner);

            base.UpdateGameLogic(pGameTime, pOwner);

            // Then we close the input reader.
            Input.CloseInput(pGameTime, pOwner);
        }

        /// <inheritdoc />
        /// <summary>
        /// logic executed when an animation Plays.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        protected override void OnAnimationPlay(GameTime pGameTime, IEntity pOwner)
        {
            base.OnAnimationPlay(pGameTime, pOwner);

            // Change the animation depending on the currentState.
            if (pOwner.Data.States.IsMatching("normal"))
            {
                pOwner.Systems.Renderer.PlayAnimation(
                    pOwner.Data.Positional.MovementSpeed(pGameTime.ElapsedGameTime.TotalSeconds)
                    != Vector2.Zero ? "move" : "idle");
            }
            else
            {
                pOwner.Systems.Renderer.PlayAnimation(
                    pOwner.Data.Positional.MovementSpeed(pGameTime.ElapsedGameTime.TotalSeconds)
                    != Vector2.Zero ? "move" : "idle");
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// logic executed when an animation has begins.
        /// </summary>
        /// <param name="pOwner"></param>
        protected override void OnStateChange(IEntity pOwner)
        {
            base.OnStateChange(pOwner);

            if (pOwner.Data.States.IsMatching("none"))
            {
                GoToNormalState(pOwner);
            }
        }


        /// <summary>
        /// Sets the velocity to vector2.Zero and changes state to normal.
        /// </summary>
        /// <param name="pOwner"></param>
        private static void GoToNormalState(IEntity pOwner)
        {
            pOwner.Data.States.Set("normal");
            pOwner.Data.Positional.Velocity = Vector2.Zero;
        }
    }
}