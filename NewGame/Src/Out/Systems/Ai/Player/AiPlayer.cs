﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Input;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Enums;
using NewGame.Out.Types.AiScripts.Player;

#endregion

namespace NewGame.Out.Systems.Ai.Player
{
    /// <summary>
    /// This is the example player AI, it contains scripts
    /// which are executed depending on the current state.
    /// </summary>
    public class AiPlayer : CustomAiController
    {
        /// <summary>
        /// Did the player trigger a combo?
        /// </summary>
        public bool IsCombo { get; set; }

        /// <summary>
        /// The input wrapper class.
        /// </summary>
        private SBaseAiInputWrapper Input { get; }

        /// <summary>
        /// The state of player weapons.
        /// </summary>
        public PlayerWeapon CurrentWeapon { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public AiPlayer(States pStates) : base(pStates)
        {
            Input = new SBaseAiInputWrapper();

            pStates.Add("idle", new PlayerStateNormal());
            pStates.Add("attack", new PlayerStateAttackProcessor());
            pStates.Add("attackSlash", new PlayerStateAttackSlash());
            pStates.Add("bow", new PlayerStateAttackBow());
            pStates.Add("damage", new SAiStateNone());
            pStates.Add("magic", new PlayerStateAttackMagic());
            pStates.Add("attackCombo", new PlayerStateAttackCombo());
            pStates.Add("dodgeRoll", new PlayerStateDodgeRoll());
            pStates.Add("jump", new PlayerStateJump());

            pStates.Set("idle");
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            if (pCollidedBy.Id.Equals("smallBush"))
            {
                if (pOwner.Data.BoundingBox.Bottom < pCollidedBy.Data.BoundingBox.Bottom)
                    pOwner.Data.Positional.SpeedCoef = 0.65f;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Executes the Ai gameLogic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            // TODO: add gameLogic logic here

            // We open the input reader.
            Input.ReadInput(pGameTime, pOwner);

            base.UpdateGameLogic(pGameTime, pOwner);

            // Then we close the input reader.
            Input.CloseInput(pGameTime, pOwner);
        }

        /// <inheritdoc />
        /// <summary>
        /// logic executed when an animation Plays.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        protected override void OnAnimationPlay(GameTime pGameTime, IEntity pOwner)
        {
            base.OnAnimationPlay(pGameTime, pOwner);

            // Change the animation depending on the currentState.
            if (pOwner.Data.States.IsMatching("idle"))
            {
                pOwner.Systems.Renderer.PlayAnimation(
                    pOwner.Data.Positional.MovementSpeed(pGameTime.ElapsedGameTime.TotalSeconds)
                    != Vector2.Zero ? "move" : "idle");
            }
            else if (pOwner.Data.States.IsMatching("attackSlash"))
            {
                pOwner.Systems.Renderer.PlayAnimation("melee1");
            }
            else if (pOwner.Data.States.IsMatching("bow"))
            {
                pOwner.Systems.Renderer.PlayAnimation("bow");
            }
            else if (pOwner.Data.States.IsMatching("magic"))
            {
                pOwner.Systems.Renderer.PlayAnimation("magic");
            }
            else if (pOwner.Data.States.IsMatching("attackCombo"))
            {
                if (pOwner.Data.States.Current is PlayerStateAttackCombo pCombo)
                    pOwner.Systems.Renderer.PlayAnimation(pCombo.CurrentCombo);
            }
            else if (pOwner.Data.States.IsMatching("dodgeRoll"))
            {
                pOwner.Systems.Renderer.PlayAnimation("dodgeRoll");
            }
            else if (pOwner.Data.States.IsMatching("jump"))
            {
                pOwner.Systems.Renderer.PlayAnimation("jump");
            }
            else
            {
                pOwner.Systems.Renderer.PlayAnimation(
                    pOwner.Data.Positional.MovementSpeed(pGameTime.ElapsedGameTime.TotalSeconds)
                    != Vector2.Zero ? "move" : "idle");
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// logic executed when an animation has ended..
        /// </summary>
        /// <param name="pOwner"></param>
        protected override void OnAnimationEnd(IEntity pOwner)
        {
            // TODO: add "when animation ends" logic here.

            // If the animation has ended.
            if (pOwner.Data.States.IsMatching("attack") ||
                pOwner.Data.States.IsMatching("attackSlash") ||
                pOwner.Data.States.IsMatching("bow") ||
                pOwner.Data.States.IsMatching("magic"))
            {
                HasFired = false;
                IsAttacking = false;
                GoToIdleState(pOwner);
            }
            else if (pOwner.Data.States.IsMatching("attackCombo"))
            {
                IsAttacking = false;
                IsCombo = false;
                GoToIdleState(pOwner);
            }
            else if (pOwner.Data.States.IsMatching("dodgeRoll"))
            {
                GoToIdleState(pOwner);
            }
            else if (pOwner.Data.States.IsMatching("jump"))
            {
                pOwner.PlaySound("land");
                GoToIdleState(pOwner);
            }

            base.OnAnimationEnd(pOwner);
        }

        /// <inheritdoc />
        /// <summary>
        /// logic executed when an animation has begins.
        /// </summary>
        /// <param name="pOwner"></param>
        protected override void OnStateChange(IEntity pOwner)
        {
            base.OnStateChange(pOwner);

            if (pOwner.Data.States.IsMatching("none"))
            {
                GoToIdleState(pOwner);
            }
            else if (pOwner.Data.States.IsMatching("attackSlash"))
            {
                pOwner.PlaySound("swordImpactLayer1");
            }
            else if (pOwner.Data.States.IsMatching("attackCombo"))
            {
                pOwner.PlaySound("swordImpactLayer2");
            }
            else if (pOwner.Data.States.IsMatching("dodgeRoll"))
            {
                pOwner.PlaySound("dodgeRoll");
                pOwner.Data.Positional.Velocity = Vector2.Zero;
            }
            else if (pOwner.Data.States.IsMatching("jump"))
            {
                pOwner.PlaySound("jump");
                pOwner.Data.Positional.Velocity = Vector2.Zero;
            }
        }


        /// <summary>
        /// Sets the velocity to vector2.Zero and changes state to normal.
        /// </summary>
        /// <param name="pOwner"></param>
        private static void GoToIdleState(IEntity pOwner)
        {
            pOwner.Data.States.Set("idle");
            pOwner.Data.Positional.Velocity = Vector2.Zero;
        }

        /// <inheritdoc />
        /// <summary>
        /// Makes a HitBox.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pRect"></param>
        public override void MakeHitBox(IEntity pOwner, Rectangle pRect)
        {
            if (!IsAttacking)
            {
                IsAttacking = true;

                var hb = new HitBox(pRect);

                // We add some entities types that can
                // be collided by the hitBox
                hb.CanCollideWith("enemy");
                hb.CanCollideWith("destructibleObject");
                hb.Data.Attributes = pOwner.Data.Attributes;
                hb.Faction = pOwner.Faction;
                hb.Systems.Family.BeAdoptedBy(pOwner);
                pOwner.OnNewEntityInstance?.Invoke(hb);
            }
        }
    }
}