﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Followers;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Systems.Ai.Followers
{
    public class AiFollower : CustomAiController
    {
        // Takes the follow range as a parameter.
        public AiFollower(States pStates) : base(pStates)
        {
            pStates.Add("follow", new ScrFollowOwnerTest());
            pStates.Set("follow");
        }

        public override void UpdateAnimations(GameTime pGameTime, IEntity pOwner)
        {
            base.UpdateAnimations(pGameTime, pOwner);

            if (pOwner.Data.States.IsMatching("follow"))
            {
                pOwner.Systems.Renderer.PlayAnimation(pOwner.Data.Positional.Velocity != Vector2.Zero ? "move" : "idle");
            }
        }
    }
}