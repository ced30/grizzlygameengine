﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.GameObjects.DestructibleObjects;

#endregion

namespace NewGame.Out.Systems.Ai.GameObjects.DestructibleObjects
{
    public class AiDestructibleObject : CustomAiController
    {
        public AiDestructibleObject(States pStates) : base(pStates)
        {
            pStates.Add("idle", new SAiStateNone());
            pStates.Add("damage", new SAiStateNone());
            pStates.Add("death", new ScrDestructibleStateDeath());

            pStates.Set("idle");
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            base.OnCollision(pOwner, pCollidedBy);

            if (pOwner.Data.Conditions.IsExpired ||
                pOwner.Data.States.IsMatching("death"))
                return;

            if (pOwner.Data.Attributes.TotalAttributes.HealthPoints <= 0)
            {
                pOwner.Data.Attributes.BaseAttributes.HealthPoints = 0;
                pOwner.Data.States.Set("death");
            }
            else
            {
                TakeDamage(pOwner, pCollidedBy);
                pOwner.Data.States.Set("damage");
                EmitDamageSound(pOwner);
            }
        }

        protected override void OnStateChange(IEntity pOwner)
        {
            base.OnStateChange(pOwner);

            if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Systems.Shader.Reset();
                EmitBreakSound(pOwner);
            }
        }

        protected override void OnAnimationEnd(IEntity pOwner)
        {
            if (pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("damage"))
            {
                pOwner.Systems.Shader.Reset();
                pOwner.Data.States.Set("idle");
            }

            base.OnAnimationEnd(pOwner);
        }

        protected override void OnAnimationPlay(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("idle"))
            {
                pOwner.Systems.Renderer.PlayAnimation("idle");
            }
            else if (pOwner.Data.States.IsMatching("damage"))
            {
                pOwner.Systems.Renderer.PlayAnimation("damage");
            }
            else if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Systems.Renderer.PlayAnimation("death");
            }

            base.OnAnimationPlay(pGameTime, pOwner);
        }

        private void EmitDamageSound(IEntity pOwner)
        {
            if (pOwner.Id.Equals("jar"))
                pOwner.PlaySound("jarDamage");
            else
            {
                pOwner.PlaySound("weaponImpactGrass");
            }
        }

        private void EmitBreakSound(IEntity pOwner)
        {
            if (pOwner.Id.Equals("jar"))
            {
                pOwner.PlaySound("smashJar");
            }
            else if (pOwner.Id.Equals("barrel") ||
                     pOwner.Id.Equals("crate"))
            {
                pOwner.PlaySound("woodCrateBreak");
            }
            else if (pOwner.Id.Equals("sign") ||
                     pOwner.Id.Equals("direction"))
            {
                pOwner.PlaySound("woodImpact");
            }
        }
    }
}