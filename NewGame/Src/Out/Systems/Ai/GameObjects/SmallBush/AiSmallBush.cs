﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.GameObjects.Bushes;

#endregion

namespace NewGame.Out.Systems.Ai.GameObjects.SmallBush
{
    public class AiSmallBush : CustomAiController
    {
        public AiSmallBush(States pStates) : base(pStates)
        {
            pStates.Add("idle", new SAiStateNone());
            pStates.Add("damage", new ScrSmallBushStateDamage());
            pStates.Add("death", new ScrSmallBushStateDeath());

            AddCoolDown("collideCd", new Cooldown());
        }

        protected override void OnAnimationPlay(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("idle"))
            {
                pOwner.Systems.Renderer.CurrentRenderer.PlayAnimation("idle");
            }
            else if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Systems.Renderer.CurrentRenderer.PlayAnimation("death");
            }
            else if (pOwner.Data.States.IsMatching("damage"))
            {
                pOwner.Systems.Renderer.CurrentRenderer.PlayAnimation("damage");
            }

            base.OnAnimationPlay(pGameTime, pOwner);
        }

        protected override void OnAnimationEnd(IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("damage"))
            {
                pOwner.Data.States.Set("idle");
            }
            if (pOwner.Data.States.IsMatching("death"))
            {
                pOwner.Data.Conditions.ToRemove = true;
            }

            pOwner.Systems.Shader.Reset();

            base.OnAnimationEnd(pOwner);
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            base.OnCollision(pOwner, pCollidedBy);
            const float collideTimer = .7f;

            if (pOwner.Data.Conditions.IsExpired ||
                pOwner.Data.States.IsMatching("death"))
                return;

            if (pCollidedBy.Type == "hitBox" ||
                pCollidedBy.Type == "spell")
            {
                pOwner.Systems.Shader.SetCurrent("whiteFlash");
                pOwner.PlaySound(
                    "grassDamage");
                pOwner.Data.States.Set("death");
            }
            if (pCollidedBy.Type == "player" ||
                pCollidedBy.Type == "enemy" ||
                pCollidedBy.Type == "projectile")
            {
                if (pCollidedBy.Data.BoundingBox.Bottom >= pOwner.Data.BoundingBox.Bottom ||
                    pCollidedBy.Data.Positional.Velocity == Vector2.Zero ||
                    DictionaryCoolDowns["collideCd"].IsRunning) return;

                pOwner.Data.States.Set("damage");
                DictionaryCoolDowns["collideCd"].Start(collideTimer);
            }
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            base.UpdateGameLogic(pGameTime, pOwner);

            for (var i = 0; i < LstColliders.Count; i++)
            {
                if (!LstColliders[i].Type.Equals("spell"))
                {
                    LstColliders.Remove(LstColliders[i]);
                }
            }
        }

        protected override void TakeDamage(IEntity pOwner, IEntity pCollidedBy)
        {
            base.TakeDamage(pOwner, pCollidedBy);

            if (pOwner.Data.Attributes.TotalAttributes.HealthPoints <= 0)
            {
                pOwner.Data.States.Set("death");
            }
        }
    }
}