﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Projectiles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace NewGame.Out.Systems.Ai.GameObjects.Projectiles
{
    /// <inheritdoc />
    public class AiProjectiles : CustomAiController
    {
        public AiProjectiles(States pStates) : base(pStates)
        {
            pStates.Add("idle", new ScrProjectileStateNormal());
            pStates.Add("death", new ScrProjectileStateDeath());
            pStates.Set("idle");
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            base.OnCollision(pOwner, pCollidedBy);

            if (pOwner.Data.States.IsMatching("death"))
                return;

            if(pCollidedBy.Id.Equals("smallBush"))
                return;

            if(pCollidedBy == pOwner.Systems.Family.Parent)
                return;

//            if (pCollidedBy.Systems.Family.Parent != null)
//            {
//                if (pCollidedBy.Systems.Family.Parent == pOwner.Systems.Family.Parent)
//                    return;
//            }

            pOwner.Data.States.Set("death");
        }
    }
}