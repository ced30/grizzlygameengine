﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using NewGame.Out.Types.AiScripts.GameObjects.Items;

#endregion

namespace NewGame.Out.Systems.Ai.GameObjects.Items
{
    public class AiItemsDefault : CustomAiController
    {
        public AiItemsDefault(States pStates) : base(pStates)
        {
            pStates.Add("normal", new ScrFadeInRemove());
            pStates.Add("collide", new ScrItemPickUp());
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            base.OnCollision(pOwner, pCollidedBy);

            pOwner.Data.States.Set("collide");
        }
    }
}