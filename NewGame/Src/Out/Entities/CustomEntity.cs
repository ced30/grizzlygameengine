﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Shader;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Shader.Models;
using Microsoft.Xna.Framework;
using GrizzlyGameEngine.Globals;

#endregion

namespace NewGame.Out.Entities
{
    public class CustomEntity : Agent
    {
        public CustomEntity(
            string pIdentifier, 
            Rectangle pRect) : 
            base(pIdentifier, pRect)
        {
            Systems.Replace(new SDefaultShaderController());
            Systems.Shader.Add(
                "NormalEffect",
                new DefaultShaderItem(GrizzlyContentManager.Instance.Factory.GetEffect("noEffect")));

            Systems.Shader.Add(
                "GhostEffect",
                new DefaultShaderItem(GrizzlyContentManager.Instance.Factory.GetEffect("GhostEffect")));

            Systems.Shader.Add(
                "whiteFlash",
                new DefaultFlashShaderItem(GrizzlyContentManager.Instance.Factory.GetEffect("whiteFlash"), Color.White));

            Systems.Shader.SetFirstAsCurrent();
        }
    }
}