﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using TiledContentImporter.TiledSharp;

#endregion

namespace NewGame.Out.Factory.Entities
{
    public class NpcFactory : AEntityFactoryBase
    {
        public override IEntity Make(TmxObject pObject)
        {
            // We query Agent class to get a
            // basic agent with collisions.
            var entity = CustomEntityFactory.BasicAgent(pObject);

            // Depending on the name of the entity,
            // we append different systems
            // before returning the entity.
            switch (pObject.Name)
            {
                case "archer":
                    break;

                case "pig":
                    break;
            }

            return entity;
        }
    }
}