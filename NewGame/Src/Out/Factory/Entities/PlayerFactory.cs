﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using NewGame.Out.Systems.Ai.Player;
using TiledContentImporter.TiledSharp;

#endregion

namespace NewGame.Out.Factory.Entities
{
    public class PlayerFactory : AEntityFactoryBase
    {
        public override IEntity Make(TmxObject pObject)
        {
            // We query the helpers Agent to get a
            // basic Agent with collisions.
            var player = CustomEntityFactory.BasicAgent(pObject);

            // We set the sound to true.
            player.Data.Conditions.IsSound = true;

            // fill up the list of entities the player can collide with
            player.CanCollideWith("destructibleObject");

            // We load the player's Ai.
            player.Systems.Ai = new AiPlayer(player.Data.States);

            // We set the current renderer to be an atlas animator
            // and we set the current atlas.
            //            player.Systems.Renderer.Set(
            //                ContentLoader.Instance.Factory.GetAtlas(pObject.Name));
            player.Systems.Renderer.Set(
                GrizzlyContentManager.Instance.TmxCManager.GetAtlasGroup(pObject.Name));

            return player;
        }
    }
}