﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using NewGame.Out.Systems.Ai.Enemies;
using TiledContentImporter.TiledSharp;

#endregion

namespace NewGame.Out.Factory.Entities
{
    public class EnemyFactory : AEntityFactoryBase
    {
        public override IEntity Make(TmxObject pObject)
        {
            IEntity entity = CustomEntityFactory.BasicAgent(pObject);
            entity.Systems.Replace(new AiEnemyCustom(entity.Data.States));
            // add entities type the object can collide with
            entity.CanCollideWith("projectile");
            entity.CanCollideWith("hitBox");
            entity.CanCollideWith("spell");
            return entity;
        }
    }
}