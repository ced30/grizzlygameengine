﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Loot;

#endregion

namespace NewGame.Out.Factory.Loot
{
    public class CustomLootTable : DefaultLootTable
    {
        protected override void MyPopulateFactories()
        {
            
        }

        protected override void MyPopulateTables()
        {
            
        }
    }
}