﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Entities;
using NewGame.Out.Systems.Ai.Spells;

#endregion

namespace NewGame.Out.Factory.Spells
{
    public class DamageOverTimeFactory
    {
        public static IEntity Dot(IEntity pOwner, IEntity pTarget)
        {
            var id = "fireDot1";
            var rect = new Rectangle(
                pTarget.Data.BoundingBox.X,
                pTarget.Data.BoundingBox.Y + 1,
                pTarget.Data.BoundingBox.Width,
                pTarget.Data.BoundingBox.Height);

            var dot = new CustomEntity(
                id,
                rect);
            InitDot(pOwner, dot);
            dot.Systems.Renderer.Set(
                GrizzlyContentManager.Instance.TmxCManager.GetStripAnim(
                    id));
            return dot;
        }

        private static void InitDot(IEntity pOwner, Agent pDot)
        {
            pDot.Type = pOwner.Type;
            pDot.Faction = pOwner.Faction;
            pDot.Data.Attributes.BaseAttributes.Damage = pOwner.Data.Attributes.BaseAttributes.Damage / 10;
            pDot.Systems.Replace(new AiDamageOverTime(pDot.Data.States));
            pDot.Systems.Replace(new SDefaultRenderManager(pDot.Data));
            pDot.Systems.Ai.CurrentCollider = pOwner.Systems.Ai.CurrentCollider;
            pDot.Data.Conditions.IsFlipX = pOwner.Data.Conditions.IsFlipX;
            // We add some entities types that can
            // be collided by the hitBox
            pDot.CanCollideWith("enemy");
            pDot.CanCollideWith("destructibleObject");
        }
    }
}