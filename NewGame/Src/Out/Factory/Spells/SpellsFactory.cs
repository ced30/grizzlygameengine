﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Systems.Ai.Spells;
using NewGame.Out.Types.AiScripts.Spells.Default;
using NewGame.Out.Types.AiScripts.Spells.Fire;
using NewGame.Out.Types.AiScripts.Spells.Shadow;

#endregion

namespace NewGame.Out.Factory.Spells
{
    public class SpellsFactory
    {
        public static IEntity Convert(IEntity pParent, Rectangle bBox)
        {
            var spell = new Agent("convert", bBox)
            {
                Type = "spell",
                Faction = pParent.Faction
            };

            InitSystems(pParent, spell);
            spell.Data.States.Replace("normal", new ScrFadeInRemove());
            spell.Data.States.Replace("collide", new ScrSpellStateConvert());
            spell.Data.States.Set("normal");
            spell.Data.Conditions.IsFlipX = !pParent.Data.Conditions.IsFlipX;

            return spell;
        }

        public static IEntity FireBreath(IEntity pParent, Rectangle bBox)
        {
            var spell = new Agent("fireBreathHigh", bBox)
            {
                Type = "spell",
                Faction = pParent.Faction
            };

            InitSystems(pParent, spell);
            spell.OnNewEntityInstance = pParent.OnNewEntityInstance;
            spell.Systems.Replace(new AiSpellDefault(spell.Data.States));
            spell.Data.States.Replace("idle", new ScrSpellMoveDefault());
            spell.Data.States.Replace("collide", new ScrSpellStateFireBreathCollide());
            spell.Data.States.Set("idle");
            spell.Data.Conditions.IsFlipX = pParent.Data.Conditions.IsFlipX;

            return spell;
        }

        public static void InitSystems(IEntity pParent, Agent pSpell)
        {
            pSpell.Systems.Family.BeAdoptedBy(pParent);
            pSpell.Systems.Renderer.Set(GrizzlyContentManager.Instance.TmxCManager.GetStripAnim(pSpell.Id));
            pSpell.Systems.Replace(new AiCollidableDefault(pSpell.Data.States));
            pSpell.Systems.Replace(new SDefaultColliderController());
            CustomEntityFactory.ShareObservers(pParent, pSpell);
            pSpell.Data.Conditions.IsCollideAbleByEntity = true;
            pSpell.CanCollideWith("destructibleObject");
            pSpell.CanCollideWith("enemy");
            pSpell.CanCollideWith("player");
        }
    }
}