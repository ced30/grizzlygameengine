﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Systems.Ai.GameObjects.Projectiles;

#endregion

namespace NewGame.Out.Factory.GameObjects
{
    public class ProjectileFactory
    {
        private const string EntityType = "projectile";

        public static IEntity Get(string pId, Rectangle pRect, int pSpeed, IEntity pOwner)
        {
            var arrow = new Agent(pId, pRect)
            {
                Faction = pOwner.Faction,
                Type = EntityType
            };

            arrow.CanCollideWith("player");
            arrow.CanCollideWith("enemy");
            arrow.CanCollideWith("destructibleObject");
            arrow.Data.Conditions.IsCollideAbleByWorld = true;
            arrow.Data.Conditions.IsCollideAbleByEntity = true;
            arrow.Data.Conditions.IsFlipX = pOwner.Data.Conditions.IsFlipX;
            arrow.Data.Positional.SpeedCoef = pSpeed;
            arrow.Data.Attributes.BaseAttributes.Damage = pOwner.Data.Attributes.BaseAttributes.Damage;
            arrow.Systems.Replace(new AiProjectiles(arrow.Data.States));
            arrow.Systems.Replace(new SDefaultPhysicsController());
            arrow.Systems.Replace(new SDefaultColliderController());
            arrow.Systems.Renderer.Set(GrizzlyContentManager.Instance.TmxCManager.GetTexture(arrow.Id));
            arrow.Systems.Family.BeAdoptedBy(pOwner);
            pOwner.Systems.Ai.IsAttacking = false;

            return arrow;
        }
    }
}