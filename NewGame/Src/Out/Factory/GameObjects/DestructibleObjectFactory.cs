﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using NewGame.Out.Systems.Ai.GameObjects.DestructibleObjects;
using NewGame.Out.Systems.Ai.GameObjects.SmallBush;
using TiledContentImporter.TiledSharp;

#endregion

namespace NewGame.Out.Factory.GameObjects
{
    public class DestructibleObjectFactory : AEntityFactoryBase
    {
        public override IEntity Make(TmxObject pObject)
        {
            // We query Agent class to get a
            // basic agent with collisions.
            IEntity entity = CustomEntityFactory.BasicAgent(pObject);
            InitProperties(entity);

            return entity;
        }

        private void InitProperties(IEntity entity)
        {
            // add entities type the object can collide with
            entity.CanCollideWith("enemy");
            entity.CanCollideWith("projectile");
            entity.CanCollideWith("hitBox");
            entity.CanCollideWith("spell");

            entity.Data.Conditions.IsCollideAbleByWorld = false;

            if (entity.Id.Equals("smallBush"))
            {
                entity.CanCollideWith("player");
                entity.Systems.Replace(new AiSmallBush(entity.Data.States));
                entity.Data.Attributes.BaseAttributes.HealthPoints = 60;
            }
            else
            {
                if (entity.Type.Equals("destructibleObject"))
                {
                    entity.Systems.Replace(new AiDestructibleObject(entity.Data.States));
                }
            }
        }
    }
}