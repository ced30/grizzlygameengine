﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using NewGame.Out.Systems.Ai.GameObjects.Items;
using TiledContentImporter.TiledSharp;

#endregion

namespace NewGame.Out.Factory.GameObjects
{
    public class ItemFactory : AEntityFactoryBase
    {
        public override IEntity Make(TmxObject pObject)
        {
            // We query Agent class to get a
            // basic agent with collisions.
            var entity = CustomEntityFactory.BasicAgent(pObject);
            entity.CanCollideWith("player");
            InitProperties(entity);

            return entity;
        }

        private void InitProperties(IEntity entity)
        {
            entity.CanCollideWith("player");

            if (entity.Id.Equals("gold1"))
            {
                entity.Systems.Replace(new AiItemsDefault(entity.Data.States));
//                entity.Systems.Ai.ReplaceState("collide", new ScrItemStateCollide());
            }
        }
    }
}