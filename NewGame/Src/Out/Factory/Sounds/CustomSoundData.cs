﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using Microsoft.Xna.Framework.Content;

#endregion

namespace NewGame.Out.Factory.Sounds
{
    public class CustomSoundData : DefaultSoundData
    {
        protected override void PopulateMusics(ContentManager pContent)
        {
            base.PopulateMusics(pContent);

            AddMusic("contemplative01", "Sounds/Gameplay/Music/Contemplative01", 0.35f, pContent);
        }

        protected override void PopulateAmbiance(ContentManager pContent)
        {
            base.PopulateAmbiance(pContent);

            AddAmbiance("rainFall", "Sounds/Gameplay/Ambiance/AmbianceRainfall", 0.35f, pContent);
        }

        protected override void PopulateSfx(ContentManager pContent)
        {
            base.PopulateSfx(pContent);

            AddSfx("arrowImpact", "Sounds/Gameplay/Sfx/ArrowImpact", 0.035f, pContent);
            AddSfx("attackBow", "Sounds/Gameplay/Sfx/AttackBow", 0.035f, pContent);
            AddSfx("bowRelease", "Sounds/Gameplay/Sfx/BowRelease", 0.035f, pContent);
            AddSfx("dodgeRoll", "Sounds/Gameplay/Sfx/DodgeRoll", 0.035f, pContent);
            AddSfx("grassColliding", "Sounds/Gameplay/Sfx/GrassColliding", 0.025f, pContent);
            AddSfx("grassDamage", "Sounds/Gameplay/Sfx/GrassDamage", 0.055f, pContent);
            AddSfx("goldPickUp", "Sounds/Gameplay/Sfx/GoldPickUp", 0.015f, pContent);
            AddSfx("jarDamage", "Sounds/Gameplay/Sfx/JarDamage", 0.035f, pContent);
            AddSfx("jump", "Sounds/Gameplay/Sfx/Jump", 0.035f, pContent);
            AddSfx("land", "Sounds/Gameplay/Sfx/Land", 0.035f, pContent);
            AddSfx("magicShadowSpell", "Sounds/Gameplay/Sfx/Spells/Shadow/MagicShadowSpell", 0.035f, pContent);
            AddSfx("pickUp", "Sounds/Gameplay/Sfx/PickUp", 0.035f, pContent);
            AddSfx("smashJar", "Sounds/Gameplay/Sfx/SmashJar", 0.035f, pContent);
            AddSfx("swordImpactLayer1", "Sounds/Gameplay/Sfx/SwordImpactLayer 1", 0.025f, pContent);
            AddSfx("swordImpactLayer2", "Sounds/Gameplay/Sfx/SwordImpactLayer 2", 0.025f, pContent);
            AddSfx("swordImpactLayer3", "Sounds/Gameplay/Sfx/SwordImpactLayer 3", 0.025f, pContent);
            AddSfx("weaponImpact", "Sounds/Gameplay/Sfx/WeaponImpact", 0.035f, pContent);
            AddSfx("weaponImpactGrass", "Sounds/Gameplay/Sfx/WeaponImpactGrass", 0.020f, pContent);
            AddSfx("weaponImpactSquish", "Sounds/Gameplay/Sfx/SwordImpactSquish", 0.035f, pContent);
            AddSfx("woodCrateBreak", "Sounds/Gameplay/Sfx/WoodCrateBreak", 0.035f, pContent);
            AddSfx("woodImpact", "Sounds/Gameplay/Sfx/WoodImpact", 0.035f, pContent);

            AddSfx("footSteps01", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_A", 0.15f, pContent);
            AddSfx("footSteps02", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_B", 0.15f, pContent);
            AddSfx("footSteps03", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_C", 0.15f, pContent);
            AddSfx("footSteps04", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_D", 0.15f, pContent);
            AddSfx("footSteps05", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_E", 0.15f, pContent);
            AddSfx("footSteps06", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_F", 0.15f, pContent);
            AddSfx("footSteps07", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_G", 0.15f, pContent);
            AddSfx("footSteps08", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_H", 0.15f, pContent);
            AddSfx("footSteps09", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_I", 0.15f, pContent);
            AddSfx("footSteps10", "Sounds/Gameplay/Sfx/FootSteps/Dirt_Light_J", 0.15f, pContent);

            AddSfx("fireBreathA", "Sounds/Gameplay/Sfx/Spells/Fire/FireBreathSpell_A", 0.15f, pContent);
            AddSfx("fireBreathB", "Sounds/Gameplay/Sfx/Spells/Fire/FireBreathSpell_B", 0.15f, pContent);
            AddSfx("fireBreathC", "Sounds/Gameplay/Sfx/Spells/Fire/FireBreathSpell_C", 0.15f, pContent);
            AddSfx("fireBreathD", "Sounds/Gameplay/Sfx/Spells/Fire/FireBreathSpell_D", 0.15f, pContent);
            AddSfx("fireBreathE", "Sounds/Gameplay/Sfx/Spells/Fire/FireBreathSpell_E", 0.15f, pContent);

            AddSfx("Test", "Sounds/Gameplay/Sfx/Crow", 0.035f, pContent);

        }
    }
}