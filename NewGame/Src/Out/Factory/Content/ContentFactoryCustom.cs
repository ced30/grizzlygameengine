﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Prefabs;
using Microsoft.Xna.Framework.Content;

#endregion

namespace NewGame.Out.Factory.Content
{
    /// <summary>
    /// This class preLoads content, load it first in your game's load order.
    /// </summary>
    public class ContentFactoryCustom : ContentFactoryDefault
    {

        public ContentFactoryCustom(ContentManager pContent) : base(pContent)
        {

        }

        /// <summary>
        /// Usage: Add.SpriteEffect(dictionary, Name, Path);
        /// </summary>
        protected override void PopulateEffects()
        {
            base.PopulateEffects();
            
            Add.SpriteEffect(SpriteEffectDictionary, "noEffect", "Effects/Normal");
            Add.SpriteEffect(SpriteEffectDictionary, "GhostEffect", "Effects/GhostEffect");
            Add.SpriteEffect(SpriteEffectDictionary, "whiteFlash", "Effects/WhiteFlash");
        }


        /// <summary>
        /// Usage: Add.Font(dictionary, Name, Path);
        /// </summary>
        protected override void PopulateFonts()
        {
            base.PopulateFonts();

            Add.Font(FontsDictionary, "SmallFont", "Fonts/SmallFont");
        }
    }
}