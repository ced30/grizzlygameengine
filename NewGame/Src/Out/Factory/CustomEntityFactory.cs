﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Entities;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types;

#endregion

namespace NewGame.Out.Factory
{
    public class CustomEntityFactory
    {
        /// <summary>
        /// Returns a basic IEntity with collisions.
        /// </summary>
        /// <param name="pObject"></param>
        /// <returns></returns>
        public static IEntity BasicAgent(TmxObject pObject)
        {
            IEntity basic = new CustomEntity(
                pObject.Name,
                new Rectangle(
                    (int)pObject.X,
                    (int)(pObject.Y - pObject.Height),
                    (int)pObject.Width,
                    (int)pObject.Height))
            {
                Type = pObject.Type
            };

            basic.Data.Conditions.IsCollideAbleByWorld = true;
            basic.Data.Conditions.IsCollideAbleByEntity = true;
            basic.Data.TmxProperties = pObject.Properties;

            basic.Systems.Replace(new SNoInputController());
            basic.Systems.Replace(new SDefaultAiController(basic.Data.States));
            basic.Systems.Replace(new SDefaultTransformController());
            basic.Systems.Replace(new SDefaultColliderController());
            basic.Systems.Replace(new SDefaultPhysicsController());
            basic.Systems.Replace(new SDefaultFamilyController());

            InitProperties(basic);

            // we select the renderer based on the tmx object property
            var defaultRenderer = TmxPropertyId.AtlasId;

            // check for "renderer" parameter
            if (basic.Data.TmxProperties.ContainsKey(TmxPropertyId.RendererId))
                defaultRenderer = basic.Data.TmxProperties[TmxPropertyId.RendererId];

            // load the current renderer
            if (defaultRenderer.Equals(TmxPropertyId.AtlasId))
            {
                basic.Systems.Renderer.Set(
                    GrizzlyContentManager.Instance.TmxCManager.GetAtlasGroup(pObject.Name));
            }
            else if (defaultRenderer.Equals(TmxPropertyId.StripId))
            {
                basic.Systems.Renderer.Set(
                    GrizzlyContentManager.Instance.TmxCManager.GetStripGroup(pObject.Name));
            }
            else if (defaultRenderer.Equals(TmxPropertyId.TextureId))
            {
                basic.Systems.Renderer.Set(
                    GrizzlyContentManager.Instance.TmxCManager.GetTexture(pObject.Name));
            }
            else
            {
                basic.Systems.Replace(new SNoRenderManager());
            }

            // check if there's a requested first animation
            if (pObject.Properties.ContainsKey(TmxPropertyId.AnimId))
            {
                basic.Systems.Renderer.PlayAnimation(pObject.Properties[TmxPropertyId.AnimId]);
            }

            return basic;
        }

        /// <summary>
        /// Attaches a shadow to the actor with a possible offset position,
        /// the shadows keeps a relative distance to it's parent.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pOffset"></param>
        public static IEntity DropShadowFor(IEntity pOwner, Vector2 pOffset)
        {
            Agent dS = new CustomEntity("dropShadow", new Rectangle(
                    0,
                    0,
                    pOwner.Data.Width * 2,
                    pOwner.Data.Height - 1));

            dS.Type = "prop";
            dS.Faction = "neutral";
            dS.Data.Conditions.IsSound = false;

            // we set the alpha to 0 and set the fade out to barely opaque,
            // the alpha will be incremented till it reaches the barelyOpaque value and
            // we will end up with a semi-transparent shadow.
            dS.Data.Colors.Fade(Colors.FadeStates.In, Colors.OpacityStates.BarelyOpaque);

            // the mobileProp Ai will follow the target whom it's
            // attached to at an offset passed as a parameter
            dS.Systems.Replace(new AiAttachedEntityDefault(dS.Data.States, pOffset));

            // we set the renderer and give it the texture
            dS.Systems.Renderer.Set(GrizzlyContentManager.Instance.TmxCManager.GetTexture(dS.Id));

            // Finally the owner adopts the shadow
            pOwner.Adopt(dS);

            return dS;
        }

        /// <summary>
        /// Initialize the tmxProperties from the tileMap
        /// </summary>
        /// <param name="pAgent"></param>
        private static void InitProperties(IEntity pAgent)
        {
            // check for "faction" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.FactionId))
            {
                pAgent.Faction = pAgent.Data.TmxProperties[TmxPropertyId.FactionId];
            }

            // check for "isSolid" parameter
            pAgent.Data.Conditions.IsSolid = Helpers.TmxBoolProperty(pAgent.Data, TmxPropertyId.SolidId);

            // check for "isBounce" parameter
            pAgent.Data.Conditions.IsBounceAble = Helpers.TmxBoolProperty(pAgent.Data, TmxPropertyId.BounceId);

            // check for "isBump" parameter
            pAgent.Data.Conditions.IsBumpAble = Helpers.TmxBoolProperty(pAgent.Data, TmxPropertyId.BumpId);


            // check for "damage" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.DamageId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.DamageId], out var damageValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxDamage = damageValue;
                    pAgent.Data.Attributes.BaseAttributes.Damage = damageValue;
                }
            }

            // check for "defense" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.DefenseId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.DefenseId], out var defenseValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxDefense = defenseValue;
                    pAgent.Data.Attributes.BaseAttributes.Defense = defenseValue;
                }
            }

            // check for "energy" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.EnergyId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.EnergyId], out var energyValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxEnergy = energyValue;
                    pAgent.Data.Attributes.BaseAttributes.EnergyPoints = energyValue;
                }
            }

            // check for "health" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.HealthId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.HealthId], out var healthValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxHealth = healthValue;
                    pAgent.Data.Attributes.BaseAttributes.HealthPoints = healthValue;
                }
            }

            // check for "mana" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.ManaId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.ManaId], out var manaValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxMana = manaValue;
                    pAgent.Data.Attributes.BaseAttributes.ManaPoints = manaValue;
                }
            }

            // check for "speed" parameter
            if (pAgent.Data.TmxProperties.ContainsKey(TmxPropertyId.SpeedId))
            {
                if (Int32.TryParse(pAgent.Data.TmxProperties[TmxPropertyId.SpeedId], out var speedValue))
                {
                    pAgent.Data.Attributes.BaseAttributes.MaxSpeed = speedValue;
                    pAgent.Data.Attributes.BaseAttributes.Speed = speedValue;
                }
            }
        }

        /// <summary>
        /// The destination entity receives the source's observers
        /// </summary>
        /// <param name="pSource"></param>
        /// <param name="pDestination"></param>
        public static void ShareObservers(IEntity pSource, IEntity pDestination)
        {
            for (var i = pSource.LstObservers.Count - 1; i >= 0; i--)
            {
                if (pDestination.LstObservers.Count > 0)
                {
                    if (pDestination.LstObservers.Contains(pSource.LstObservers[i]))
                    {
                        pDestination.Attach(pSource.LstObservers[i]);
                    }
                }
                else
                {
                    pDestination.Attach(pSource.LstObservers[i]);
                }
            }
        }
    }
}