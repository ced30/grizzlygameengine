﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.ParticleEmitter;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.Particles.Rain;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Components.ParticleEmitters
{
    /// <inheritdoc />
    /// <summary>
    /// This class is an example of rain particles emitter,
    /// it generates rain particles and updates / draws them.
    /// </summary>
    public class ParticleEmitterRain : ParticlesEmitter
    {
        public ParticleEmitterRain()
        {
            Id = "rain";
        }

        /// <inheritdoc />
        /// <summary>
        /// Applies global velocity variations to particles.
        /// </summary>
        protected override void ApplyGlobalVelocity()
        {
            //            var xSway = (float)World.Instance.R.Next(-2, 2);
            //
            //            for (int i = 0; i < LstParticles.Count; i++)
            //            {
            //                LstParticles[i].Transform.Velocity.X =
            //                    (xSway * LstParticles[i].Transform.Zoom) / 50;
            //            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Returns a particle.
        /// </summary>
        /// <returns></returns>
        protected override Particle MakeParticle()
        {
            var particle = new Particle(ImporterState.Texture, Id, Rectangle.Empty);
            particle.Data.States.Replace("normal", new ScrRainStateNormal());
            particle.Data.States.Replace("randomize", new ScrRainStateRandomize());
            particle.Data.States.Set("randomize");

            return particle;
        }
    }
}