﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.ParticleEmitter;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using Microsoft.Xna.Framework;
using NewGame.Out.Types.AiScripts.Particles.RainDrops;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Components.ParticleEmitters
{
    /// <inheritdoc />
    /// <summary>
    /// This class is an example of rainDrops particles emitter,
    /// it generates rain particles and updates / draws them.
    /// </summary>
    public class ParticleEmitterRainDrops : ParticlesEmitter
    {
        /// <inheritdoc />
        /// <summary>
        /// Constructor.
        /// </summary>
        public ParticleEmitterRainDrops()
        {
            Id = "rainDrop";
            MaxParticles = 100;
        }

        /// <inheritdoc />
        /// <summary>
        /// Apply global velocity changes.
        /// </summary>
        protected override void ApplyGlobalVelocity()
        {
            
        }

        /// <inheritdoc />
        /// <summary>
        /// Returns a particle.
        /// </summary>
        /// <returns></returns>
        protected override Particle MakeParticle()
        {
            var particle = new Particle(ImporterState.StripAnim, Id, new Rectangle(0, 0, 16, 16));
            particle.Data.States.Replace("normal", new ScrRainDropStateNormal());
            particle.Data.States.Replace("randomize", new ScrRainDropsStateRandomize());
            particle.Data.States.Set("randomize");

            return particle;
        }
    }
}