﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateAttackCombo : SBaseAiScript
    {
        private string[] Combos { get; }
        private int ComboIndex { get; set; }
        public string CurrentCombo => Combos[ComboIndex];

        public PlayerStateAttackCombo()
        {
            Combos = new[]
            {
                "melee2",
                "melee3"
            };
        }

        /// <summary>
        /// Returns true if the animation Id corresponds
        /// to 1 of the 2 combo animations.
        /// </summary>
        /// <param name="pOwnerController"></param>
        /// <returns></returns>
        private bool IsCombo(IAnimationController pOwnerController)
        {
            return pOwnerController.IsAnimId(Combos[0]) ||
                   pOwnerController.IsAnimId(Combos[1]);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(2))
            {
                pOwner.Systems.Ai.MakeHitBox(pOwner, HitBox.GetMeleeHitBox(pOwner.Data));
            }

            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;

            #region Keys

            if (World.Instance.Input.Keyboard.AttackPressed &&
                !World.Instance.Input.Keyboard.OldAttackPressed)
            {
                if (pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(6))
                {
                    ComboIndex++;
                    if (ComboIndex > Combos.Length - 1)
                    {
                        if (IsCombo(pOwner.Systems.Renderer.CurrentRenderer))
                        {

                            ComboIndex = 0;
                            pOwner.Systems.Ai.IsAttacking = false;
                            pOwner.Data.States.Set("idle");
                        }
                    }

                    else
                    {
                        pOwner.PlaySound("swordImpactLayer3");
                        pOwner.Systems.Ai.IsAttacking = false;
                        pOwner.Data.States.Set("attackCombo");
                    }
                }
            }

            #endregion

            // if the animation is finished.
            if (pOwner.Systems.Renderer.IsFinishedAnimation)
            {
                if (IsCombo(pOwner.Systems.Renderer.CurrentRenderer))
                {
                    // We reset the combo counter when combo animation is finished.
                    ComboIndex = 0;
                    pOwner.Systems.Ai.IsAttacking = false;
                    pOwner.Data.States.Set("idle");
                }
            }
        }
    }
}