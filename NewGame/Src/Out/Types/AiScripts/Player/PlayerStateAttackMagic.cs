﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Factory.Spells;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateAttackMagic : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;


            FireBreath(pOwner);

            //            Convert(pOwner);
        }

        private void Convert(IEntity pOwner)
        {
            if (!pOwner.Systems.Renderer.IsFinishedAnimation ||
                !pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("magic")) return;

            Rectangle bBox = new Rectangle(
                (int)(pOwner.Data.BoundingBox.X + pOwner.Data.BoundingBox.Width * 1.5f * pOwner.Data.XFlip),
                pOwner.Data.BoundingBox.Y,
                pOwner.Data.BoundingBox.Width,
                pOwner.Data.BoundingBox.Height);

            var spell = SpellsFactory.Convert(pOwner, bBox);
            pOwner.OnNewEntityInstance?.Invoke(spell);
            pOwner.PlaySound("magicShadowSpell");
            pOwner.Systems.Ai.HasFired = true;
        }

        private void FireBreath(IEntity pOwner)
        {
            if (pOwner.Systems.Ai.HasFired)
                return;

            if (!pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("magic") ||
                !pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(3)) return;

            var width = (int)(pOwner.Data.BoundingBox.Width * 1.5f);
            var pos = new Vector2(0, pOwner.Data.Position.Y);
            if (pOwner.Data.XFlip > 0)
                pos.X = pOwner.Data.BoundingBox.Right;
            else if (pOwner.Data.XFlip < 0)
                pos.X = pOwner.Data.BoundingBox.Left - width;

            var bBox = new Rectangle(
                (int)(pos.X),
                (int)pos.Y,
                width,
                pOwner.Data.BoundingBox.Height);

            var spell = SpellsFactory.FireBreath(pOwner, bBox);
            pOwner.OnNewEntityInstance?.Invoke(spell);
            pOwner.PlaySound("fireBreathB");
            PushBack(pOwner);
            pOwner.Systems.Ai.HasFired = true;

            Camera.Instance.Shake(ScreenShake.Impact.Small);
        }

        private static void PushBack(IEntity pOwner)
        {
            pOwner.Data.Positional.Velocity += new Vector2(
                            ((pOwner.Data.Positional.Accel / 10) + pOwner.Data.Positional.SpeedCoef) * (0 - pOwner.Data.XFlip) / 10, 
                0);
        }
    }
}