﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateJump : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Data.Positional.SpeedCoef = 1.5f;

            #region Keys

            if (World.Instance.Input.Keyboard.ActionPressed &&
                !World.Instance.Input.Keyboard.OldActionPressed)
            {
                pOwner.PlaySound("Test");
            }

            #endregion

            if (pOwner.Data.Positional.WMove == 0)
                pOwner.Data.Positional.WMove = pOwner.Data.XFlip;

            pOwner.Systems.Transform.MoveWithNormals(pOwner.Data, pOwner.Data.Positional.WMove, 0);
        }
    }
}