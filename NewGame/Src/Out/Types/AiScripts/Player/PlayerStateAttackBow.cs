﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Factory;
using NewGame.Out.Factory.GameObjects;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateAttackBow : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Systems.Ai.MakeHitBox(
                pOwner,
                pOwner.Data.BoundingBox);

            if (pOwner.Systems.Ai.IsAttacking)
            {
                if (pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(7))
                {
                    int offset = 4;
                    Rectangle aRect = new Rectangle(
                        pOwner.Data.BoundingBox.Left + (pOwner.Data.XFlip * pOwner.Data.BoundingBox.Width) - (offset * pOwner.Data.XFlip),
                        pOwner.Data.BoundingBox.Top + offset,
                        16,
                        8);

                    var arrow = ProjectileFactory.Get("arrow", aRect, 5, pOwner);
                    CustomEntityFactory.ShareObservers(pOwner, arrow);
                    pOwner.OnNewEntityInstance?.Invoke(arrow);

                    pOwner.PlaySound("attackBow");
                }

            }

            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;
        }
    }
}