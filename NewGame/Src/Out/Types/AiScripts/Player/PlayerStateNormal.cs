﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Objects;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Enums;
using NewGame.Out.Systems.Ai.Player;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateNormal : SBaseAiScript
    {
        private SoundCollectionRepeater Repeater { get; }
        private int WeaponCounter { get; set; }
        private int WeaponsCount => Enum.GetNames(typeof(PlayerWeapon)).Length;

        public PlayerStateNormal()
        {
            var footSteps = new List<string>()
            {
                "footSteps01","footSteps02","footSteps03",
                "footSteps04","footSteps05","footSteps06",
                "footSteps07","footSteps08","footSteps09",
                "footSteps10",
            };
            Repeater = new SoundCollectionRepeater(footSteps, 0.3f);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Data.Positional.SpeedCoef = 1;

            #region Directions

            // Re-init move normals.
            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;

            if (World.Instance.Input.Keyboard.RightPressed)
            {
                pOwner.Data.Conditions.IsFlipX = false;
                pOwner.Data.Positional.WMove = 1;
            }

            else if (World.Instance.Input.Keyboard.LeftPressed)
            {
                pOwner.Data.Conditions.IsFlipX = true;
                pOwner.Data.Positional.WMove = -1;
            }

            if (World.Instance.Input.Keyboard.UpPressed)
            {
                pOwner.Data.Positional.HMove = -1;
            }

            else if (World.Instance.Input.Keyboard.DownPressed)
            {
                pOwner.Data.Positional.HMove = 1;
            }

            #endregion

            #region Keys

            // DEBUG
            if (World.Instance.Input.Keyboard.SelectPressed &&
                !World.Instance.Input.Keyboard.OldSelectPressed)
            {
                World.Instance.WorldStatus.IsDebug = !World.Instance.WorldStatus.IsDebug;
            }

            if (World.Instance.Input.Keyboard.JumpPressed &&
                !World.Instance.Input.Keyboard.OldJumpPressed)
            {
                pOwner.Data.Positional.Velocity = Vector2.Zero;
                pOwner.Data.States.Set("dodgeRoll");
            }

            if (World.Instance.Input.Keyboard.RightShoulderPressed &&
                !World.Instance.Input.Keyboard.OldRightShoulderPressed)
            {
                pOwner.Data.Positional.Velocity = Vector2.Zero;
                pOwner.Data.States.Set("jump");
            }

            if (World.Instance.Input.Keyboard.LeftShoulderPressed &&
                !World.Instance.Input.Keyboard.OldLeftShoulderPressed)
            {
                WeaponCounter++;
                if (WeaponCounter > WeaponsCount - 1)
                    WeaponCounter = 0;

                if (pOwner.Systems.Ai is AiPlayer player)
                    player.CurrentWeapon = (PlayerWeapon)WeaponCounter;
            }

            if (World.Instance.Input.Keyboard.AttackPressed &&
                !World.Instance.Input.Keyboard.OldAttackPressed)
            {
                pOwner.Data.Positional.Velocity = Vector2.Zero;

                pOwner.Data.States.Set("attack");
            }

            #endregion

            // Move
            pOwner.Systems.Transform.MoveWithNormals(
                pOwner.Data, pOwner.Data.Positional.WMove,
                pOwner.Data.Positional.HMove);

            EmitFootSteps(pGameTime, pOwner);
        }

        private void EmitFootSteps(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.Positional.Velocity != Vector2.Zero)
            {
                Repeater.Start();
            }
            else
            {
                Repeater.Stop();
            }
            Repeater.Update(pGameTime);

            if (!Repeater.RepeatCd.IsRunning)
            {
                if (!pOwner.Systems.Collider.IsWorldCollisions)
                    pOwner.PlaySound(Repeater.CurrentSoundId);
            }
        }
    }
}