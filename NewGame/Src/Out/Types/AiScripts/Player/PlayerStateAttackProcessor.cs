﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using NewGame.Out.Enums;
using NewGame.Out.Systems.Ai.Player;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    public class PlayerStateAttackProcessor : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;

            if (pOwner.Systems.Ai is AiPlayer player)
            {
                if (player.CurrentWeapon == PlayerWeapon.Melee)
                {
                    pOwner.Data.States.Set("attackSlash");
                }
                else if (player.CurrentWeapon == PlayerWeapon.Bow)
                {
                    pOwner.Data.States.Set("bow");
                }
                else if (player.CurrentWeapon == PlayerWeapon.Magic)
                {
                    pOwner.Data.States.Set("magic");
                }
            }
        }
    }
}