﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Objects;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Systems.Ai.Followers;
using NewGame.Out.Systems.Ai.Player;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    public class PlayerStateControlNormal : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Data.Positional.SpeedCoef = 1;

            #region Directions

            // Re-init move normals.
            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;

            if (World.Instance.Input.Keyboard.RightPressed)
            {
                pOwner.Data.Conditions.IsFlipX = false;
                pOwner.Data.Positional.WMove = 1;
            }

            else if (World.Instance.Input.Keyboard.LeftPressed)
            {
                pOwner.Data.Conditions.IsFlipX = true;
                pOwner.Data.Positional.WMove = -1;
            }

            if (World.Instance.Input.Keyboard.UpPressed)
            {
                pOwner.Data.Positional.HMove = -1;
            }

            else if (World.Instance.Input.Keyboard.DownPressed)
            {
                pOwner.Data.Positional.HMove = 1;
            }

            #endregion

            #region Keys


            if (World.Instance.Input.Keyboard.AttackPressed &&
                !World.Instance.Input.Keyboard.OldAttackPressed)
            {
                pOwner.Systems.Family.Parent.Systems.Replace(new AiPlayer(pOwner.Data.States));
                pOwner.Systems.Replace(new AiFollower(pOwner.Data.States));
                Camera.Instance.Ai.Follow(pOwner.Systems.Family.Parent);
            }

            #endregion

            // Move
            pOwner.Systems.Transform.MoveWithNormals(
                pOwner.Data, pOwner.Data.Positional.WMove, 
                pOwner.Data.Positional.HMove);
        }
    }
}