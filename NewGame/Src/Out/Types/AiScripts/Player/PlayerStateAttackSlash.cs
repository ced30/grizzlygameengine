﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Systems.Ai.Player;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;

#endregion

namespace NewGame.Out.Types.AiScripts.Player
{
    /// <inheritdoc />
    /// <summary>
    /// This is an example script, containing the logic for 1 state
    /// </summary>
    public class PlayerStateAttackSlash : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);
            if (pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(2))
            {
                pOwner.Systems.Ai.MakeHitBox(pOwner, HitBox.GetMeleeHitBox(pOwner.Data));
            }

            pOwner.Data.Positional.WMove = 0;
            pOwner.Data.Positional.HMove = 0;

            #region Keys

            if (World.Instance.Input.Keyboard.AttackPressed &&
                !World.Instance.Input.Keyboard.OldAttackPressed)
            {
                if (pOwner.Systems.Renderer.CurrentRenderer.IsFrameGreater(3))
                {
                    if (pOwner.Systems.Ai is AiPlayer player)
                        player.IsCombo = true;

                    pOwner.Systems.Ai.IsAttacking = false;
                    pOwner.Data.States.Set("attackCombo");
                }
            }

            #endregion
        }
    }
}