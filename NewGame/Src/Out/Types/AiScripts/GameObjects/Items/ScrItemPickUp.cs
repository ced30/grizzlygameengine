﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Types.AiScripts.GameObjects.Items
{
    public class ScrItemPickUp : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            // Make hit particle
            var particle = new Particle(ImporterState.StripAnim, "pickUp", pOwner.Data.BoundingBox);
            particle.Data.States.Replace("idle", new ScrFadeInRemove());
            pOwner.OnNewParticleInstance?.Invoke(EmitterState.Front, particle);
            pOwner.PlaySound("goldPickUp");
            pOwner.Data.Conditions.ToRemove = true;

            particle.Data.States.Set("idle");

            base.Update(pGameTime, pOwner);
        }
    }
}