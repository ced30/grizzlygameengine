﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Types.AiScripts.GameObjects.Bushes
{
    public class ScrSmallBushStateDeath : SBaseAiScript
    {
        private int NbParticles { get; set; }

        public ScrSmallBushStateDeath()
        {
            NbParticles = World.Instance.R.Next(4);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("death"))
            {
                var particleId = "leaf";
                if (!pOwner.Data.Conditions.IsExpired)
                {
                    for (int i = 0; i < NbParticles; i++)
                    {
                        var particle = new Particle(ImporterState.StripAnim, particleId, pOwner.Data.BoundingBox);
                        particle.Data.Positional.Friction = 100;
                        particle.Data.States.Replace("idle", new ScrDeathStateTimer());
                        particle.Systems.Renderer.CurrentRenderer.PlayFirst();
                        particle.Data.Positional.Rotation = World.Instance.R.Next(360);
                        var angleRadian = MathHelper.ToRadians(particle.Data.Positional.Rotation);
                        var dX = pOwner.Data.Center.X + Math.Cos(angleRadian) * pOwner.Data.Positional.Accel * Math.PI / 2;
                        var dY = pOwner.Data.Center.Y + Math.Sin(angleRadian) * pOwner.Data.Positional.Accel * Math.PI / 2;
                        particle.Systems.Transform.LerpToTarget(particle.Data, new Vector2((float)dX, (float)dY));
                        pOwner.OnNewParticleInstance?.Invoke(EmitterState.Back, particle);
                    }

                    pOwner.PlaySound("");
                    pOwner.Data.Conditions.IsExpired = true;
                }

                if (pOwner.Systems.Renderer.CurrentRenderer.IsFinished)
                {
                    pOwner.Data.Conditions.ToRemove = true;
                }
            }
        }
    }
}