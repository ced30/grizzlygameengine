﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Types.AiScripts.GameObjects.Bushes
{
    public class ScrSmallBushStateDamage : ScrCollideStateDefault
    {
        private Cooldown LeafCd { get; }
        private const float LeafTimer = 1f;

        public ScrSmallBushStateDamage()
        {
            LeafCd = new Cooldown();
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (!LeafCd.IsRunning)
            {
                var particleId = "leaf";
                var particle = new Particle(ImporterState.StripAnim, particleId, pOwner.Data.BoundingBox);
                particle.Data.Positional.Friction = 100;
                particle.Data.States.Replace("idle", new ScrDeathStateTimer());
                particle.Systems.Renderer.CurrentRenderer.PlayFirst();
                particle.Data.Positional.Rotation = World.Instance.R.Next(360);
                var angleRadian = MathHelper.ToRadians(particle.Data.Positional.Rotation);
                var dX = pOwner.Data.Center.X + Math.Cos(angleRadian) * pOwner.Data.Positional.Accel * Math.PI / 2;
                var dY = pOwner.Data.Center.Y + Math.Sin(angleRadian) * pOwner.Data.Positional.Accel * Math.PI / 2;
                particle.Systems.Transform.LerpToTarget(particle.Data, new Vector2((float)dX, (float)dY));
                pOwner.OnNewParticleInstance?.Invoke(EmitterState.Back, particle);

                LeafCd.Start(LeafTimer);
            }

            LeafCd.Update(pGameTime);
        }
    }
}