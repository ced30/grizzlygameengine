﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame.Out.Types.AiScripts.GameObjects.DestructibleObjects
{
    public class ScrDestructibleStateDeath : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("death"))
            {
                if (pOwner.Systems.Renderer.CurrentRenderer.IsFinished)
                {
                    if (!pOwner.Systems.Ai.HasFired)
                    {
                        var particle = new Particle(ImporterState.StripAnim, pOwner.Id, pOwner.Data.BoundingBox);
                        particle.Systems.Renderer.CurrentRenderer.PlayAnimation(pOwner.Id);
                        pOwner.OnNewParticleInstance?.Invoke(EmitterState.Back, particle);

                        pOwner.Systems.Ai.HasFired = true;
                    }

                    if (pOwner.Data.Colors.Alpha <= 0)
                    {
                        pOwner.Data.Conditions.ToRemove = true;
                    }
                    else
                    {
                        pOwner.Data.Colors.Fade(
                            Colors.FadeStates.In, Colors.OpacityStates.Transparent);
                    }
                }
            }

            base.Update(pGameTime, pOwner);
        }
    }
}