﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Particles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Particles.RainDrops
{
    public class ScrRainDropStateNormal : ScrParticleStateNormal
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Renderer.CurrentRenderer.IsFinished)
            {
                pOwner.Data.States.Set("randomize");
            }
        }
    }
}