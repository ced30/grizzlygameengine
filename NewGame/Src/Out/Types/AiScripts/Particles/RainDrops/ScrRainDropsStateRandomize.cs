﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Particles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Particles.RainDrops
{
    public class ScrRainDropsStateRandomize : ScrParticleStateRandomize
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Systems.Transform.SetRotation(pOwner.Data, World.Instance.R.Next(359));
            var xPos = World.Instance.R.Next(0, Screen.Width - pOwner.Data.Width);
            var yPos = World.Instance.R.Next(0, Screen.Height - pOwner.Data.Height);

            pOwner.Systems.Transform.SetPosition(pOwner.Data, new Vector2(
                Camera.Instance.DrawRectangle.Left + xPos, Camera.Instance.DrawRectangle.Top + yPos));

            pOwner.Data.Positional.Zoom = (float)World.Instance.R.NextDouble() +
                               World.Instance.R.Next(0, 2);
            
            if (pOwner.Systems.Renderer.CurrentRenderer is SDefaultStripAnimator animator)
                animator.Restart();

            pOwner.Data.States.Set("normal");
        }
    }
}