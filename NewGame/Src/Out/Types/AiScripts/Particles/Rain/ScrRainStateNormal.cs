﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Particles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Particles.Rain
{
    public class ScrRainStateNormal : ScrParticleStateNormal
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            Move(pOwner);

            ClampPosition(pOwner);

        }

        private void ClampPosition(IEntity pOwner)
        {
            if (pOwner.Data.BoundingBox.Top > Camera.Instance.DrawRectangle.Bottom + pOwner.Data.BoundingBox.Height)
                pOwner.Data.States.Set("randomize");
        }

        public void Move(IEntity pOwner)
        {
            var speed = World.Instance.R.Next(10, 100) / 10f;
            pOwner.Systems.Transform.MoveAtConstantSpeed(pOwner.Data, speed);
        }
    }
}