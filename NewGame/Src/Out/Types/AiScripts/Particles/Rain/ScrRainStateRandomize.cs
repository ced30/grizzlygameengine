﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Particles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Particles.Rain
{
    public class ScrRainStateRandomize : ScrParticleStateRandomize
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Systems.Transform.SetRotation(pOwner.Data, 120 + World.Instance.R.Next(-100, 100) / 10f);
            pOwner.Data.Positional.SpeedCoef = World.Instance.R.Next(10, 50) / 10f;
            var xPos = World.Instance.R.Next(0, Screen.Width);
            var yPos = World.Instance.R.Next(0, Screen.Height);

            if (World.Instance.R.Next(100) > 50)
                pOwner.Data.Position = new Vector2(
                    Camera.Instance.DrawRectangle.Left + xPos, Camera.Instance.Transform.Position.Y - pOwner.Data.BoundingBox.Height);
            else
                pOwner.Data.Position = new Vector2(
                    Camera.Instance.DrawRectangle.Right + pOwner.Data.BoundingBox.Width, Camera.Instance.DrawRectangle.Top + yPos);

            pOwner.Data.Colors.SetAlpha((float)World.Instance.R.NextDouble());
            pOwner.Data.Positional.Zoom = (float)World.Instance.R.NextDouble() +
                               World.Instance.R.Next(0, 2);

            pOwner.Data.States.Set("normal");
        }
    }
}