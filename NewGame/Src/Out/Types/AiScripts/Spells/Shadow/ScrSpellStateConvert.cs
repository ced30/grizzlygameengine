﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using NewGame.Out.Systems.Ai.Followers;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.Shadow
{
    public class ScrSpellStateConvert : ScrCollideStateDefault
    {
        protected override void Collide(IEntity pOwner)
        {
            base.Collide(pOwner);

            for (var i = 0; i < pOwner.Systems.Ai.LstColliders.Count; i++)
            {
                var collider = pOwner.Systems.Ai.LstColliders[i];

                if (collider.Faction.Equals("enemy") &&
                    pOwner.Data.States.IsMatching("death"))
                {
                    collider.Data.Attributes.BaseAttributes.HealthPoints =
                        collider.Data.Attributes.BaseAttributes.MaxHealth;
                    collider.Systems.Replace(new AiFollower(collider.Data.States));
                    collider.Systems.Family.BeAdoptedBy(pOwner.Systems.Family.Parent);
                    collider.Data.Conditions.IsExpired = false;
                    collider.Systems.Shader.SetCurrent("GhostEffect");
                    if (collider.Systems.Family.HasMembers)
                    {
                        for (var j = collider.Systems.Family.Members.Count - 1; j >= 0; j--)
                        {
                            var member = collider.Systems.Family.Members[i];
                            member?.Systems.Shader.SetCurrent("GhostEffect");
                        }
                    }

                    pOwner.PlaySound("pickUp");
                }
            }
        }
    }
}