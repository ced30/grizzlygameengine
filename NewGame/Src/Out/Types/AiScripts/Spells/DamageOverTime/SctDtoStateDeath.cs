﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.DamageOverTime
{
    public class SctDtoStateDeath : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Systems.Renderer.IsFinishedAnimation)
            {
                if (pOwner.Data.Colors.Alpha <= 0)
                {
                    pOwner.Systems.Ai.CurrentCollider?.Data.DeBuffs.Remove(pOwner.Id);
                    pOwner.Data.Conditions.ToRemove = true;
                }
                else pOwner.Data.Colors.Fade(Colors.FadeStates.In, Colors.OpacityStates.Transparent);
            }

            base.Update(pGameTime, pOwner);
        }
    }
}