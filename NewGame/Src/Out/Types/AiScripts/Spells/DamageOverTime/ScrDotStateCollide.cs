﻿#region Usings

using System.Diagnostics;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.DamageOverTime
{
    public class ScrDotStateCollide : SBaseAiScript
    {
        private int _debugCounter;
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Ai.CurrentCollider == null)
                return;

            var selfDamage = pOwner.Data.Attributes.TotalAttributes.Damage * 5;
            var entityToDamage = pOwner.Systems.Ai.CurrentCollider;

            if (entityToDamage.Data.Conditions.ToRemove ||
                entityToDamage.Data.Conditions.IsExpired ||
                pOwner.Data.Attributes.BaseAttributes.HealthPoints <= 0)
            {
                pOwner.Data.States.Set("death");
            }

            if (pOwner.Systems.Renderer.IsFinishedAnimation)
            {
                var tick = pOwner.Systems.Ai.GetCooldown("tick");
                if (tick.IsRunning) return;

                // Stay on the target
                StickOnTarget(pOwner, entityToDamage);

                // Damage the target
                entityToDamage.Systems.Ai.CollideWithoutCheck(entityToDamage, pOwner);

                // Damage itself
                pOwner.Data.Attributes.BaseAttributes.HealthPoints -= selfDamage;

                // Restart the animation
                pOwner.Systems.Renderer.CurrentRenderer.Restart();

                // Restart the coolDown
                tick.Start(tick.LastTimer);
                //                DotDebugInfos(pOwner);
            }
        }

        private void DotDebugInfos(IEntity pOwner)
        {
            _debugCounter += 1;
            Debug.Print("/////////////////////////////////");
            Debug.Print(pOwner.Id + ", Tick n° " + _debugCounter + ", dot health: " + pOwner.Data.Attributes.BaseAttributes.HealthPoints);
        }

        private static void StickOnTarget(IEntity pOwner, IEntity collider)
        {
            var rect = new Vector2(
                                collider.Data.BoundingBox.X,
                                collider.Data.BoundingBox.Y + 1);

            pOwner.Systems.Transform.SetPosition(pOwner.Data, rect);
        }
    }
}