﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.Default
{
    public class ScrSpellMoveDefault : ScrFadeInRemove
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            pOwner.Data.Positional.SpeedCoef = 2;
            pOwner.Data.Positional.Velocity += new Vector2(pOwner.Data.Positional.Accel * pOwner.Data.XFlip, 0);

            base.Update(pGameTime, pOwner);
        }
    }
}