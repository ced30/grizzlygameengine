﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.Fire
{
    public class ScrSpellStateFireBreathCollide : ScrCollideStateDefault
    {
        protected override void Collide(IEntity pOwner)
        {
            base.Collide(pOwner);

            var dotId = "fireDot1";
            if (pOwner.Systems.Ai.CurrentCollider.Data.DeBuffs.Contains(dotId)) return;

            var dot = MakeDamageOverTime(pOwner);
            pOwner.Systems.Ai.CurrentCollider.Data.DeBuffs.Add(dotId);
            pOwner.Data.Positional.SpeedCoef = 2;
            Camera.Instance.Shake(ScreenShake.Impact.Mini);
            pOwner.Data.Positional.Velocity += new Vector2(pOwner.Data.Positional.Accel * pOwner.Data.XFlip, 0);
            pOwner.OnNewEntityInstance?.Invoke(dot);
        }

        private static IEntity MakeDamageOverTime(IEntity pOwner)
        {
            var dot = Factory.Spells.DamageOverTimeFactory.Dot(pOwner, pOwner.Systems.Ai.CurrentCollider);
            dot.Systems.Family.BeAdoptedBy(pOwner);
            return dot;
        }
    }
}