﻿namespace NewGame.Out.Enums
{
    public enum PlayerWeapon
    {
        Melee,
        Bow,
        Magic
    }
}