﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using GrizzlyGameEngine.Core.Out.Scenes.Components;
using GrizzlyGameEngine.Core.Out.Scenes.Components.Base;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace NewGame.Out.Scenes.Components
{
    public class TileMapSceneSystems : BaseSceneSystems
    {
        #region Declarations

        /// <summary>
        /// Handles all the scene rendering
        /// </summary>
        public SceneRenderer Renderer { get; set; }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pData"></param>
        public TileMapSceneSystems(GameData pData)
        {
            Random = pData.Random;
            Input = new GeneralInput();
            Screen = new Screen(pData.Graphics);
            Screen.SetResolution(Resolution.Res.Small);
            Camera.Instance.InitCamera(pData.Graphics.GraphicsDevice.Viewport);
            Factory = new EntityFactoryManager();
            RequestManager = new PathRequestManager();
            ParticlesManager = new ParticleManager();

            // Load fadeEffect and start with a fadeIn.
            Fade = new FadeEffect(
                GrizzlyContentManager.Instance.TmxCManager.GetTexture("fadeRectangle").Texture,
                Camera.Instance);

            Renderer = new SceneRenderer(
                pData,
                ParticlesManager,
                Fade);
        }
        
        public override void LoadContent()
        {
            Renderer.LoadContent();
            Fade.Start(FadeEffect.FadeStates.FadeIn);
        }

        public override void Update(GameTime pGameTime)
        {
            ParticlesManager.Update(pGameTime);
            RequestManager.Update();
            TileMap.Update(pGameTime);
            SoundControl?.Update(pGameTime);
            Fade.Update(pGameTime);
            Camera.Instance.Update(pGameTime, TileMap.Data.MapRectangle);
        }

        public override void Draw(SpriteBatch pSpriteBatch, List<IEntity> pEntities)
        {
            Renderer.Draw(
                pSpriteBatch,
                TileMap,
                pEntities);
        }
    }
}