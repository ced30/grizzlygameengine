﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using GrizzlyGameEngine.Core.Out.Scenes.Components.Base;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NewGame.Out.Globals;
using TiledContentImporter.Components.Extractors;

#endregion

namespace NewGame.Out.Scenes.Components
{
    public class ProceduralSceneSystems : BaseSceneSystems
    {
        private ProceduralRenderer Renderer { get; }

        public ProceduralSceneSystems(
            GameData pData, SpriteFont pFont)
        {
            Content = pData.Content;
            Font = pFont;
            Random = pData.Random;
            Input = new GeneralInput();
            Screen = new Screen(pData.Graphics);
            Screen.SetResolution(Resolution.Res.Medium);
            Camera.Instance.InitCamera(pData.Graphics.GraphicsDevice.Viewport);
            Factory = new EntityFactoryManager();
            RequestManager = new PathRequestManager();
            MapGenerator = new ProceduralManager(Random, pFont);
            ParticlesManager = new ParticleManager();

            // Load fadeEffect and start with a fadeIn.
            Fade = new FadeEffect(
                GrizzlyContentManager.Instance.TmxCManager.GetTexture("fadeRectangle").Texture,
                Camera.Instance);

            Fade.Start(FadeEffect.FadeStates.FadeIn);

            pData.MapData = MapGenerator.GetPackedMap(
                "testMap",
                TileMapExtractor.GetMap(
                    GameGlobals.CurrentInDoorLevelPath).Tilesets[0],
                Content.Load<Texture2D>(
                    GameGlobals.InDoorTexturePath));

            ProceduralMap = pData.MapData;

            TileMap = new TileMap {Collider = new TileMapCollider(ProceduralMap)};

            Renderer = new ProceduralRenderer(pData, ParticlesManager, Fade, MapGenerator);
            Renderer.SetFont(pFont);
        }

        public override void LoadContent()
        {
            Renderer.LoadContent();
            Camera.Instance.SetCenter(new Vector2(Screen.Center.X, Screen.Center.Y));
        }

        public override void Update(GameTime pGameTime)
        {
            ReadKeys();

            ParticlesManager.Update(pGameTime);
            SoundControl?.Update(pGameTime);
            Fade.Update(pGameTime);
            Camera.Instance.Update(pGameTime, ProceduralMap.MapRectangle);
        }

        private void ReadKeys()
        {
            Input.ReadKeyboard();

            if (Input.Keyboard.UpPressed &&
                !Input.Keyboard.OldUpPressed)
            {
                MapGenerator.SetCurrentQuad(new Vector2(0, -1));
            }

            if (Input.Keyboard.DownPressed &&
                !Input.Keyboard.OldDownPressed)
            {
                MapGenerator.SetCurrentQuad(new Vector2(0, 1));
            }

            if (Input.Keyboard.LeftPressed &&
                !Input.Keyboard.OldLeftPressed)
            {
                MapGenerator.SetCurrentQuad(new Vector2(-1, 0));
            }

            if (Input.Keyboard.RightPressed &&
                !Input.Keyboard.OldRightPressed)
            {
                MapGenerator.SetCurrentQuad(new Vector2(1, 0));
            }

            if (Input.Keyboard.AttackPressed &&
                !Input.Keyboard.OldAttackPressed)
            {
                MapGenerator.Reload();

                ProceduralMap = MapGenerator.GetPackedMap(
                    "testMap",
                    TileMapExtractor.GetMap(
                        GameGlobals.CurrentInDoorLevelPath).Tilesets[0],
                    Content.Load<Texture2D>(
                        GameGlobals.InDoorTexturePath));
            }

            if (Input.Keyboard.JumpPressed &&
                !Input.Keyboard.OldJumpPressed)
            {
                MapGenerator.ToggleState();
            }

            Input.CloseKeyboard();
        }

        public override void Draw(SpriteBatch pSpriteBatch, List<IEntity> pList)
        {
            Renderer.Draw(pSpriteBatch, pList, ProceduralMap);
        }
    }
}