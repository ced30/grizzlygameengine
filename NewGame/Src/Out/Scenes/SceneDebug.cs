﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.ParticleEmitter;
using GrizzlyGameEngine.Core.Out.Components.SoundControl;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Scenes;
using GrizzlyGameEngine.Core.Out.Scenes.Components;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using NewGame.Out.Factory;
using NewGame.Out.Factory.Entities;
using NewGame.Out.Factory.GameObjects;
using NewGame.Out.Factory.Sounds;
using NewGame.Out.Globals;
using NewGame.Out.Scenes.Components;

#endregion

namespace NewGame.Out.Scenes
{
    /// <summary>
    /// This is the example scene, use it to build yours.
    /// </summary>
    public class SceneDebug : Scene
    {
        /// <summary>
        /// The player entity.
        /// </summary>
        public IEntity Player { get; set; }

        public SceneDebug(GameData pData) : base(pData)
        {
            Systems = new TileMapSceneSystems(SceneManager.Instance.GameData);
        }

        #region Scene Load

        /// <inheritdoc />
        /// <summary>
        /// Loads the scene's content,
        /// Load inherited classes loadContent methods first.
        /// </summary>
        public override void LoadContent()
        {
            // load content
            //-------------//
            LoadSounds();
            LoadMusic();
            LoadFactories();
            LoadMap();
            LoadPathFinding();
            BuildEntities();
            SortEntities();
            LoadEmitters();
            SetCamera();
            //-------------//

            World.Instance.LoadContent(
                Systems.Input, Systems.TileMap);

            base.LoadContent();
        }

        private void LoadSounds()
        {
            Systems.SoundControl = new SoundControl(GameData.Content, new CustomSoundData());
        }

        private void LoadMusic()
        {
            Systems.SoundControl.PlayAmbiance(
                "rainFall",
                5f);

            Systems.SoundControl.PlayMusic(
                "contemplative01",
                50f);
        }

        /// <summary>
        /// Add factory types before loading the tileMap.
        /// </summary>
        private void LoadFactories()
        {
            Systems.Factory.AddFactory("enemy", new EnemyFactory());
            Systems.Factory.AddFactory("destructibleObject", new DestructibleObjectFactory());
            Systems.Factory.AddFactory("npc", new NpcFactory());
            Systems.Factory.AddFactory("player", new PlayerFactory());
            Systems.Factory.AddFactory("item", new ItemFactory());
        }

        /// <summary>
        /// Takes a map and a collision layer as arguments.
        /// </summary>
        private void LoadPathFinding()
        {
            Systems.RequestManager.LoadContent(
                Systems.TileMap.Data.Map,
                Systems.TileMap.Data.CurrentCollisionLayer);
        }

        /// <summary>
        /// Loads the map and loads the world.Map
        /// </summary>
        private void LoadMap()
        {
            var mapData = GrizzlyContentManager.Instance.TmxCManager.GetMapData(GameGlobals.CurrentOutDoorLevelPath);

            Systems.TileMap = new TileMap();
            Systems.TileMap.LoadContent(mapData, Systems.Factory);
        }

        /// <summary>
        /// we retrieve the list of entities from the tileMap.
        /// </summary>
        private void BuildEntities()
        {
            Data.LstEntities.AddRange(Systems.TileMap.GetEntities(Systems.Factory));
        }

        /// <summary>
        /// adds dropShadows to the entities and
        /// performs specific init tasks on various entities.
        /// </summary>
        private void SortEntities()
        {
            for (int i = 0; i < Data.LstEntities.Count; i++)
            {
                // if the entity has sound enabled, we attach the sound observer.
                if (Data.LstEntities[i].Data.Conditions.IsSound)
                    Data.LstEntities[i].Attach(Systems.SoundControl.Observer);

                // Attach the delegate to create and pass entities to the main list.
                Data.LstEntities[i].OnNewEntityInstance = OnNewEntity;
                Data.LstEntities[i].OnNewParticleInstance = OnNewParticle;

                var faction = Data.LstEntities[i].Faction;
                if (faction.Equals("player") ||
                    faction.Equals("npc") ||
                    faction.Equals("enemy") ||
                    faction.Equals("destructible"))
                {

                    // we attach the drop shadow under the entity.
                    //                    LstEntities.Add(Agent.GetDropShadowFor(LstEntities[i], new Vector2(0 - LstEntities[i].Data.BoundingBox.Width / 2f, 1)));

                    Data.LstEntities.Add(CustomEntityFactory.DropShadowFor(Data.LstEntities[i], new Vector2(1, 0)));

                    // we look for a player id and set the player1 entity.
                    if (Data.LstEntities[i].Id.Equals("player"))
                    {
                        Player = Data.LstEntities[i];
                    }
                }
            }
        }

        /// <summary>
        /// Gives the camera a target and sets the camera to follow it.
        /// </summary>
        private void SetCamera()
        {
            Camera.Instance.SetCenter(Player.Data.Center);
            Camera.Instance.Ai.Follow(Player);
        }

        /// <summary>
        /// Loads weather system (load particle emitters).
        /// </summary>
        private void LoadEmitters()
        {
            Systems.ParticlesManager.AddEmitter(new ParticleEmitterCustom());
            //            Systems.ParticlesManager.AddEmitter(new ParticleEmitterRain());
            //            Systems.ParticlesManager.AddEmitter(new ParticleEmitterRainDrops());
        }

        #endregion

        /// <summary>
        /// Uses a delegate to pass spawned entities
        /// to the main list.
        /// </summary>
        /// <param name="pEntityToCreate"></param>
        protected virtual void OnNewEntity(IEntity pEntityToCreate)
        {
            pEntityToCreate.OnNewEntityInstance = OnNewEntity;
            Data.LstEntities.Add(pEntityToCreate);
        }

        /// <summary>
        /// Uses a delegate to pass spawned particles
        /// to the particle manager.
        /// </summary>
        /// <param name="pState"></param>
        /// <param name="pParticle"></param>
        protected virtual void OnNewParticle(EmitterState pState, Particle pParticle)
        {
            Systems.ParticlesManager.AddParticle(pState, pParticle);
        }

        /// <inheritdoc />
        /// <summary>
        /// Updates the entities and checks for collisions.
        /// </summary>
        /// <param name="pGameTime"></param>
        protected override void UpdateEntities(GameTime pGameTime)
        {
            if (World.Instance.WorldStatus.IsPaused)
                return;

            // entities
            if (Data.LstEntities != null &&
                Data.LstEntities.Count > 0)
            {
                for (int i = 0; i < Data.LstEntities.Count; i++)
                {
                    var entityA = Data.LstEntities[i];

                    // Update the entity.
                    entityA.Update(pGameTime, Systems.TileMap.Collider, Data.LstEntities);

                    // collision test
                    for (var j = Data.LstEntities.Count - 1; j >= 0; j--)
                    {
                        // if we are testing the same entity => skip.
                        if (entityA == Data.LstEntities[j])
                            continue;

                        IEntity entityB = Data.LstEntities[j];

                        // we test if A collides B.
                        entityA.Collide(entityA, entityB);
                        entityB.Collide(entityB, entityA);
                    }

                    // remove the entity if .ToRemove = true.
                    CleanUp(entityA);
                }
            }
        }
    }
}