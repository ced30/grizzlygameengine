﻿#region Usings

using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Scenes;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NewGame.Out.Scenes.Components;
using NewGame.Out.Systems.Ai.Player;

#endregion

namespace NewGame.Out.Scenes
{
    public class SceneProcedural : Scene
    {
        private IEntity Player { get; set; }

        public SceneProcedural(GameData pData) : base(pData)
        {
            Systems = new ProceduralSceneSystems(
                pData, GrizzlyContentManager.Instance.Factory.GetFont("SmallFont"));
        }

        public override void LoadContent()
        {
            Systems.LoadContent();

            // We query the helpers Agent to get a
            // basic Agent with collisions.
            //            var player = new Agent("player", new Rectangle(
            //                3345,
            //                2436, 
            //                16, 16));

            LoadPlayer();

            Camera.Instance.Ai.Follow(Player);
            Camera.Instance.Focus.ZoomFactor = 3;


            World.Instance.LoadContent(new GeneralInput(), null);
        }

        private void LoadPlayer()
        {
            var player = new Agent("player", new Rectangle(
                            6000,
                            1000,
                            16, 16));

            player.Data.Conditions.IsCollideAbleByWorld = true;

            // We set the sound to true.
            player.Data.Conditions.IsSound = true;

            // fill up the list of entities the player can collide with
            player.CanCollideWith("destructibleObject");

            // We load the player's Ai.
            player.Systems.Ai = new AiPlayer(player.Data.States);

            player.Systems.Replace(new SDefaultColliderController());

            // We set the current renderer to be an atlas animator
            // and we set the current atlas.
            //            player.Systems.Renderer.Set(
            //                ContentLoader.Instance.Factory.GetAtlas(pObject.Name));
            player.Systems.Renderer.Set(
                GrizzlyContentManager.Instance.TmxCManager.GetAtlasGroup(player.Id));

            Player = player;
            Data.LstEntities.Add(player);
        }

        public override void Update(GameTime pGameTime)
        {
            UpdateEntities(pGameTime);
            Systems.Update(pGameTime);
            Data.SortZOrder();
        }

        protected override void UpdateEntities(GameTime pGameTime)
        {
            // entities
            if (Data.LstEntities == null || Data.LstEntities.Count <= 0) return;

            for (var i = Data.LstEntities.Count - 1; i >= 0; i--)
            {
                var entityA = Data.LstEntities[i];

                // Update the entity.
                entityA.Update(pGameTime, Systems.TileMap.Collider, Data.LstEntities);

                // collision test
                for (var j = Data.LstEntities.Count - 1; j >= 0; j--)
                {
                    // if we are testing the same entity => skip.
                    if (entityA == Data.LstEntities[j])
                        continue;

                    IEntity entityB = Data.LstEntities[j];

                    // we test if A collides B.
                    entityA.Collide(entityA, entityB);
                    entityB.Collide(entityB, entityA);
                }

                // remove the entity if .ToRemove = true.
                CleanUp(entityA);
            }
        }

        public override void Draw(SpriteBatch pSpriteBatch)
        {
            Systems.Draw(pSpriteBatch, Data.LstEntities);
        }
    }
}