﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Primitives2D;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Scenes.Components;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NewGame.Out.Factory.Content;
using NewGame.Out.Factory.Loot;
using NewGame.Out.Globals;
using NewGame.Out.Scenes;
using TiledContentImporter.Types.Enums;

#endregion

namespace NewGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        #region Declarations

        /// <summary>
        /// Contains game class relative data to pass to the scenes
        /// </summary>
        public GameData GameData { get; set; }

        /// <summary>
        /// Main game's graphicsDeviceManager.
        /// </summary>
        public GraphicsDeviceManager Graphics { get; set; }

        /// <summary>
        /// Main game's spriteBatch,
        /// don't create a new one, pass this one instead.
        /// </summary>
        public SpriteBatch SpriteBatch { get; set; }

        #endregion

        /// <summary>
        /// Constructor: initializes game1.
        /// </summary>
        public Game1()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            GameData = new GameData(
                Content,
                Window,
                Graphics);

            GameData.SetNewSeed(GameGlobals.Seed);
            IsMouseVisible = GameData.IsMouseVisible;
            Window.AllowUserResizing = GameData.IsResizable;
            Window.Title = GameData.Title;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            // Initialize the sceneManager:
            SceneManager.Instance.InitManager(GameData);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // Load the globals first.
            GrizzlyContentManager.Instance.LoadContent(Content, new ContentFactoryCustom(Content), new CustomLootTable());

            // Load TileSets data content.
            GrizzlyContentManager.Instance.TmxCManager.LoadContent(ImporterState.AtlasGroup, GameGlobals.AtlasGroupsPath);
            GrizzlyContentManager.Instance.TmxCManager.LoadContent(ImporterState.StripGroup, GameGlobals.StripGroupsPath);
            GrizzlyContentManager.Instance.TmxCManager.LoadContent(ImporterState.StripAnim, GameGlobals.StripAnimPath);
            GrizzlyContentManager.Instance.TmxCManager.LoadContent(ImporterState.Texture, GameGlobals.TexturesPath);
            GrizzlyContentManager.Instance.TmxCManager.LoadContent(ImporterState.PngFont, GameGlobals.PngFontsPath);

            // Load primitives static class.
            Primitives2D.LoadContent(GraphicsDevice);

            // Set fullScreen or not and Change scene to your first scene.
            //            Screen.SetFullScreen(true);
            SceneManager.Instance.ChangeScene(new SceneDebug(SceneManager.Instance.GameData));
//            SceneManager.Instance.ChangeScene(new SceneProcedural(SceneManager.Instance.GameData));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            SceneManager.Instance.CurrentScene.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // TODO: Add your drawing code here
            SceneManager.Instance.CurrentScene.Draw(SpriteBatch);

            base.Draw(gameTime);
        }
    }
}
