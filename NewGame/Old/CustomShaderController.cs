﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Systems.Shader
{
    public class SCustomShader: SDefaultShaderController
    {
        public string FlashId { get; set; }

        public SCustomShader()
        {
            FlashId = "whiteFlash";
        }

        public override void Apply()
        {
            if (ShaderDictionary.ContainsKey(FlashId))
            {
                if (CurrentShader == ShaderDictionary[FlashId])
                {
                    
                }
            }

            base.Apply();
        }
    }
}