﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace NewGame.Out.Types.AiScripts.Spells.Default
{
    public class ScrCollideStateDefault : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Systems.Ai.CurrentCollidedBy == null)
            {
                pOwner.Systems.Ai.SetState("normal");
                return;
            }
            else if (pOwner.Systems.Ai.CurrentCollidedBy == null)
            {
                pOwner.Systems.Ai.SetState("normal");
            }

            base.Update(pGameTime, pOwner);
        }
    }
}