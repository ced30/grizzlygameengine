﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.ParticleEmitter;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Components.SoundControl;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Scene;
using GrizzlyGameEngine.Core.Out.Managers;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes.Components
{
    public class TileMapSceneSystems : ISceneSystems
    {
        #region Declarations

        /// <summary>
        /// The class containing the factories, we load it with
        /// the desired factories and pass it to the tileMap
        /// to spawn the entities.
        /// </summary>
        public EntityFactoryManager Factory { get; set; }

        /// <summary>
        /// Main game's fade effect, use it to
        /// fadeIn or fadeOut the whole screen.
        /// </summary>
        public FadeEffect Fade { get; set; }

        /// <summary>
        /// Main game's input reading class.
        /// </summary>
        public GeneralInput Input { get; set; }

        /// <summary>
        /// The particles generator, contains dictionary string, particle
        /// </summary>
        public ParticleManager ParticlesManager { get; set; }

        /// <summary>
        /// Handles all the scene rendering
        /// </summary>
        public SceneRenderer Renderer { get; set; }

        /// <summary>
        /// The pathFinding request handler
        /// </summary>
        public PathRequestManager RequestManager { get; set; }
        
        /// <summary>
        /// Main game's screen class,
        /// handles the resolutions.
        /// </summary>
        public Screen Screen { get; set; }

        /// <summary>
        /// Main game's soundControl class,
        /// Handles the audio output.
        /// </summary>
        public SoundControl SoundControl { get; set; }

        /// <summary>
        /// Main game's tileMap,
        /// Handles tileMap loading, parsing, drawing and animating.
        /// </summary>
        public TileMap TileMap { get; set; }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pData"></param>
        public TileMapSceneSystems(GameData pData)
        {
            Input = new GeneralInput();
            Screen = new Screen(pData.Graphics);
            Screen.SetResolution(Resolution.Res.Small);
            Camera.Instance.InitCamera(pData.Graphics.GraphicsDevice.Viewport);
            Factory = new EntityFactoryManager();
            RequestManager = new PathRequestManager();
            ParticlesManager = new ParticleManager();

            // Load fadeEffect and start with a fadeIn.
            Fade = new FadeEffect(
                GrizzlyContentManager.Instance.TmxCManager.GetTexture("fadeRectangle").Texture,
                Camera.Instance);

            Renderer = new SceneRenderer(
                pData,
                ParticlesManager,
                Fade);
        }

        #region Loading

        public void LoadContent()
        {
            Renderer.LoadContent();
            Fade.Start(FadeEffect.FadeStates.FadeIn);
        }

        private void LoadSounds()
        {
            SoundControl = new SoundControl(GameData.Content, new CustomSoundData());
        }

        private void LoadMusic()
        {
            SoundControl.PlayAmbiance(
                "rainFall",
                5f);

            SoundControl.PlayMusic(
                "contemplative01",
                50f);
        }

        /// <summary>
        /// Add factory types before loading the tileMap.
        /// </summary>
        private void LoadFactories()
        {
            Factory.AddFactory("enemy", new EnemyFactory());
            Factory.AddFactory("destructibleObject", new DestructibleObjectFactory());
            Factory.AddFactory("npc", new NpcFactory());
            Factory.AddFactory("player", new PlayerFactory());
            Factory.AddFactory("item", new ItemFactory());
        }

        /// <summary>
        /// Takes a map and a collision layer as arguments.
        /// </summary>
        private void LoadPathFinding()
        {
            RequestManager.LoadContent(
                TileMap.Data.Map,
                TileMap.Data.CurrentCollisionLayer);
        }

        /// <summary>
        /// Loads the map and loads the world.Map
        /// </summary>
        private void LoadMap()
        {
            var mapData = GrizzlyContentManager.Instance.TmxCManager.GetMapData(GameGlobals.CurrentLevelPath);

            TileMap = new TileMap();
            TileMap.LoadContent(mapData, Factory);
        }

        /// <summary>
        /// we retrieve the list of entities from the tileMap.
        /// </summary>
        /// <param name="pLstEntities"></param>
        private void BuildEntities(List<IEntity> pLstEntities)
        {
            pLstEntities.AddRange(TileMap.GetEntities(Factory));
        }

        /// <summary>
        /// Loads weather system (load particle emitters).
        /// </summary>
        private void LoadEmitters()
        {
            ParticlesManager.AddEmitter(new ParticleEmitterCustom());
            //            Systems.ParticlesManager.AddEmitter(new ParticleEmitterRain());
            //            Systems.ParticlesManager.AddEmitter(new ParticleEmitterRainDrops());
        }

        #endregion







        public void Update(GameTime pGameTime)
        {
            ParticlesManager.Update(pGameTime);
            RequestManager.Update();
            TileMap?.Update(pGameTime);
            SoundControl?.Update(pGameTime);
            Fade.Update(pGameTime);
            Camera.Instance.Update(pGameTime, TileMap);
        }

        public virtual void Draw(SpriteBatch pSpriteBatch, List<IEntity> pEntities)
        {
            Renderer.Draw(
                pSpriteBatch,
                TileMap,
                pEntities);
        }
    }
}