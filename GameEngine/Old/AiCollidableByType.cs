﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai
{
    public class AiCollidableByType : AiCollidableDefault
    {
        protected override void UpdateCollisions(IEntity pOwner)
        {
            base.UpdateCollisions(pOwner);

            if (pOwner.Systems.Ai.LstColliders.Count > 0)
            {
                var isType = false;
                for (int i = 0; i < pOwner.Data.LstCanCollideWithType.Count; i++)
                {
                    if (CurrentCollidedBy.Type.Equals(pOwner.Data.LstCanCollideWithType[i]))
                        isType = true;
                }

                if (isType)
                {
                    SetState("collide");
                }
            }
        }
    }
}