﻿#region Usings

using GrizzlyGameEngine.Core.Out.Game;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes
{
    public class SceneDeath : Scene
    {
        public SceneDeath(GameData pData) : base(pData)
        {
        }
    }
}