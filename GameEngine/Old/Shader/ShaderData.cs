﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.Shader
{
    public class ShaderData
    {
        private float _flashAmount;
        private const float FlashStep = 0.16f;

        public Color FlashColor { get; set; }

        public float FlashAmount
        {
            get => _flashAmount;
            set => _flashAmount = MathHelper.Clamp(value, 0, 1);
        }

        public ShaderData()
        {
            FlashAmount = 1;
            FlashColor = Color.White;
        }

        public void DecreaseFlash()
        {
            FlashAmount -= FlashStep;
        }

        public void ResetFlash()
        {
            FlashAmount = 1;
        }
    }
}