﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Base.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Shader.Offline
{
    public class NoShaderItem : ABaseShaderItem
    {
        public override void Apply()
        {

        }

        public override void Reset()
        {

        }

        public override void Update(GameTime pGameTime)
        {

        }
    }
}