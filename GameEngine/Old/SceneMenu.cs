﻿#region Usings

using GrizzlyGameEngine.Core.Out.Game;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes
{
    public class SceneMenu : Scene
    {
        public SceneMenu(GameData pData) : base(pData)
        {
        }
    }
}