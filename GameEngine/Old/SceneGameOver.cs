﻿#region Usings

using GrizzlyGameEngine.Core.Out.Game;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes
{
    public class SceneGameOver : Scene
    {
        public SceneGameOver(GameData pData) : base(pData)
        {
        }
    }
}