﻿using GrizzlyGameEngine.Core.Out.Components.Procedural;
using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Scenes.Components.Base
{
    public class ProceduralSceneSystem : BaseSceneSystems
    {
        private ProceduralMapGenerator MapGenerator { get; set; }

        public ProceduralSceneSystem()
        {
            Rectangle dimensions = new Rectangle(
                0,
                0,
                50,
                50);

            MapGenerator = new ProceduralMapGenerator(dimensions, 16);
        }
    }
}