﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class Quadrant
    {
        private float PercentageToFill { get; }
        private float SpawnChance { get; }
        private float ChanceToTurn { get; }
        private int MaxIterations { get; }
        private int MaxWalkers { get; }

        /// <summary>
        /// The number of cells in a quad
        /// </summary>
        private int NbCellsX { get; }
        private int NbCellsY { get; }
        private int TileSize { get; }
        private int DrawSize => TileSize / 2;
        private Cell[,] Cells { get; set; }
        private List<Walker> Walkers { get; set; }
        private System.Random Random { get; set; }

        public bool IsActive { get; set; }
        public bool IsOpenBottom { get; set; }
        public bool IsOpenLeft { get; set; }
        public bool IsOpenRight { get; set; }
        public bool IsOpenTop { get; set; }
        public int Id { get; set; }

        public Rectangle BoundingBox { get; set; }

        public Quadrant(Vector2 pPosition, int pNbCellsX, int pNbCellsY, int pTileSize, System.Random pRandom)
        {
            Random = pRandom;
            TileSize = pTileSize;
            NbCellsX = pNbCellsX;
            NbCellsY = pNbCellsY;
            BoundingBox = new Rectangle(
                (int)(pPosition.X * pNbCellsX),
                (int)(pPosition.Y * pNbCellsY),
                pNbCellsX,
                pNbCellsY);


            ChanceToTurn = 100f;
            SpawnChance = 0.0005f;
            PercentageToFill = 0.25f;
            MaxIterations = 250;
            MaxWalkers = 10;
            Walkers = new List<Walker>();
            Initialize();
        }

        /// <summary>
        /// Adds a non-solid rectangle at the desired location,
        /// with the desired dimensions (in cell unit)
        /// </summary>
        /// <param name="pRect"></param>
        public void AddConnection(Rectangle pRect)
        {
            for (int x = 0; x < pRect.Width; x++)
            {
                for (int y = 0; y < pRect.Height; y++)
                {
                    SetCellId(pRect.X + x, pRect.Y + y, ProceduralManager.GetGid(TileType.Floor));
                    Cells[pRect.X + x, pRect.Y + y].IsActive = false;
                }
            }
        }

        private void Initialize()
        {
            Cells = new Cell[NbCellsX, NbCellsY];
            for (var x = 0; x < NbCellsX; x++)
            {
                for (var y = 0; y < NbCellsY; y++)
                {
                    Cells[x, y] = new Cell(new Vector2(x, y), TileSize) { GiD = ProceduralManager.GetGid(TileType.Grass), IsActive = true };
                }
            }
        }

        public void ExpandFromHub(Vector2 pPos)
        {
            Walkers.Clear();
            var iterations = 0;
            var pos = new Vector2(
                (int)pPos.X,
                (int)pPos.Y);
            Walkers.Add(new Walker(pos));

            SetCellId((int)pos.X, (int)pos.Y, ProceduralManager.GetGid(TileType.Floor));
            Cells[(int)pos.X, (int)pos.Y].IsActive = false;

            while (iterations < MaxIterations)
            {
                for (var i = 0; i < Walkers.Count; i++)
                {
                    var walker = Walkers[i];
                    var num = Random.Next(4);
                    if (Random.Next(100) <= ChanceToTurn)
                    {
                        switch (num)
                        {
                            case 0:
                                walker.Position += new Vector2(1, 0);
                                break;
                            case 1:
                                walker.Position += new Vector2(0, 1);
                                break;
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (num)
                        {
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                            default:
                                break;
                        }
                    }

                    walker.Position = Vector2.Clamp(
                        walker.Position,
                        new Vector2(1, 1),
                        new Vector2(NbCellsX - 2, NbCellsY - 2));

                    // Set the cell not solid
                    SetCellId((int)walker.Position.X, (int)walker.Position.Y, ProceduralManager.GetGid(TileType.Floor));
                    Cells[(int)walker.Position.X, (int)walker.Position.Y].IsActive = false;

                    // Chance to spawn a new walker
                    if (RandF() < SpawnChance && Walkers.Count < MaxWalkers)
                    {
                        Walkers.Add(new Walker(walker.Position));
                    }

                    // Chance to delete a walker
                    if (RandF() < SpawnChance / 2 && Walkers.Count > 1)
                    {
                        Walkers.Remove(walker);
                    }

                    iterations += 1;
                }
            }
        }

        private void MoveWalkers()
        {
            var iterations = 0;
            var pos = new Vector2(
                NbCellsX / 2f,
                NbCellsY / 2f);
            Walkers.Add(new Walker(pos));
            while (iterations < MaxIterations)
            {
                for (var i = 0; i < Walkers.Count; i++)
                {
                    var walker = Walkers[i];
                    var num = Random.Next(4);
                    if (Random.Next(100) <= ChanceToTurn)
                    {
                        switch (num)
                        {
                            case 0:
                                walker.Position += new Vector2(1, 0);
                                break;
                            case 1:
                                walker.Position += new Vector2(0, 1);
                                break;
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (num)
                        {
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                            default:
                                break;
                        }
                    }

                    walker.Position = Vector2.Clamp(
                        walker.Position,
                        new Vector2(2, 2),
                        new Vector2(NbCellsX - 3, NbCellsY - 3));

                    SetCellId((int)walker.Position.X, (int)walker.Position.Y, ProceduralManager.GetGid(TileType.Floor));
                    Cells[(int)walker.Position.X, (int)walker.Position.Y].IsActive = false;

                    // Chance to spawn a new walker
                    if (RandF() < SpawnChance && Walkers.Count < MaxWalkers)
                    {
                        Walkers.Add(new Walker(walker.Position));
                    }

                    // Chance to delete a walker
                    if (RandF() < SpawnChance / 2 && Walkers.Count > 1)
                    {
                        Walkers.Remove(walker);
                    }

                    iterations += 1;

                    if (FloorCount() / (float)(NbCellsX * NbCellsY) > PercentageToFill)
                        break;
                }
            }
        }

        private int FloorCount()
        {
            var count = 0;
            for (int i = 0; i < NbCellsX; i++)
            {
                for (int j = 0; j < NbCellsY; j++)
                {
                    var cellId = GetCellId(i, j);
                    if (cellId == ProceduralManager.GetGid(TileType.Floor))
                    {
                        count += 1;
                    }
                }
            }

            return count;
        }

        public Cell GetCell(int pX, int pY)
        {
            return Cells[pX, pY];
        }

        public int GetCellId(int pX, int pY)
        {
            return Cells[pX, pY].GiD;
        }

        public void SetCellId(int pX, int pY, int pId)
        {
            Cells[pX, pY].GiD = pId;
        }

        private float RandF()
        {
            return Random.Next(1000) / 1000f;
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            if (Cells == null)
                return;

            for (var x = 0; x < NbCellsX; x++)
            {
                for (var y = 0; y < NbCellsY; y++)
                {
                    if (Cells[x, y].GiD == ProceduralManager.GetGid(TileType.Floor))
                    {
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                x * DrawSize + Camera.Camera.Instance.WorldRectangle.Left + Screen.Screen.Width / 3,
                                y * DrawSize + Camera.Camera.Instance.WorldRectangle.Top,
                                DrawSize,
                                DrawSize),
                            Color.Gray);
                    }
                    else if (Cells[x, y].GiD == ProceduralManager.GetGid(TileType.Grass))
                    {
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                x * DrawSize + Camera.Camera.Instance.WorldRectangle.Left + Screen.Screen.Width / 3,
                                y * DrawSize + Camera.Camera.Instance.WorldRectangle.Top,
                                DrawSize,
                                DrawSize),
                            Color.Green);
                    }
                }
            }
        }
    }
}