﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class Sector
    {
        #region Declarations

        private const int Zoom = 4;

        /// <summary>
        /// the number oc active quads in the sector
        /// </summary>
        private int NbActive { get; set; }

        /// <summary>
        /// The number of cells in a quad
        /// </summary>
        private int NbCellsX { get; }
        private int NbCellsY { get; }

        /// <summary>
        /// The number of quads in a sector
        /// </summary>
        private int NbQuadX { get; }
        private int NbQuadY { get; }
        private int TileSize { get; }

        private Quadrant CurrentQuadrant { get; set; }
        private SpriteFont Font { get; }
        private System.Random Random { get; set; }
        private Vector2 Counter { get; set; }
        public Quadrant[,] Quadrants { get; set; }
        public SectorSize CurrentSize { get; set; }

        #endregion

        public Sector(int pNbQuadX, int pNbQuadY, int pNbCellsX, int pNbCellsY, int pTileSize, System.Random pRandom)
        {
            Font = GrizzlyContentManager.Instance.Factory.GetFont("SmallFont");
            Random = pRandom;
            NbQuadX = pNbQuadX;
            NbQuadY = pNbQuadY;
            NbCellsX = pNbCellsX;
            NbCellsY = pNbCellsY;
            TileSize = pTileSize;
            Reload();
        }

        public void Reload()
        {
            InitSector();
            BuildSector();
            BuildHubs();
            BuildRooms();
            Counter = Vector2.Zero;
            SetCurrentQuad(Vector2.Zero);
        }

        #region Helper methods

        private void BuildRooms()
        {
            var hubWidth = 4;
            var hubHeight = 4;
            var startX = Random.Next(NbCellsX / 2 - 4, NbCellsX / 2 + 4);
            var startY = Random.Next(NbCellsY / 2 - 4, NbCellsY / 2 + 4);
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var quad = Quadrants[x, y];
                    if (!quad.IsActive)
                        continue;

                    if (quad.IsOpenTop)
                    {
                        for (var tY = 0; tY < NbCellsY / 2f + hubHeight; tY += hubHeight)
                        {
                            quad.ExpandFromHub(new Vector2(startX, 0 + tY));
                        }
                    }

                    if (quad.IsOpenBottom)
                    {
                        for (int tY = NbCellsY - hubWidth; tY >= NbCellsY / 2f - hubHeight; tY -= hubHeight)
                        {
                            quad.ExpandFromHub(new Vector2(startX, tY));
                        }
                    }
                    if (quad.IsOpenLeft)
                    {
                        for (int tY = 0; tY < NbCellsX / 2 + hubWidth; tY += hubWidth)
                        {
                            quad.ExpandFromHub(new Vector2(0 + tY, startY));
                        }

                    }
                    if (quad.IsOpenRight)
                    {
                        for (int tY = NbCellsX - hubWidth; tY >= NbCellsX / 2 - hubWidth; tY -= hubHeight)
                        {
                            quad.ExpandFromHub(new Vector2(tY, startY));
                        }
                    }
                }
            }
        }

        private void BuildHubs()
        {
            var hubWidth = 4;
            var hubHeight = 4;
            var startX = Random.Next(NbCellsX / 2 - 4, NbCellsX / 2 + 4);
            var startY = Random.Next(NbCellsY / 2 - 4, NbCellsY / 2 + 4);
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var quad = Quadrants[x, y];
                    if (!quad.IsActive)
                        continue;

                    if (quad.IsOpenTop)
                    {
                        for (var tY = 0; tY < NbCellsY / 2f + hubHeight; tY += hubHeight)
                        {
                            quad.AddConnection(new Rectangle(startX, 0 + tY, hubWidth, hubHeight));
                        }
                    }

                    if (quad.IsOpenBottom)
                    {
                        for (int tY = NbCellsY - hubWidth; tY >= NbCellsY / 2f - hubHeight; tY -= hubHeight)
                        {
                            quad.AddConnection(new Rectangle(startX, tY, hubWidth, hubHeight));
                        }
                    }
                    if (quad.IsOpenLeft)
                    {
                        for (int tY = 0; tY < NbCellsX / 2 + hubWidth; tY += hubWidth)
                        {
                            quad.AddConnection(new Rectangle(0 + tY, startY, hubWidth, hubHeight));
                        }

                    }
                    if (quad.IsOpenRight)
                    {
                        for (int tY = NbCellsX - hubWidth; tY >= NbCellsX / 2 - hubWidth; tY -= hubHeight)
                        {
                            quad.AddConnection(new Rectangle(tY, startY, hubWidth, hubHeight));
                        }
                    }
                }
            }
        }

        private void InitSector()
        {
            Quadrants = new Quadrant[NbQuadX, NbQuadY];
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    Quadrants[x, y] = new Quadrant(new Vector2(x, y), NbCellsX, NbCellsY, TileSize, Random);
                }
            }
        }

        private void BuildSector()
        {
            var id = 1;
            List<Vector2> positions = new List<Vector2>();

            // find an activate first quad
            var firstX = Random.Next(NbQuadX);
            var firstPos = new Vector2(firstX, 0);
            var firstQuadrant = Quadrants[(int)firstPos.X, (int)firstPos.Y];
            firstQuadrant.Id = id;
            firstQuadrant.IsActive = true;

            // create the walker
            Walker walker = new Walker(new Vector2(firstPos.X, firstPos.Y));

            // and add the position to the list
            positions.Add(firstPos);

            // while the walker has not reached the bottom of the
            // sector, we iterate
            while (walker.Position.Y < NbQuadY - 1)
            {
                var num = Random.Next(3);
                if (num == 0)
                {
                    walker.Position += new Vector2(1, 0);
                }
                else if (num == 1)
                {
                    walker.Position += new Vector2(-1, 0);
                }
                else if (num == 2)
                {
                    if (walker.Position.Y >= NbQuadY - 2 &&
                        ((int)walker.Position.X == 0 || (int)walker.Position.X == NbQuadX - 1))
                        continue;

                    walker.Position += new Vector2(0, 1);
                }

                walker.Position = Vector2.Clamp(
                    walker.Position,
                    new Vector2(0, 0),
                    new Vector2(NbQuadX - 1, NbQuadY - 1));

                // We check for openings
                if (!positions.Contains(walker.Position))
                {
                    id++;
                    var currentQuad = Quadrants[(int)(walker.Position.X), (int)(walker.Position.Y)];
                    currentQuad.Id = id;
                    currentQuad.IsActive = true;
                    positions.Add(walker.Position);
                }
            }

            CheckOpenings();

            NbActive = positions.Count;
            if (NbActive <= 5)
                CurrentSize = SectorSize.Small;
            else if (NbActive > 5 && NbActive <= 9)
                CurrentSize = SectorSize.Medium;
            else CurrentSize = SectorSize.Big;
        }

        private void CheckOpenings()
        {
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var currentQuad = Quadrants[x, y];
                    if (!currentQuad.IsActive)
                        continue;

                    // Top
                    var dY = y - 1;
                    if (dY >= 0)
                    {
                        var topQuad = Quadrants[x, dY];
                        if (topQuad.IsActive)
                        {
                            topQuad.IsOpenBottom = true;
                            currentQuad.IsOpenTop = true;
                        }
                    }

                    // Bottom
                    dY = y + 1;
                    if (dY <= NbQuadY - 1)
                    {
                        var bottomQuad = Quadrants[x, y + 1];
                        if (bottomQuad.IsActive)
                        {
                            bottomQuad.IsOpenTop = true;
                            currentQuad.IsOpenBottom = true;
                        }
                    }

                    // Left
                    var dX = x - 1;
                    if (dX >= 0)
                    {
                        var leftQuad = Quadrants[dX, y];
                        if (leftQuad.IsActive)
                        {
                            leftQuad.IsOpenRight = true;
                            currentQuad.IsOpenLeft = true;
                        }
                    }

                    // Right
                    dX = x + 1;
                    if (dX <= NbQuadX - 1)
                    {
                        var rightQuad = Quadrants[dX, y];
                        if (rightQuad.IsActive)
                        {
                            rightQuad.IsOpenLeft = true;
                            currentQuad.IsOpenRight = true;
                        }
                    }
                }
            }
        }

        public void SetCurrentQuad(Vector2 pPos)
        {
            Counter += pPos;
            if (Counter.X < 0)
                Counter = new Vector2(NbQuadX - 1, Counter.Y);
            if (Counter.X > NbQuadX - 1)
                Counter = new Vector2(0, Counter.Y);
            if (Counter.Y < 0)
                Counter = new Vector2(Counter.X, NbQuadY - 1);
            if (Counter.Y > NbQuadY - 1)
                Counter = new Vector2(Counter.X, 0);

            CurrentQuadrant = Quadrants[(int)Counter.X, (int)Counter.Y];
        }

        #endregion

        #region Draw Sector

        public void DrawSector(SpriteBatch pSpriteBatch)
        {
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    if (Quadrants[x, y].IsActive)
                    {
                        // Draw the quadrant
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                x * TileSize * Zoom,
                                y * TileSize * Zoom,
                                TileSize * Zoom,
                                TileSize * Zoom),
                            Color.Gray);

                        // Draw the outline
                        Primitives2D.Primitives2D.DrawRectangle(
                            pSpriteBatch,
                            new Rectangle(
                                x * TileSize * Zoom,
                                y * TileSize * Zoom,
                                TileSize * Zoom,
                                TileSize * Zoom),
                            Color.Black, 1);


                        var strColor = Color.White;
                        if (Counter == new Vector2(x, y))
                            strColor = Color.Red;

                        // Draw the quad ID
                        pSpriteBatch.DrawString(
                            Font,
                            Quadrants[x, y].Id.ToString(),
                            new Vector2(
                                (x * TileSize * Zoom) + TileSize,
                                (y * TileSize * Zoom) + TileSize),
                            strColor);

                        DrawQuadsOutLines(pSpriteBatch, x, y);
                    }
                    else
                    {
                        var emptyColor = Color.Green;
                        if (Counter == new Vector2(x, y))
                            emptyColor = Color.Red;
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                x * TileSize * Zoom,
                                y * TileSize * Zoom,
                                TileSize * Zoom,
                                TileSize * Zoom),
                            emptyColor);
                    }
                }
            }

            DrawSectorInfo(pSpriteBatch);
        }

        private void DrawSectorInfo(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.DrawString(
                Font,
                "Active quadrants: " + NbActive + "\n" + "\n" +
                "Inactive quadrants: " + ((NbQuadX * NbQuadY) - NbActive) + "\n" + "\n" +
                "Size: " + CurrentSize + "\n" + "\n" +
                "CurrentQuadrant ID: " + CurrentQuadrant.Id + ",    IsActive: " + CurrentQuadrant.IsActive + "\n" + "\n" +
                "Nb horizontal cells: " + NbCellsX +
                "   Nb vertical cells: " + NbCellsY,
                new Vector2(TileSize * Zoom, (NbQuadY * TileSize * Zoom) * 0.9f + (TileSize * Zoom)),
                Color.White);
        }

        private void DrawQuadsOutLines(SpriteBatch pSpriteBatch, int x, int y)
        {
            var width = (TileSize * Zoom) / 2;
            var height = (TileSize * Zoom) / 2;
            var color = Color.Yellow;

            if (Quadrants[x, y].IsOpenTop)
            {
                // Draw Top
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (x * TileSize * Zoom) + width / 2,
                        y * TileSize * Zoom,
                        width,
                        0),
                    color, 1);
            }

            if (Quadrants[x, y].IsOpenBottom)
            {
                // Draw bottom
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (x * TileSize * Zoom) + width / 2,
                        (y * TileSize * Zoom) + TileSize * Zoom,
                        width,
                        0),
                    color, 1);

            }

            if (Quadrants[x, y].IsOpenLeft)
            {
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        x * TileSize * Zoom,
                        (y * TileSize * Zoom) + height / 2,
                        0,
                        height),
                    color, 1);
            }

            if (Quadrants[x, y].IsOpenRight)
            {
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (x * TileSize * Zoom) + TileSize * Zoom,
                        (y * TileSize * Zoom) + height / 2,
                        0,
                        height),
                    color, 1);
            }
        }

        #endregion

        #region Draw CurrentQuad

        public void DrawCurrentQuad(SpriteBatch pSpriteBatch)
        {
            CurrentQuadrant.Draw(pSpriteBatch);
        }

        #endregion
    }
}