﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class Cell
    {
        private int TileSize { get; }
        public bool IsActive { get; set; }
        public int GiD;
        public Rectangle BoundingBox { get; set; }
        public Vector2 Position { get; set; }
        public Cell(Vector2 pPosition, int pTileSize)
        {
            TileSize = pTileSize;
            Position = new Vector2(pPosition.X * TileSize, pPosition.Y * TileSize);
            BoundingBox = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                TileSize,
                TileSize);
        }
    }
}