﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IAiController : IUpdatableSystem
    {
        bool IsFinished(string pCdId);
        IAiSystem CurrentBehavior { get; set; }
        void SetBehavior(IAiSystem pAi);
        void UpdateGameLogic(GameTime pGameTime, IEntity pOwner);
        void UpdateAnimations(GameTime pGameTime, IEntity pOwner);
    }
}