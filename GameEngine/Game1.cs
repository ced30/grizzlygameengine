﻿#region Usings

using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.Primitives2D;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Components.SoundControl;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Managers;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

#endregion

namespace GrizzlyGameEngine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        #region Declarations
        
        public AssetManager Manager { get; private set; }
        public Camera Camera { get; set; }
        public FadeEffect Fade { get; set; }
        public GeneralInput Input { get; set; }
        public GraphicsDeviceManager Graphics { get; set; }
        public SceneManager SceneManager { get; set; }
        public Screen Screen { get; set; }
        public SoundControl SoundControl { get; set; }
        public SpriteBatch SpriteBatch { get; set; }
        public TileMap TileMap { get; set; }

        #endregion

        public Game1()
        {
            Window.Title = "NewGame";
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Input = new GeneralInput();
            Screen = new Screen(this);
            Screen.SetResolution(Resolution.Res.Small);
            Manager = new AssetManager(this);
            SceneManager = new SceneManager(this);
            Camera = new Camera(this);
            SoundControl = new SoundControl(Content);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            ContentLoader.Instance.LoadContent(this);

            Primitives2D.LoadContent(GraphicsDevice);

            World.Instance.LoadContent(this);

            Fade = new FadeEffect(
                ContentLoader.Instance.Factory.GetTexture(
                    "fadeRectangle"));

            Fade.Start(FadeEffect.Type.FadeIn);

            //            Screen.SetFullScreen(true);

            SceneManager.ChangeScene(SceneManager.Type.Debug);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            TileMap?.Update(gameTime);
            SceneManager?.CurrentScene.Update(gameTime);
            Camera?.Update(gameTime);
            Screen?.Update(gameTime);
            Fade?.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            SceneManager.CurrentScene.Draw(SpriteBatch);

            base.Draw(gameTime);
        }
    }
}
