﻿namespace GrizzlyGameEngine.Globals.Types
{
    /// <summary>
    /// This class holds the status of the world static class.
    /// </summary>
    public class WorldStatus
    {
        public bool IsDebug { get; set; }
        public bool IsPaused { get; set; }
    }
}