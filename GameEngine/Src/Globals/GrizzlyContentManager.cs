﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Loot;
using GrizzlyGameEngine.Core.Out.Factory.Prefabs;
using Microsoft.Xna.Framework.Content;
using TiledContentImporter.Managers;

#endregion

namespace GrizzlyGameEngine.Globals
{
    public class GrizzlyContentManager
    {
        private static readonly GrizzlyContentManager _instance = new GrizzlyContentManager();

        #region Constructors

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforeFieldInit
        static GrizzlyContentManager()
        {
        }

        private GrizzlyContentManager()
        {
        }


        #endregion


        #region Public

        public static GrizzlyContentManager Instance => _instance;

        public TmxManager TmxCManager { get; set; }
        public DefaultLootTable LootFactory { get; set; }
        public ContentFactoryDefault Factory { get; set; }
        public ContentManager CManager { get; set; }

        #endregion

        /// <summary>
        /// Initializes the class and populates the content list.
        /// </summary>
        /// <param name="pContent"></param>
        /// <param name="pFactory"></param>
        /// <param name="pLoot"></param>
        public void LoadContent(ContentManager pContent, 
            ContentFactoryDefault pFactory,
            DefaultLootTable pLoot)
        {
            if (CManager != null)
                return;
            TmxCManager = new TmxManager(pContent);
            CManager = pContent;
            Factory = pFactory;
            Factory.Populate();
            LootFactory = pLoot;
        }
    }
}