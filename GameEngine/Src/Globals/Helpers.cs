﻿#region Usings

using Microsoft.Xna.Framework;
using System;
using System.Xml.Linq;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using TiledContentImporter.TiledSharp;

#endregion

namespace GrizzlyGameEngine.Globals
{
    public static class Helpers
    {
        #region Entity Stuff

        public static Vector2 ParentPos(IEntity pEntity)
        {
            if (pEntity.Systems.Family.Parent != null)
            {
                return pEntity.Systems.Family.Parent.Data.Position;
            }
            else return pEntity.Data.Position;
        }

        #endregion

        #region Tmx Stuff

        /// <summary>
        /// Adds a tmx property to an XElement.
        /// </summary>
        /// <param name="pElem"></param>
        /// <param name="pPropertyName"></param>
        /// <param name="pPropertyValue"></param>
        public static void AddTmxProperty(XElement pElem, string pPropertyName, string pPropertyValue)
        {
            XElement property = new XElement(
                "properties",
                new XElement("property",
                    new XAttribute("name", pPropertyName),
                    new XAttribute("value", pPropertyValue)));

            pElem.Add(property);
        }

        /// <summary>
        /// Builds a TmxObject from scratch.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pType"></param>
        /// <param name="pFaction"></param>
        /// <param name="pRect"></param>
        /// <returns></returns>
        public static TmxObject BuildTmxObject(
            string pName, string pType, string pFaction,
            Rectangle pRect)
        {
            XElement elem = new XElement(
                pName,
                new XAttribute("name", pName),
                new XAttribute("type", pType),
                new XAttribute("faction", pFaction),
                new XAttribute("x", pRect.X),
                new XAttribute("y", pRect.Y),
                new XAttribute("width", pRect.Width),
                new XAttribute("height", pRect.Height));

            AddTmxProperty(elem, "Faction", pFaction);
            TmxObject obj = new TmxObject(elem);

            return obj;
        }

        public static bool TmxBoolProperty(IDataController pData, string pId)
        {
            if (pData.TmxProperties.ContainsKey(pId))
            {
                bool.TryParse(pData.TmxProperties[pId], out var isTrueOrNot);
                return isTrueOrNot;
            }

            return false;
        }

        #endregion

        #region Trigonometry Stuff

        /// <summary>
        /// Returns the angle between two points.
        /// </summary>
        /// <param name="pPos1"></param>
        /// <param name="pPos2"></param>
        /// <returns></returns>
        public static double Angle(Vector2 pPos1, Vector2 pPos2)
        {
            return Math.Atan2(pPos2.Y - pPos1.Y, pPos2.X - pPos1.X);
        }
        

        /// <summary>
        /// Returns the distance between two points.
        /// </summary>
        /// <param name="pPos1"></param>
        /// <param name="pPos2"></param>
        /// <returns></returns>
        public static double Dist(Vector2 pPos1, Vector2 pPos2)
        {
            return Vector2.Distance(pPos2, pPos1);
        }

        #endregion
    }
}