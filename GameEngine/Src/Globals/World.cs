﻿#region Usings

using System;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Globals.Types;

#endregion

namespace GrizzlyGameEngine.Globals
{
    public class World : GrizzlyComponent
    {
        #region Private

        private static readonly World _instance = new World();

        #endregion


        #region Public

        public GeneralInput Input { get; set; }
        public int TileSize = 16;
        public Random R = new Random();
        public TileMap TileMap { get; set; }
        public WorldStatus WorldStatus { get; set; }

        #endregion


        #region Constructors

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforeFieldInit
        static World()
        {
        }

        private World()
        {
        }


        #endregion


        public static World Instance => _instance;

        /// <summary>
        /// Initializes the class.
        /// </summary>
        public virtual void LoadContent(
            GeneralInput pInput,
            TileMap pTileMap)
        {
            Input = pInput;

            if (pTileMap != null)
                TileMap = pTileMap;

            WorldStatus = new WorldStatus() { IsPaused = false, IsDebug = false };
        }
    }
}