﻿#region Usings

using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Globals
{
    public static class Collision
    {
        /// <summary>
        /// Checks if a vector2 is comprised inside a rectangle.
        /// </summary>
        /// <param name="pSource"></param>
        /// <param name="pDestination"></param>
        /// <returns></returns>
        public static bool IsPosInsideRect(Rectangle pSource, Rectangle pDestination)
        {
            if (pSource.X >= pDestination.Left ||
                pSource.X <= pDestination.Right ||
                pSource.Y >= pDestination.Top ||
                pSource.Y <= pDestination.Bottom)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// checks if there's a collision between a vector2 and a rectangle on a given direction.
        /// </summary>
        /// <param name="pPos"></param>
        /// <param name="pRect"></param>
        /// <param name="pDirection"></param>
        /// <returns></returns>
        public static bool IsCollideRect(Vector2 pPos, Rectangle pRect, Direction pDirection)
        {
            switch (pDirection)
            {
                case Direction.Up:
                    if (pPos.Y < pRect.Top)
                        return true;
                    return false;

                case Direction.Down:
                    if (pPos.Y > pRect.Bottom)
                        return true;
                    return false;

                case Direction.Left:
                    if (pPos.X < pRect.Left)
                        return true;
                    return false;

                case Direction.Right:
                    if (pPos.X > pRect.Right)
                        return true;
                    return false;
            }

            return false;
        }

        /// <summary>
        /// checks if there's a collision between 2 rectangles on a given direction.
        /// </summary>
        /// <param name="pSource"></param>
        /// <param name="pRect"></param>
        /// <param name="pDirection"></param>
        /// <returns></returns>
        public static bool IsCollideRect(Rectangle pSource, Rectangle pRect, Direction pDirection)
        {
            switch (pDirection)
            {
                case Direction.Up:
                    if (pSource.Top <= pRect.Top)
                        return true;
                    return false;

                case Direction.Down:
                    if (pSource.Bottom >= pRect.Bottom)
                        return true;
                    return false;

                case Direction.Left:
                    if (pSource.Left <= pRect.Left)
                        return true;
                    return false;

                case Direction.Right:
                    if (pSource.Right >= pRect.Right)
                        return true;
                    return false;
            }

            return false;
        }
    }
}