﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Base
{
    /// <summary>
    /// This class holds entities without their animations / textures, etc,
    /// so we can fill up the dictionary when we load resources from the tileMap
    /// and clone entities + add animations during gamePlay.
    /// </summary>
    public class CloneFactory
    {

        private Dictionary<string, IEntity> DictionaryClones { get; set; }
        public CloneFactory()
        {
            DictionaryClones = new Dictionary<string, IEntity>();
        }

        public bool Add(IEntity pEntity)
        {
            if (!DictionaryClones.ContainsKey(pEntity.Id))
            {
                DictionaryClones.Add(pEntity.Id, pEntity);
                return true;
            }

            return false;
        }

        public IEntity GetClone(string pId)
        {
            return null;
        }
    }
}