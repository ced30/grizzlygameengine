﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Inventory;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory.Base;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Base
{
    public abstract class ALootFactoryBase : IFactory
    {
        public static void InitData(IEntity pLoot, IEntity pSource)
        {
            if (pLoot == null)
                return;

            pLoot.Data.Position = new Vector2(
                pSource.Data.BoundingBox.X + pLoot.Data.BoundingBox.Width / 2f,
                pSource.Data.BoundingBox.Y - pLoot.Data.BoundingBox.Height / 2f);

            do
            {
                pLoot.Data.Positional.Velocity = new Vector2(
                    World.Instance.R.Next(0 - (int)(pLoot.Data.Positional.Accel / 4f), (int)(pLoot.Data.Positional.Accel / 4f)),
                    World.Instance.R.Next(0 - (int)(pLoot.Data.Positional.Accel / 4f), (int)(pLoot.Data.Positional.Accel / 4f)));

            } while ((int)pLoot.Data.Positional.Velocity.X == 0);
        }

        public virtual IEntity MakeLoot(string pType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This is an example on how your make functions should look like
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        public InventoryItem MakeItem(string pType)
        {
            switch (pType)
            {
                case "null":
                    break;
                
                case "bomb":
                    return new InventoryItem()
                    {
                        Id = "BOMB",
                        Description = "Large Bomb",
                        IsAvailableInStore = true,
                        IsSellAble = true,
                        BuyPrice = 100,
                        Equip = "toolSlot",
                        Type = "tool",
                        IsStackAble = true,
                    };
            }

            return null;
        }
    }
}