﻿#region Usings

using System.Collections.Generic;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Base.SubSystems
{
    public class ContentFactoryAdder
    {
        public void Strip(Dictionary<string, Dictionary<string, StripData>> pTarget,
            string pName, 
            Dictionary<string, StripData> pDico)
        {
            pTarget.Add(pName, pDico);
        }

        public void Font(Dictionary<string, string> pTarget, 
            string pName, string pPath)
        {
            pTarget.Add(pName, pPath);
        }

        public void SpriteEffect(Dictionary<string, string> pTarget,
            string pName, string pPath)
        {
            pTarget.Add(pName, pPath);
        }
    }
}