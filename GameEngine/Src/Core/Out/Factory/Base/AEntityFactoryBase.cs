﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory;
using TiledContentImporter.TiledSharp;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Base
{
    /// <summary>
    /// Abstract class implementing IEntityFactory,
    /// this is a class with a virtual method returning an entity,
    /// inherit from this class to make your entity factories.
    /// </summary>
    public abstract class AEntityFactoryBase : IEntityFactory
    {
        public virtual IEntity Make(TmxObject pObject)
        {
            throw new System.NotImplementedException();
        }
    }
}