﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Factory.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Base
{
    /// <inheritdoc />
    /// <summary>
    /// Base factory content loader.
    /// </summary>
    public abstract class AContentFactoryBase : IContentFactory
    {
        /// <summary>
        /// Load your content in the derivatives of this class.
        /// </summary>
        protected ContentFactoryAdder Add { get; set; }

        public ContentManager CManager { get; set; }

        /// <summary>
        /// Stores Font reference as Dictionary name, path.
        /// </summary>
        protected Dictionary<string, string> FontsDictionary { get; set; }

        /// <summary>
        /// Stores Font reference as Dictionary name, path.
        /// </summary>
        protected Dictionary<string, string> SpriteEffectDictionary { get; set; }

        #region GetMethods
        
        public SpriteFont GetFont(string pString)
        {
            return CManager.Load<SpriteFont>(FontsDictionary[pString]);
        }

        public Effect GetEffect(string pString)
        {
            return CManager.Load<Effect>(SpriteEffectDictionary[pString]);
        }

        #endregion


        /// <summary>
        /// PreLoads the content references when the program starts.
        /// </summary>
        public virtual void Populate()
        {
            
        }
    }
}