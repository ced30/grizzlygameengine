﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Factory.Base;
using GrizzlyGameEngine.Core.Out.Factory.Base.SubSystems;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Prefabs
{
    public class ContentFactoryDefault : AContentFactoryBase
    {
        public ContentFactoryDefault(ContentManager pContent)
        {
            CManager = pContent;

            Add = new ContentFactoryAdder();
            FontsDictionary = new Dictionary<string, string>();
            SpriteEffectDictionary = new Dictionary<string, string>();
        }

        #region Population

        public sealed override void Populate()
        {
            base.Populate();

            PopulateEffects();
            PopulateFonts();
            PopulateSounds();
        }

        protected virtual void PopulateEffects()
        {
            // TODO: Add spriteEffects elements.
        }

        protected virtual void PopulateFonts()
        {
            // TODO: Add fonts elements.
        }

        protected virtual void PopulateSounds()
        {
            // TODO: Add sound elements.
        }

        #endregion
    }
}