﻿using Microsoft.Xna.Framework.Content;

namespace GrizzlyGameEngine.Core.Out.Factory.Prefabs.OfflineFactories
{
    public class NoContentFactory : ContentFactoryDefault
    {
        public NoContentFactory(ContentManager pContent) : base(pContent)
        {
            
        }
    }
}