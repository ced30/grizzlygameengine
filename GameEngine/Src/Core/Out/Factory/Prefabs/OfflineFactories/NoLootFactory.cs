﻿#region Usings

using GrizzlyGameEngine.Core.Out.Factory.Base;

#endregion

namespace GrizzlyGameEngine.Core.Out.Factory.Prefabs.OfflineFactories
{
    public class NoLootFactory : AContentFactoryBase
    {
        public override void Populate()
        {
            
        }
    }
}