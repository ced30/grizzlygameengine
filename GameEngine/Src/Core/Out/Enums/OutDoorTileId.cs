﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    /// <summary>
    /// Each type equals a tileSet tile ID
    /// </summary>
    public enum OutDoorTileId
    {
        Floor = 24,
        Grass = 154,
        WallLeft = 229,
        WallRight = 232,
        WallTop = 102,
        WallBottom = 295,
        BottomLeftCornerOut = 293,
        BottomRightCornerOut = 296,
        TopLeftCornerOut = 101,
        TopRightCornerOut = 104,

        TopLeftCornerIn = 30,
        TopRightCornerIn = 35,
        BottomLeftCornerIn = 350,
        BottomRightCornerIn = 355,

        RoundedBottomLeftCorner = 118,
        RoundedBottomRightCorner = 119,
        RoundedTopLeftCorner = 54,
        RoundedTopRightCorner = 55,

    }
}