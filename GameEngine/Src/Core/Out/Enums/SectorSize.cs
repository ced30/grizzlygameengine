﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum SectorSize
    {
        Small,
        Medium,
        Big
    }
}