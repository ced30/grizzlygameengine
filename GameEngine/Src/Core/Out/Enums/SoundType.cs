﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum SoundType
    {
        Ambiance,
        Music
    }
}