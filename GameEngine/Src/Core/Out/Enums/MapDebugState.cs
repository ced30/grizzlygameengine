﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum MapDebugState
    {
        OverView,
        MapView
    }
}