﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum SceneStates
    {
        None,
        Loading,
        LoadingFinished,
        Change,
        Reload,
        Unload
    }
}