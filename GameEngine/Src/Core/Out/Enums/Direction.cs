﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}