﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum CollisionType
    {
        Aabb,
        Circular
    }
}