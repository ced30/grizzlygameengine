﻿namespace GrizzlyGameEngine.Core.Out.Enums
{
    public enum AutoTileType
    {
        EightBits,
        FourBits
    }
}