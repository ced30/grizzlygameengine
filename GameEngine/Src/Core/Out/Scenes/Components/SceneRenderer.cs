﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes.Components
{
    public class SceneRenderer : GrizzlyComponent
    {

        #region Declarations

        public GameData Data { get; set; }
        public FadeEffect Fade { get; set; }
        public ParticleManager Particles { get; set; }
        public RenderTarget2D BackLayer { get; set; }
        public RenderTarget2D FrontLayer { get; set; }
        public RenderTarget2D MergedLayers { get; set; }
        public RenderTarget2D MidLayer { get; set; }

        #endregion


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pParticle"></param>
        /// <param name="pFade"></param>
        public SceneRenderer(
            GameData pData, ParticleManager pParticle, FadeEffect pFade)
        {
            Data = pData;
            Particles = pParticle;
            Fade = pFade;
        }


        #region Initialization

        public override void LoadContent()
        {
            InitRenderTargets();
            base.LoadContent();
        }

        private void InitRenderTargets()
        {
            PresentationParameters pp =
               Data.Graphics.GraphicsDevice.PresentationParameters;

            // Back layer.
            BackLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Out.Components.Screen.Screen.Width, Out.Components.Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // Mid layer.
            MidLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Out.Components.Screen.Screen.Width, Out.Components.Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // Front Layer.
            FrontLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Out.Components.Screen.Screen.Width, Out.Components.Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // Total of all layers.
            MergedLayers = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Out.Components.Screen.Screen.Width, Out.Components.Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);
        }

        #endregion


        /// <summary>
        /// The main Draw Sequence.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pMap"></param>
        /// <param name="pList"></param>
        public void Draw(SpriteBatch pSpriteBatch, TileMap pMap, List<IEntity> pList)
        {
            DrawBackLayers(pSpriteBatch, pMap);
            DrawMidLayers(pSpriteBatch, pList);
            DrawFrontLayers(pSpriteBatch, pMap);

            SuperPoseRenderTargets(pSpriteBatch);
        }

        /// <summary>
        /// The mid layer is the entities layer, it's drawn onto the backBuffer and we use spriteEffect Immediate,
        /// otherwise the shader will not work.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pList"></param>
        public void DrawMidLayers(SpriteBatch pSpriteBatch, List<IEntity> pList)
        {
            if (pList != null &&
                pList.Count > 0)
            {
                BeginDraw(
                    pSpriteBatch, MidLayer, 
                    SpriteSortMode.Immediate, 
                    BlendState.AlphaBlend);

                for (int i = 0; i < pList.Count; i++)
                {
                    var sprite = pList[i];

                    // if the entity is not on camera, we don't draw it.
                    if (Camera.Instance.DrawRectangle.Intersects(sprite.Data.BoundingBox))
                    {
                        sprite.Draw(pSpriteBatch);
                    }
                }

                CloseDraw(pSpriteBatch);
            }
        }

        /// <summary>
        /// Tha back layers are drawn onto the backBuffer and before the entities.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pMap"></param>
        public virtual void DrawBackLayers(SpriteBatch pSpriteBatch, TileMap pMap)
        {
            BeginDraw(
                pSpriteBatch, 
                BackLayer, 
                SpriteSortMode.Deferred, 
                BlendState.AlphaBlend);

            // MidLayers
            if (pMap?.Data.MidLayers != null)
            {
                if (pMap.Data.MidLayers.Count > 0)
                {
                    TileMapRenderer.DrawLayer(
                        pSpriteBatch,
                        pMap.Data.MidLayers,
                        pMap.Data);
                }
            }

            // AnimatedLayers
            if (pMap?.Data.AnimatedLayers != null)
            {
                if (pMap.Data.AnimatedLayers.Count > 0)
                {
                    TileMapRenderer.DrawLayer(
                        pSpriteBatch,
                        pMap.Data.AnimatedLayers,
                        pMap.Data);
                }
            }

            Particles.DrawBack(pSpriteBatch);

            CloseDraw(pSpriteBatch);
        }

        /// <summary>
        /// The front layer is drawn onto the backBuffer and after the entities.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pMap"></param>
        public virtual void DrawFrontLayers(SpriteBatch pSpriteBatch, TileMap pMap)
        {
            if (pMap?.Data.FrontLayers == null ||
                pMap.Data.FrontLayers.Count <= 0)
                return;

            if (pMap.Data.FrontLayers == null)
                return;

            // Draw the front Layer.
            BeginDraw(
                pSpriteBatch, FrontLayer, 
                SpriteSortMode.Deferred, 
                BlendState.AlphaBlend);

            // Draw the tileMap.
            TileMapRenderer.DrawLayer(
                pSpriteBatch,
                pMap.Data.FrontLayers,
                pMap.Data);

            // quadTree stuff
            // ------------//
            //            MyGame.SceneManager.CurrentScene.QuadTree?.Draw(pSpriteBatch);
            //            MyGame.SceneManager.CurrentScene.DrawQuadTreeEntities(pSpriteBatch);
            // ------------//

            //            MyGame.SceneManager.CurrentScene.RequestManager?.Draw(pSpriteBatch);

            // weather stuff
            // ------------//
            Particles.DrawFront(pSpriteBatch);
            // ------------//

            // draw the camera.
            //            Camera?.Draw(pSpriteBatch);

            // Draw the fade effects.
            Fade.Draw(pSpriteBatch);

            // ------------//
            CloseDraw(pSpriteBatch);
        }

        /// <summary>
        /// Finally, we draw all the layers on a merged layer, that's the one we will
        /// actually resize and draw on the screen.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void SuperPoseRenderTargets(SpriteBatch pSpriteBatch)
        {
            Data.Graphics.GraphicsDevice.SetRenderTargets(MergedLayers);
            Data.Graphics.GraphicsDevice.Clear(Color.Transparent);

            pSpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);


            pSpriteBatch.Draw(BackLayer, Vector2.Zero, Color.White);
            pSpriteBatch.Draw(MidLayer, Vector2.Zero, Color.White);
            pSpriteBatch.Draw(FrontLayer, Vector2.Zero, Color.White);

            CloseDraw(pSpriteBatch);

            Resize(pSpriteBatch, MergedLayers);
        }


        #region Helpers methods

        /// <summary>
        /// Opens the spriteBatch
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pRenderTarget2D"></param>
        /// <param name="pMode"></param>
        /// <param name="pBlend"></param>
        public void BeginDraw(
            SpriteBatch pSpriteBatch, 
            RenderTarget2D pRenderTarget2D, 
            SpriteSortMode pMode,
            BlendState pBlend)
        {
            Data.Graphics.GraphicsDevice.SetRenderTargets(pRenderTarget2D);
            Data.Graphics.GraphicsDevice.Clear(Color.TransparentBlack);

            pSpriteBatch.Begin(
                pMode,
                pBlend,
                SamplerState.PointClamp, null, null, null,
                Camera.Instance.Transform.Matrix1);
        }

        /// <summary>
        /// Resize the merged layer.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pTarget"></param>
        public void Resize(SpriteBatch pSpriteBatch, RenderTarget2D pTarget)
        {
            // Now, we draw the render target on the screen
            float ratio = 1;
            int marginV = 0;
            int marginH = 0;
            float currentAspect = Data.Window.ClientBounds.Width / (float)Data.Window.ClientBounds.Height;
            float virtualAspect = (float)Out.Components.Screen.Screen.Width / Out.Components.Screen.Screen.Height;
            if (Out.Components.Screen.Screen.Height != Data.Window.ClientBounds.Height)
            {
                if (currentAspect > virtualAspect)
                {
                    ratio = Data.Window.ClientBounds.Height / (float)Out.Components.Screen.Screen.Height;
                    marginH = (int)((Data.Window.ClientBounds.Width - Out.Components.Screen.Screen.Width * ratio) / 2);
                }
                else
                {
                    ratio = Data.Window.ClientBounds.Width / (float)Out.Components.Screen.Screen.Width;
                    marginV = (int)((Data.Window.ClientBounds.Height - Out.Components.Screen.Screen.Height * ratio) / 2);
                }
            }

            Rectangle dst = new Rectangle(
                marginH, 
                marginV, 
                (int)(Out.Components.Screen.Screen.Width * ratio), 
                (int)(Out.Components.Screen.Screen.Height * ratio));

            pSpriteBatch.Begin(
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointClamp);

            pSpriteBatch.Draw(pTarget, dst, Color.White);

            pSpriteBatch.End();
        }

        /// <summary>
        /// Closes the spriteBatch.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void CloseDraw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.End();
            Data.Graphics.GraphicsDevice.SetRenderTarget(null);
        }

        #endregion
    }
}