﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Components.SoundControl;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Scene;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes.Components.Base
{
    public abstract class BaseSceneSystems : ISceneSystems
    {
        protected SpriteFont Font { get; set; }
        public Random Random { get; set; }
        public EntityFactoryManager Factory { get; set; }
        public FadeEffect Fade { get; set; }
        public GeneralInput Input { get; set; }
        public ParticleManager ParticlesManager { get; set; }
        public PathRequestManager RequestManager { get; set; }
        public Screen Screen { get; set; }
        public SoundControl SoundControl { get; set; }
        public TileMap TileMap { get; set; }
        protected ProceduralMapData ProceduralMap { get; set; }
        protected ContentManager Content { get; set; }
        protected ProceduralManager MapGenerator { get; set; }

        public virtual void LoadContent()
        {
            throw new System.NotImplementedException();
        }

        public virtual void Update(GameTime pGameTime)
        {
            throw new System.NotImplementedException();
        }

        public virtual void Draw(SpriteBatch pSpriteBatch, List<IEntity> dataLstEntities)
        {
            throw new System.NotImplementedException();
        }
    }
}