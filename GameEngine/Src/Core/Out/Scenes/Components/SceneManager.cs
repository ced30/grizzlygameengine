﻿#region Usings

using GrizzlyGameEngine.Core.Out.Game;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes.Components
{
    public class SceneManager
    {
        #region Declarations

        private static readonly SceneManager _instance = new SceneManager();
        public GameData GameData { get; set; }
        public Scene CurrentScene { get; set; }

        #endregion


        #region Constructors

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforeFieldInit
        static SceneManager()
        {
        }

        private SceneManager()
        {
        }

        #endregion

        public static SceneManager Instance => _instance;

        /// <summary>
        /// Takes no arguments.
        /// </summary>
        /// <param name="pData"></param>
        public void InitManager(GameData pData)
        {
            GameData = pData;
        }

        /// <summary>
        /// Unloads the current scene end loads a new scene as the current scene.
        /// </summary>
        /// <param name="pScene"></param>
        public void ChangeScene(Scene pScene)
        {
            if (CurrentScene != null)
            {
                CurrentScene.Unload();
                CurrentScene = null;
            }

            CurrentScene = pScene;

            CurrentScene?.LoadContent();
        }
    }
}