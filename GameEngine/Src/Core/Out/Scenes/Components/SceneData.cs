﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes.Components
{
    public class SceneData
    {
        public List<IEntity> LstEntities { get; set; }

        public SceneData()
        {
            LstEntities = new List<IEntity>();
        }

        /// <summary>
        /// sorts the entities so they are in the correct z-order for drawing purposes.
        /// </summary>
        public void SortZOrder()
        {
            if (LstEntities.Count < 2)
                return;

            LstEntities.Sort((x, y) => x.Data.Position.Y.CompareTo(y.Data.Position.Y));
        }
    }
}