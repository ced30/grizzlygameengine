﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Scene;
using GrizzlyGameEngine.Core.Out.Scenes.Components;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Scenes
{
    public class Scene : GrizzlyComponent
    {
        protected SceneData Data { get; set; }
        protected ISceneSystems Systems { get; set; }
        protected GameData GameData { get; set; }
        protected SceneStates CurrentState { get; set; }

        protected bool
            IsDebug => World.Instance.WorldStatus.IsDebug;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pData"></param>
        public Scene(GameData pData)
        {
            GameData = pData;
            Data = new SceneData();

            // Once we finished initializing,
            // we change the SceneState to Loading.
            CurrentState = SceneStates.Loading;
        }

        /// <summary>
        /// Removes the entities from the list if the have the ToRemove flag == true.
        /// </summary>
        /// <param name="pEntity"></param>
        public void CleanUp(IEntity pEntity)
        {
            if (pEntity.Data.Conditions.ToRemove)
            {
                Data.LstEntities.Remove(pEntity);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Loads the scene's content,
        /// Load inherited classes loadContent methods first.
        /// </summary>
        public override void LoadContent()
        {
            // inherited class LoadContent(); goes first
            // then:

            Systems.LoadContent();

            // Once we finished loading the content,
            // we change the SceneState to GamePlay.
            CurrentState = SceneStates.LoadingFinished;
        }

        /// <inheritdoc />
        /// <summary>
        /// Unloads the scene.
        /// </summary>
        public override void Unload() { }

        /// <inheritdoc />
        /// <summary>
        /// Updates the scene.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            UpdateEntities(pGameTime);
            Systems.Update(pGameTime);
            Data.SortZOrder();
        }

        /// <summary>
        /// Updates the entities and checks for collisions.
        /// </summary>
        /// <param name="pGameTime"></param>
        protected virtual void UpdateEntities(GameTime pGameTime)
        {
            
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the scene.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public override void Draw(SpriteBatch pSpriteBatch)
        {
            Systems.Draw(pSpriteBatch, Data.LstEntities);
        }
    }
}