﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Observer
{
    /// <inheritdoc />
    /// <summary>
    /// Class that receive notifications with sounds to play,
    /// Entities must subscribe to emit sounds.
    /// </summary>
    public class SoundObserver : IObserver
    {
        /// <summary>
        /// The current soundControl.
        /// </summary>
        protected SoundControl.SoundControl MySoundControl { get; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pSControl"></param>
        public SoundObserver(SoundControl.SoundControl pSControl)
        {
            MySoundControl = pSControl;
        }

        /// <summary>
        /// This is the main update method,
        /// it receives an entity as a parameter
        /// </summary>
        /// <param name="pSubject"></param>
        public void Update(ISubject pSubject)
        {
            if (pSubject is IEntity pEntity)
            {
                if (pEntity.Data.SoundToPlay == null)
                    return;
                
                if (pEntity.Data.SoundToPlay.EmitterPos != Vector2.Zero &&
                    pEntity.Data.SoundToPlay.ReceiverPos != Vector2.Zero)
                {
                    MySoundControl.PlaySound(
                        pEntity.Data.SoundToPlay.Id,
                        pEntity.Data.SoundToPlay.EmitterPos,
                        pEntity.Data.SoundToPlay.ReceiverPos);
                }
                else
                {
                    MySoundControl.PlaySound(pEntity.Data.SoundToPlay.Id);
                }
            }
        }
    }
}