﻿#region Usings


#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Loot.Types
{
    public struct LootTableItem
    {
        public string Id;
        public int Qty;
        public int Weight;
        public int RangeMin;
        public int RangeMax;

        public LootTableItem(string pId, int qty, int weight) : this()
        {
            Id = pId;
            Qty = qty;
            Weight = weight;
        }
    }
}