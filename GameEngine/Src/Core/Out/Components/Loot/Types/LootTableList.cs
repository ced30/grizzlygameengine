﻿#region Usings

using System.Collections.Generic;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Loot.Types
{
    public struct LootTableList
    {
        public List<LootTableItem> Items { get; set; }
        public int ItemsWeight { get; set; }
        public int TableWeight { get; set; }

        public void Sort()
        {
            var tempList = new List<LootTableItem>();

            int totalWeight = 0;
            for (int i = 0; i < Items.Count; i++)
            {
                var item = Items[i];
                item.RangeMin = totalWeight + 1;
                item.RangeMax = totalWeight + item.Weight;
                totalWeight += item.Weight;

                tempList.Add(item);
            }

            Items.Clear();
            Items.AddRange(tempList);
            ItemsWeight = totalWeight;
        }
    }
}