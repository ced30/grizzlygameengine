﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Loot.Base;
using GrizzlyGameEngine.Core.Out.Components.Loot.Types;
using GrizzlyGameEngine.Core.Out.Factory.Prefabs;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Loot
{
    public class DefaultLootTable : ABaseLootTable
    {
        public DefaultLootTable()
        {
            Factories = new Dictionary<string, LootFactoryDefault>();
            Tables = new Dictionary<string, LootTableList>();
            PopulateFactories();
            Populate();
        }
    }
}