﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Loot.Types;
using GrizzlyGameEngine.Core.Out.Factory.Prefabs;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Loot.Base
{
    /// <summary>
    /// This class handles the loot system,
    /// it contains item factories and loot tables.
    /// </summary>
    public abstract class ABaseLootTable
    {
        protected Dictionary<string, LootFactoryDefault> Factories { get; set; }
        protected Dictionary<string, LootTableList> Tables { get; set; }
        protected LootFactoryDefault CurrentFactory { get; set; }
        protected LootTableList CurrentTable { get; set; }
        

        /// <summary>
        /// We populate the factories just once, at class initialization
        /// </summary>
        protected void PopulateFactories()
        {
            MyPopulateFactories();
        }
        

        #region Load

        /// <summary>
        /// We populate the tables once when we initialize, then
        /// we repopulate each time we get items from the table.
        /// </summary>
        protected void Populate()
        {
            PrePopulate();

            // this is where you populate your factories / tables.
            MyPopulateTables();

            PostPopulate();
        }

        /// <summary>
        /// This is where you add factories
        /// </summary>
        protected virtual void MyPopulateFactories()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// This is where you add tables
        /// </summary>
        protected virtual void MyPopulateTables()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sorts the tables after they have been populated
        /// </summary>
        protected void PostPopulate()
        {
            foreach (var tableList in Tables)
            {
                tableList.Value.Sort();
            }
        }

        /// <summary>
        /// Clears the tables before population
        /// </summary>
        protected void PrePopulate()
        {
            Tables.Clear();
        }

        
        /// <summary>
        /// This is an example on how your populate functions should look like
        /// </summary>
        private void ExamplePopulate()
        {
            var tableCommon = new LootTableList
            {
                Items = new List<LootTableItem>()
                {
                    // Null
                    new LootTableItem("null", 1, 50),

                    // Weapons
                    new LootTableItem("axe", 1, 2),
                    new LootTableItem("dagger", 1, 2),

                    //Collectables
                    new LootTableItem("gold", World.Instance.R.Next(1, 3), 100),

                    //Consumables
                    new LootTableItem("potion", 1, 30),
                }
            };

            tableCommon.Sort();
            AddList("common", tableCommon);
        }

        #endregion


        /// <summary>
        /// Returns a list of loot items.
        /// </summary>
        /// <param name="pQuality"></param> the quality of the loot.
        /// <param name="qTy"></param> the number of rolls we want.
        /// <returns></returns>
        public List<IEntity> GetLoot(string pQuality, int qTy)
        {
            CurrentTable = Tables[pQuality];
            CurrentFactory = Factories[pQuality];

            // the list of LootItems in the loot table list.
            var lstItems = CurrentTable.Items;

            // the list of loot sprites that we return.
            var objectList = new List<IEntity>();

            // if the roll doesn't correspond to a Null
            for (int i = 0; i < qTy; i++)
            {
                var success = GetItems(lstItems, CurrentTable.ItemsWeight) != null;
                if (success)
                {
                    var items = GetItems(lstItems, CurrentTable.ItemsWeight);

                    if (items != null)
                        objectList.AddRange(items);
                }
            }

            Populate();
            return objectList;
        }


        #region Helper Methods

        /// <summary>
        /// Adds a new list to the global loot tables list.
        /// </summary>
        /// <param name="pQuality"></param>
        /// <param name="pTable"></param>
        protected void AddList(string pQuality, LootTableList pTable)
        {
            Tables.Add(pQuality, pTable);
        }

        /// <summary>
        /// Adds a new list to the global loot tables list.
        /// </summary>
        /// <param name="pQuality"></param>
        /// <param name="pFactory"></param>
        protected void AddFactory(string pQuality, LootFactoryDefault pFactory)
        {
            Factories.Add(pQuality, pFactory);
        }

        /// <summary>
        /// Returns an item from a loot table.
        /// </summary>
        /// <param name="pList"></param>
        /// <param name="pTotalWeight"></param>
        /// <returns></returns>
        protected List<IEntity> GetItems(List<LootTableItem> pList, int pTotalWeight)
        {
            var rnd = World.Instance.R.Next(pTotalWeight);

            for (int i = 0; i < pList.Count; i++)
            {
                var obj = pList[i];
                if (obj.Id == "null")
                    continue;

                if (rnd >= obj.RangeMin &&
                    rnd <= obj.RangeMax)
                {
                    var lstObjects = new List<IEntity>();

                    for (int j = 0; j < obj.Qty; j++)
                    {
                        lstObjects.Add(CurrentFactory.MakeLoot(obj.Id));
                    }

                    return lstObjects;
                }
            }

            return null;
        }

        #endregion
    }
}