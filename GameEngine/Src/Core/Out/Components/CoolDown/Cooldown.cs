﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.CoolDown
{
    /// <summary>
    /// This class is used to act like an alarm,
    /// start it with a timer value and check the
    /// isRunning or isFinished for your game logic.
    /// </summary>
    public class Cooldown
    {
        /// <summary>
        /// Is the coolDown running?
        /// </summary>
        private bool _isRunning;

        /// <summary>
        /// Is the coolDown finished?
        /// </summary>
        private bool _isFinished;

        /// <summary>
        /// Is the timer finished?
        /// </summary>
        /// <returns></returns>
        public bool IsFinished => _isFinished;

        /// <summary>
        /// Is the timer running?
        /// </summary>
        /// <returns></returns>
        public bool IsRunning => _isRunning;

        /// <summary>
        /// The time limit which the timer must reach to finish.
        /// </summary>
        public double GoalTime { get; private set; }

        /// <summary>
        /// The current timer which must reach the GoalTime to finish.
        /// </summary>
        public double Timer { get; private set; }

        /// <summary>
        /// The last timer used to run the coolDown
        /// </summary>
        public float LastTimer { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Cooldown()
        {
            _isRunning = false;
            _isFinished = false;
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <param name="pTimer"></param>
        public void Start(float pTimer)
        {
            GoalTime = pTimer;
            LastTimer = pTimer;
            _isRunning = true;
            _isFinished = false;
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void Stop()
        {
            Timer = 0;
            _isFinished = true;
            _isRunning = false;
        }

        /// <summary>
        /// main update, increments the timer.
        /// </summary>
        /// <param name="pGameTime"></param>
        public void Update(GameTime pGameTime)
        {
            if (_isRunning)
            {
                Timer += pGameTime.ElapsedGameTime.TotalSeconds;

                if (Timer >= GoalTime)
                {
                    Stop();
                }
            }
        }
    }
}