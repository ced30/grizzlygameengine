﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl.Data
{
    public abstract class BaseSoundData
    {
        protected float _ambianceVolume;
        protected float _musicVolume;
        protected float _sfxVolume;

        public Dictionary<string, SoundItem> Ambiances { get; set; }
        public Dictionary<string, SoundItem> Musics { get; set; }
        public Dictionary<string, SoundItem> Sounds { get; set; }
        public double DistanceMax { get; set; }
        public SoundItem Ambiance { get; set; }
        public SoundItem BkgMusic { get; set; }

        public float AmbianceVolume
        {
            get => _ambianceVolume;
            set
            {
                if (value <= 0)
                    _ambianceVolume = 0;
                else if (value >= 100)
                    _ambianceVolume = 100;
                else
                    _ambianceVolume = value;
            }
        }

        public float MusicVolume
        {
            get => _musicVolume;
            set
            {
                if (value <= 0)
                    _musicVolume = 0;
                else if (value >= 100)
                    _musicVolume = 100;
                else
                    _musicVolume = value;
            }
        }

        public float SfxVolume
        {
            get => _sfxVolume;
            set
            {
                if (value <= 0)
                    _sfxVolume = 0;
                else if (value >= 100)
                    _sfxVolume = 100;
                else
                    _sfxVolume = value;
            }
        }

        /// <summary>
        /// Adds a sound to the dictionary, takes a name, a path and a volume as arguments.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pPath"></param>
        /// <param name="pVolume"></param>
        /// <param name="pContent"></param>
        public virtual void AddAmbiance(string pName, string pPath, float pVolume, ContentManager pContent)
        {
            if (!Ambiances.ContainsKey(pName))
                Ambiances.Add(pName, new SoundItem(pName, pPath, pVolume, pContent));
        }

        /// <summary>
        /// Adds a sound to the dictionary, takes a name, a path and a volume as arguments.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pPath"></param>
        /// <param name="pVolume"></param>
        /// <param name="pContent"></param>
        public virtual void AddSfx(string pName, string pPath, float pVolume, ContentManager pContent)
        {
            if (!Sounds.ContainsKey(pName))
                Sounds.Add(pName, new SoundItem(pName, pPath, pVolume, pContent));
        }

        /// <summary>
        /// Adds a sound to the dictionary, takes a name, a path and a volume as arguments.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pPath"></param>
        /// <param name="pVolume"></param>
        /// <param name="pContent"></param>
        public virtual void AddMusic(string pName, string pPath, float pVolume, ContentManager pContent)
        {
            if (!Musics.ContainsKey(pName))
                Musics.Add(pName, new SoundItem(pName, pPath, pVolume, pContent));
        }

        public void Populate(ContentManager pContent)
        {
            PopulateAmbiance(pContent);
            PopulateMusics(pContent);
            PopulateSfx(pContent);
        }

        protected virtual void PopulateAmbiance(ContentManager pContent)
        {

        }

        protected virtual void PopulateMusics(ContentManager pContent)
        {

        }

        protected virtual void PopulateSfx(ContentManager pContent)
        {

        }
    }
}