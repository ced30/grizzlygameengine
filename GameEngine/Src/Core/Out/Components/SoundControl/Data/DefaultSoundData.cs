﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl.Data
{
    public class DefaultSoundData : BaseSoundData
    {
        public DefaultSoundData()
        {
            DistanceMax = Screen.Screen.Width;
            AmbianceVolume = 100;
            MusicVolume = 100;
            SfxVolume = 100;
            Ambiances = new Dictionary<string, SoundItem>();
            Musics = new Dictionary<string, SoundItem>();
            Sounds = new Dictionary<string, SoundItem>();
        }
    }
}