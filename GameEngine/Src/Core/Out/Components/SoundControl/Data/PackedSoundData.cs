﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl.Data
{
    public class PackedSoundData
    {
        public PackedSoundData()
        {
        }

        public PackedSoundData(string id) : this()
        {
            Id = id;
            EmitterPos = Vector2.Zero;
            ReceiverPos = Vector2.Zero;
        }

        public PackedSoundData(string id, Vector2 emitterPos, Vector2 receiverPos) : this()
        {
            Id = id;
            EmitterPos = emitterPos;
            ReceiverPos = receiverPos;
        }

        public string Id { get; set; }

        public Vector2 EmitterPos { get; set; }

        public Vector2 ReceiverPos { get; set; }
    }
}