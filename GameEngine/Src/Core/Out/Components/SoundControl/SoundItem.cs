﻿#region Usings

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl
{
    public class SoundItem
    {
        public float Volume;
        public string Name;
        public SoundEffect Sound;
        public SoundEffectInstance Instance;

        public SoundItem(string pName, string pPath ,float pVolume, ContentManager pContent)
        {
            Name = pName;
            Volume = pVolume;
            Sound = pContent.Load<SoundEffect>(pPath);
            CreateInstance();
        }

        public void CreateInstance()
        {
            Instance = Sound.CreateInstance();
        }
    }
}