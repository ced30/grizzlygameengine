﻿#region Usings

using System;
using System.Diagnostics;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Components.Observer;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl
{
    public class SoundControl
    {
        #region Declarations

        private Cooldown Cooldown { get; set; }
        private float CdTimer { get; set; }
        private static double Dist(Vector2 pPos1, Vector2 pPos2)
        {
            return Vector2.Distance(pPos2, pPos1);
        }
        private string LastSoundPlayed { get; set; }

        public BaseSoundData Data { get; set; }
        public SoundObserver Observer { get; set; }

        #endregion


        /// <summary>
        /// Constructor, takes a music path as argument and starts playing it.
        /// </summary>
        public SoundControl(ContentManager pContent, BaseSoundData pData)
        {
            Data = pData;
            Data.Populate(pContent);
            Cooldown = new Cooldown();
            CdTimer = 0.16f;

            Observer = new SoundObserver(this);
        }

        /// <summary>
        /// Adjusts the music's volume, takes a percentage as argument.
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pPercent"></param>
        public virtual void AdjustVolume(SoundType pType, float pPercent)
        {
            if (pType == SoundType.Ambiance)
            {
                if (Data.Ambiance.Instance != null)
                {
                    Data.Ambiance.Instance.Volume = pPercent * Data.Ambiance.Volume * 0.1f;
                }
            }
            else if (pType == SoundType.Music)
            {
                if (Data.BkgMusic.Instance != null)
                {
                    Data.BkgMusic.Instance.Volume = pPercent * Data.BkgMusic.Volume * 0.1f;
                }
            }
        }

        public void PlayAmbiance(string pId, float pVolume)
        {
            Data.Ambiance = Data.Ambiances[pId];
            Data.Ambiance.CreateInstance();

            float ambianceVolumePercent = 1.0f;
            if ((int)Data.AmbianceVolume != 0)
            {
                ambianceVolumePercent = (pVolume / (float)Convert.ToDecimal(Data.AmbianceVolume) * 10);
            }

            AdjustVolume(SoundType.Ambiance, ambianceVolumePercent);
            Data.Ambiance.Instance.IsLooped = true;
            Data.Ambiance.Instance.Play();
        }

        public void PlayMusic(string pId, float pVolume)
        {
            Data.BkgMusic = Data.Musics[pId];
            Data.BkgMusic.CreateInstance();

            float musicVolumePercent = 1.0f;
            if ((int)Data.MusicVolume != 0)
            {
                musicVolumePercent = (pVolume / (float)Convert.ToDecimal(Data.MusicVolume) * 10);
            }

            AdjustVolume(SoundType.Music, musicVolumePercent);
            Data.BkgMusic.Instance.IsLooped = true;
            Data.BkgMusic.Instance.Play();
        }

        private bool IsReady(string pId)
        {
            if (pId.Equals(LastSoundPlayed))
            {
                if (!Cooldown.IsRunning)
                {
                    Cooldown.Start(CdTimer);
                    return true;
                }

                return false;
            }
            else
            {
                return !Cooldown.IsRunning;
            }

        }

        /// <summary>
        /// Takes the sound name as argument.
        /// </summary>
        /// <param name="pId"></param>
        public virtual void PlaySound(string pId)
        {
            if(pId == "")
                return;

            if (Data.Sounds.ContainsKey(pId))
            {
                if (IsReady(pId))
                {
                    var sound = Data.Sounds[pId];
                    sound.CreateInstance();
                    RunSound(pId, sound.Sound, sound.Instance, sound.Volume);
                    LastSoundPlayed = pId;
                }
            }
            else
            {
                Debug.Print("GrizzlyGameEngine.Core.Out.Components.SoundControl => Sound not found, ID: " + pId);
            }
        }

        /// <summary>
        /// 2 positions as arguments, doesn't play sound if they are too far from each other,
        /// adjusts the volume according to the distance.
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pEmitterPos"></param>
        /// <param name="pReceiverPos"></param>
        public virtual void PlaySound(string pId, Vector2 pEmitterPos, Vector2 pReceiverPos)
        {
            double distToReceiver = Dist(pReceiverPos, pEmitterPos);
            if (distToReceiver > Data.DistanceMax)
                return;

            if (Data.Sounds.ContainsKey(pId))
            {
                if (IsReady(pId))
                {
                    var sound = Data.Sounds[pId];
                    double volumeRatio = distToReceiver / Data.DistanceMax;
                    float adjustedVolume = (float)(sound.Volume - sound.Volume * volumeRatio);

                    sound.CreateInstance();
                    RunSound(pId, sound.Sound, sound.Instance, adjustedVolume);
                    LastSoundPlayed = pId;
                }
            }
            else
            {
                Debug.Print("Sound not found: " + pId);
            }
        }

        /// <summary>
        /// Actually plays the sound.
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="pSound"></param>
        /// <param name="pInstance"></param>
        /// <param name="pVolume"></param>
        public virtual void RunSound(string pName, SoundEffect pSound, SoundEffectInstance pInstance, float pVolume)
        {
            float soundVolumePercent = 1.0f;
            if ((int)Data.MusicVolume != 0)
            {
                soundVolumePercent = (float)Convert.ToDecimal(Data.MusicVolume) / 30f;
            }

            pInstance.Volume = soundVolumePercent * pVolume;
            pInstance.Play();
        }

        public void Update(GameTime pGameTime)
        {
            Cooldown.Update(pGameTime);
        }
    }
}