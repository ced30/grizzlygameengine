﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl.Objects
{
    public class SoundRepeater : GrizzlyComponent
    {
        protected Cooldown RepeatCd { get; set; }
        protected float RepeatTimer { get; set; }
        protected State CurrentState { get; set; }
        protected bool IsStopped { get; set; }
        public string SoundId { get; set; }

        public SoundRepeater(string pName, float pTimer)
        {
            SoundId = pName;
            RepeatTimer = pTimer;
            CurrentState = State.Stopped;
            RepeatCd = new Cooldown();
        }

        protected void StatesLogic()
        {
            switch (CurrentState)
            {
                case State.Running:
                    if (!RepeatCd.IsRunning)
                    {
                        if (!IsStopped)
                        {
                            RepeatCd.Start(RepeatTimer);
                        }
                    }
                    break;

                case State.Stopped:
                    break;
            }
        }

        public void New(string pName, float pTimer)
        {
            SoundId = pName;
            RepeatTimer = pTimer;
            CurrentState = State.Running;
            IsStopped = false;
            RepeatCd = new Cooldown();
        }

        public void Start()
        {
            CurrentState = State.Running;
            IsStopped = false;
        }

        public void Stop()
        {
            CurrentState = State.Stopped;
            IsStopped = true;
        }

        public override void Update(GameTime pGameTime)
        {
            StatesLogic();
            CoolDowns(pGameTime);
            base.Update(pGameTime);
        }

        protected void CoolDowns(GameTime pGameTime)
        {
            RepeatCd.Update(pGameTime);
        }

        public enum State
        {
            Running,
            Stopped
        }
    }
}