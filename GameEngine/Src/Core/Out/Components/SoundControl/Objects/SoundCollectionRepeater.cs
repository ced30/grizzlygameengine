﻿using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Components.SoundControl.Objects
{
    public class SoundCollectionRepeater : GrizzlyComponent
    {
        private int SoundIndex { get; set; }
        private bool IsStopped { get; set; }
        private float RepeatTimer { get; set; }
        private List<string> Sounds { get; set; }
        private State CurrentState { get; set; }

        public Cooldown RepeatCd { get; set; }
        public string CurrentSoundId { get; set; }

        public SoundCollectionRepeater(List<string> pSounds, float pTimer)
        {
            Sounds = pSounds;
            RepeatTimer = pTimer;
            CurrentState = State.Stopped;
            RepeatCd = new Cooldown();
        }

        protected void StatesLogic()
        {
            switch (CurrentState)
            {
                case State.Running:
                    if (!RepeatCd.IsRunning)
                    {
                        if (!IsStopped)
                        {
                            CurrentSoundId = Sounds[SoundIndex];
                            SoundIndex++;
                            if (SoundIndex > Sounds.Count - 1)
                            {
                                SoundIndex = 0;
                            }
                            RepeatCd.Start(RepeatTimer);
                        }
                    }
                    break;

                case State.Stopped:
                    CurrentSoundId = "";
                    break;

                default:
                    CurrentSoundId = "";
                    break;
            }
        }

        public void New(List<string> pSounds, float pTimer)
        {
            CurrentState = State.Running;
            IsStopped = false;
            RepeatCd = new Cooldown();
            RepeatTimer = pTimer;
            Sounds = pSounds;
        }

        public void Start()
        {
            CurrentState = State.Running;
            IsStopped = false;
        }

        public void Stop()
        {
            CurrentState = State.Stopped;
            IsStopped = true;
        }

        public void Restart()
        {
            CurrentState = State.Running;
            IsStopped = false;
            RepeatCd = new Cooldown();
            SoundIndex = 0;
        }

        public override void Update(GameTime pGameTime)
        {
            StatesLogic();
            CoolDowns(pGameTime);
            base.Update(pGameTime);
        }

        protected void CoolDowns(GameTime pGameTime)
        {
            RepeatCd.Update(pGameTime);
        }

        public enum State
        {
            Running,
            Stopped
        }
    }
}