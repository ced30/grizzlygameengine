﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Colors
{
    /// <inheritdoc />
    /// <summary>
    /// This class is used to have control over colors and transparency,
    /// you can use it to make anything fade in / out, or blink for example.
    /// Use the color property of this class anywhere you need a color.
    /// </summary>
    public class Colors : GrizzlyComponent
    {
        #region Declarations

        /// <summary>
        /// The base color used in the alpha computation.
        /// </summary>
        private Color BaseColor { get; set; }

        /// <summary>
        /// The coolDown used to time the blink effect
        /// </summary>
        private Cooldown BlinkCd { get; set; }

        /// <summary>
        /// Maximum alpha value, can be 0.1 to 1;
        /// </summary>
        private float AlphaMax { get; set; }

        /// <summary>
        /// The minimum alpha value, can be 0.9 to 0.
        /// </summary>
        private float AlphaMin { get; set; }

        /// <summary>
        /// Alpha decrement value.
        /// </summary>
        private float AlphaStepMinus { get; set; }

        /// <summary>
        /// Alpha increment value.
        /// </summary>
        private float AlphaStepPlus { get; set; }

        /// <summary>
        /// The timer of the blink effect.
        /// </summary>
        private float BlinkTimer { get; set; }

        /// <summary>
        /// The current value of alpha.
        /// </summary>
        private float _currentAlpha;

        /// <summary>
        /// The current Opacity state.
        /// </summary>
        private OpacityStates CurrentOpacityState { get; set; }

        /// <summary>
        /// The output color with processed alpha that you use wherever you want.
        /// </summary>
        public Color Color => BaseColor * Alpha;

        /// <summary>
        /// The output Alpha value.
        /// </summary>
        public float Alpha
        {
            get => _currentAlpha;
            set => _currentAlpha = MathHelper.Clamp(value, AlphaMin, AlphaMax);
        }

        /// <summary>
        /// Is the color blinking?
        /// </summary>
        public bool IsBlinking { get; set; }

        /// <summary>
        /// Is the color set to blink indefinitely?
        /// </summary>
        public bool IsBlinkingPermanent { get; set; }

        /// <summary>
        /// Is the color visible?
        /// </summary>
        public bool IsVisible => (CurrentFadeState == FadeStates.In &&
                                  Alpha > AlphaMin) ||
                                 CurrentFadeState == FadeStates.Out;

        /// <summary>
        /// Is the fade finished?
        /// </summary>
        public bool IsFinished => (CurrentFadeState == FadeStates.In &&
                                   Alpha <= AlphaMin) ||
                                  CurrentFadeState == FadeStates.Out &&
                                  Alpha >= AlphaMax;

        /// <summary>
        /// The current fade state.
        /// </summary>
        public FadeStates CurrentFadeState;

        #endregion


        /// <summary>
        /// We initialize with no-fade state,
        /// opaque state and alpha = 1.
        /// </summary>
        public Colors()
        {
            BlinkCd = new Cooldown();
            BaseColor = Color.White;
            CurrentFadeState = FadeStates.None;
            SetOpacity(OpacityStates.Opaque);
            AlphaMax = 1f;
            Alpha = 1f;
            AlphaStepMinus = 0.05f;
            AlphaStepPlus = 0.05f;
        }

        /// <inheritdoc />
        /// <summary>
        ///  Main update, updates the alpha depending of the current state.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            BlinkClamp();

            // Depending on the opacity state, we change
            // the AlphaMax value with preset values.
            switch (CurrentOpacityState)
            {
                case OpacityStates.SemiOpaque:
                    AlphaMax = 0.75f;
                    break;

                case OpacityStates.Opaque:
                    AlphaMax = 1;
                    break;

                case OpacityStates.SemiTransparent:
                    AlphaMax = 0.5f;
                    break;

                case OpacityStates.Transparent:
                    AlphaMax = 0;
                    break;

                case OpacityStates.BarelyOpaque:
                    AlphaMax = 0.25f;
                    break;
            }

            // Depending on current fade state, we compute the alpha.
            switch (CurrentFadeState)
            {
                case FadeStates.None:
                    break;

                case FadeStates.In:
                    if (Alpha > AlphaMax)
                    {
                        Alpha -= AlphaStepMinus;

                        // If the fade is finished, we set the state to None.
                        if (Alpha <= AlphaMax)
                        {
                            if (!IsBlinking)
                                CurrentFadeState = FadeStates.None;
                        }
                    }
                    break;

                case FadeStates.Out:
                    if (Alpha < AlphaMax)
                    {
                        Alpha += AlphaStepPlus;

                        // If the fade is finished, we set the state to None.
                        if (Alpha >= AlphaMax)
                        {
                            if (!IsBlinking)
                                CurrentFadeState = FadeStates.None;
                        }
                    }
                    break;
            }

            // If IsBlinking, we update the blink coolDown.
            if (IsBlinking)
                BlinkCd.Update(pGameTime);
        }


        #region Checks

        /// <summary>
        /// Stops the blinking if the coolDown is finished
        /// and sets the base color to white.
        /// </summary>
        private void BlinkClamp()
        {
            if (IsBlinking ||
                IsBlinkingPermanent)
            {
                if (Alpha <= AlphaMin)
                    Fade(FadeStates.Out, OpacityStates.Opaque);

                if (Alpha >= AlphaMax)
                    Fade(FadeStates.In, OpacityStates.Opaque);
            }

            if (BlinkCd.IsFinished)
            {
                IsBlinking = false;
                BaseColor = Color.White;
                Fade(FadeStates.Out, OpacityStates.Opaque);
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Blink for pTimer seconds.
        /// </summary>
        /// <param name="pTimer"></param>
        public void Blink(float pTimer)
        {
            IsBlinking = true;
            BlinkCd.Start(pTimer);
        }

        /// <summary>
        /// Toggle blinking with pBlink.
        /// </summary>
        /// <param name="pBlink"></param>
        public void Blink(bool pBlink)
        {
            IsBlinkingPermanent = pBlink;
        }

        /// <summary>
        /// Blink for pTimer seconds and be pColor color.
        /// </summary>
        /// <param name="pTimer"></param>
        /// <param name="pColor"></param>
        public void Blink(float pTimer, Color pColor)
        {
            IsBlinking = true;
            BaseColor = pColor;
            BlinkCd.Start(pTimer);
        }

        /// <summary>
        /// Fade in or out depending on pMotion, alpha's max value depends on pTransparency.
        /// </summary>
        /// <param name="pMotion"></param>
        /// <param name="pTransparency"></param>
        public void Fade(FadeStates pMotion, OpacityStates pTransparency)
        {
            SetOpacity(pTransparency);
            SetMotion(pMotion);
        }

        /// <summary>
        /// Sets alpha to pAlpha.
        /// </summary>
        /// <param name="pAlpha"></param>
        public void SetAlpha(float pAlpha)
        {
            Alpha = pAlpha;
        }

        /// <summary>
        /// Set the base-color to pColor.
        /// </summary>
        /// <param name="pColor"></param>
        public void SetColor(Color pColor)
        {
            BaseColor = pColor;
        }

        /// <summary>
        /// The increment for fading (how fast the fade will be, 0.02f is fast enough).
        /// </summary>
        /// <param name="pStep"></param>
        public void SetStep(float pStep)
        {
            AlphaStepPlus = pStep;
            AlphaStepMinus = pStep;
        }

        /// <summary>
        /// Sets the opacity (alpha max).
        /// </summary>
        /// <param name="pTransparency"></param>
        public void SetOpacity(OpacityStates pTransparency)
        {
            CurrentOpacityState = pTransparency;
        }

        /// <summary>
        /// Sets the motion, in or out.
        /// </summary>
        /// <param name="pMotion"></param>
        public void SetMotion(FadeStates pMotion)
        {
            CurrentFadeState = pMotion;
        }

        #endregion

        /// <summary>
        /// The fade states.
        /// </summary>
        public enum FadeStates
        {
            None,
            In,
            Out,
        }

        /// <summary>
        /// The opacity states.
        /// </summary>
        public enum OpacityStates
        {
            SemiOpaque,
            Opaque,
            SemiTransparent,
            BarelyOpaque,
            Transparent,
        }
    }
}