﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.ParticleEmitter
{
    /// <inheritdoc />
    /// <summary>
    /// This particle emitter spawns particles via a delegate triggered from the entities
    /// </summary>
    public class ParticleEmitterCustom : ParticlesEmitter
    {
        protected override void ApplyGlobalVelocity()
        {
            throw new System.NotImplementedException();
        }

        protected override Particle MakeParticle()
        {
            throw new System.NotImplementedException();
        }

        public override void Update(GameTime pGameTime)
        {
            Dt = (float)pGameTime.ElapsedGameTime.TotalSeconds;
            UpdateTimer();

            Id = "custom";

            UpdateParticles(pGameTime);
        }
    }
}