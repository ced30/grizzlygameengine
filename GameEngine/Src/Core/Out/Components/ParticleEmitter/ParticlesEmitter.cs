﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.ParticleEmitter
{
    /// <inheritdoc />
    /// <summary>
    /// This class manages particles which are stored in the list of particles.
    /// </summary>
    public abstract class ParticlesEmitter : GrizzlyComponent
    {
        /// <summary>
        /// Delta-time.
        /// </summary>
        public float Dt { get; set; }

        /// <summary>
        /// Timer for the generation coolDown.
        /// </summary>
        private float GenerateTimer { get; set; }

        /// <summary>
        /// Timer for the global velocity change to the particles.
        /// </summary>
        private float GlobalVelocityTimer { get; set; }

        /// <summary>
        /// List of particles.
        /// </summary>
        protected List<Particle> LstParticles { get; set; }

        /// <summary>
        /// The max number of particles that can be spawned.
        /// </summary>
        public int MaxParticles { get; set; }

        /// <summary>
        /// How often a particle is generated.
        /// </summary>
        public float GenerateSpeed { get; set; }

        /// <summary>
        /// How often we apply GlobalVelocity to our particles.
        /// </summary>
        public float GlobalVelocitySpeed { get; set; }

        /// <summary>
        /// identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        protected ParticlesEmitter()
        {
            GenerateSpeed = 0.005f;
            GlobalVelocitySpeed = 1;
            LstParticles = new List<Particle>();
            MaxParticles = 50;
        }

        /// <summary>
        /// Removes the particle from the list if it has the "ToRemove" flag.
        /// </summary>
        /// <param name="pParticle"></param>
        protected void CleanUp(Particle pParticle)
        {
            if (pParticle.Data.Conditions.ToRemove)
                LstParticles.Remove(pParticle);
        }

        /// <summary>
        /// Adds a particle to the list when the timer is finished.
        /// </summary>
        protected void AddParticle()
        {
            if (GenerateTimer > GenerateSpeed)
            {
                GenerateTimer = 0;

                if (LstParticles.Count < MaxParticles)
                    LstParticles.Add(MakeParticle());

            }
        }

        /// <summary>
        /// Adds a particle to the list when the timer is finished.
        /// </summary>
        public virtual void AddParticle(Particle pParticle)
        {
            LstParticles.Add(pParticle);
        }

        /// <summary>
        /// Returns a particle.
        /// </summary>
        /// <returns></returns>
        protected abstract Particle MakeParticle();

        /// <inheritdoc />
        /// <summary>
        /// Main update method, changes the global velocity
        /// on coolDown and updates the particles.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            Dt = (float)pGameTime.ElapsedGameTime.TotalSeconds;
            UpdateTimer();
            AddParticle();

            if (GlobalVelocityTimer > GlobalVelocitySpeed)
            {
                GlobalVelocityTimer = 0;

                ApplyGlobalVelocity();
            }

            UpdateParticles(pGameTime);
        }

        /// <summary>
        /// Apply global velocity changes.
        /// </summary>
        protected abstract void ApplyGlobalVelocity();

        /// <summary>
        /// Updates the generation coolDown.
        /// </summary>
        protected void UpdateTimer()
        {
            GenerateTimer += Dt;
            GlobalVelocityTimer += Dt;
        }

        /// <summary>
        /// Updates the particles from the list.
        /// </summary>
        /// <param name="pGameTime"></param>
        protected void UpdateParticles(GameTime pGameTime)
        {
            if (LstParticles.Count <= 0)
                return;

            for (var i = LstParticles.Count - 1; i >= 0; i--)
            {
                LstParticles[i].Update(pGameTime, null, null);
                CleanUp(LstParticles[i]);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the particles from the list.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public override void Draw(SpriteBatch pSpriteBatch)
        {
            if (LstParticles.Count <= 0)
                return;

            for (var i = LstParticles.Count - 1; i >= 0; i--)
            {
                LstParticles[i].Draw(pSpriteBatch);
            }

            base.Draw(pSpriteBatch);
        }
    }
}