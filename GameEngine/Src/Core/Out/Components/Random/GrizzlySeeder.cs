﻿namespace GrizzlyGameEngine.Core.Out.Components.Random
{
    public class GrizzlySeeder
    {
        private int Seed { get; set; }
        public GrizzlySeeder(string pSeed)
        {
            SetNew(pSeed);
        }

        private static int GetHash(string pSeed)
        {
            return pSeed.GetHashCode();
        }

        public int Get()
        {
            return Seed;
        }

        public void SetNew(string pNewSeed)
        {
            Seed = GetHash(pNewSeed);
        }
    }
}