﻿#region Usings

using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.FadeEffect
{
    /// <summary>
    /// This class is used to make
    /// </summary>
    public class FadeEffect : GrizzlyComponent
    {

        #region Declarations

        /// <summary>
        /// The camera used to set the fade position.
        /// </summary>
        protected Camera.Camera Camera { get; }

        /// <summary>
        /// The padding we set to make sure there are no
        /// visible pixels on the edges.
        /// </summary>
        protected const int Padding = 32;

        /// <summary>
        /// the rectangle containing the position + width && weight to draw the
        /// texture at the desired dimensions.
        /// </summary>
        protected Rectangle BoundingBox => new Rectangle(
            (int) Position.X - Padding, (int) Position.Y - Padding, 
            Screen.Screen.Width + Padding * 2, Screen.Screen.Height + Padding * 2);

        /// <summary>
        /// The position of the texture.
        /// </summary>
        protected Vector2 Position => new Vector2(
            _position.X + Camera.Position.X,
            _position.Y + Camera.Position.Y);

        /// <summary>
        /// The colors component used fo the alpha transitions.
        /// </summary>
        protected Colors.Colors Colors { get; set; }

        /// <summary>
        /// The angle of the fade effect.
        /// </summary>
        private float Angle { get; set; }

        /// <summary>
        /// The position of the fade effect.
        /// </summary>
        private Vector2 _position { get; }

        /// <summary>
        /// The texture of the fade effect.
        /// </summary>
        private Texture2D Texture { get; set; }

        /// <summary>
        /// The FadeState of the fade effect.
        /// </summary>
        private FadeStates CurrentFadeState { get; set; }

        /// <summary>
        /// Is the fadeEffect running?
        /// </summary>
        public bool IsRunning => Colors.IsVisible;

        /// <summary>
        /// Is the fadeEffect Finished?
        /// </summary>
        public bool IsFinished => Colors.IsFinished;

        #endregion


        /// <summary>
        /// Takes a shape as argument.
        /// </summary>
        /// <param name="pTexture"></param>
        /// <param name="pCamera"></param>
        public FadeEffect(Texture2D pTexture, Camera.Camera pCamera)
        {
            Angle = 0;
            _position = Vector2.Zero;
            Texture = pTexture;
            CurrentFadeState = FadeStates.None;

            Camera = pCamera;

            Colors = new Colors.Colors();
            Colors.SetStep(0.02f);
        }


        #region Helper Methods

        /// <summary>
        /// Starts fading in or out.
        /// </summary>
        /// <param name="pType"></param>
        public void Start(FadeStates pType)
        {
            if (Texture == null)
                return;

            CurrentFadeState = pType;

            switch (CurrentFadeState)
            {
                case FadeStates.FadeIn:
                    Colors.Fade(Components.Colors.Colors.FadeStates.In, 
                        Components.Colors.Colors.OpacityStates.Transparent);
                    break;
                case FadeStates.FadeOut:
                    Colors.Fade(Components.Colors.Colors.FadeStates.Out, 
                        Components.Colors.Colors.OpacityStates.Opaque);
                    break;
                case FadeStates.Inventory:
                    Colors.Fade(Components.Colors.Colors.FadeStates.Out, 
                        Components.Colors.Colors.OpacityStates.SemiTransparent);
                    break;
                case FadeStates.LevelFinished:
                    Colors.Fade(Components.Colors.Colors.FadeStates.Out,
                        Components.Colors.Colors.OpacityStates.SemiOpaque);
                    break;
                case FadeStates.GamePlay:
                    Colors.Fade(Components.Colors.Colors.FadeStates.In, 
                        Components.Colors.Colors.OpacityStates.Opaque);
                    break;
            }
        }

        /// <summary>
        /// Sets the alpha value directly.
        /// </summary>
        /// <param name="pFloat"></param>
        public void SetAlpha(float pFloat)
        {
            Colors.SetAlpha(pFloat);
        }

        /// <summary>
        /// Unloads the class.
        /// </summary>
        public override void Unload()
        {
            Texture = null;
            Debug.Print("Unloaded FadeEffectTexture");
        }

        #endregion

        
        /// <summary>
        /// Updates the colors for alpha computations.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            Colors.Update(pGameTime);
        }

        /// <summary>
        /// Draws the fade texture.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public override void Draw(SpriteBatch pSpriteBatch)
        {
            if(!Colors.IsVisible)
                return;

            pSpriteBatch.Draw(Texture, Position, BoundingBox, Colors.Color, Angle, Vector2.Zero, 1, SpriteEffects.None, 1.0f);

            base.Draw(pSpriteBatch);
        }

        public enum FadeStates
        {
            None,
            FadeIn,
            FadeOut,
            Inventory,
            LevelFinished,
            GamePlay
        }

        public enum DurationStates
        {
            Short,
            Medium,
            Long
        }
    }
}