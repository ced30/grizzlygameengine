﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.QuadTree
{
    /// <summary>
    /// This class sorts the entities of the main list,
    /// builds quadTree and returns list of entities in range.
    /// </summary>
    public class QuadTreeEntityProcessor
    {
        /// <summary>
        /// Number of divisions of a quadTree rectangle.
        /// </summary>
        private int Capacity { get; }

        /// <summary>
        /// List of entities present in a specific range.
        /// </summary>
        public List<IEntity> EntitiesInRange { get; set; }

        /// <summary>
        /// Main QuadTree.
        /// </summary>
        public QuadTreeIEntities QuadTree { get; set; }

        /// <summary>
        /// Constructor, takes a capacity as argument.
        /// </summary>
        /// <param name="pCapacity"></param>
        public QuadTreeEntityProcessor(int pCapacity)
        {
            Capacity = pCapacity;
        }

        /// <summary>
        /// Makes a new quadTree from a tmxMap and a list of entities.
        /// </summary>
        /// <param name="pMapHeightInPixels"></param>
        /// <param name="pEntities"></param>
        /// <param name="pMapWidthInPixels"></param>
        private void MakeNewTree(int pMapWidthInPixels, int pMapHeightInPixels, List<IEntity> pEntities)
        {
            QuadTree = new QuadTreeIEntities(
                new Rectangle(
                    0,
                    0,
                    pMapWidthInPixels,
                    pMapHeightInPixels),
                Capacity);

            for (int i = 0; i < pEntities.Count; i++)
            {
                QuadTree.Insert(pEntities[i]);
            }
        }

        /// <summary>
        /// Returns a list of all entities in a certain (rectangle) range,
        /// used to determine which entities will be drawn / updated.
        /// </summary>
        /// <param name="pRect"></param>
        private void GetEntitiesInRange(Rectangle pRect)
        {
            EntitiesInRange = QuadTree.Query(pRect);
        }

        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pMapWidthInPixels"></param>
        /// <param name="pMapHeightInPixels"></param>
        /// <param name="pEntities"></param>
        /// <param name="pRect"></param>
        public void UpdateTree(int pMapWidthInPixels, int pMapHeightInPixels, List<IEntity> pEntities, Rectangle pRect)
        {
            MakeNewTree(pMapWidthInPixels, pMapHeightInPixels, pEntities);
            GetEntitiesInRange(pRect);
        }
    }
}