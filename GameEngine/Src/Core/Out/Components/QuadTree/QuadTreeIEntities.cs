﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.QuadTree
{
    public class QuadTreeIEntities
    {
        private bool CanDivide => Rectangle.Width > MinSize &&
                                  Rectangle.Height > MinSize;

        private bool IsDivided { get; set; }
        private int Capacity { get; }
        private int MinSize { get; }
        public Rectangle Rectangle { get; }

        public List<IEntity> Entities { get; set; }
        public List<Point> Points { get; set; }
        public QuadTreeIEntities[] SubQuads { get; set; }

        public QuadTreeIEntities(Rectangle pRectangle, int pCapacity)
        {
            Rectangle = pRectangle;
            Capacity = pCapacity;
            Points = new List<Point>();
            Entities = new List<IEntity>();
            MinSize = 16;
        }

        private bool Contains(Rectangle pRect, Point pPoint)
        {
            return pRect.Contains(pPoint.X, pPoint.Y);

            //            return (pPoint.X >= pRect.Left &&
            //                    pPoint.X <= pRect.Right &&
            //                    pPoint.Y >= pRect.Top &&
            //                    pPoint.Y <= pRect.Bottom);
        }

        private void Divide()
        {
            SubQuads = new[]
            {
                // NorthWest
                new QuadTreeIEntities(
                    new Rectangle(
                        Rectangle.X,
                        Rectangle.Y,
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity), 

                // NorthEast
                new QuadTreeIEntities(
                    new Rectangle(
                        Rectangle.X + (int)(Rectangle.Width / 2f),
                        Rectangle.Y,
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),

                // SouthWest
                new QuadTreeIEntities(
                    new Rectangle(
                        Rectangle.X,
                        Rectangle.Y + (int)(Rectangle.Height / 2f),
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),

                // SouthEast
                new QuadTreeIEntities(
                    new Rectangle(
                        Rectangle.X + (int)(Rectangle.Width / 2f),
                        Rectangle.Y + (int)(Rectangle.Height / 2f),
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),
            };

            IsDivided = true;
        }

        public void Insert(IEntity pEntity)
        {
            Point entityCenter = new Point(
                (int) pEntity.Data.Center.X,
                (int) pEntity.Data.Center.Y);

            // If the point is outside the rectangle,
            // we don't process it.
            if (!Rectangle.Contains(entityCenter))
                return;

            if (!IsDivided)
            {
                if (Points.Count >= Capacity &&
                    CanDivide)
                {
                    Divide();

                    SplitExistingPairs();

                    NullMainLists();

                    SortExcessPair(entityCenter, pEntity);
                }
                else
                {
                    Points.Add(entityCenter);
                    Entities.Add(pEntity);
                }
            }
            else
            {
                SortExcessPair(entityCenter, pEntity);
            }
        }

        private void NullMainLists()
        {
            Points = null;
            Entities = null;
        }

        public List<IEntity> Query(Rectangle pRange)
        {
            if (IsDivided)
            {
                if (Points != null)
                    if (Points.Count > 0)
                        throw new Exception(
                            "The number of points excesses the capacity (the quadTree is divided)");
            }

            List<IEntity> found = new List<IEntity>();

            if (Rectangle.Intersects(pRange))
            {
                if (!IsDivided)
                {
                    for (int i = 0; i < Entities.Count; i++)
                    {
                        found.Add(Entities[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < Capacity; i++)
                    {
                        found.AddRange(SubQuads[i].Query(pRange));
                    }
                }
            }

            return found;
        }

        private void SplitExistingPairs()
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                SortExcessPair(Points[i], Entities[i]);
            }
        }

        private void SortExcessPair(Point pPoint, IEntity pEntity)
        {
            for (int i = 0; i < SubQuads.Length; i++)
            {
                if (Contains(SubQuads[i].Rectangle, pPoint))
                {
                    SubQuads[i].Insert(pEntity);
                    return;
                }
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            // Draw rectangle
            Primitives2D.Primitives2D.DrawRectangle(
                pSpriteBatch,
                Rectangle,
                Color.White,
                1);

            // Draw points
            if (Points != null)
                for (int i = 0; i < Points.Count; i++)
                {
                    Primitives2D.Primitives2D.DrawCircle(
                        pSpriteBatch,
                        new Vector2(Points[i].X, Points[i].Y),
                        1,
                        Color.Blue,
                        1);
                }

            // Draw subDivision.
            if (IsDivided)
            {
                for (int i = 0; i < Capacity; i++)
                {
                    var subDiv = SubQuads[i];

                    subDiv.Draw(pSpriteBatch);
                }
            }
        }
    }
}