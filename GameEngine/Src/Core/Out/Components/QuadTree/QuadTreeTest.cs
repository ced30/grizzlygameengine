﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.QuadTree
{
    public class QuadTreeTest
    {
        private bool IsDivided { get; set; }
        public int Index { get; set; }
        private int Capacity { get; }

        public IEntity[] Entities { get; set; }
        public Point[] Points { get; set; }
        public QuadTreeTest[] SubQuads { get; set; }
        public Rectangle Rectangle { get; }


        public QuadTreeTest(Rectangle pRectangle, int pCapacity)
        {
            Rectangle = pRectangle;
            Capacity = pCapacity;
            Points = new Point[Capacity];
            Entities = new IEntity[Capacity];
        }

        private bool Contains(Rectangle pRect, Point pPoint)
        {
            return pRect.Contains(pPoint);
        }

        private void Divide()
        {
            SubQuads = new[]
        {
                // NorthWest
                new QuadTreeTest(
                    new Rectangle(
                        Rectangle.X,
                        Rectangle.Y,
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),

                // NorthEast
                new QuadTreeTest(
                    new Rectangle(
                        Rectangle.X + (int)(Rectangle.Width / 2f),
                        Rectangle.Y,
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),

                // SouthWest
                new QuadTreeTest(
                    new Rectangle(
                        Rectangle.X,
                        Rectangle.Y + (int)(Rectangle.Height / 2f),
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),

                // SouthEast
                new QuadTreeTest(
                    new Rectangle(
                        Rectangle.X + (int)(Rectangle.Width / 2f),
                        Rectangle.Y + (int)(Rectangle.Height / 2f),
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity),
            };

            IsDivided = true;
        }

        public void Insert(Point pPoint, IEntity pEntity)
        {
            if (!Rectangle.Contains(pPoint))
                return;

            if (IsDivided)
            {
                SortExcessPoint(pPoint, pEntity);
            }
            else
            {
                if (Index < Capacity)
                {
                    Points[Index] = pPoint;
                    Entities[Index] = pEntity;
                    Index++;
                }
                else
                {
                    Divide();

                    SplitExistingPoints();

                    NullMainArrays();

                    SortExcessPoint(pPoint, pEntity);
                }
            }
        }

        private void NullMainArrays()
        {
            Index = 0;
            Points = null;
            Entities = null;
        }

        public List<IEntity> Query(Rectangle pRange)
        {
            if (Index > Capacity)
                throw new Exception(
                    "The number of points excesses the capacity");

            if (IsDivided)
                if (Index > 0)
                    throw new Exception(
                        "The number of points excesses the capacity (the quadTree is divided)");

            List<IEntity> found = new List<IEntity>();

            if (Rectangle.Intersects(pRange))
            {
                if (!IsDivided)
                {
                    for (int i = 0; i < Index; i++)
                    {
                        found.Add(Entities[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < Capacity; i++)
                    {
                        found.AddRange(SubQuads[i].Query(pRange));
                    }
                }
            }

            return found;
        }

        private void SplitExistingPoints()
        {
            for (int i = 0; i < Index; i++)
            {
                SortExcessPoint(Points[i], Entities[i]);
            }
        }

        private void SortExcessPoint(Point pPoint, IEntity pEntity)
        {
            for (int i = 0; i < Capacity; i++)
            {
                if (Contains(SubQuads[i].Rectangle, pPoint))
                {
                    SubQuads[i].Insert(pPoint, pEntity);
                    return;
                }
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            // Draw rectangle
            Primitives2D.Primitives2D.DrawRectangle(
                pSpriteBatch,
                Rectangle,
                Color.White,
                1);

            // Draw points
            if (Points != null)
                for (int i = 0; i < Index; i++)
                {

                    Primitives2D.Primitives2D.DrawCircle(
                        pSpriteBatch,
                        new Vector2(Points[i].X, Points[i].Y),
                        1,
                        Color.Blue,
                        1);
                }

            // Draw subDivision.
            if (IsDivided)
            {
                for (int i = 0; i < Capacity; i++)
                {
                    var subDiv = SubQuads[i];

                    subDiv.Draw(pSpriteBatch);
                }
            }
        }
    }
}