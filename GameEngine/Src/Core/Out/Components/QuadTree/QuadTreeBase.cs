﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.QuadTree
{
    /// <summary>
    /// This is the main quadTree class, it handles the creation and query of quadTrees,
    /// (used to reduce the number of units to draw / update and the number of collision checks).
    /// </summary>
    public class QuadTreeBase
    {
        #region Declarations

        private bool IsDivided { get; set; }
        private int Capacity { get; }
        public List<Point> Points { get; set; }

        private Point Center => new Point(
            Rectangle.X + Rectangle.Width / 2,
            Rectangle.Y + Rectangle.Height / 2);

        public QuadTreeBase[] Quads { get; set; }
        public Dictionary<Orientation, QuadTreeBase> Subdivisions { get; set; }

        private Rectangle Rectangle { get; }

        #endregion

        public QuadTreeBase(Rectangle pRectangle, int pCapacity)
        {
            Rectangle = pRectangle;
            Capacity = pCapacity;
            IsDivided = false;
            Points = new List<Point>();
        }

        private bool Contains(Rectangle pRect, Point pPoint)
        {
            return pRect.Contains(pPoint);
        }

        public void Insert(Point pPoint)
        {
            if (!Rectangle.Contains(pPoint))
                return;

            if (IsDivided)
            {
                SortExcessPoint(pPoint);
            }
            else
            {
                if (Points.Count < Capacity)
                {
                    Points.Add(pPoint);
                }
                else
                {
                    Divide();

                    SplitExistingPoints();

                    SortExcessPoint(pPoint);
                }
            }
        }

        private void SplitExistingPoints()
        {
            if (Points.Count <= 0)
                return;

            for (int i = 0; i < Points.Count; i++)
            {
                SortExcessPoint(Points[i]);
            }

            Points = new List<Point>();
        }

        private void SortExcessPoint(Point pPoint)
        {
            if (Contains(Subdivisions[Orientation.NorthWest].Rectangle, pPoint))
                Subdivisions[Orientation.NorthWest].Insert(pPoint);

            else if (Contains(Subdivisions[Orientation.NorthEast].Rectangle, pPoint))
                Subdivisions[Orientation.NorthEast].Insert(pPoint);

            else if (Contains(Subdivisions[Orientation.SouthWest].Rectangle, pPoint))
                Subdivisions[Orientation.SouthWest].Insert(pPoint);

            else if (Contains(Subdivisions[Orientation.SouthEast].Rectangle, pPoint))
                Subdivisions[Orientation.SouthEast].Insert(pPoint);
        }

        public List<Point> Query(Rectangle pRange)
        {
            if (Points.Count > Capacity)
                throw new Exception("The number of points excesses the capacity");

            if (IsDivided)
                if (Points.Count > 0)
                    throw new Exception("The number of points excesses the capacity (the quadTree is divided)");

            List<Point> found = new List<Point>();

            if (!Rectangle.Intersects(pRange))
            {
                return found;
            }
            else
            {
                // add the points if they are contained
                // in the range.
                for (int i = 0; i < Points.Count; i++)
                {
                    if (pRange.Contains(Points[i]))
                    {
                        found.Add(Points[i]);
                    }
                }

                // Check the subDivisions.
                if (IsDivided)
                {
                    for (int i = 0; i < Enum.GetNames(typeof(Orientation)).Length; i++)
                    {
                        var dir = (Orientation)(i);
                        var subDiv = Subdivisions[dir];

                        found.AddRange(subDiv.Query(pRange));
                    }
                }

                return found;
            }
        }

        private void Divide()
        {
            Subdivisions = new Dictionary<Orientation, QuadTreeBase>()
            {
                {
                    Orientation.NorthWest,
                    new QuadTreeBase(
                    new Rectangle(
                        Rectangle.X,
                        Rectangle.Y,
                        (int)(Rectangle.Width / 2f),
                        (int)(Rectangle.Height / 2f)),
                    Capacity)
                },

                {
                    Orientation.NorthEast,
                    new QuadTreeBase(
                        new Rectangle(
                            Rectangle.X + (int)(Rectangle.Width / 2f),
                            Rectangle.Y,
                            (int)(Rectangle.Width / 2f),
                            (int)(Rectangle.Height / 2f)),
                        Capacity)
                },

                {
                    Orientation.SouthWest,
                    new QuadTreeBase(
                        new Rectangle(
                            Rectangle.X,
                            Rectangle.Y + (int)(Rectangle.Height / 2f),
                            (int)(Rectangle.Width / 2f),
                            (int)(Rectangle.Height / 2f)),
                        Capacity)
                },

                {
                    Orientation.SouthEast,
                    new QuadTreeBase(
                        new Rectangle(
                            Rectangle.X + (int)(Rectangle.Width / 2f),
                            Rectangle.Y + (int)(Rectangle.Height / 2f),
                            (int)(Rectangle.Width / 2f),
                            (int)(Rectangle.Height / 2f)),
                        Capacity)
                }
            };

            IsDivided = true;
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            // Draw rectangle
            Primitives2D.Primitives2D.DrawRectangle(
                pSpriteBatch,
                Rectangle,
                Color.White,
                1);

            //            // Draw points
            //            for (int i = 0; i < Points.Count; i++)
            //            {
            //
            //                Primitives2D.Primitives2D.DrawCircle(
            //                    pSpriteBatch,
            //                    new Vector2(Points[i].X, Points[i].Y),
            //                    1,
            //                    Color.Blue,
            //                    1);
            //            }

            // Draw subDivision.
            if (IsDivided)
            {
                for (int i = 0; i < Enum.GetNames(typeof(Orientation)).Length; i++)
                {
                    var dir = (Orientation)(i);
                    var subDiv = Subdivisions[dir];

                    subDiv.Draw(pSpriteBatch);
                }
            }
        }

        public enum Orientation
        {
            NorthWest,
            NorthEast,
            SouthWest,
            SouthEast
        }
    }
}