﻿#region Usings

using System.Collections;
using System.Collections.Generic;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    /// <summary>
    /// This class emulates unity's coRoutines system.
    /// </summary>
    public class CoRoutine
    {
        public List<IEnumerator> Routines { get; }

        public CoRoutine()
        {
            Routines = new List<IEnumerator>();
        }

        public void Start(IEnumerator routine)
        {
            Routines.Add(routine);
        }

        public void StopAll()
        {
            Routines.Clear();
        }

        public void Update()
        {
            for (int i = 0; i < Routines.Count; i++)
            {
                if (Routines[i].Current is IEnumerator)
                    if (MoveNext((IEnumerator)Routines[i].Current))
                        continue;
                if (!Routines[i].MoveNext())
                    Routines.RemoveAt(i--);
            }
        }

        bool MoveNext(IEnumerator routine)
        {
            if (routine.Current is IEnumerator)
                if (MoveNext((IEnumerator)routine.Current))
                    return true;
            return routine.MoveNext();
        }

        public int Count
        {
            get { return Routines.Count; }
        }

        public bool Running
        {
            get { return Routines.Count > 0; }
        }
    }
}