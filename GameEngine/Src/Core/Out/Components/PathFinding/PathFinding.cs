﻿#region Usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    /// <summary>
    /// This class contains methods to calculate paths.
    /// </summary>
    public class PathFinding
    {
        /// <summary>
        /// The grid used to calculate paths.
        /// </summary>
        public Grid Grid1 { get; set; }

        /// <summary>
        /// The coRoutine to calculate paths without slowing down the game.
        /// </summary>
        public static CoRoutine Routine { get; private set; }

        /// <summary>
        /// The manager of path requests.
        /// </summary>
        private readonly PathRequestManager _requestManager;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pManager"></param>
        public PathFinding(PathRequestManager pManager)
        {
            Grid1 = new Grid();
            Routine = new CoRoutine();
            _requestManager = pManager;
        }

        /// <summary>
        /// Loads and initializes the grid.
        /// </summary>
        /// <param name="pMap"></param>
        /// <param name="pLayer"></param>
        public void LoadContent(TmxMap pMap, MapLayer pLayer)
        {
            Grid1.LoadContent(pMap, pLayer);
        }

        /// <summary>
        /// Starts the coRoutine to begin the pathFinding.
        /// </summary>
        /// <param name="pPathStart"></param>
        /// <param name="pPathEnd"></param>
        public void StartFindingPath(Vector2 pPathStart, Vector2 pPathEnd)
        {
            Routine.Start(FindPath(pPathStart, pPathEnd));
        }

        /// <summary>
        /// Finds a path between 2 vector2.
        /// </summary>
        /// <param name="pStartPos"></param>
        /// <param name="pTargetPos"></param>
        /// <returns></returns>
        public IEnumerator FindPath(Vector2 pStartPos, Vector2 pTargetPos)
        {
            Vector2[] waypoints = new Vector2[0];
            bool pathSuccess = false;

            Node startNode = Grid1.NodeFromPos(pStartPos);
            Node targetNode = Grid1.NodeFromPos(pTargetPos);
            startNode.Parent = startNode;

            if (startNode.Walkable && targetNode.Walkable)
            {
                Heap<Node> openSet = new Heap<Node>(Grid1.GridSurface);
                HashSet<Node> closedSet = new HashSet<Node>();
                openSet.Add(startNode);

                while (openSet.Count > 0)
                {
                    Node currentNode = openSet.RemoveFirst();
                    closedSet.Add(currentNode);

                    if (currentNode == targetNode)
                    {
                        pathSuccess = true;
                        break;
                    }

                    foreach (var neighbor in Grid1.GetNeighbors(currentNode))
                    {
                        if (!neighbor.Walkable || closedSet.Contains(neighbor))
                        {
                            continue;
                        }

                        int newCost = currentNode.GCost + GetDistance(currentNode, neighbor);
                        if (newCost < neighbor.GCost || !openSet.Contains(neighbor))
                        {
                            neighbor.GCost = newCost;
                            neighbor.HCost = GetDistance(neighbor, targetNode);
                            neighbor.Parent = currentNode;

                            if (!openSet.Contains(neighbor))
                                openSet.Add(neighbor);
                        }
                    }
                }
            }

            yield return null;
            if (pathSuccess)
            {
                if (startNode != targetNode)
                    waypoints = ReTracePath(startNode, targetNode);
            }

            _requestManager.FinishedProcessingPath(waypoints, pathSuccess);
        }

        /// <summary>
        /// Reverses the path [] to get the nodes in the right order.
        /// </summary>
        /// <param name="pStartNode"></param>
        /// <param name="pEndNode"></param>
        /// <returns></returns>
        private Vector2[] ReTracePath(Node pStartNode, Node pEndNode)
        {
            List<Node> path = new List<Node>();
            Node currentNode = pEndNode;

            while (currentNode != pStartNode)
            {
                path.Add(currentNode);
                currentNode = currentNode.Parent;
            }

            Vector2[] wayPoints = SimplifyPath(path);
            Array.Reverse(wayPoints);
            return wayPoints;
        }

        /// <summary>
        /// Simplifies the path by removing nodes if they are in the same
        /// direction as the previous one, kinda buggy.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns></returns>
        Vector2[] SimplifyPath(List<Node> pPath)
        {
            List<Vector2> wayPoints = new List<Vector2>();
            //            Vector2 oldDirection = Vector2.Zero;
            wayPoints.Add(new Vector2(pPath[0].Pos.X, pPath[0].Pos.Y));

            for (int i = 1; i < pPath.Count; i++)
            {
                //                Vector2 newDirection = new Vector2(pPath[i - 1].Line - pPath[i].Line,
                //                    pPath[i - 1].Col - pPath[i].Col);
                //                if (newDirection != oldDirection)
                {
                    Point p = pPath[i].Pos;
                    wayPoints.Add(new Vector2(p.X, p.Y));
                }

                //                oldDirection = newDirection;
            }
            //            wayPoints.Add(new Vector2(pPath[pPath.Count - 1].Pos.X, pPath[pPath.Count - 1].Pos.Y));

            wayPoints.Remove(wayPoints.First());
            return wayPoints.ToArray();
        }

        /// <summary>
        /// Returns the distance between node A and B.
        /// </summary>
        /// <param name="pNodeA"></param>
        /// <param name="pNodeB"></param>
        /// <returns></returns>
        private int GetDistance(Node pNodeA, Node pNodeB)
        {
            var diag = 14;
            var straightLine = 10;

            int distX = Math.Abs(pNodeA.Pos.X - pNodeB.Pos.X);
            int distY = Math.Abs(pNodeA.Pos.Y - pNodeB.Pos.Y);

            if (distX > distY)
                return diag * distY + straightLine * (distX - distY);
            else
                return diag * distX + straightLine * (distY - distX);
        }

        /// <summary>
        /// Main update method, updates the coRoutines.
        /// </summary>
        public void Update()
        {
            if (Routine.Running)
            {
                Routine.Update();
            }
        }

        /// <summary>
        /// Draws the grid debug.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void Draw(SpriteBatch pSpriteBatch)
        {
            Grid1.Draw(pSpriteBatch);
        }
    }
}