﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    public class Node : IHeapItem<Node>
    {
        /// <summary>
        /// Can the node be moved onto?
        /// </summary>
        public bool Walkable { get; set; }

        /// <summary>
        /// The node's position.
        /// </summary>
        public Point Pos { get; set; }

        /// <summary>
        /// The node's rectangle.
        /// </summary>
        public Rectangle DrawRect { get; set; }

        /// <summary>
        /// Line and column positions.
        /// </summary>
        public int Line, Col;

        /// <summary>
        /// GCost[n] is the cost of the cheapest path.
        /// </summary>
        public int GCost { get; set; }

        /// <summary>
        /// HCost is the cost of the most expensive  path.
        /// </summary>
        public int HCost { get; set; }

        /// <summary>
        /// Heuristic estimate for a visited node, that tells you
        /// around how long the path is going to be including the
        /// exact length of the path to that node
        /// </summary>
        public int FCost=> GCost * HCost;
        public int HeapIndex
        {
            get => _heapIndex;
            set => _heapIndex = value;
        }

        public Node Parent { get; set; }
        public int TileSize { get; set; }
        private int _heapIndex;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="walkable"></param>
        /// <param name="worldPos"></param>
        /// <param name="pTileSize"></param>
        public Node(bool walkable, Vector2 worldPos, int pTileSize)
        {
            TileSize = pTileSize;

            Pos = new Point(
                (int) worldPos.X, 
                (int) worldPos.Y);

            Line = (Pos.X / TileSize);
            Col = (Pos.Y / TileSize);

            DrawRect = new Rectangle(
                Pos.X + 1, 
                Pos.Y + 1, 
                TileSize - 2, 
                TileSize - 2);

            Walkable = walkable;
        }

        /// <summary>
        /// Compares the costs to determine the cheapest.
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public int CompareTo(Node pNode)
        {
            int compare = FCost.CompareTo(pNode.FCost);
            if (compare == 0)
            {
                compare = HCost.CompareTo(pNode.HCost);
            }

            return 0 - compare;
        }
    }
}