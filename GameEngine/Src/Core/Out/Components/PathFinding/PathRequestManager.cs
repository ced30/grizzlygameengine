﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Types;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    public class PathRequestManager
    {
        private Queue<PathRequest> _pathRequestQueue = new Queue<PathRequest>();
        private PathRequest _currentPathRequest;
        private static PathRequestManager _instance;
        private PathFinding _pathFinding;

        private bool _isProcessingPath;

        public void LoadContent(TmxMap pMap, MapLayer pLayer)
        {
            _instance = this;
            _pathFinding = new PathFinding(_instance);
            _pathFinding.LoadContent(pMap, pLayer);
        }

        public static void RequestPath(Vector2 pStart, Vector2 pEnd,
            Action<Vector2[], bool> pCallback)
        {
            PathRequest newRequest = new PathRequest(pStart, pEnd, pCallback);
            _instance._pathRequestQueue.Enqueue(newRequest);
            _instance.TryProcessNext();

            //            Debug.Print("Path Requested (PathRequestManager)");
        }

        private void TryProcessNext()
        {
            if (!_isProcessingPath && _pathRequestQueue.Count > 0)
            {
                _currentPathRequest = _pathRequestQueue.Dequeue();
                _isProcessingPath = true;
                _pathFinding.StartFindingPath(_currentPathRequest.PathStart,
                    _currentPathRequest.PathEnd);
            }
        }

        public void FinishedProcessingPath(Vector2[] pPath, bool pSuccess)
        {
            _currentPathRequest.Callback(pPath, pSuccess);
            _isProcessingPath = false;
            TryProcessNext();
        }

        public void Update()
        {
            _pathFinding.Update();
        }

        struct PathRequest
        {
            public readonly Vector2 PathStart;
            public readonly Vector2 PathEnd;
            public readonly Action<Vector2[], bool> Callback;

            public PathRequest(Vector2 pStart, Vector2 pEnd, Action<Vector2[], bool> pCallback)
            {
                PathStart = pStart;
                PathEnd = pEnd;
                Callback = pCallback;
            }
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            _pathFinding.Draw(pSpriteBatch);
        }
    }
}