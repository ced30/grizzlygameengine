﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    /// <summary>
    /// The grid for the pathFinding system, load content with
    /// a tmxMap and a collisions layer to initialize the grid.
    /// </summary>
    public class Grid
    {
        #region Declarations

        /// <summary>
        /// The size of 1 tile.
        /// </summary>
        protected int TileSize { get; set; }

        /// <summary>
        /// Color and alpha.
        /// </summary>
        protected Color Color1 { get; set; }

        /// <summary>
        /// Width and height of the grid.
        /// </summary>
        protected int GridSizeX, GridSizeY;

        /// <summary>
        /// An array of nodes to hold cells data (collisions, etc).
        /// </summary>
        protected Node[,] Grid1 { get; set; }

        /// <summary>
        /// Is the grid visible?
        /// </summary>
        protected bool IsVisible { get; set; }

        /// <summary>
        /// Those are colors for visual debugging purposes.
        /// </summary>
        private const float Alpha = 0.5f;
        private Color _black = Color.White * Alpha;
        private readonly Color _cyan = Color.Cyan * Alpha;
        private readonly Color _white = Color.White * Alpha;
        private readonly Color _red = Color.Red * Alpha;

        /// <summary>
        /// Layer of tiles, which the entities cannot move onto or through
        /// (collisions layer).
        /// </summary>
        public MapLayer UnWalkMapLayer { get; set; }

        /// <summary>
        /// The size of the grid.
        /// </summary>
        public Vector2 GridWorldSize { get; set; }

        /// <summary>
        /// the surface of the grid.
        /// </summary>
        public int GridSurface => GridSizeX * GridSizeY;

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        public Grid()
        {
            Color1 = Color.White;
        }

        /// <summary>
        /// Sets grid size values and the collisions layer.
        /// </summary>
        /// <param name="pMap"></param>
        /// <param name="pLayer"></param>
        public void LoadContent(TmxMap pMap, MapLayer pLayer)
        {
            TileSize = pMap.TileWidth;
            UnWalkMapLayer = pLayer;

            GridWorldSize = new Vector2(pMap.Width * TileSize, pMap.Height * TileSize);
            GridSizeX = pMap.Width;
            GridSizeY = pMap.Height;

            CreateGrid();
        }

        /// <summary>
        /// Creates a new grid and sets the type of tiles.
        /// </summary>
        private void CreateGrid()
        {
            Grid1 = new Node[GridSizeX, GridSizeY];

            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if(UnWalkMapLayer.Tiles.Count <= 0)
                        continue;

                    var tile = UnWalkMapLayer.Tiles[y * GridSizeX + x];

                    Grid1[x, y] = new Node(tile.CurrentType != TileItem.Type.Solid,
                        tile.Position, TileSize);
                }
            }
        }

        /// <summary>
        /// Returns a list of neighbor nodes, can be tweaked
        /// to allow diagonal movement or not.
        /// </summary>
        /// <param name="pNode"></param>
        /// <returns></returns>
        public List<Node> GetNeighbors(Node pNode)
        {
            List<Node> neighbors = new List<Node>();
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    // no diagonals
                    //                    if ((x == -1 || x == 1) && (y == -1 || y == 1))
                    //                        continue;

                    int checkX = pNode.Line + x;
                    int checkY = pNode.Col + y;

                    if (checkX >= 0 && checkX < GridSizeX &&
                        checkY >= 0 && checkY < GridSizeY)
                    {
                        neighbors.Add(Grid1[checkX, checkY]);
                    }
                }
            }

            return neighbors;
        }

        /// <summary>
        /// Returns a node from the grid, given a vector2 argument. 
        /// </summary>
        /// <param name="pPos"></param>
        /// <returns></returns>
        public Node NodeFromPos(Vector2 pPos)
        {
            Point worldPos = new Point((int)(pPos.X / TileSize),
                (int)(pPos.Y / TileSize));

            return Grid1[worldPos.X, worldPos.Y];
        }

        /// <summary>
        /// Draws the grid (for debug purposes).
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void Draw(SpriteBatch pSpriteBatch)
        {
            if (IsVisible)
            {
                if (Grid1 != null && Grid1.Length > 0)
                {
                    foreach (var node in Grid1)
                    {
                        Color1 = (node.Walkable) ? _white : _red;

                        Primitives2D.Primitives2D.DrawRectangleFilled(pSpriteBatch, node.DrawRect, Color1);

                        if (node.Parent != null)
                            Primitives2D.Primitives2D.DrawRectangleFilled(pSpriteBatch, node.Parent.DrawRect, Color.Yellow * 0.2f);
                    }
                }
            }
        }
    }
}