﻿#region Usings

using System;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.PathFinding
{
    /// <summary>
    /// This class is a kind of stack used with the pathFinding system to process path and nodes.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Heap<T> where T : IHeapItem<T>
    {
        public T[] Items { get; }
        private int CurrentItemsCount { get; set; }

        public int Count => CurrentItemsCount;

        public Heap(int pMaxHeapSize)
        {
            Items = new T[pMaxHeapSize];
        }

        public void Add(T pItem)
        {
            pItem.HeapIndex = CurrentItemsCount;
            Items[CurrentItemsCount] = pItem;
            SortUp(pItem);
            CurrentItemsCount++;
        }

        public T RemoveFirst()
        {
            T firstItem = Items[0];
            CurrentItemsCount--;
            Items[0] = Items[CurrentItemsCount];
            Items[0].HeapIndex = 0;
            SortDown(Items[0]);
            return firstItem;
        }

        public bool Contains(T pItem)
        {
            return Equals(Items[pItem.HeapIndex], pItem);
        }

        private void SortDown(T pItem)
        {
            while (true)
            {
                int childIndexLeft = pItem.HeapIndex * 2 + 1;
                int childIndexRight = pItem.HeapIndex * 2 + 2;

                if (childIndexLeft < CurrentItemsCount)
                {
                    var swapIndex = childIndexLeft;

                    if (childIndexRight < CurrentItemsCount)
                    {
                        if (Items[childIndexLeft].CompareTo(Items[childIndexRight]) < 0)
                        {
                            swapIndex = childIndexRight;
                        }
                    }

                    if (pItem.CompareTo(Items[swapIndex]) < 0)
                    {
                        Swap(pItem, Items[swapIndex]);
                    }
                    else
                    {
                        return;
                    }
                }

                else
                {
                    return;
                }
            }
        }

        private void SortUp(T pItem)
        {
            int parentIndex = (pItem.HeapIndex - 1) / 2;

            while (true)
            {
                T parentItem = Items[parentIndex];
                if (pItem.CompareTo(parentItem) > 0)
                {
                    Swap(pItem, parentItem);
                }
                else
                {
                    break;
                }

                parentIndex = (pItem.HeapIndex - 1) / 2;
            }
        }

        private void Swap(T pItemA, T pItemB)
        {
            Items[pItemA.HeapIndex] = pItemB;
            Items[pItemB.HeapIndex] = pItemA;
            int itemAIndex = pItemA.HeapIndex;
            pItemA.HeapIndex = pItemB.HeapIndex;
            pItemB.HeapIndex = itemAIndex;
        }

        public void UpdateItem(T pItem)
        {
            SortUp(pItem);
        }
    }

    public interface IHeapItem<T> : IComparable<T>
    {
        int HeapIndex
        {
            get;
            set;
        }
    }
}