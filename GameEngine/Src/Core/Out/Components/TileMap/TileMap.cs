﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.TileMap
{
    /// <summary>
    /// This class is the tile map Component, it handles all tileMap manipulations.
    /// </summary>
    public class TileMap
    {
        private string SpawnsStr { get; set; }
        public TileMapCollider Collider { get; set; }
        public TileMapDataPack Data { get; set; }
        public TileMapRenderer Renderer { get; set; }

        public TileMap()
        {
            SpawnsStr = "SPAWNS";
        }

        public List<IEntity> GetEntities(EntityFactoryManager pFactory)
        {
            if (Data.Map == null)
                return null;

            List<IEntity> entities = new List<IEntity>();
            for (int i = 0; i < Data.Map.ObjectGroups.Count; i++)
            {
                var objGroup = Data.Map.ObjectGroups[i];
                if (objGroup.Name.ToUpper() == SpawnsStr)
                {
                    for (int j = 0; j < objGroup.Objects.Count; j++)
                    {
                        var obj = objGroup.Objects[j];
                        entities.Add(
                            pFactory.Get(obj));
                    }
                }
            }

            return entities;
        }

        public virtual void LoadContent(TileMapDataPack pData,
            EntityFactoryManager pFactory)
        {
            if (Data != null)
                Unload();

            Data = pData;
            Collider = new TileMapCollider(Data);
            Renderer = new TileMapRenderer(Data);
        }

        public void Unload()
        {
            Data = null;
        }

        #region Helpers Methods

        public void SetCollisionLayer(string pId)
        {
            for (int i = 0; i < Data.CollisionLayers.Count; i++)
            {
                if (Data.CollisionLayers[i].Identifier.Equals(pId))
                {
                    Data.CurrentCollisionLayer = Data.CollisionLayers[i];
                    Collider.SetCollisionLayer(Data.CurrentCollisionLayer);
                }
            }
        }

        public void SetCurrentCollisionLayerSolid(bool pSolid)
        {
            for (int i = 0; i < Data.CollisionLayers.Count; i++)
            {
                Data.CurrentCollisionLayer.IsSolid = pSolid;
            }
        }

        #endregion

        public void Update(GameTime pGameTime)
        {
            Renderer.Update(pGameTime, this);
        }
    }
}