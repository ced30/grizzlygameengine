﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.TileMap.Systems
{
    /// <inheritdoc />
    /// <summary>
    /// This class is a wrapper handling the animations of the tileMap.
    /// </summary>
    public class TileMapAnimator : GrizzlyComponent
    {
        /// <summary>
        /// Map dimensions in pixels.
        /// </summary>
        private int MapHeight { get; }
        private int MapWidth { get; }
        private int TileHeight { get; }
        private int TileWidth { get; }

        /// <summary>
        /// Constructor: takes a TileMapData as argument.
        /// </summary>
        /// <param name="pData"></param>
        public TileMapAnimator(TileMapDataPack pData)
        {
            MapHeight = pData.MapHeight;
            MapWidth = pData.MapWidth;
            TileHeight = pData.TileHeight;
            TileWidth = pData.TileWidth;
        }

        /// <summary>
        /// Animates the tiles if they are animated and visible.
        /// </summary>
        /// <param name="pLayer"></param>
        /// <param name="pGameTime"></param>
        public void Animate(List<MapLayer> pLayer, GameTime pGameTime)
        {
            int nbLayers = pLayer.Count;
            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                var tLayer = pLayer[nLayer];
//                if(tLayer.IsSolid)
//                    return;

                if (tLayer.IsVisible)
                {
                    int columnStart = Camera.Camera.Instance.DrawRectangle.Left / TileWidth;
                    int columnEnd = Camera.Camera.Instance.DrawRectangle.Right / TileWidth;
                    int lineStart = Camera.Camera.Instance.DrawRectangle.Top / TileHeight;
                    int lineEnd = Camera.Camera.Instance.DrawRectangle.Bottom / TileHeight;
                    for (int column = columnStart; column <= columnEnd; column++)
                    {
                        for (int line = lineStart; line <= lineEnd; line++)
                        {
                            if (column < 0 || column >= MapWidth ||
                                line < 0 || line >= MapHeight)
                                continue;

                            var tile = tLayer.Tiles[column + line * MapWidth];
                            if (tile is AnimatedTile tAnimated)
                            {
                                int gid = tAnimated.Gid;
                                if (gid != 0 && tAnimated.IsVisible)
                                {
                                    tAnimated.Update(pGameTime);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}