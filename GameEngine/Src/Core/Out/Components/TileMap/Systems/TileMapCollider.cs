﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.TileMap.Systems
{
    public class TileMapCollider : GrizzlyComponent
    {
        private int MapHeight { get; }
        private int MapWidth { get; }
        private int TileHeight { get; }
        private int TileWidth { get; }
        private MapLayer CollisionsLayer { get; set; }

        public TileMapCollider(ProceduralMapData pData)
        {
            MapHeight = pData.MapHeight;
            MapWidth = pData.MapWidth;
            TileHeight = pData.TileHeight;
            TileWidth = pData.TileWidth;
            CollisionsLayer = pData.CurrentCollisionLayer;
        }

        public TileMapCollider(TileMapDataPack pData)
        {
            MapHeight = pData.MapHeight;
            MapWidth = pData.MapWidth;
            TileHeight = pData.TileHeight;
            TileWidth = pData.TileWidth;
            CollisionsLayer = pData.CurrentCollisionLayer;
        }

        #region Collisions

        /// <summary>
        /// Checks if a sprite is colliding in a given direction.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDirection"></param>
        /// <returns></returns>
        public bool Collide(IDataController pData, Direction pDirection)
        {
            if (CollisionsLayer.Tiles.Count <= 0)
                return false;

            switch (pDirection)
            {
                case Direction.Up:
                    var id1A = GetTilesAt(new Vector2(
                        pData.BoundingBox.Left + 1,
                        pData.BoundingBox.Top - 1));

                    var id2A = GetTilesAt(new Vector2(
                        pData.BoundingBox.Right - 1,
                        pData.BoundingBox.Top - 1));

                    if (IsSolid(id1A) || IsSolid(id2A))
                    {
                        return !IsJumpThrough(id1A) && 
                               (!IsJumpThrough(id2A) || 
                                !(pData.Positional.Velocity.Y < 0));
                    }
                    return false;

                case Direction.Down:
                    var iUnder = GetTilesAt(new Vector2(
                        pData.BoundingBox.Left + 1,
                        pData.BoundingBox.Bottom - 1));

                    var iMid = GetTilesAt(new Vector2(
                       (int)pData.Center.X,
                       pData.BoundingBox.Bottom));

                    var overLap = GetTilesAt(new Vector2(
                        pData.BoundingBox.Right - 1,
                        pData.BoundingBox.Bottom - 1));

                    if (IsSolid(iUnder) || IsSolid(overLap) || IsSolid(iMid))
                        return true;
                    return false;

                case Direction.Left:
                    var id1L = GetTilesAt(new Vector2(
                        pData.BoundingBox.Left - 1,
                        pData.BoundingBox.Top + 1));

                    var id2L = GetTilesAt(new Vector2(
                        pData.BoundingBox.Left - 1,
                        pData.BoundingBox.Bottom - 1));

                    if (IsSolid(id1L) || IsSolid(id2L))
                    {
                        return !IsJumpThrough(id1L) && !IsJumpThrough(id2L);
                    }
                    return false;

                case Direction.Right:
                    var id1R = GetTilesAt(new Vector2(
                        pData.BoundingBox.Right + 1,
                        pData.BoundingBox.Top + 1));

                    var id2R = GetTilesAt(new Vector2(
                        pData.BoundingBox.Right + 1,
                        pData.BoundingBox.Bottom - 1));

                    if (IsSolid(id1R) || IsSolid(id2R))
                    {
                        return !IsJumpThrough(id1R) && !IsJumpThrough(id2R);
                    }
                    return false;
            }

            return false;
        }

        /// <summary>
        /// Returns a tile at (vector2) position.
        /// </summary>
        /// <param name="pPos"></param>
        public TileItem.Type GetTilesAt(Vector2 pPos)
        {
            if (CollisionsLayer.Tiles.Count <= 0)
                return TileItem.Type.None;

            var mobRect = new Rectangle((int)pPos.X, (int)pPos.Y, TileWidth, TileHeight);

            var col = Math.Floor(pPos.X / TileWidth);
            var lig = Math.Floor(pPos.Y / TileHeight);

            if (!(col >= 0) || !(col < MapWidth) || !(lig >= 0) || !(lig < MapHeight))
                return TileItem.Type.None;
            try
            {
                var index = (int)(lig * MapWidth + col);
                var tile = CollisionsLayer.Tiles[index];

                if (mobRect.Intersects(tile.Rectangle))
                    return tile.CurrentType;
            }
            catch (Exception e)
            {
                Console.WriteLine("no tiles found" + e);
                throw;
            }

            return TileItem.Type.None;
        }

        /// <summary>
        /// Can we jump through the tile?
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        protected static bool IsJumpThrough(TileItem.Type pType)
        {
            if (pType == TileItem.Type.JumpThrough)
                return true;

            return false;
        }

        /// <summary>
        /// Is the tile a ladder?
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        public bool IsLadder(TileItem.Type pType)
        {
            if (pType == TileItem.Type.Ladder)
                return true;

            return false;
        }

        /// <summary>
        /// Is the tile solid?
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        protected static bool IsSolid(TileItem.Type pType)
        {
            return pType == TileItem.Type.Solid ||
                   pType == TileItem.Type.Break ||
                   pType == TileItem.Type.JumpThrough ||
                   pType == TileItem.Type.Climb;
        }

        #endregion

        public void SetCollisionLayer(MapLayer pLayer)
        {
            CollisionsLayer = pLayer;
        }
    }
}