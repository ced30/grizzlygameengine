﻿#region Usings

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.TileMap.Systems
{
    public class TileMapRenderer : GrizzlyComponent
    {
        public TileMapAnimator Animator { get; }

        public TileMapRenderer(TileMapDataPack pData)
        {
            Animator = new TileMapAnimator(pData);
        }

        public virtual void Update(GameTime pGameTime, TileMap pTMap)
        {
            Animator.Animate(pTMap.Data.AnimatedLayers, pGameTime);

            base.Update(pGameTime);
        }

        #region Draw

        public static void DrawLayer(SpriteBatch pSpriteBatch, List<MapLayer> pLayer, TileMapDataPack pData)
        {
            int nbLayers = pLayer.Count;
            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                var tLayer = pLayer[nLayer];
                if (tLayer.IsVisible)
                {
                    int columnStart = Camera.Camera.Instance.DrawRectangle.Left / pData.TileWidth;
                    int columnEnd = Camera.Camera.Instance.DrawRectangle.Right / pData.TileWidth;
                    int lineStart = Camera.Camera.Instance.DrawRectangle.Top / pData.TileHeight;
                    int lineEnd = Camera.Camera.Instance.DrawRectangle.Bottom / pData.TileHeight;
                    for (int column = columnStart; column <= columnEnd; column++)
                    {
                        for (int line = lineStart; line <= lineEnd; line++)
                        {
                            if (column < 0 || column >= pData.MapWidth ||
                                line < 0 || line >= pData.MapHeight)
                                continue;

                            var tile = tLayer.Tiles[column + line * pData.MapWidth];

                            int gid = tile.Gid;
                            if (gid != 0 && tile.IsVisible)
                            {
                                int tileFrame = gid - 1;

                                if (tile is AnimatedTile tAnimated)
                                {
                                    tileFrame = tAnimated.AnimationManager.Animation.CurrentFrame;
                                    int tileSetColumn = tileFrame % pData.TileSetColumns;
                                    int tileSetLine =
                                        (int)Math.Floor(
                                            tileFrame / (double)pData.TileSetColumns
                                        );
                                    float x = tAnimated.X;
                                    float y = tAnimated.Y;
                                    Rectangle tileSetRec =
                                        new Rectangle(
                                            pData.TileWidth * tileSetColumn,
                                            pData.TileHeight * tileSetLine,
                                            pData.TileWidth, pData.TileHeight);

                                    pSpriteBatch.Draw(
                                        pData.TileSetTexture2D,
                                        new Vector2(x, y),
                                        tileSetRec, Color.White);
                                }
                                else
                                {
                                    int tileSetColumn = tileFrame % pData.TileSetColumns;
                                    int tileSetLine =
                                        (int)Math.Floor(
                                            tileFrame / (double)pData.TileSetColumns
                                        );
                                    int x = column * pData.TileWidth;
                                    int y = line * pData.TileHeight;
                                    Rectangle tileSetRec =
                                        new Rectangle(
                                            pData.TileWidth * tileSetColumn,
                                            pData.TileHeight * tileSetLine,
                                            pData.TileWidth, pData.TileHeight);

                                    pSpriteBatch.Draw(
                                        pData.TileSetTexture2D,
                                        new Vector2(x, y),
                                        tileSetRec, tile.Color);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}