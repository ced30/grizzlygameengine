﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class ProceduralMapData
    {
        public List<MapLayer> BackLayers { get; set; }

        /// <summary>
        /// List of mapLayer items.
        /// </summary>
        public List<MapLayer> CollisionLayers { get; set; }

        /// <summary>
        /// List of mapLayer items.
        /// </summary>
        public MapLayer CurrentCollisionLayer { get; set; }
        
        /// <summary>
        /// The map dimensions in col, lines
        /// </summary>
        public int MapHeight { get; set; }
        public int MapWidth { get; set; }

        /// <summary>
        /// The map dimensions in pixels
        /// </summary>
        public int TileHeight { get; set; }
        public int TileWidth { get; set; }
        public int MapHeightInPixels => MapHeight * TileHeight;
        public int MapWidthInPixels => MapWidth * TileWidth;

        /// <summary>
        /// The number or Columns
        /// </summary>
        public int TileSetColumns { get; set; }

        /// <summary>
        /// Rectangle representing the map dimensions
        /// </summary>
        public Rectangle MapRectangle { get; set; }

        /// <summary>
        /// The tileSet texture
        /// </summary>
        public Texture2D TileSetTexture2D { get; set; }

        /// <summary>
        /// The TmxTileSet from tiled editor
        /// </summary>
        public TmxTileSet TileSet { get; set; }


        public ProceduralMapData()
        {
            BackLayers = new List<MapLayer>();
            CollisionLayers = new List<MapLayer>();
            CurrentCollisionLayer = new MapLayer();
        }
    }
}