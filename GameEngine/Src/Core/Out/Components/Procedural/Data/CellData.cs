﻿using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class CellData
    {
        private int TileSize { get; }
        public bool IsWalkable { get; set; }
        public bool IsDebug { get; set; }
        public int GiD { get; set; }
        public Rectangle BoundingBox { get; set; }
        public Vector2 WorldPos { get; set; }
        public Vector2 GridPos { get; set; }
        public CellData(Vector2 pPosition, int pTileSize)
        {
            TileSize = pTileSize;
            GridPos = pPosition;
            WorldPos = new Vector2(pPosition.X * TileSize, pPosition.Y * TileSize);
            BoundingBox = new Rectangle(
                (int)WorldPos.X,
                (int)WorldPos.Y,
                TileSize,
                TileSize);
        }
    }
}