﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class QuadData
    {
        public CellData[,] Cells { get; set; }
        public bool IsActive { get; set; }
        public bool IsOpenBottom { get; set; }
        public bool IsOpenLeft { get; set; }
        public bool IsOpenRight { get; set; }
        public bool IsOpenTop { get; set; }
        public int Id { get; set; }
        public int NbCellX { get; set; }
        public int NbCellY { get; set; }
        public Rectangle BoundingBox { get; set; }
        public Vector2 GridPos { get; set; }
        public Vector2 WorldPos { get; set; }

        public QuadData(Vector2 pPos, int pNbCellX, int pNbCellY, int pTileSize)
        {
            GridPos = pPos;
            WorldPos = new Vector2(
                pPos.X * pTileSize, pPos.Y + pTileSize);
            NbCellX = pNbCellX;
            NbCellY = pNbCellY;
            BoundingBox = new Rectangle(
                (int)WorldPos.X, (int)WorldPos.Y,
                NbCellX * pTileSize,
                NbCellY * pTileSize);
            Cells = new CellData[NbCellX, NbCellY];
        }

        public int GetCellId(int pX, int pY)
        {
            return Cells[pX, pY].GiD;
        }

        public void SetCellId(int pX, int pY, int pId)
        {
            Cells[pX, pY].GiD = pId;
        }
    }
}