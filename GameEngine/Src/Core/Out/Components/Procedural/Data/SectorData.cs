﻿#region Usings

using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.Data
{
    public class SectorData
    {
        #region Declarations

        public int TileSize { get; set; }
        public int NbActive { get; set; }
        public int NbQuadX { get; }
        public int NbQuadY { get; }
        public QuadData[,] Quadrants { get; set; }
        public SectorSize CurrentSize { get; set; }

        #endregion

        public SectorData(int pNbQuadX, int pNbQuadY, int pTileSize)
        {
            NbQuadX = pNbQuadX;
            NbQuadY = pNbQuadY;
            TileSize = pTileSize;
        }
    }
}