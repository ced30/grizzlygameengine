﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class ProceduralPacker
    {
        private ProceduralAutoTiler AutoTiler { get; set; }
        public ProceduralPacker()
        {
            AutoTiler = new ProceduralAutoTiler();
        }

        public ProceduralMapData Pack(
            string pId,
            SectorData pCurrentSector,
            CellData[,] pCurrentMap,
            TmxTileSet pTileSet,
            Texture2D pTexture)
        {
            var nbCellsX = pCurrentSector.Quadrants[0, 0].NbCellX;
            var nbCellsY = pCurrentSector.Quadrants[0, 0].NbCellY;

            // Collisions layer
            MapLayer collisions = GetConvertedLayer(pId, pCurrentSector, pCurrentMap);
            collisions.IsSolid = true;

            // AutoTiled layer
            var autoTiledMap = AutoTiler.AutoTileInDoor(
                AutoTileType.EightBits,
                pCurrentMap,
                pCurrentSector.NbQuadX * nbCellsX,
                pCurrentSector.NbQuadX * nbCellsY);
            MapLayer defaultLayer = GetConvertedLayer(pId, pCurrentSector, autoTiledMap);

            var mapData = new ProceduralMapData();

            mapData.TileWidth = pCurrentSector.TileSize;
            mapData.TileHeight = pCurrentSector.TileSize;
            mapData.TileSetColumns = pTexture.Width / mapData.TileWidth;
            mapData.MapWidth = pCurrentSector.NbQuadX * nbCellsX;
            mapData.MapHeight = pCurrentSector.NbQuadY * nbCellsY;
            mapData.MapRectangle = new Rectangle(
                0,
                0,
                mapData.MapWidthInPixels,
                mapData.MapHeightInPixels);
            mapData.TileSet = pTileSet;
            mapData.TileSetTexture2D = pTexture;
            mapData.BackLayers.Add(defaultLayer);
            mapData.CollisionLayers.Add(collisions);
            mapData.CurrentCollisionLayer = collisions;

            return mapData;
        }

        private static MapLayer GetConvertedLayer(string pId, SectorData pCurrentSector, CellData[,] autoTiledMap)
        {
            var mapLayerTiles = ConvertToTileList(pCurrentSector, autoTiledMap);

            var defaultLayer = new MapLayer()
            {
                IsVisible = true,
                Identifier = pId,
                Tiles = mapLayerTiles
            };
            return defaultLayer;
        }

        //        private static TileItem[,] GetConvertedTiles(SectorData currentSector)
        //        {
        //            var nbCellX = currentSector.Quadrants[0, 0].NbCellX;
        //            var nbCellY = currentSector.Quadrants[0, 0].NbCellY;
        //            TileItem[,] lstTiles = new TileItem[nbCellX * currentSector.NbQuadX, nbCellY * currentSector.NbQuadY];
        //
        //            for (var y = 0; y < currentSector.NbQuadY; y++)
        //            {
        //                for (var x = 0; x < currentSector.NbQuadX; x++)
        //                {
        //                    var cQuad = currentSector.Quadrants[x, y];
        //
        //                    for (var dY = 0; dY < nbCellY; dY++)
        //                    {
        //                        for (var dX = 0; dX < nbCellX; dX++)
        //                        {
        //                            var cell = cQuad.Cells[dX, dY];
        //
        //                            lstTiles[dX + (x * nbCellX) , dY + (y * nbCellY)] = (new TileItem()
        //                            {
        //                                Gid = cell.GiD,
        //                                Column = (int)cell.GridPos.X,
        //                                Line = (int)cell.GridPos.Y,
        //                                TileSize = currentSector.TileSize,
        //                            });
        //                        }
        //                    }
        //                }
        //            }
        //
        //            return lstTiles;
        //        }

        private static List<TileItem> ConvertToTileList(SectorData currentSector, CellData[,] pMap)
        {
            var lstTiles = new List<TileItem>();

            var nbCellX = currentSector.Quadrants[0, 0].NbCellX;
            var nbCellY = currentSector.Quadrants[0, 0].NbCellY;

            // we iterate the sector for each quad.
            for (var y = 0; y < currentSector.NbQuadY * nbCellY; y++)
            {
                for (var x = 0; x < currentSector.NbQuadX * nbCellX; x++)
                {
                    var cell = pMap[x, y];
                    var tile = new TileItem()
                    {
                        Gid = cell.GiD,
                        Column = (int) cell.GridPos.X,
                        IsDebug = cell.IsDebug,
                        Line = (int) cell.GridPos.Y,
                        TileSize = currentSector.TileSize,
                    };

                    if (!cell.IsWalkable)
                        tile.CurrentType = TileItem.Type.Solid;

                    lstTiles.Add(tile);
                }
            }

            return lstTiles;
        }
    }
}