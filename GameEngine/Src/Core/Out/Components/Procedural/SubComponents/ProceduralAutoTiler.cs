﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Managers;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class ProceduralAutoTiler
    {
        private Dictionary<int, int> EightBitsAssignments { get; set; }
        private Dictionary<int, int> FourBitsAssignments { get; set; }

        public ProceduralAutoTiler()
        {
            #region Indoor

            #region Eight bits index

            EightBitsAssignments = new Dictionary<int, int>()
            {
                {255, ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap) },
                {254,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomRightCornerIn)},
                {251,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomLeftCornerIn)},
                {250,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {248,  ProceduralManager.GetInDoorGid(IndoorTileId.WallTop)},
                {223,  ProceduralManager.GetInDoorGid(IndoorTileId.TopRightCornerIn)},
                {222,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {219,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {218,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {216,  ProceduralManager.GetInDoorGid(IndoorTileId.TopLeftCornerOut)},
                {214,  ProceduralManager.GetInDoorGid(IndoorTileId.WallLeft)},
                {210,  ProceduralManager.GetInDoorGid(IndoorTileId.LeftJunction)},        // Buggy
                {208,  ProceduralManager.GetInDoorGid(IndoorTileId.TopLeftCornerOut)},
                {127,  ProceduralManager.GetInDoorGid(IndoorTileId.TopLeftCornerIn)},
                {126,  ProceduralManager.GetInDoorGid(IndoorTileId.MidJunction)},                // Buggy
                {123,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {122,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {120,  ProceduralManager.GetInDoorGid(IndoorTileId.TopJunctionLeft)},
                {107,  ProceduralManager.GetInDoorGid(IndoorTileId.WallRight)},
                {106,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {104,  ProceduralManager.GetInDoorGid(IndoorTileId.TopRightCornerOut)},
                {95,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {94,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {91,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {90,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {88,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {86,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomLeftCornerOut)},
                {82,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {80,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {75,  ProceduralManager.GetInDoorGid(IndoorTileId.RightJunction)},
                {74,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {72,  ProceduralManager.GetInDoorGid(IndoorTileId.TopRightCornerOut)},
                {66,  ProceduralManager.GetInDoorGid(IndoorTileId.TopDeadEnd)},
                {64,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {31,  ProceduralManager.GetInDoorGid(IndoorTileId.WallBottom)},
                {30,  ProceduralManager.GetInDoorGid(IndoorTileId.WallBottom)},
                {27,  ProceduralManager.GetInDoorGid(IndoorTileId.WallBottom)},
                {26,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {24,  ProceduralManager.GetInDoorGid(IndoorTileId.WallTop)},
                {22,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomLeftCornerOut)},
                {18,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {16,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                //
                {11,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomRightCornerOut)},
                {10,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {8,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},
                {2,  ProceduralManager.GetInDoorGid(IndoorTileId.BottomDeadEnd)},
                {0,  ProceduralManager.GetInDoorGid(IndoorTileId.OutOfMap)},                 //
            };

            #endregion

            #endregion

            #region OutDoor

            //            #region Eight bits index
            //
            //            EightBitsAssignments = new Dictionary<int, int>()
            //            {
            //                {255, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //                {254,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomRightCornerIn)},
            //                {251,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomLeftCornerIn)},
            //                {250,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {248,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallTop)},
            //                {223,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopRightCornerIn)},
            //                {222,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {219,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {218,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {216,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopLeftCornerOut)},
            //                {214,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallLeft)},
            //                {210,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopLeftCornerOut)},        // Buggy
            //                {208,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopLeftCornerOut)},
            //                {127,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopLeftCornerIn)},
            //                {126,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomLeftCornerOut)},                // Buggy
            //                {123,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {122,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {120,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {107,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallRight)},
            //                {106,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {104,  ProceduralManager.GetOutDoorGid(OutDoorTileId.TopRightCornerOut)},
            //                {95,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {94,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {91,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {90,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {88,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {86,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {82,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {80,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {75,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomRightCornerOut)},
            //                {74,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {72,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {66,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {64,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {31,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallBottom)},
            //                {30,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallBottom)},
            //                {27,  ProceduralManager.GetOutDoorGid(OutDoorTileId.WallBottom)},
            //                {26,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {24,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {22,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomLeftCornerOut)},
            //                {18,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {16,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                //
            //                {11,  ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomRightCornerOut)},
            //                {10,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {8,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {2,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},
            //                {0,  ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass)},                 //
            //            };
            //
            //            #endregion
            //
            //            #region Four bits tile index
            //
            //            FourBitsAssignments = new Dictionary<int, int>()
            //            {
            //
            //                {15, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //                {14, ProceduralManager.GetOutDoorGid(OutDoorTileId.WallTop) },
            //                {13, ProceduralManager.GetOutDoorGid(OutDoorTileId.WallLeft) },
            //                {12, ProceduralManager.GetOutDoorGid(OutDoorTileId.TopLeftCornerOut) },
            //                {11, ProceduralManager.GetOutDoorGid(OutDoorTileId.WallRight) },
            //                {10, ProceduralManager.GetOutDoorGid(OutDoorTileId.TopRightCornerOut) },
            //                {9, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //                {7, ProceduralManager.GetOutDoorGid(OutDoorTileId.WallBottom) },
            //                {6, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //                {5, ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomLeftCornerOut) },
            //                {4, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //                {3, ProceduralManager.GetOutDoorGid(OutDoorTileId.BottomRightCornerOut) },
            //                {1, ProceduralManager.GetOutDoorGid(OutDoorTileId.Grass) },
            //            };
            //
            //#endregion

            #endregion
        }

        public CellData[,] AutoTileInDoor(AutoTileType pType, CellData[,] pMap, int pLimitX, int pLimitY)
        {
            for (var x = 1; x < pLimitX - 1; x++)
            {
                for (var y = 1; y < pLimitY - 1; y++)
                {
                    var cell = pMap[x, y];
                    if (cell.IsWalkable)
                    {
                        cell.GiD = ProceduralManager.GetInDoorGid(IndoorTileId.Floor); // Implement a variant of the loot system to get varied ground tiles.
                        //                        cell.IsDebug = true;
                        continue;
                    }

                    var index = 0;
                    switch (pType)
                    {
                        case AutoTileType.EightBits:
                            index = GetEightBitsTileIndex(pMap, x, y);
                            cell.GiD = EightBitsAssignments[index];
                            break;
                        case AutoTileType.FourBits:
                            index = GetFourBitsTileIndex(pMap, x, y);
                            cell.GiD = FourBitsAssignments[index];
                            break;
                    }
//                    cell.IsDebug = true;
                    //                    if (index == 255)
                    //                        cell.IsDebug = true;
                }
            }

            return pMap;
        }

        public void AutoTileOutDoor(AutoTileType pType, CellData[,] pMap, int pLimitX, int pLimitY)
        {
            for (var x = 1; x < pLimitX - 1; x++)
            {
                for (var y = 1; y < pLimitY - 1; y++)
                {
                    var cell = pMap[x, y];
                    if (cell.IsWalkable)
                    {
                        cell.IsDebug = false;
                        continue;
                    }
                    var index = 0;

                    //                    cell.IsDebug = true;
                    switch (pType)
                    {
                        case AutoTileType.EightBits:
                            index = GetEightBitsTileIndex(pMap, x, y);
                            cell.GiD = EightBitsAssignments[index];
                            break;
                        case AutoTileType.FourBits:
                            index = GetFourBitsTileIndex(pMap, x, y);
                            cell.GiD = FourBitsAssignments[index];
                            break;
                    }

                    //                    if (index == 210)
                    //                        cell.IsDebug = true;
                }
            }
        }

        public static int GetFourBitsTileIndex(CellData[,] pMap, int x, int y)
        {
            var north = CheckWalkable(pMap, x, y - 1);
            var south = CheckWalkable(pMap, x, y + 1);
            var west = CheckWalkable(pMap, x - 1, y);
            var east = CheckWalkable(pMap, x + 1, y);

            return north +
                   2 * west +
                   4 * east +
                   8 * south;
        }

        public static int GetEightBitsTileIndex(CellData[,] pMap, int x, int y)
        {
            var north = CheckWalkable(pMap, x, y - 1);
            var south = CheckWalkable(pMap, x, y + 1);
            var west = CheckWalkable(pMap, x - 1, y);
            var east = CheckWalkable(pMap, x + 1, y);

            var northWest = CheckWalkable(pMap, x - 1, y - 1);
            if (north == 0 || west == 0)
                northWest = 0;

            var northEast = CheckWalkable(pMap, x + 1, y - 1);
            if (north == 0 || east == 0)
                northEast = 0;

            var southWest = CheckWalkable(pMap, x - 1, y + 1);
            if (south == 0 || west == 0)
                southWest = 0;

            var southEast = CheckWalkable(pMap, x + 1, y + 1);
            if (south == 0 || east == 0)
                southEast = 0;

            return northWest +
                   2 * north +
                   4 * northEast +
                   8 * west +
                   16 * east +
                   32 * southWest +
                   64 * south +
                   128 * southEast;
        }

        private static int CheckWalkable(CellData[,] pMap, int pX, int pY)
        {
            return pMap[pX, pY].IsWalkable ? 0 : 1;
        }
    }
}