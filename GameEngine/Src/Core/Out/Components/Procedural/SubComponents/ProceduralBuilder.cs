﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class ProceduralBuilder
    {

        #region Declarations

        private bool IsTrimmed { get; set; }
        private const int ConnectionW = 4;
        private const int ConnectionH = 4;
        private float ChanceToTurn { get; }
        private float SpawnChance { get; }
        private int MaxIterations { get; }
        private int MaxWalkers { get; }
        private int NbCellX { get; set; }
        private int NbCellY { get; set; }
        private int NbQuadX { get; set; }
        private int NbQuadY { get; set; }
        private int TileSize { get; set; }
        private List<Walker> Walkers { get; set; }
        private SectorData Data { get; set; }
        private System.Random Random { get; }

        #endregion

        public ProceduralBuilder(System.Random pRandom)
        {
            Random = pRandom;
            ChanceToTurn = 100f;
            SpawnChance = 0.0005f;
            MaxIterations = 250;
            MaxWalkers = 10;
            Walkers = new List<Walker>();
        }

        public SectorData GetRawSector(
            int pNbQuadX, int pNbQuadY,
            int pNbCellX, int pNbCellY,
            int pTileSize)
        {
            NbCellX = pNbCellX;
            NbCellY = pNbCellY;
            NbQuadX = pNbQuadX;
            NbQuadY = pNbQuadY;
            TileSize = pTileSize;

            InitSector();
            GenerateSector();
            FlagConnections();
            InitializeQuads();
            ConnectSectors();
            BuildRooms();

            while (!IsTrimmed)
            {
                TrimRooms();
            }

            return Data;
        }

        #region Sector

        public void AddConnection(QuadData pData, Rectangle pRect)
        {
            for (var x = 0; x < pRect.Width; x++)
            {
                for (var y = 0; y < pRect.Height; y++)
                {
                    pData.SetCellId(pRect.X + x, pRect.Y + y, ProceduralManager.GetInDoorGid(IndoorTileId.Floor));
                    pData.Cells[pRect.X + x, pRect.Y + y].IsWalkable = true;
                }
            }
        }

        private void ConnectSectors()
        {
            var startX = Random.Next(NbCellX / 2 - ConnectionW, NbCellX / 2 + ConnectionH);
            var startY = Random.Next(NbCellY / 2 - ConnectionW, NbCellY / 2 + ConnectionH);
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var quad = Data.Quadrants[x, y];
                    if (!quad.IsActive)
                        continue;

                    if (quad.IsOpenTop)
                    {
                        for (var tY = 0; tY < NbCellY / 2f + ConnectionH; tY += ConnectionH)
                        {
                            AddConnection(quad, new Rectangle(startX, tY, ConnectionW, ConnectionH));
                        }
                    }

                    if (quad.IsOpenBottom)
                    {
                        for (int tY = NbCellY - ConnectionW; tY >= NbCellY / 2f - ConnectionH; tY -= ConnectionH)
                        {
                            AddConnection(quad, new Rectangle(startX, tY, ConnectionW, ConnectionH));
                        }
                    }
                    if (quad.IsOpenLeft)
                    {
                        for (int tY = 0; tY < NbCellX / 2 + ConnectionW; tY += ConnectionW)
                        {
                            AddConnection(quad, new Rectangle(tY, startY, ConnectionW, ConnectionH));
                        }

                    }
                    if (quad.IsOpenRight)
                    {
                        for (int tY = NbCellX - ConnectionW; tY >= NbCellX / 2 - ConnectionW; tY -= ConnectionH)
                        {
                            AddConnection(quad, new Rectangle(tY, startY, ConnectionW, ConnectionH));
                        }
                    }
                }
            }
        }

        private void FlagConnections()
        {
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var currentQuad = Data.Quadrants[x, y];
                    if (!currentQuad.IsActive)
                        continue;

                    // Top
                    var dY = y - 1;
                    if (dY >= 0)
                    {
                        var topQuad = Data.Quadrants[x, dY];
                        if (topQuad.IsActive)
                        {
                            topQuad.IsOpenBottom = true;
                            currentQuad.IsOpenTop = true;
                        }
                    }

                    // Bottom
                    dY = y + 1;
                    if (dY <= NbQuadY - 1)
                    {
                        var bottomQuad = Data.Quadrants[x, y + 1];
                        if (bottomQuad.IsActive)
                        {
                            bottomQuad.IsOpenTop = true;
                            currentQuad.IsOpenBottom = true;
                        }
                    }

                    // Left
                    var dX = x - 1;
                    if (dX >= 0)
                    {
                        var leftQuad = Data.Quadrants[dX, y];
                        if (leftQuad.IsActive)
                        {
                            leftQuad.IsOpenRight = true;
                            currentQuad.IsOpenLeft = true;
                        }
                    }

                    // Right
                    dX = x + 1;
                    if (dX <= NbQuadX - 1)
                    {
                        var rightQuad = Data.Quadrants[dX, y];
                        if (rightQuad.IsActive)
                        {
                            rightQuad.IsOpenLeft = true;
                            currentQuad.IsOpenRight = true;
                        }
                    }
                }
            }
        }

        private void GenerateSector()
        {
            var id = 1;
            List<Vector2> positions = new List<Vector2>();

            // find an activate first quad
            var firstX = Random.Next(NbQuadX);
            var firstPos = new Vector2(firstX, 0);
            var firstQuadrant = Data.Quadrants[(int)firstPos.X, (int)firstPos.Y];
            firstQuadrant.Id = id;
            firstQuadrant.IsActive = true;

            // create the walker
            Walker walker = new Walker(new Vector2(firstPos.X, firstPos.Y));

            // and add the position to the list
            positions.Add(firstPos);

            // while the walker has not reached the bottom of the
            // sector, we iterate
            while (walker.Position.Y < NbQuadY - 1)
            {
                var num = Random.Next(3);
                if (num == 0)
                {
                    walker.Position += new Vector2(1, 0);
                }
                else if (num == 1)
                {
                    walker.Position += new Vector2(-1, 0);
                }
                else if (num == 2)
                {
                    if (walker.Position.Y >= NbQuadY - 2 &&
                        ((int)walker.Position.X == 0 || (int)walker.Position.X == NbQuadX - 1))
                        continue;

                    walker.Position += new Vector2(0, 1);
                }

                walker.Position = Vector2.Clamp(
                    walker.Position,
                    new Vector2(0, 0),
                    new Vector2(NbQuadX - 1, NbQuadY - 1));

                // We check for openings
                if (!positions.Contains(walker.Position))
                {
                    id++;
                    var currentQuad = Data.Quadrants[(int)(walker.Position.X), (int)(walker.Position.Y)];
                    currentQuad.Id = id;
                    currentQuad.IsActive = true;
                    positions.Add(walker.Position);
                }
            }

            Data.NbActive = positions.Count;
            if (Data.NbActive <= 5)
                Data.CurrentSize = SectorSize.Small;
            else if (Data.NbActive > 5 && Data.NbActive <= 9)
                Data.CurrentSize = SectorSize.Medium;
            else Data.CurrentSize = SectorSize.Big;
        }

        private void InitSector()
        {
            Data = new SectorData(NbQuadX, NbQuadY, TileSize)
            { Quadrants = new QuadData[NbQuadX, NbQuadY] };

            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    Data.Quadrants[x, y] = new QuadData(new Vector2(x, y), NbCellX, NbCellY, TileSize);
                }
            }
        }

        public void SetNewSectorDimensions(int pNbQuadX, int pNbQuadY)
        {
            NbQuadX = pNbQuadX;
            NbQuadY = pNbQuadY;
        }

        #endregion


        #region Quadrant

        private void BuildRooms()
        {
            var startX = Random.Next(NbCellX / 2 - 4, NbCellX / 2 + 4);
            var startY = Random.Next(NbCellY / 2 - 4, NbCellY / 2 + 4);
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var quad = Data.Quadrants[x, y];
                    if (!quad.IsActive)
                        continue;

                    Walkers = new List<Walker>();
                    if (quad.IsOpenTop)
                    {
                        for (var tY = 0; tY < NbCellY / 2f + ConnectionH; tY += ConnectionH)
                        {
                            ExpandFromConnection(quad, new Vector2(startX, 0 + tY));
                        }
                    }

                    if (quad.IsOpenBottom)
                    {
                        for (var tY = NbCellY - ConnectionH; tY >= NbCellY / 2f - ConnectionH; tY -= ConnectionH)
                        {
                            ExpandFromConnection(quad, new Vector2(startX, tY));
                        }
                    }
                    if (quad.IsOpenLeft)
                    {
                        for (var tX = 0; tX < NbCellX / 2 + ConnectionW; tX += ConnectionW)
                        {
                            ExpandFromConnection(quad, new Vector2(0 + tX, startY));
                        }

                    }
                    if (quad.IsOpenRight)
                    {
                        for (var tX = NbCellX - ConnectionW; tX >= NbCellX / 2 - ConnectionW; tX -= ConnectionW)
                        {
                            ExpandFromConnection(quad, new Vector2(tX, startY));
                        }
                    }
                }
            }
        }

        public void ExpandFromConnection(
            QuadData pData, Vector2 pPos)
        {
            Walkers.Clear();
            var iterations = 0;
            var pos = new Vector2(
                (int)pPos.X,
                (int)pPos.Y);
            Walkers.Add(new Walker(pos));

            pData.SetCellId((int)pos.X, (int)pos.Y, ProceduralManager.GetInDoorGid(IndoorTileId.Floor));
            pData.Cells[(int)pos.X, (int)pos.Y].IsWalkable = true;

            while (iterations < MaxIterations * 2)
            {
                for (var i = 0; i < Walkers.Count; i++)
                {
                    var walker = Walkers[i];
                    var num = Random.Next(4);
                    if (Random.Next(100) <= ChanceToTurn)
                    {
                        switch (num)
                        {
                            case 0:
                                walker.Position += new Vector2(1, 0);
                                break;
                            case 1:
                                walker.Position += new Vector2(0, 1);
                                break;
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                        }
                    }
                    else
                    {
                        switch (num)
                        {
                            case 2:
                                walker.Position += new Vector2(-1, 0);
                                break;
                            case 3:
                                walker.Position += new Vector2(0, -1);
                                break;
                        }
                    }

                    walker.Position = Vector2.Clamp(
                        walker.Position,
                        new Vector2(1, 1),
                        new Vector2(NbCellX - 2, NbCellY - 2));

                    // Set the cell not solid
                    pData.SetCellId((int)walker.Position.X, (int)walker.Position.Y, ProceduralManager.GetInDoorGid(IndoorTileId.Floor));
                    pData.Cells[(int)walker.Position.X, (int)walker.Position.Y].IsWalkable = true;

                    // Chance to spawn a new walker
                    if (RandF() < SpawnChance && Walkers.Count < MaxWalkers)
                    {
                        Walkers.Add(new Walker(walker.Position));
                    }

                    // Chance to delete a walker
                    if (RandF() < SpawnChance / 2 && Walkers.Count > 1)
                    {
                        Walkers.Remove(walker);
                    }

                    iterations += 1;
                }
            }
        }

        private void InitializeQuads()
        {
            foreach (var cellData in Data.Quadrants)
            {
                cellData.Cells = new CellData[NbCellX, NbCellY];
                for (var x = 0; x < NbCellX; x++)
                {
                    for (var y = 0; y < NbCellY; y++)
                    {
                        cellData.Cells[x, y] = new CellData(new Vector2(x, y), TileSize)
                        { GiD = 0, IsWalkable = false };
                    }
                }
            }
        }

        private void TrimRooms()
        {
            var trimmedCounter = 0;

            // Quads iteration
            for (var x = 0; x < NbQuadX; x++)
            {
                for (var y = 0; y < NbQuadY; y++)
                {
                    var quad = Data.Quadrants[x, y];
                    if (!quad.IsActive)
                        continue;

                    // Cells iteration
                    for (var cX = 1; cX < quad.NbCellX - 1; cX++)
                    {
                        for (var cY = 1; cY < quad.NbCellY - 1; cY++)
                        {
                            var cell = quad.Cells[cX, cY];
                            if (cell.IsWalkable)
                                continue;

                            var top = quad.Cells[cX, cY - 1];
                            var bottom = quad.Cells[cX, cY + 1];
                            var left = quad.Cells[cX - 1, cY];
                            var right = quad.Cells[cX + 1, cY];
                            var topLeft = quad.Cells[cX - 1, cY - 1];
                            var topRight = quad.Cells[cX + 1, cY - 1];
                            var bottomLeft = quad.Cells[cX - 1, cY + 1];
                            var bottomRight = quad.Cells[cX + 1, cY + 1];

                            // eliminate single cells
                            var condition1 = (top.IsWalkable &&
                                              bottom.IsWalkable);

                            var condition2 = (left.IsWalkable &&
                                              right.IsWalkable);

                            var condition3 = (bottomLeft.IsWalkable &&
                                              topRight.IsWalkable);

                            var condition4 = (topLeft.IsWalkable &&
                                              bottomRight.IsWalkable);

                            if (condition1 || condition2 || condition3 || condition4)
                            {
                                cell.GiD = ProceduralManager.GetInDoorGid(IndoorTileId.Floor);
                                cell.IsWalkable = true;
                                trimmedCounter++;
                            }
                        }
                    }
                }
            }

            if (trimmedCounter == 0)
                IsTrimmed = true;
        }

        #endregion


        #region Helper Methods

        private float RandF()
        {
            return Random.Next(1000) / 1000f;
        }

        public SectorData GetNew()
        {
            InitSector();
            GenerateSector();
            FlagConnections();
            InitializeQuads();
            ConnectSectors();
            BuildRooms();

            while (!IsTrimmed)
            {
                TrimRooms();
            }

            return Data;
        }

        public static CellData[,] ConvertToMap(SectorData pData)
        {
            var nbCellX = pData.Quadrants[0, 0].NbCellX;
            var nbCellY = pData.Quadrants[0, 0].NbCellY;
            var mapWidth = pData.NbQuadX * nbCellX;
            var mapHeight = pData.NbQuadY * nbCellY;

            var map = new CellData[mapWidth, mapHeight];

            for (var quadX = 0; quadX < pData.NbQuadX; quadX++)
            {
                for (var quadY = 0; quadY < pData.NbQuadY; quadY++)
                {
                    var quad = pData.Quadrants[quadX, quadY];

                    for (var cellX = 0; cellX < nbCellX; cellX++)
                    {
                        for (var cellY = 0; cellY < nbCellY; cellY++)
                        {
                            var cX = quadX * nbCellX + cellX;
                            var cY = quadY * nbCellY + cellY;
                            map[cX, cY] = quad.Cells[cellX, cellY];
                        }
                    }
                }
            }

            return map;
        }
        
        #endregion
    }
}