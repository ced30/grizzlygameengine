﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class ProceduralDebugger
    {

        private MapDebugState CurrentState { get; set; }
        private const float Zoom = 3;
        private int OverviewDrawSize => Sectors.TileSize / 4;
        private int MapDrawSize => Sectors.TileSize / 8;
        private Vector2 Counter { get; set; }
        private QuadData CurrentQuadrant { get; set; }
        private SectorData Sectors { get; set; }
        private CellData[,] Map { get; set; }

        public ProceduralDebugger(SectorData pData, CellData[,] currentMap)
        {
            Sectors = pData;
            SetMap(currentMap);
            SetCurrentQuad(Vector2.Zero);

            CurrentState = MapDebugState.OverView;
        }

        public void SetCurrentQuad(Vector2 pPos)
        {
            Counter += pPos;
            if (Counter.X < 0)
                Counter = new Vector2(Sectors.NbQuadX - 1, Counter.Y);
            if (Counter.X > Sectors.NbQuadX - 1)
                Counter = new Vector2(0, Counter.Y);
            if (Counter.Y < 0)
                Counter = new Vector2(Counter.X, Sectors.NbQuadY - 1);
            if (Counter.Y > Sectors.NbQuadY - 1)
                Counter = new Vector2(Counter.X, 0);

            CurrentQuadrant = Sectors.Quadrants[(int)Counter.X, (int)Counter.Y];
        }

        public void Draw(SpriteBatch pSpriteBatch, SpriteFont pFont)
        {
            if (CurrentState == MapDebugState.OverView)
            {
                DrawCurrentQuad(pSpriteBatch);
                DrawSector(pSpriteBatch, pFont);
            }
            else if (CurrentState == MapDebugState.MapView)
            {
                DrawMap(pSpriteBatch);
            }
        }

        public void DrawCurrentQuad(SpriteBatch pSpriteBatch)
        {
            if (CurrentQuadrant.Cells == null)
                return;

            for (var x = 0; x < CurrentQuadrant.NbCellX; x++)
            {
                for (var y = 0; y < CurrentQuadrant.NbCellY; y++)
                {
                    Primitives2D.Primitives2D.DrawRectangleFilled(
                        pSpriteBatch,
                        new Rectangle(
                            x * OverviewDrawSize + Camera.Camera.Instance.WorldRectangle.Left + Screen.Screen.Width / 3,
                            y * OverviewDrawSize + Camera.Camera.Instance.WorldRectangle.Top,
                            OverviewDrawSize,
                            OverviewDrawSize),
                        CurrentQuadrant.Cells[x, y].IsWalkable ? Color.Gray : Color.Green);
                }
            }
        }

        public void DrawMap(SpriteBatch pSpriteBatch)
        {
            var nbCellX = Sectors.Quadrants[0, 0].NbCellX;
            var nbCellY = Sectors.Quadrants[0, 0].NbCellY;
            var width = Sectors.NbQuadX * nbCellX;
            var height = Sectors.NbQuadY * nbCellY;

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    var cell = Map[x, y];

                    Primitives2D.Primitives2D.DrawRectangleFilled(
                        pSpriteBatch,
                        new Rectangle(
                            x * MapDrawSize + Camera.Camera.Instance.WorldRectangle.Left,
                            y * MapDrawSize + Camera.Camera.Instance.WorldRectangle.Top,
                            MapDrawSize,
                            MapDrawSize),
                        cell.IsWalkable ? Color.Gray : Color.Green);
                }
            }
        }

        public void DrawSector(SpriteBatch pSpriteBatch, SpriteFont pFont)
        {
            for (var x = 0; x < Sectors.NbQuadX; x++)
            {
                for (var y = 0; y < Sectors.NbQuadY; y++)
                {
                    if (Sectors.Quadrants[x, y].IsActive)
                    {
                        // Draw the quadrant
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                (int)(x * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Left,
                                (int)(y * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Top,
                                (int)(Sectors.TileSize * Zoom),
                                (int)(Sectors.TileSize * Zoom)),
                            Color.Gray);

                        // Draw the outline
                        Primitives2D.Primitives2D.DrawRectangle(
                            pSpriteBatch,
                            new Rectangle(
                                (int)(x * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Left,
                                (int)(y * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Top,
                                (int)(Sectors.TileSize * Zoom),
                                (int)(Sectors.TileSize * Zoom)),
                            Color.Black, 1);


                        var strColor = Color.White;
                        if (Counter == new Vector2(x, y))
                            strColor = Color.Red;

                        // Draw the quad ID
                        pSpriteBatch.DrawString(
                            pFont,
                            Sectors.Quadrants[x, y].Id.ToString(),
                            new Vector2(
                                (x * Sectors.TileSize * Zoom) + Sectors.TileSize + Camera.Camera.Instance.WorldRectangle.Left,
                                (y * Sectors.TileSize * Zoom) + Sectors.TileSize + Camera.Camera.Instance.WorldRectangle.Top),
                            strColor);

                        DrawQuadsOutLines(pSpriteBatch, x, y);
                    }
                    else
                    {
                        var emptyColor = Color.Green;
                        if (Counter == new Vector2(x, y))
                            emptyColor = Color.Red;
                        Primitives2D.Primitives2D.DrawRectangleFilled(
                            pSpriteBatch,
                            new Rectangle(
                                (int)(x * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Left,
                                (int)(y * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Top,
                                (int)(Sectors.TileSize * Zoom),
                                (int)(Sectors.TileSize * Zoom)),
                            emptyColor);
                    }
                }
            }

            DrawSectorInfo(pSpriteBatch, pFont);
        }

        private void DrawSectorInfo(SpriteBatch pSpriteBatch, SpriteFont pFont)
        {
            pSpriteBatch.DrawString(
                pFont,
                "   Active quadrants: " + Sectors.NbActive + "\n" + "\n" +
                "   Inactive quadrants: " + ((Sectors.NbQuadX * Sectors.NbQuadY) - Sectors.NbActive) + "\n" + "\n" +
                "   Size: " + Sectors.CurrentSize + "\n" + "\n" +
                "   CurrentQuadrant ID: " + CurrentQuadrant.Id + ",    IsActive: " + CurrentQuadrant.IsActive + "\n" + "\n" +
                "   Nb horizontal cells: " + Sectors.Quadrants[0, 0].NbCellX + "\n" + "\n" +
                "   Nb vertical cells: " + Sectors.Quadrants[0, 0].NbCellY,
                new Vector2(
                    0 + Camera.Camera.Instance.WorldRectangle.Left, 
                    (Sectors.NbQuadY * Sectors.TileSize * Zoom) * 0.9f + (Sectors.TileSize * Zoom) - Sectors.TileSize + Camera.Camera.Instance.WorldRectangle.Top),
                Color.White);
        }

        private void DrawQuadsOutLines(SpriteBatch pSpriteBatch, int x, int y)
        {
            var width = Sectors.TileSize * Zoom / 2;
            var height = Sectors.TileSize * Zoom / 2;
            var color = Color.Yellow;

            if (Sectors.Quadrants[x, y].IsOpenTop)
            {
                // Draw Top
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (int)(x * Sectors.TileSize * Zoom + width / 2) + Camera.Camera.Instance.WorldRectangle.Left,
                        (int)(y * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Top,
                        (int)width,
                        0),
                    color, 1);
            }

            if (Sectors.Quadrants[x, y].IsOpenBottom)
            {
                // Draw bottom
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (int)(x * Sectors.TileSize * Zoom + width / 2) + Camera.Camera.Instance.WorldRectangle.Left,
                        (int)(y * Sectors.TileSize * Zoom + Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Top,
                        (int)width,
                        0),
                    color, 1);

            }

            if (Sectors.Quadrants[x, y].IsOpenLeft)
            {
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (int)(x * Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Left,
                        (int)(y * Sectors.TileSize * Zoom + height / 2) + Camera.Camera.Instance.WorldRectangle.Top,
                        0,
                        (int)height),
                    color, 1);
            }

            if (Sectors.Quadrants[x, y].IsOpenRight)
            {
                Primitives2D.Primitives2D.DrawRectangle(
                    pSpriteBatch,
                    new Rectangle(
                        (int)(x * Sectors.TileSize * Zoom + Sectors.TileSize * Zoom) + Camera.Camera.Instance.WorldRectangle.Left,
                        (int)(y * Sectors.TileSize * Zoom + height / 2) + Camera.Camera.Instance.WorldRectangle.Top,
                        0,
                        (int)height),
                    color, 1);
            }
        }


        #region Helper methods

        public void SetMap(CellData[,] pMap)
        {
            Map = pMap;
        }

        public void SetSector(SectorData pSectors)
        {
            Sectors = pSectors;
            SetMap(ProceduralBuilder.ConvertToMap(Sectors));
            SetCurrentQuad(Vector2.Zero);
        }

        public void ToggleState()
        {
            var length = Enum.GetNames(typeof(MapDebugState)).Length;
            var counter = (int)CurrentState;
            counter++;
            if (counter > length - 1)
                counter = 0;

            CurrentState = (MapDebugState)counter;
        }

        #endregion

    }
}