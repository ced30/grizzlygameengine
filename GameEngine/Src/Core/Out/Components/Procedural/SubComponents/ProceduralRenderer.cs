﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Game;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class ProceduralRenderer : GrizzlyComponent
    {
        private SpriteFont Font { get; set; }
        public GameData Data { get; set; }
        private ProceduralManager MapGenerator { get; set; }
        public FadeEffect.FadeEffect Fade { get; set; }
        public ParticleManager Particles { get; set; }
        public RenderTarget2D DebugLayer { get; set; }
        public RenderTarget2D BackLayer { get; set; }
        public RenderTarget2D MidLayer { get; set; }
        public RenderTarget2D MergedLayers { get; set; }

        public ProceduralRenderer(
            GameData pData, ParticleManager pParticle,
            FadeEffect.FadeEffect pFade, ProceduralManager pGenerator)
        {
            Data = pData;
            Particles = pParticle;
            Fade = pFade;
            MapGenerator = pGenerator;
        }

        #region Initialization  

        public override void LoadContent()
        {
            InitRenderTargets();
            base.LoadContent();
        }

        private void InitRenderTargets()
        {
            PresentationParameters pp =
               Data.Graphics.GraphicsDevice.PresentationParameters;

            // back layer.
            BackLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Screen.Screen.Width, Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // mid layer.
            MidLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Screen.Screen.Width, Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // debug layer.
            DebugLayer = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Screen.Screen.Width, Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);

            // Total of all layers.
            MergedLayers = new RenderTarget2D(Data.Graphics.GraphicsDevice,
                Screen.Screen.Width, Screen.Screen.Height,
                false,
                SurfaceFormat.Color,
                DepthFormat.None,
                pp.MultiSampleCount,
                RenderTargetUsage.DiscardContents);
        }

        #endregion

        /// <summary>
        /// The main Draw Sequence.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pList"></param>
        /// <param name="pData"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch, List<IEntity> pList, ProceduralMapData pData)
        {
            DrawBackLayer(pSpriteBatch, pData);
            DrawSprites(pSpriteBatch, pList);
            //            DrawGeneratorDebug(pSpriteBatch);
            SuperPoseRenderTargets(pSpriteBatch);

            Resize(pSpriteBatch, MergedLayers);
        }

        /// <summary>
        /// The mid layer is the entities layer, it's drawn onto the backBuffer and we use spriteEffect Immediate,
        /// otherwise the shader will not work.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public void DrawBackLayer(SpriteBatch pSpriteBatch, ProceduralMapData pData)
        {
            BeginDraw(
                pSpriteBatch, BackLayer,
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend);

            if (pData.BackLayers.Count > 0)
            {
                DrawTileLayer(pSpriteBatch, pData.BackLayers, pData);
            }

            //            if (pList != null &&
            //                pList.Count > 0)
            //            {
            //
            //                for (int i = 0; i < pList.Count; i++)
            //                {
            //                    var sprite = pList[i];
            //
            //                    // if the entity is not on camera, we don't draw it.
            //                    if (Camera.Camera.Instance.DrawRectangle.Intersects(sprite.Data.BoundingBox))
            //                    {
            //                        sprite.Draw(pSpriteBatch);
            //                    }
            //                }
            //            }


            CloseDraw(pSpriteBatch);
        }

        /// <summary>
        /// The mid layer is the entities layer, it's drawn onto the backBuffer and we use spriteEffect Immediate,
        /// otherwise the shader will not work.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pList"></param>
        public void DrawSprites(SpriteBatch pSpriteBatch, List<IEntity> pList)
        {
            BeginDraw(
                pSpriteBatch, MidLayer,
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend);

            if (pList != null &&
                pList.Count > 0)
            {

                for (var i = pList.Count - 1; i >= 0; i--)
                {
                    var sprite = pList[i];

                    // if the entity is not on camera, we don't draw it.
                    if (Camera.Camera.Instance.DrawRectangle.Intersects(sprite.Data.BoundingBox))
                    {
                        sprite.Draw(pSpriteBatch);
                        if(sprite.Id.Equals("player"))
                            pSpriteBatch.DrawString(
                                Font,
                                "X: " + (int)sprite.Data.Position.X + "\n" +
                                "Y: " + (int)sprite.Data.Position.Y,
                                Camera.Camera.Instance.Position,
                                Color.White);
                    }
                }
            }

            CloseDraw(pSpriteBatch);
        }

        /// <summary>
        /// The mid layer is the entities layer, it's drawn onto the backBuffer and we use spriteEffect Immediate,
        /// otherwise the shader will not work.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void DrawGeneratorDebug(SpriteBatch pSpriteBatch)
        {
            BeginDraw(
                pSpriteBatch, DebugLayer,
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend);

            MapGenerator.Draw(pSpriteBatch);

            CloseDraw(pSpriteBatch);
        }

        public static void DrawTileLayer(SpriteBatch pSpriteBatch, List<MapLayer> pLayer, ProceduralMapData pData)
        {
            var nbLayers = pLayer.Count;
            for (var nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                var tLayer = pLayer[nLayer];
                if (!tLayer.IsVisible) continue;

                var columnStart = Camera.Camera.Instance.DrawRectangle.Left / pData.TileWidth;
                var columnEnd = Camera.Camera.Instance.DrawRectangle.Right / pData.TileWidth;
                var lineStart = Camera.Camera.Instance.DrawRectangle.Top / pData.TileHeight;
                var lineEnd = Camera.Camera.Instance.DrawRectangle.Bottom / pData.TileHeight;
                for (var column = columnStart; column <= columnEnd; column++)
                {
                    for (var line = lineStart; line <= lineEnd; line++)
                    {
                        if (column < 0 || column >= pData.MapWidth ||
                            line < 0 || line >= pData.MapHeight)
                            continue;

                        var tile = tLayer.Tiles[column + line * pData.MapWidth];

                        var gid = tile.Gid;
                        if (gid == 0 || !tile.IsVisible) continue;

                        //                                int tileFrame = gid - 1;
                        var tileFrame = gid;

                        if (tile is AnimatedTile tAnimated)
                        {
                            tileFrame = tAnimated.AnimationManager.Animation.CurrentFrame;
                            var tileSetColumn = tileFrame % pData.TileSetColumns;
                            var tileSetLine =
                                (int)Math.Floor(
                                    tileFrame / (double)pData.TileSetColumns
                                );
                            float x = tAnimated.X;
                            float y = tAnimated.Y;
                            var tileSetRec =
                                new Rectangle(
                                    pData.TileWidth * tileSetColumn,
                                    pData.TileHeight * tileSetLine,
                                    pData.TileWidth, pData.TileHeight);

                            pSpriteBatch.Draw(
                                pData.TileSetTexture2D,
                                new Vector2(x, y),
                                tileSetRec, Color.White);
                        }
                        else
                        {
                            var tileSetColumn = tileFrame % pData.TileSetColumns;
                            var tileSetLine =
                                (int)Math.Floor(
                                    tileFrame / (double)pData.TileSetColumns
                                );
                            var x = column * pData.TileWidth;
                            var y = line * pData.TileHeight;
                            var tileSetRec =
                                new Rectangle(
                                    pData.TileWidth * tileSetColumn,
                                    pData.TileHeight * tileSetLine,
                                    pData.TileWidth, pData.TileHeight);

                            // Debug
                            tile.Color = tile.IsDebug ? Color.Red : Color.White;

                            pSpriteBatch.Draw(
                                pData.TileSetTexture2D,
                                new Vector2(x, y),
                                tileSetRec, tile.Color);
                        }
                    }
                }
            }
        }

        public static void DrawLayer(SpriteBatch pSpriteBatch, List<MapLayer> pLayer, ProceduralMapData pData)
        {
            int nbLayers = pLayer.Count;
            for (int nLayer = 0; nLayer < nbLayers; nLayer++)
            {
                var tLayer = pLayer[nLayer];
                if (tLayer.IsVisible)
                {
                    int columnStart = Camera.Camera.Instance.DrawRectangle.Left / pData.TileWidth;
                    int columnEnd = Camera.Camera.Instance.DrawRectangle.Right / pData.TileWidth;
                    int lineStart = Camera.Camera.Instance.DrawRectangle.Top / pData.TileHeight;
                    int lineEnd = Camera.Camera.Instance.DrawRectangle.Bottom / pData.TileHeight;
                    for (int column = columnStart; column <= columnEnd; column++)
                    {
                        for (int line = lineStart; line <= lineEnd; line++)
                        {
                            if (column < 0 || column >= pData.MapWidth ||
                                line < 0 || line >= pData.MapHeight)
                                continue;

                            var tile = tLayer.Tiles[column + line * pData.MapWidth];

                            int gid = tile.Gid;
                            if (gid != 0 && tile.IsVisible)
                            {
                                int tileFrame = gid - 1;

                                if (tile is AnimatedTile tAnimated)
                                {
                                    tileFrame = tAnimated.AnimationManager.Animation.CurrentFrame;
                                    int tileSetColumn = tileFrame % pData.TileSetColumns;
                                    int tileSetLine =
                                        (int)Math.Floor(
                                            tileFrame / (double)pData.TileSetColumns
                                        );
                                    float x = tAnimated.X;
                                    float y = tAnimated.Y;
                                    Rectangle tileSetRec =
                                        new Rectangle(
                                            pData.TileWidth * tileSetColumn,
                                            pData.TileHeight * tileSetLine,
                                            pData.TileWidth, pData.TileHeight);

                                    pSpriteBatch.Draw(
                                        pData.TileSetTexture2D,
                                        new Vector2(x, y),
                                        tileSetRec, Color.White);
                                }
                                else
                                {
                                    int tileSetColumn = tileFrame % pData.TileSetColumns;
                                    int tileSetLine =
                                        (int)Math.Floor(
                                            tileFrame / (double)pData.TileSetColumns
                                        );
                                    int x = column * pData.TileWidth;
                                    int y = line * pData.TileHeight;
                                    Rectangle tileSetRec =
                                        new Rectangle(
                                            pData.TileWidth * tileSetColumn,
                                            pData.TileHeight * tileSetLine,
                                            pData.TileWidth, pData.TileHeight);

                                    pSpriteBatch.Draw(
                                        pData.TileSetTexture2D,
                                        new Vector2(x, y),
                                        tileSetRec, tile.Color);
                                }
                            }
                        }
                    }
                }
            }
        }

        #region Helpers methods

        /// <summary>
        /// Opens the spriteBatch
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pRenderTarget2D"></param>
        /// <param name="pMode"></param>
        /// <param name="pBlend"></param>
        public void BeginDraw(
            SpriteBatch pSpriteBatch,
            RenderTarget2D pRenderTarget2D,
            SpriteSortMode pMode,
            BlendState pBlend)
        {
            Data.Graphics.GraphicsDevice.SetRenderTargets(pRenderTarget2D);
            Data.Graphics.GraphicsDevice.Clear(Color.TransparentBlack);

            pSpriteBatch.Begin(
                pMode,
                pBlend,
                SamplerState.PointClamp, null, null, null,
                Camera.Camera.Instance.Transform.Matrix1);
        }

        /// <summary>
        /// Resize the merged layer.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pTarget"></param>
        public void Resize(SpriteBatch pSpriteBatch, RenderTarget2D pTarget)
        {
            // Now, we draw the render target on the screen
            float ratio = 1;
            int marginV = 0;
            int marginH = 0;
            float currentAspect = Data.Window.ClientBounds.Width / (float)Data.Window.ClientBounds.Height;
            float virtualAspect = (float)Screen.Screen.Width / Screen.Screen.Height;
            if (Screen.Screen.Height != Data.Window.ClientBounds.Height)
            {
                if (currentAspect > virtualAspect)
                {
                    ratio = Data.Window.ClientBounds.Height / (float)Screen.Screen.Height;
                    marginH = (int)((Data.Window.ClientBounds.Width - Screen.Screen.Width * ratio) / 2);
                }
                else
                {
                    ratio = Data.Window.ClientBounds.Width / (float)Screen.Screen.Width;
                    marginV = (int)((Data.Window.ClientBounds.Height - Screen.Screen.Height * ratio) / 2);
                }
            }

            Rectangle dst = new Rectangle(
                marginH,
                marginV,
                (int)(Screen.Screen.Width * ratio),
                (int)(Screen.Screen.Height * ratio));

            pSpriteBatch.Begin(
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointClamp);

            pSpriteBatch.Draw(pTarget, dst, Color.White);

            pSpriteBatch.End();
        }

        /// <summary>
        /// Finally, we draw all the layers on a merged layer, that's the one we will
        /// actually resize and draw on the screen.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void SuperPoseRenderTargets(SpriteBatch pSpriteBatch)
        {
            Data.Graphics.GraphicsDevice.SetRenderTargets(MergedLayers);
            Data.Graphics.GraphicsDevice.Clear(Color.Transparent);

            pSpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);

            pSpriteBatch.Draw(BackLayer, Vector2.Zero, Color.White);
            pSpriteBatch.Draw(MidLayer, Vector2.Zero, Color.White);
            pSpriteBatch.Draw(DebugLayer, Vector2.Zero, Color.White);

            CloseDraw(pSpriteBatch);
        }

        /// <summary>
        /// Closes the spriteBatch.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public void CloseDraw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.End();
            Data.Graphics.GraphicsDevice.SetRenderTarget(null);
        }

        public void SetFont(SpriteFont pFont)
        {
            Font = pFont;
        }

        #endregion
    }
}