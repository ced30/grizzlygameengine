﻿using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents
{
    public class Walker
    {
        public Vector2 Position { get; set; }
        public Walker(Vector2 position)
        {
            Position = position;
        }
    }
}