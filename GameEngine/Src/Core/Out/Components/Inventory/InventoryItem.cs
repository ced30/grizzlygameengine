﻿#region Usings

#endregion

using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;

namespace GrizzlyGameEngine.Core.Out.Components.Inventory
{
    public class InventoryItem
    {
        #region Public

        public IAttributesController Attributes { get; set; }
        public bool IsAvailableInStore { get; set; }
        public bool IsSellAble { get; set; }
        public bool IsStackAble { get; set; }
        public int BuyBackPrice => (int)(SellPrice + SellPrice * 0.1);
        public int BuyPrice { get; set; }
        public int InventorySlot { get; set; }
        public int Quantity { get; set; }
        public int SellPrice => (int)(BuyPrice - BuyPrice * .45f);
        public int Value { get; set; }
        public string Equip { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }

        #endregion

        public InventoryItem()
        {
        }

        public InventoryItem(InventoryItem pCopy)
        {
            Attributes = pCopy.Attributes;
            BuyPrice = pCopy.BuyPrice;
            Equip = pCopy.Equip;
            Id = pCopy.Id;
            IsSellAble = pCopy.IsSellAble;
            InventorySlot = pCopy.InventorySlot;
            IsAvailableInStore = pCopy.IsAvailableInStore;
            IsStackAble = pCopy.IsStackAble;
            Description = pCopy.Description;
            Quantity = pCopy.Quantity;
            Type = pCopy.Type;
            Value = pCopy.Value;
        }
    }
}