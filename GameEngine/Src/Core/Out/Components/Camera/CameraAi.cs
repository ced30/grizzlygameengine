﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Camera
{
    /// <summary>
    /// This class contains the camera logic for following targets.
    /// </summary>
    public class CameraAi : GrizzlyComponent
    {
        /// <summary>
        /// Target we want the camera to follow.
        /// </summary>
        public IEntity TargetToFollow { get; set; }

        /// <summary>
        /// Is the camera switching from target to target?
        /// </summary>
        public bool IsSwitchingTargets { get; set; }

        /// <summary>
        /// The current camera Ai state.
        /// </summary>
        public CamAiState CurrentCamAiState { get; set; }

        /// <summary>
        /// Attaches the camera to a target.
        /// </summary>
        /// <param name="pEntity"></param>
        public void Follow(IEntity pEntity)
        {
            if (pEntity == null)
                return;

            TargetToFollow = pEntity;
        }

        /// <summary>
        /// Lerp camera position to the target's positions.
        /// </summary>
        /// <param name="pCamera"></param>
        /// <param name="pBoundaries"></param>
        private void FollowTarget(Camera pCamera, Rectangle pBoundaries)
        {
            pCamera.Transform.LinearLerpToTarget(TargetToFollow, pCamera);
            pCamera.Transform.ClampPosition(pCamera, pBoundaries);
        }

        /// <summary>
        /// State logic.
        /// </summary>
        protected virtual void StatesLogic()
        {
            switch (CurrentCamAiState)
            {
                case CamAiState.Static:
                    break;

                case CamAiState.Center:

                    break;

                case CamAiState.Normal:
                    break;

                case CamAiState.Chase:
                    break;

                case CamAiState.SwitchTarget:
                    break;
            }
        }

        /// <summary>
        /// Clears the target and sets flag to no-target mode.
        /// </summary>
        public void UnFollowTarget()
        {
            CurrentCamAiState = CamAiState.Normal;
            TargetToFollow = null;
        }

        /// <summary>
        /// General update.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pCamera"></param>
        /// <param name="pBoundaries"></param>
        public virtual void Update(GameTime pGameTime, Camera pCamera, Rectangle pBoundaries)
        {
            StatesLogic();

            if (TargetToFollow == null)
                return;

            FollowTarget(pCamera, pBoundaries);
        }

        /// <summary>
        /// Camera Ai States.
        /// </summary>
        public enum CamAiState
        {
            Static,
            Center,
            Normal,
            Chase,
            SwitchTarget
        }
    }
}