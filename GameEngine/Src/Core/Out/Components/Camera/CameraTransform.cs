﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Camera
{
    /// <summary>
    /// This class handles the camera movement.
    /// </summary>
    public class CameraTransform : GrizzlyComponent
    {
        #region Private

        /// <summary>
        /// Camera Lerp.
        /// </summary>
        private CameraLerp Lerp { get; }

        /// <summary>
        /// The base speed of the camera (used to reset the speed after variations).
        /// </summary>
        private float BaseSpeed { get; }

        /// <summary>
        /// The base rotation speed of the camera (used to reset the rotation speed after variations).
        /// </summary>
        private float CurrentRotationSpeed { get; }

        /// <summary>
        /// The coefficient we want our current coefficient to reach.
        /// </summary>
        private float GoalCoef { get; }

        /// <summary>
        /// Our current coefficient value.
        /// </summary>
        private float SpeedCoef { get; set; }

        /// <summary>
        /// ViewPort, used to update the view matrix.
        /// </summary>
        private Viewport ViewPort { get; }

        #endregion


        #region Public

        public float Rotation { get; set; }
        public float Speed { get; set; }
        public int ZoomFactor { get; set; }
        public Matrix Matrix1 { get; set; }
        public Vector2 Center { get; set; }
        public Vector2 Position => new Vector2((int)(Center.X - ViewPort.Width / 2f),
            (int)(Center.Y - ViewPort.Height / 2f));
        public Vector2 Velocity { get; set; }

        #endregion


        /// <summary>
        /// Takes a Game1 as argument.
        /// </summary>
        /// <param name="pViewport"></param>
        public CameraTransform(Viewport pViewport)
        {
            BaseSpeed = 1f;
            CurrentRotationSpeed = 1;
            GoalCoef = 1;
            Lerp = new CameraLerp();
            Matrix1 = new Matrix();
            Speed = 3f;
            SpeedCoef = 1;
            ViewPort = pViewport;
            ZoomFactor = 1;
        }


        /// <summary>
        /// Keeps the camera from going off map.
        /// </summary>
        /// <param name="pCamera"></param>
        /// <param name="pTileMap"></param>
        /// <param name="pBoundaries"></param>
        public void ClampPosition(Camera pCamera, Rectangle pBoundaries)
        {
            // check if the camera goes out of map limits.
            // Left
            if (pCamera.WorldRectangle.Left <= 0)
                SetCenter(new Vector2(0 + Screen.Screen.Center.X, Center.Y));

            // Right
            if (pCamera.WorldRectangle.Right >= pBoundaries.Right)
                SetCenter(new Vector2(pBoundaries.Right - Screen.Screen.Center.X, Center.Y));

            // Up
            if (pCamera.WorldRectangle.Top <= 0)
                SetCenter(new Vector2(Center.X, (int)Screen.Screen.Center.Y));

            // Down
            if (pCamera.WorldRectangle.Bottom >= pBoundaries.Bottom)
                SetCenter(new Vector2(Center.X,
                    (int)(pBoundaries.Bottom - Screen.Screen.Center.Y)));
        }

        /// <summary>
        /// Interpolates the camera position to reach the target's position.
        /// </summary>
        /// <param name="pEntity"></param>
        /// <param name="pCamera"></param>
        public void LerpToTarget(IEntity pEntity, Camera pCamera)
        {
            // we update the offset.
            Lerp.GetOffset(pEntity);

            // we se the camera position.
            pCamera.Transform.Velocity = new Vector2(
                MathHelper.Lerp((int)pCamera.Transform.Center.X, (int)pEntity.Data.Center.X + Lerp.OffsetX, Lerp.Coef),
                MathHelper.Lerp((int)pCamera.Transform.Center.Y, (int)pEntity.Data.Center.Y + Lerp.OffsetY, Lerp.Coef));

            pCamera.Transform.SetCenter(Velocity);
        }

        /// <summary>
        /// Interpolates the camera position to reach the target's position.
        /// </summary>
        /// <param name="pTarget"></param>
        /// <param name="pCamera"></param>
        public void LinearLerpToTarget(IEntity pTarget, Camera pCamera)
        {
            const float minimumDist = .1f;
            var directionX = (pTarget.Data.Center.X - Center.X) / Lerp.LinearCoef;
            var directionY = (pTarget.Data.Center.Y - Center.Y) / Lerp.LinearCoef;

            if (Math.Abs(directionX) <= minimumDist)
                directionX = 0;
            if (Math.Abs(directionY) <= minimumDist)
                directionY = 0;

            SetCenter(new Vector2(Center.X + directionX, Center.Y + directionY));
        }

        /// <summary>
        /// Updates the position with params pDx and pDy.
        /// </summary>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        public void Move(float pDx, float pDy)
        {
            SetCenter(new Vector2(Center.X + pDx, Center.Y + pDy));
        }

        /// <summary>
        /// Sets the position to the center of the screen.
        /// </summary>
        public void ResetPosition()
        {
            Center = Screen.Screen.Center;
        }

        /// <summary>
        /// Sets the center position of the camera.
        /// </summary>
        /// <param name="pPosition"></param>
        public void SetCenter(Vector2 pPosition)
        {
            Center = new Vector2((int)pPosition.X, (int)pPosition.Y);
        }

        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            Lerp.Update(pGameTime);
        }

        /// <summary>
        /// Updates the view Matrix.
        /// </summary>
        /// <param name="pShake"></param>
        public void UpdateMatrix(Vector2 pShake)
        {
            Matrix1 = Matrix.CreateTranslation(new Vector3(
                          (0 - Center.X + pShake.X),
                          (0 - Center.Y + pShake.Y), 0)) *
                      Matrix.CreateRotationZ(Rotation) *
                      Matrix.CreateScale(ZoomFactor) *
                      Matrix.CreateTranslation(new Vector3((int)(Screen.Screen.Width / 2f), (int)(Screen.Screen.Height / 2f), 0));
        }
    }
}