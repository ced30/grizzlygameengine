﻿using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Components.Camera
{
    /// <inheritdoc />
    /// <summary>
    /// This class handles the focus and zoom of the camera.
    /// </summary>
    public class CameraFocus : GrizzlyComponent
    {
        #region Declarations

        private float _currentFocusSpeed;
        private float _goalCoef;
        private float _goalZoom;
        private float _speed;
        private float _speedCoef;
        private float _zoom;

        protected float CurrentFocusSpeed
        {
            get => _currentFocusSpeed;
            set => _currentFocusSpeed = MathHelper.Clamp(value, 0, 3);
        }

        protected float GoalCoef
        {
            get => _goalCoef;
            set => _goalCoef = MathHelper.Clamp(value, 0, 3);
        }

        protected float GoalZoom
        {
            get => _goalZoom;
            set => _goalZoom = MathHelper.Clamp(value, 0, 3);
        }

        protected float Speed
        {
            get => _speed;
            set => _speed = MathHelper.Clamp(value, 0, 3);
        }

        protected float SpeedCoef
        {
            get => _speedCoef;
            set => _speedCoef = MathHelper.Clamp(value, 0, 3);
        }

        #endregion

        public CameraFocus()
        {
            GoalCoef = 1;
            GoalZoom = 1;
            Speed = 1;
            SpeedCoef = 1;
            _zoom = 1;
        }

        /// <summary>
        /// The zoom scale value
        /// </summary>
        public float ZoomFactor
        {
            get => _zoom;
            set
            {
                _zoom = value;
                if (_zoom < 0.1f)
                {
                    _zoom = 0.1f;
                }
            }
        }

        /// <summary>
        /// zoom in or out
        /// </summary>
        private void RunZoom()
        {
            if (ZoomFactor < GoalZoom)
                ZoomFactor += CurrentFocusSpeed;
            if (ZoomFactor > GoalZoom)
                ZoomFactor -= CurrentFocusSpeed;
        }

        /// <summary>
        /// Sets the focus speed
        /// </summary>
        /// <param name="pSpeed"></param>
        public void SetZoomSpeed(CamZoomSpeed pSpeed)
        {
            switch (pSpeed)
            {
                case CamZoomSpeed.Slow:
                    GoalCoef = 0.5f;
                    break;
                case CamZoomSpeed.Medium:
                    GoalCoef = 1f;
                    break;
                case CamZoomSpeed.Fast:
                    GoalCoef = 1.5f;
                    break;
                case CamZoomSpeed.Stop:
                    GoalCoef = 0f;
                    break;
            }
        }

        /// <summary>
        /// Sets the zoom speed and focus
        /// </summary>
        /// <param name="pZoom"></param>
        /// <param name="pSpeed"></param>
        public void SetZoom(float pZoom, CamFocusSpeed pSpeed)
        {
            GoalZoom = pZoom;
            switch (pSpeed)
            {
                case CamFocusSpeed.None:
                    CurrentFocusSpeed = 0f;
                    break;

                case CamFocusSpeed.VerySlow:
                    CurrentFocusSpeed = 0.001f;
                    break;

                case CamFocusSpeed.Slow:
                    CurrentFocusSpeed = 0.008f;
                    break;

                case CamFocusSpeed.Medium:
                    CurrentFocusSpeed = 0.01f;
                    break;

                case CamFocusSpeed.Fast:
                    CurrentFocusSpeed = 0.1f;
                    break;

                case CamFocusSpeed.Instant:
                    _zoom = pZoom;
                    GoalZoom = pZoom;
                    CurrentFocusSpeed = 1f;
                    break;
            }

            RunZoom();
        }

        /// <summary>
        /// Resets the zoom to 1
        /// </summary>
        public void ResetZoom()
        {
            GoalZoom = 1.0f;
        }

        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            RunZoom();
        }

        public enum CamFocusSpeed
        {
            None,
            VerySlow,
            Slow,
            Medium,
            Fast,
            Instant
        }

        public enum CamZoomSpeed
        {
            Stop,
            Slow,
            Medium,
            Fast,
            Instant
        }
    }
}