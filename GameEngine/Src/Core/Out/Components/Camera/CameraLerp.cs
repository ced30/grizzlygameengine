﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Camera
{
    /// <summary>
    /// This class handles the lerp of the camera when it's following a target.
    /// </summary>
    public class CameraLerp : GrizzlyComponent
    {
        private float CoefIncrement { get; set; }
        private float CoefMax { get; set; }
        private float CoefMin { get; set; }
        private float OffsetMax { get; set; }
        public float Coef { get; set; }
        public float LinearCoef { get; set; }
        public float OffsetX { get; set; }
        public float OffsetY { get; set; }

        public CameraLerp()
        {
            Coef = 0.025f;
            CoefIncrement = 0.0025f;
            CoefMax = 0.25f;
            CoefMin = 0.025f;
            LinearCoef = 20;
            OffsetMax = 0;
        }

        /// <summary>
        /// Gives the camera position an offset when the player moves,
        /// based on offsetMax.
        /// </summary>
        /// <param name="pEntity"></param>
        public void GetOffset(IEntity pEntity)
        {
            if (pEntity.Data.Positional.Velocity != Vector2.Zero)
            {
                Coef = CoefMin;
            }
            else
            {
                Coef += CoefIncrement;
                if (Coef > CoefMax)
                    Coef = CoefMax;
            }

            if (pEntity.Data.Positional.Velocity.X > 0)
                OffsetX = OffsetMax;
            else if (pEntity.Data.Positional.Velocity.X < 0)
                OffsetX = 0 - OffsetMax;
            else OffsetX = 0;

            if (pEntity.Data.Positional.Velocity.Y > 0)
                OffsetY = OffsetMax;
            else if (pEntity.Data.Positional.Velocity.Y < 0)
                OffsetY = 0 - OffsetMax;
            else OffsetY = 0;
        }
    }
}