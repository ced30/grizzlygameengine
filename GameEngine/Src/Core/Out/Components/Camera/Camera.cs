﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Camera
{
    /// <summary>
    /// The main Camera, it's a singleton class.
    /// </summary>
    public class Camera
    {
        #region Declarations

        private static readonly Camera _instance = new Camera();

        /// <summary>
        /// Used to calculate the drawRectangle.
        /// </summary>
        private const int DrawRectanglePadding = 64;

        /// <summary>
        /// The component handling focus / zoom.
        /// </summary>
        public CameraFocus Focus = new CameraFocus();

        /// <summary>
        /// The component handling the tracking of targets.
        /// </summary>
        public CameraAi Ai = new CameraAi();

        /// <summary>
        /// The component handling the movement.
        /// </summary>
        public CameraTransform Transform { get; set; }

        public float Rotation => Transform.Rotation;
        public float Zoom => Focus.ZoomFactor;

        /// <summary>
        /// zone inside the camera rectangle where the target
        /// can move without the camera following.
        /// </summary>
        public Rectangle DeadZone => new Rectangle(
            (int)(Transform.Center.X - WorldRectangle.Width / 12f),
            (int)(Transform.Center.Y - WorldRectangle.Height / 8f),
            (int)(WorldRectangle.Width / 6f),
            (int)(WorldRectangle.Height / 4f));

        /// <summary>
        /// The entities present inside that zone are drawn.
        /// </summary>
        public Rectangle DrawRectangle => new Rectangle(
            (int)Position.X - DrawRectanglePadding,
            (int)Position.Y - DrawRectanglePadding,
            Screen.Screen.Width + DrawRectanglePadding * 2,
            Screen.Screen.Height + DrawRectanglePadding * 2);

        /// <summary>
        /// Rectangle representing the position of the camera.
        /// </summary>
        public Rectangle WorldRectangle => new Rectangle((int)Position.X,
            (int)Position.Y,
            Screen.Screen.Width,
            Screen.Screen.Height);

        /// <summary>
        /// Component handling the screenShake.
        /// </summary>
        public ScreenShake Shaker = new ScreenShake();

        /// <summary>
        /// Camera position and center.
        /// </summary>
        public Vector2 Center => Transform.Center;
        public Vector2 Position => Transform.Position;

        #endregion

        #region Constructors

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforeFieldInit
        static Camera()
        {
        }

        private Camera()
        {
        }

        #endregion

        public static Camera Instance => _instance;

        /// <summary>
        /// Initializes the camera with w viewPort
        /// and sets the camera to the center of the screen.
        /// </summary>
        /// <param name="pViewport"></param>
        public void InitCamera(Viewport pViewport)
        {
            Transform = new CameraTransform(pViewport);
            Transform.SetCenter(Screen.Screen.Center);
        }

        #region Collisions Object/Camera

        /// <summary>
        /// checks if an object collides with the camera's bounding box on any side
        /// </summary>
        /// <param name="pBoundingBox"></param>
        /// <returns></returns>
        public bool Contains(Rectangle pBoundingBox)
        {
            return pBoundingBox.Intersects(DrawRectangle);
        }

        /// <summary>
        /// Transforms a vector 2 relative to the position of the camera.
        /// </summary>
        /// <param name="pPos"></param>
        /// <returns></returns>
        public Vector2 PositionRelativeTo(Vector2 pPos)
        {
            return new Vector2(
                pPos.X + WorldRectangle.X,
                pPos.Y + WorldRectangle.Y
                );
        }

        #endregion


        /// <summary>
        /// Sets a new position and unFollows the owner.
        /// </summary>
        /// <param name="pPosition"></param>
        public void SetCenter(Vector2 pPosition)
        {
            Transform.SetCenter(pPosition);
            Ai.UnFollowTarget();
        }

        /// <summary>
        /// ScreenShake, pass the type of impact.
        /// </summary>
        /// <param name="pImpact"></param>
        public void Shake(ScreenShake.Impact pImpact)
        {
            Shaker.Shake(pImpact);
        }

        /// <summary>
        /// Main update, updates the components.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pBoundaries"></param>
        public void Update(GameTime pGameTime, Rectangle? pBoundaries)
        {
            var camRect = pBoundaries ?? Rectangle.Empty;

            Ai.Update(pGameTime, this, camRect);
            Focus.Update(pGameTime);
            Shaker.Update(pGameTime);
            Transform.Update(pGameTime);
            Transform.UpdateMatrix(Shaker.Intensity);
        }
    }
}