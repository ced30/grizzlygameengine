﻿#region Usings

using System;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Screen
{
    public class ScreenShake : GrizzlyComponent
    {
        protected Impact CurrentImpact;

        protected float DampenNing { get; set; }
        protected float MaxTrauma { get; set; }
        protected float MinTrauma { get; set; }
        protected float Trauma { get; set; }
        protected float TraumaStep { get; set; }
        protected int BaseIntensity { get; set; }
        protected int MaxShakes { get; set; }
        protected int NbShakes { get; set; }
        protected int Offset { get; set; }
        public bool IsShaking;
        public Vector2 Intensity { get; set; }

        public ScreenShake()
        {
            BaseIntensity = 1;
            DampenNing = 0.1f;
            MaxShakes = 15;
            MaxTrauma = 4f;
            MinTrauma = 1f;
            Offset = 4;
            Trauma = 1f;
            TraumaStep = 0.2f;
        }

        public Vector2 GetShake()
        {
            return new Vector2(Intensity.X * (float)Math.Pow(Trauma, 2),
                Intensity.Y * (float)Math.Pow(Trauma, 2));
        }

        public void Shake(Impact pImpact)
        {
            Trauma += TraumaStep;
            if (Trauma > MaxTrauma)
                Trauma = MaxTrauma;

            SetStrength(pImpact);
            IsShaking = true;
        }

        private void SetStrength(Impact pImpact)
        {
            CurrentImpact = pImpact;
        }

        private void ShakeStrength()
        {
            switch (CurrentImpact)
            {
                case Impact.Default:
                    BaseIntensity = 1;
                    MaxShakes = 15;
                    break;
                case Impact.Mini:
                    BaseIntensity = 1;
                    MaxShakes = 2;
                    break;
                case Impact.Small:
                    BaseIntensity = 1;
                    MaxShakes = 20;
                    break;
                case Impact.Medium:
                    BaseIntensity = 2;
                    MaxShakes = 15;
                    break;
                case Impact.Strong:
                    BaseIntensity = 3;
                    MaxShakes = 15;
                    break;
            }
        }

        private void ScreenShakeCheck()
        {
            if (IsShaking)
            {
                ShakeStrength();

                if (NbShakes < MaxShakes)
                    NbShakes += 1;

                if (NbShakes > 0)
                {
                    Intensity = new Vector2(
                        World.Instance.R.Next(-BaseIntensity, BaseIntensity),
                        World.Instance.R.Next(-BaseIntensity, BaseIntensity));
                }
            }

            if (NbShakes >= MaxShakes)
            {
                NbShakes = 0;
                IsShaking = false;
                Intensity = Vector2.Zero;
            }
        }

        protected virtual void ScreenShakeLogic()
        {
            ScreenShakeCheck();
            TraumaCheck();
        }

        private void TraumaCheck()
        {
            if (Trauma > MinTrauma)
            {
                Trauma -= DampenNing;
                if (Trauma < MinTrauma)
                    Trauma = MinTrauma;
            }
        }

        public override void Update(GameTime pGameTime)
        {
            if(!IsShaking)
                return;

            ScreenShakeLogic();
        }

        public enum Impact
        {
            Default,
            Mini,
            Small,
            Medium,
            Strong
        }
    }
}