﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Screen
{
    /// <summary>
    /// This class handles the resolution changes.
    /// </summary>
    public class Resolution : GrizzlyComponent
    {

        #region Public

        public bool IsFullScreen;
        public float Ratio => (_graphics.PreferredBackBufferWidth /
                               (float)_graphics.PreferredBackBufferHeight);
        public int Origin;
        public Rectangle Dim { get; private set; }
        public int Width => Dim.Width;
        public int Height => Dim.Height;
        public Vector2 Center;

        #endregion


        #region Private

        private readonly Dictionary<Res, Vector2> _resolutions =
            new Dictionary<Res, Vector2>()
            {
                {Res.Small, new Vector2(426, 240) },
                {Res.Medium, new Vector2(848, 480) },
                {Res.High, new Vector2(1600, 900) },
                {Res.VeryHigh, new Vector2(1980, 1080) },
            };

        private GraphicsDeviceManager _graphics;

        #endregion


        /// <summary>
        /// Takes a Game1 as arguments.
        /// </summary>
        /// <param name="pGraphics"></param>
        public void Init(GraphicsDeviceManager pGraphics)
        {
            _graphics = pGraphics;
            _graphics.HardwareModeSwitch = false;
            _graphics.PreferredBackBufferHeight = Height;
            _graphics.PreferredBackBufferWidth = Width;
            Dim = new Rectangle(0, 0, Width, Height);
        }

        public Vector3 GetScreenScale()
        {
            var scaleX = _graphics.GraphicsDevice.Viewport.Width / (float)Screen.Width;
            var scaleY = _graphics.GraphicsDevice.Viewport.Height / (float)Screen.Height;
            return new Vector3(scaleX, scaleY, 1.0f);
        }

        /// <summary>
        /// Sets a new resolution.
        /// </summary>
        /// <param name="pResolution"></param>
        public void Set(Res pResolution)
        {
            var requestedResolution = _resolutions[pResolution];
            Dim = new Rectangle(0, 0, (int)requestedResolution.X, (int)requestedResolution.Y);
            Center = new Vector2(Width / 2f, Height / 2f);
            _graphics.PreferredBackBufferWidth = Width;
            _graphics.PreferredBackBufferHeight = Height;
            _graphics.ApplyChanges();
            TraceScreen();
        }

        /// <summary>
        /// Sets full-screen mode.
        /// </summary>
        public void SetFullScreen()
        {
            if (!_graphics.IsFullScreen)
            {
                _graphics.PreferredBackBufferWidth = _graphics.GraphicsDevice.DisplayMode.Width;
                _graphics.PreferredBackBufferHeight = _graphics.GraphicsDevice.DisplayMode.Height;
                IsFullScreen = true;
            }
            else
            {
                _graphics.PreferredBackBufferWidth = Width;
                _graphics.PreferredBackBufferHeight = Height;
                IsFullScreen = false;
            }

            _graphics.ToggleFullScreen();
            TraceScreen();
            Dim = new Rectangle(Origin, Origin, Width, Height);
        }

        /// <summary>
        /// Sets full-screen mode on or of depending on pBool.
        /// </summary>
        /// <param name="pBool"></param>
        public void SetFullScreen(bool pBool)
        {
            if (pBool ||
                !_graphics.IsFullScreen)
            {
                _graphics.PreferredBackBufferWidth = _graphics.GraphicsDevice.DisplayMode.Width;
                _graphics.PreferredBackBufferHeight = _graphics.GraphicsDevice.DisplayMode.Height;
                IsFullScreen = true;
            }
            else
            {
                _graphics.PreferredBackBufferWidth = Width;
                _graphics.PreferredBackBufferHeight = Height;
                IsFullScreen = false;
            }

            _graphics.ToggleFullScreen();
            TraceScreen();
            Dim = new Rectangle(Origin, Origin, Width, Height);
        }

        /// <summary>
        /// Logs new screen size after a resolution change.
        /// </summary>
        public void TraceScreen()
        {
            Debug.WriteLine("Full screen is " + _graphics.IsFullScreen);
            Debug.WriteLine("Game Resolution is " + _graphics.PreferredBackBufferWidth +
                            " x " + _graphics.PreferredBackBufferHeight);
            Debug.WriteLine("Screen Resolution is " + _graphics.GraphicsDevice.DisplayMode.Width +
                            " x " + _graphics.GraphicsDevice.DisplayMode.Height);
            Debug.WriteLine("Viewport Resolution is " + _graphics.GraphicsDevice.Viewport.Width +
                            " x " + _graphics.GraphicsDevice.Viewport.Height);
            //            Debug.WriteLine("Client Resolution is " + pGame.Window.ClientBounds.Width +
            // " x " + pGame.Window.ClientBounds.Height);
            Debug.WriteLine("Aspect ratio (Game) is {0}", Ratio);
        }

        /// <summary>
        /// Resolution descriptions.
        /// </summary>
        public enum Res
        {
            Small,
            Medium,
            High,
            VeryHigh
        }
    }
}
