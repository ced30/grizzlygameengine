﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Screen
{
    public class Screen : GrizzlyComponent
    {
        public Resolution Resolution { get; set; }
        public ScreenTransform Transform { get; set; }
        public static int Height { get; set; }
        public static int Width { get; set; }
        public static Vector2 Center { get; set; }

        public Screen(GraphicsDeviceManager pGraphics)
        {
            Resolution = new Resolution();
            Transform = new ScreenTransform();
            Resolution.Init(pGraphics);
        }

        public void SetResolution(Resolution.Res pResolution)
        {
            Resolution.Set(pResolution);
            UpdateGlobals();
        }

        public void SetFullScreen()
        {
            Resolution.SetFullScreen();
        }

        public void SetFullScreen(bool pBool)
        {
            Resolution.SetFullScreen(pBool);
        }

        private void UpdateGlobals()
        {
            Center = Resolution.Center;
            Height = Resolution.Height;
            Width = Resolution.Width;
        }
    }
}