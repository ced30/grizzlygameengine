﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Components.Screen
{
    public class ScreenTransform
    {
        public Matrix Matrix1 { get; set; }
        public int ZoomFactor { get; set; }

        public ScreenTransform()
        {
            Matrix1 = new Matrix();
        }

        public void UpdateMatrix(Vector2 pPosition, Vector2 pShake, float pRotation, float pZoom)
        {
            Matrix1 = Matrix.CreateTranslation(new Vector3((int)(0 - pPosition.X + pShake.X),
                          (int)(0 - pPosition.Y + pShake.Y), 0)) *
                         Matrix.CreateRotationZ(pRotation) *
                         Matrix.CreateScale(pZoom) *
                         Matrix.CreateTranslation(new Vector3(Screen.Width / 2f, Screen.Height / 2f, 0));
        }
    }
}