﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Components
{
    public interface IGrizzlyComponent
    {
        /// <summary>
        /// Loads the content after initialization.
        /// </summary>
        void LoadContent();

        /// <summary>
        /// UnLoads the content before making the class null.
        /// </summary>
        void Unload();

        /// <summary>
        /// Main update method, takes a gameTime as argument.
        /// </summary>
        /// <param name="pGameTime"></param>
        void Update(GameTime pGameTime);

        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        void Draw(SpriteBatch pSpriteBatch);

        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        void DrawDebug(SpriteBatch pSpriteBatch);
    }
}