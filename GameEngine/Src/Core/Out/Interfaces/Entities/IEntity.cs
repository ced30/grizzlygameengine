﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities
{
    public delegate void OnNewEntity(IEntity pEntityToAddToList);

    public delegate void OnNewParticle(EmitterState pState, Particle pParticle);

    public interface IEntity : ISubject
    {
        /// <summary>
        /// Used to pass spawned entities to the main list.
        /// </summary>
        OnNewEntity OnNewEntityInstance { get; set; }

        /// <summary>
        /// Used to pass spawned particles to the particle manager.
        /// </summary>
        OnNewParticle OnNewParticleInstance { get; set; }

        /// <summary>
        /// Contains all the common entity data..
        /// </summary>
        IDataController Data { get; set; }

        /// <summary>
        /// Modular system manager, derive from systems to make your own.
        /// </summary>
        ISystemManager Systems { get; set; }

        /// <summary>
        /// List of subscribers. In real life, the list of subscribers can be
        /// stored more comprehensively (categorized by event type, etc.).
        /// </summary>
        List<IObserver> LstObservers { get; set; }

        /// <summary>
        /// The faction of the entity.
        /// </summary>
        string Faction { get; set; }

        /// <summary>
        /// The identifier of the entity.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// The type of the entity.
        /// </summary>
        string Type { get; set; }

        /// <summary>
        /// Used to put entities in your family list.
        /// </summary>
        /// <param name="pEntity"></param>
        void Adopt(IEntity pEntity);

        /// <summary>
        /// Adds types of entities we can collide with.
        /// </summary>
        /// <param name="pType"></param>
        void CanCollideWith(string pType);

        /// <summary>
        /// Tests entity collisions.
        /// </summary>
        /// <param name="pEntityA"></param>
        /// <param name="pEntityB"></param>
        void Collide(IEntity pEntityA, IEntity pEntityB);

        /// <summary>
        /// passes the collision to the ia who handles the logic.
        /// </summary>
        /// <param name="pEntity"></param>
        void CollidedBy(IEntity pEntity);


        /// <summary>
        /// Draws the entity.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        void Draw(SpriteBatch pSpriteBatch);

        /// <summary>
        /// Plays a sound (sound observer must be attached and
        /// Data.IsSound must be true).
        /// </summary>
        /// <param name="pId"></param>
        void PlaySound(string pId);

        /// <summary>
        /// Plays a sound (sound observer must be attached and
        /// Data.IsSound must be true).
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pEmitterPos"></param>
        /// <param name="pReceiverPos"></param>
        void PlaySound(string pId, Vector2 pEmitterPos, Vector2 pReceiverPos);

        /// <summary>
        /// Main update.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pCollider"></param>
        /// <param name="pList"></param>
        void Update(GameTime pGameTime, TileMapCollider pCollider, List<IEntity> pList);
    }
}