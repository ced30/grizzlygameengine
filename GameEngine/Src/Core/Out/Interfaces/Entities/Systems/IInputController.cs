﻿using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IInputController : ISystem
    {
        void Update(GameTime pGameTime, IEntity pOwner);
    }
}