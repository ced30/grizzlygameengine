﻿namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IPhysicsController : IUpdatableSystem
    {
        void ApplyFriction(IDataController pData);
    }
}