﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IAnimationController : IUpdatableSystem
    {
        Texture2D Texture { get; set; }

        /// <summary>
        /// Is the animation finished?
        /// </summary>
        bool IsFinished { get; }

        /// <summary>
        /// Is the animation identifier equals to the param pIdentifier?
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        bool IsAnimId(string pId);

        /// <summary>
        /// Is the currentFrame > than the parameter pNbFrame?
        /// </summary>
        /// <param name="pNbFrame"></param>
        /// <returns></returns>
        bool IsFrameGreater(float pNbFrame);

        /// <summary>
        /// Offset vector used to draw the animations / textures.
        /// </summary>
        Vector2 FrameOffset(int pFlipX);

        /// <summary>
        /// The size of the frame.
        /// </summary>
        Vector2 FrameCenter { get; }

        /// <summary>
        /// Offset to center or offCenter the frame.
        /// </summary>
        Vector2 OffsetPos { get; }

        /// <summary>
        /// Plays the animation corresponding to the pId parameter
        /// </summary>
        /// <param name="pId"></param>
        void PlayAnimation(string pId);

        /// <summary>
        /// Plays the first animation
        /// </summary>
        void PlayFirst();

        /// <summary>
        /// Restarts the animation
        /// </summary>
        void Restart();

        /// <summary>
        /// Draws the animation.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        void Draw(SpriteBatch pSpriteBatch, IDataController pData);
    }
}