﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IAiController : ISystem
    {
        /// <summary>
        /// Is the entity attacking at this instant?
        /// </summary>
        bool IsAttacking { get; set; }

        /// <summary>
        /// has the entity attacked?
        /// </summary>
        bool HasFired { get; set; }

        /// <summary>
        /// The dictionary containing your coolDowns.
        /// </summary>
        Dictionary<string, Cooldown> DictionaryCoolDowns { get; set; }

        /// <summary>
        /// the current collided entity
        /// </summary>
        IEntity CurrentCollider { get; set; }

        /// <summary>
        /// The list of entities recently collided by
        /// </summary>
        List<IEntity> LstColliders { get; set; }

        /// <summary>
        /// Adds a coolDown to the dictionary,
        /// takes a string and a coolDown as parameters.
        /// </summary>
        /// <param name="pCdName"></param>
        /// <param name="pCooldown"></param>
        void AddCoolDown(string pCdName, Cooldown pCooldown);

        /// <summary>
        /// Returns the cooldown corresponding to the provided Id
        /// </summary>
        /// <param name="pCdId"></param>
        /// <returns></returns>
        Cooldown GetCooldown(string pCdId);

        /// <summary>
        /// Collisions logic.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        void Collide(IEntity pOwner, IEntity pCollider);

        /// <summary>
        /// Collides without checking if the entity has already collided
        /// (used for damage over time effects)
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        void CollideWithoutCheck(IEntity pOwner, IEntity pCollider);

        /// <summary>
        /// Spawns a hitBox.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pRect"></param>
        void MakeHitBox(IEntity pOwner, Rectangle pRect);

        /// <summary>
        /// animation logic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        void UpdateAnimations(GameTime pGameTime, IEntity pOwner);

        /// <summary>
        /// Executes the Ai gameLogic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        void UpdateGameLogic(GameTime pGameTime, IEntity pOwner);
    }
}