﻿#region Usings

using Microsoft.Xna.Framework;
using GrizzlyGameEngine.Core.Out.Enums;

#endregion


namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface ITransformController : IUpdatableSystem
    {
        /// <summary>
        /// Bounce an entity when there is collision with another entity
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        void Bounce(IDataController pOwner, IDataController pCollider);

        /// <summary>
        /// Lerp at constant speed.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        void LerpAtConstantSpeed(IDataController pData, Vector2 pTargetPos);

        /// <summary>
        /// Interpolates the camera position to reach the target's position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pParent"></param>
        void LerpToTarget(IDataController pData, Vector2 pParent);

        /// <summary>
        /// pWMove and pHMove are normals, they equals 1, 0 or -1.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pWMove"></param>
        /// <param name="pHMove"></param>
        void MoveWithNormals(IDataController pData, int pWMove, int pHMove);


        /// <summary>
        /// Used to move particles around, pSpeed is the speed value.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pSpeed"></param>
        void MoveAtConstantSpeed(IDataController pData, float pSpeed);

        /// <summary>
        /// Calculates the angle and apply forces to the initial position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        void MoveWithAngles(IDataController pData, Vector2 pTargetPos);

        /// <summary>
        /// Moves to the specified direction.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDirection"></param>
        void MoveWithDirection(IDataController pData, Direction pDirection);

        /// <summary>
        /// Sets the position given a param vector2
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pPos"></param>
        void SetPosition(IDataController pData, Vector2 pPos);

        /// <summary>
        /// Adds param increment to "Rotation".
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pIncr"></param>
        void Rotate(IDataController pData, float pIncr);

        /// <summary>
        /// Sets the angle (rotation).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pRotation"></param>
        void SetRotation(IDataController pData, float pRotation);

        /// <summary>
        /// Sets the scale (zoom).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pZoom"></param>
        void SetZoom(IDataController pData, float pZoom);
    }
}