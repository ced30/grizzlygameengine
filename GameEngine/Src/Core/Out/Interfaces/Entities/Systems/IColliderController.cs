﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;

#endregion


namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IColliderController : ISystem
    {
        bool IsWorldCollisions { get; set; }

        void AlignOnColumn(IDataController pData);
        void AlignOnLine(IDataController pData);

        void Update(IDataController pData, TileMapCollider pCollider);
        void WorldCollisions(IDataController pData, TileMapCollider pCollider);
    }
}