﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.DeBuff;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IDataController
    {
        /// <summary>
        /// The colors class handles colors / alpha
        /// FadeIn / FadeOut
        /// </summary>
        Colors Colors { get; set; }

        /// <summary>
        /// Contains a list of currently applied deBuffs id
        /// </summary>
        DeBuffs DeBuffs { get; set; }

        /// <summary>
        /// Contains the entity's attributes
        /// </summary>
        DefaultAttributes Attributes { get; set; }

        /// <summary>
        /// Dictionary of imported Tmx properties
        /// </summary>
        Dictionary<string, string> TmxProperties { get; set; }

        /// <summary>
        /// Dimensions
        /// </summary>
        int Height { get; set; }
        int Width { get; set; }

        /// <summary>
        /// Horizontal and vertical image flip
        /// </summary>
        int XFlip { get; }
        int YFlip { get; }

        /// <summary>
        /// Contains the id or types of entities we can collide with
        /// </summary>
        List<string> LstCanCollideWithType { get; set; }

        /// <summary>
        /// Contains the movement related data
        /// </summary>
        PositionalData Positional { get; set; }

        /// <summary>
        /// The sound we are passing to the sound observer,
        /// null if no sound is to be played
        /// </summary>
        PackedSoundData SoundToPlay { get; set; }

        /// <summary>
        /// Contains the boolean properties
        /// </summary>
        Conditions Conditions { get; set; }

        /// <summary>
        /// Rectangle defining the entity position and dimensions
        /// </summary>
        Rectangle BoundingBox { get; }

        /// <summary>
        /// The current image flip effect
        /// </summary>
        SpriteEffects CurrentFlipX { get; }

        /// <summary>
        /// The entity's state machine
        /// </summary>
        States States { get; set; }

        /// <summary>
        /// The center of the boundingBox
        /// </summary>
        Vector2 Center { get; }

        /// <summary>
        /// The entity's position
        /// </summary>
        Vector2 Position { get; set; }
    }
}