﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    /// <summary>
    /// Handles the current renderer and the animation changes.
    /// </summary>
    public interface IRenderManager : IUpdatableSystem
    {
        /// <summary>
        /// Is the animation finished?
        /// </summary>
        bool IsFinishedAnimation { get; }

        /// <summary>
        /// The current renderer, can be:
        /// 1-Texture-renderer
        /// 2-Strip animation renderer
        /// 3-Atlas animation renderer
        /// </summary>
        IAnimationController CurrentRenderer { get; set; }

        /// <summary>
        /// Draws the current renderer.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        void Draw(SpriteBatch pSpriteBatch, IDataController pData);

        /// <summary>
        /// Play the animation corresponding to parameter ID.
        /// </summary>
        /// <param name="pId"></param>
        void PlayAnimation(string pId);

        /// <summary>
        /// Sets the current renderer to be an atlas-animation renderer,
        /// takes an atlas container as a parameter.
        /// </summary>
        /// <param name="pContainer"></param>
        void Set(AtlasExtracted pContainer);

        /// <summary>
        /// Sets the current renderer to be a texture renderer,
        /// takes a Texture2D as argument.
        /// </summary>
        /// <param name="pTexturePack"></param>
        void Set(TexturePack pTexturePack);

        /// <summary>
        /// Sets the current renderer to be a strip-animation renderer,
        /// takes a dictionary of strip animations as a parameter.
        /// </summary>
        /// <param name="pDictionary"></param>
        void Set(Dictionary<string, StripAnimationItem> pDictionary);
    }
}