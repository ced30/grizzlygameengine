﻿#region Usings

using System.Collections.Generic;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IStateMachineController : ISystem
    {
        Dictionary<string, string> StatesDictionary { get; set; }
        string CurrentState { get; set; }
        string PreviousState { get; set; }

        void Add(string pStateName, string pStateIdentifier);
        bool Equals(string pString);
        void Set(string pStateName);
    }
}