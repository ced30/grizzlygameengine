﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IShaderController : ISystem
    {
        IShaderItem CurrentShader { get; set; }
        void Add(string pName, IShaderItem pItem);
        void Apply();
        void Reset();
        void SetCurrent(string pShaderId);
        void SetFirstAsCurrent();
        void Update(GameTime pGameTime);
    }
}