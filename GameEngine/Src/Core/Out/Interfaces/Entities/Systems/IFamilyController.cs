﻿#region Usings

using System.Collections.Generic;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems
{
    public interface IFamilyController : ISystem
    {
        bool HasAParent { get; }
        bool HasMembers { get; }
        IEntity GetChild(string pName);
        IEntity Parent { get; set; }
        List<IEntity> Members { get; set; }
        void Adopt(IEntity pChild, IEntity pParent);
        void BeAdoptedBy(IEntity pEntity);
        void Update(IDataController pData);
    }
}