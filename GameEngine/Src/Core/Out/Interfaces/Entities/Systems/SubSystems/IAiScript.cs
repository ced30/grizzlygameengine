﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems
{
    /// <summary>
    /// This is an AI script, containing the logic for 1 state,
    /// it contains a dictionary of coolDowns to time actions,
    /// they are automatically updated through the Update method.
    /// </summary>
    public interface IAiScript
    {
        /// <summary>
        /// Delta-time.
        /// </summary>
        double Dt { get; set; }

        /// <summary>
        /// The identifier of the state.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Main update.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        void Update(GameTime pGameTime, IEntity pOwner);
    }
}