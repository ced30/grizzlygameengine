﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities
{
    /// <summary>
    /// Handles the entity systems.
    /// </summary>
    public interface ISystemManager
    {
        /// <summary>
        /// Modular Ai system.
        /// </summary>
        IAiController Ai { get; set; }

        /// <summary>
        /// Collider, handles entity and world collisions.
        /// </summary>
        IColliderController Collider { get; set; }

        /// <summary>
        /// Family controller, handles the owned entities.
        /// </summary>
        IFamilyController Family { get; set; }

        /// <summary>
        /// Input controller, handles the input reading.
        /// </summary>
        IInputController Input { get; set; }

        /// <summary>
        /// Physics controller, handles the friction, etc.
        /// </summary>
        IPhysicsController Physics { get; set; }

        /// <summary>
        /// Renderer, handles the textures, atlas and strip animations.
        /// </summary>
        IRenderManager Renderer { get; set; }

        /// <summary>
        /// Handles the shader, contains a dictionary string, effect
        /// </summary>
        IShaderController Shader { get; set; }

        /// <summary>
        /// Transform, handles all the movement.
        /// </summary>
        ITransformController Transform { get; set; }

        /// <summary>
        /// Replaces a system with a provided one.
        /// </summary>
        /// <param name="pSystem"></param>
        void Replace(ISystem pSystem);

        /// <summary>
        /// Main Update, pCollider parameter can be null.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        /// <param name="pList"></param>
        void Update(GameTime pGameTime, IEntity pOwner, TileMapCollider pCollider, List<IEntity> pList);

        /// <summary>
        /// Main draw method.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pOwner"></param>
        void Draw(SpriteBatch pSpriteBatch, IEntity pOwner);
    }
}