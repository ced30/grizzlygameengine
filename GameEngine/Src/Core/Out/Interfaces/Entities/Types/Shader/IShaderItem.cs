﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader
{
    public interface IShaderItem
    {
        Effect Effect { get; set; }
        string Id { get; set; }
        void Apply();
        void Reset();
        void Update(GameTime pGameTime);
    }
}