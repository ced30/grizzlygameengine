﻿using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

namespace GrizzlyGameEngine.Core.Out.Interfaces.Entities
{
    public interface IUpdatableSystem : ISystem
    {
        /// <summary>
        /// Delta-time, the time elapsed between 2 updates.
        /// </summary>
        double Dt { get; set; }

        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        void Update(GameTime pGameTime, IDataController pData);
    }
}