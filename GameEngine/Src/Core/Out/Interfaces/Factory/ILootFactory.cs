﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory.Base;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Factory
{
    public interface ILootFactory : IFactory
    {
        void InitData(IEntity pLoot, IEntity pSource);
        IEntity MakeLoot(string pType);
    }
}