﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Factory.Base;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Factory
{
    /// <summary>
    /// Loaders for all type of content, initialize at application start.
    /// </summary>
    public interface IContentFactory : IFactory
    {
        void Populate();
    }
}