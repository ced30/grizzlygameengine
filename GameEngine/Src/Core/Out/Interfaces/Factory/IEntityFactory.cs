﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory.Base;
using Microsoft.Xna.Framework;
using TiledContentImporter.TiledSharp;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Factory
{
    public interface IEntityFactory : IFactory
    {
        IEntity Make(TmxObject pObject);
    }
}