﻿namespace GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base
{
    /// <summary>
    /// Receive notifications with sounds to play,
    /// Entities must subscribe to emit sounds.
    /// </summary>
    public interface IObserver
    {
        // Receive update from subject
        void Update(ISubject subject);
    }
}