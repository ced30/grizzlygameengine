﻿namespace GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base
{
    public interface ISubject
    {
        // Attach an observer to the subject.
        void Attach(IObserver pObserver);

        // Detach an observer from the subject.
        void Detach(IObserver pObserver);

        // Notify all observers about an event.
        void Notify();
    }
}