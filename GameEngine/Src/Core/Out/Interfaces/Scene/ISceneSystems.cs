﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Components.FadeEffect;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Screen;
using GrizzlyGameEngine.Core.Out.Components.SoundControl;
using GrizzlyGameEngine.Core.Out.Components.TileMap;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Interfaces.Scene
{
    public interface ISceneSystems
    {
        Random Random { get; set; }

        /// <summary>
        /// The class containing the factories, we load it with
        /// the desired factories and pass it to the tileMap
        /// to spawn the entities.
        /// </summary>
        EntityFactoryManager Factory { get; set; }

        /// <summary>
        /// Main game's fade effect, use it to
        /// fadeIn or fadeOut the whole screen.
        /// </summary>
        FadeEffect Fade { get; set; }

        /// <summary>
        /// Main game's input reading class.
        /// </summary>
        GeneralInput Input { get; set; }

        /// <summary>
        /// The particles generator, contains dictionary string, particle
        /// </summary>
        ParticleManager ParticlesManager { get; set; }

        /// <summary>
        /// The pathFinding request handler
        /// </summary>
        PathRequestManager RequestManager { get; set; }

        /// <summary>
        /// Main game's screen class,
        /// handles the resolutions.
        /// </summary>
        Screen Screen { get; set; }

        /// <summary>
        /// Main game's soundControl class,
        /// Handles the audio output.
        /// </summary>
        SoundControl SoundControl { get; set; }

        /// <summary>
        /// Main game's tileMap,
        /// Handles tileMap loading, parsing, drawing and animating.
        /// </summary>
        TileMap TileMap { get; set; }

        void LoadContent();

        void Update(GameTime pGameTime);

        void Draw(SpriteBatch pSpriteBatch, List<IEntity> dataLstEntities);
    }
}