﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Components.Primitives2D;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.SystemsManager
{
    /// <inheritdoc />
    /// <summary>
    /// Handles the entity systems.
    /// </summary>
    public abstract class ABaseSystemManager : ISystemManager
    {
        /// <inheritdoc />
        /// <summary>
        /// Modular Ai system.
        /// </summary>
        public IAiController Ai { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Collider, handles entity and world collisions.
        /// </summary>
        public IColliderController Collider { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Family controller, handles the owned entities.
        /// </summary>
        public IFamilyController Family { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Input controller, handles the input reading.
        /// </summary>
        public IInputController Input { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Physics controller, handles the friction, etc.
        /// </summary>
        public IPhysicsController Physics { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Renderer, handles the textures, atlas and strip animations.
        /// </summary>
        public IRenderManager Renderer { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Handles the shader, contains a dictionary string, effect
        /// </summary>
        public IShaderController Shader { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Transform, handles all the movement.
        /// </summary>
        public ITransformController Transform { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Replaces a system with a provided one.
        /// </summary>
        /// <param name="pSystem"></param>
        public void Replace(ISystem pSystem)
        {
            switch (pSystem)
            {
                case IAiController pAi:
                    Ai = pAi;
                    break;
                case IColliderController pCol:
                    Collider = pCol;
                    break;
                case IFamilyController pFam:
                    Family = pFam;
                    break;
                case IInputController pInput:
                    Input = pInput;
                    break;
                case IPhysicsController pPhy:
                    Physics = pPhy;
                    break;
                case IRenderManager pRd:
                    Renderer = pRd;
                    break;
                case IShaderController pRd:
                    Shader = pRd;
                    break;
                case ITransformController pTr:
                    Transform = pTr;
                    break;
                default:
                    throw new NotImplementedException("System not implemented. (SystemController)");
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Main Update.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        /// <param name="pList"></param>
        public void Update(GameTime pGameTime, IEntity pOwner, TileMapCollider pCollider, List<IEntity> pList)
        {
            Input.Update(pGameTime, pOwner);
            Ai.UpdateGameLogic(pGameTime, pOwner);
            Transform.Update(pGameTime, pOwner.Data);

            if (pCollider != null)
                Collider?.Update(pOwner.Data, pCollider);

            Physics.Update(pGameTime, pOwner.Data);
            Ai.UpdateAnimations(pGameTime, pOwner);
            Shader.Update(pGameTime);
            Renderer.Update(pGameTime, pOwner.Data);

            Family.Update(pOwner.Data);
        }

        /// <inheritdoc />
        /// <summary>
        /// Main draw method.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pOwner"></param>
        public void Draw(SpriteBatch pSpriteBatch, IEntity pOwner)
        {
            if (Camera.Instance.Contains(pOwner.Data.BoundingBox))
            {
                // Apply the shader if it's available.
                pOwner.Systems.Shader.Apply();

                // Draw
                Renderer.Draw(pSpriteBatch, pOwner.Data);
            }
        }

        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pOwnerData"></param>
        protected virtual void DrawDebug(SpriteBatch pSpriteBatch, IDataController pOwnerData)
        {
            Primitives2D.DrawRectangle(pSpriteBatch, pOwnerData.BoundingBox, Color.White, 1);
        }
    }
}