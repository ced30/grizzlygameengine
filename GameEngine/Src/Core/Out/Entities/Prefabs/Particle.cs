﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Prefabs
{
    public class Particle : DefaultEntity
    {
        public Particle(
            ImporterState pRenderer,
            string pIdentifier,
            Rectangle pRect)
        {
            Faction = "neutral";
            Id = pIdentifier;
            Type = "particle";
            Data.Conditions.IsCollideAbleByEntity = false;
            Data.Conditions.IsCollideAbleByWorld = false;
            Data.Conditions.IsVisible = true;
            Data.Position = new Vector2(pRect.X, pRect.Y);

            Data.Width = pRect.Width;
            Data.Height = pRect.Height;

            Systems.Replace(new AiParticleDefault(Data.States));
            Systems.Replace(new SNoFamilyController());
            Systems.Replace(new SNoColliderController());
            Systems.Replace(new SNoInputController());
            Systems.Replace(new SDefaultRenderManager(Data));
            Systems.Replace(new SDefaultTransformController());
            Systems.Replace(new SDefaultPhysicsController());

            switch (pRenderer)
            {
                case ImporterState.AtlasGroup:
                    Systems.Renderer.Set(
                        GrizzlyContentManager.Instance.TmxCManager.GetAtlasGroup(Id));
                    break;

                case ImporterState.StripAnim:
                    Systems.Renderer.Set(
                        GrizzlyContentManager.Instance.TmxCManager.GetStripAnim(Id));
                    break;

                case ImporterState.StripGroup:
                    Systems.Renderer.Set(
                        GrizzlyContentManager.Instance.TmxCManager.GetStripGroup(Id));
                    break;

                case ImporterState.Texture:
                    Systems.Renderer.Set(
                        GrizzlyContentManager.Instance.TmxCManager.GetTexture(Id));
                    break;

                default:
                    Systems.Renderer.Set(
                        GrizzlyContentManager.Instance.TmxCManager.GetStripGroup(Id));
                    break;
            }
        }

        public virtual Delegate OnNewEntityCreation(IEntity pEntityToAddToList)
        {
            throw new NotImplementedException();
        }
    }
}