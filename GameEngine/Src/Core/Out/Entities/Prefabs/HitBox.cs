﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Prefabs
{
    /// <summary>
    /// You must add some entity types that the hitBox can interact with,
    /// using the "AddTargetType" method when you instanciate this class
    /// </summary>
    public class HitBox : DefaultEntity
    {
        public HitBox(
            Rectangle pRect)
        {
            Id = "hitBox";
            Type = "hitBox";
            Data.Conditions.IsCollideAbleByEntity = true;
            Data.Position = new Vector2(pRect.X, pRect.Y);

            Data.Width = pRect.Width;
            Data.Height = pRect.Height;

            Systems.Replace(new AiHitBoxDefault(Data.States));
            Systems.Replace(new SDefaultFamilyController());
            Systems.Replace(new SDefaultColliderController());
        }

        /// <summary>
        /// Returns a rectangle to place your bounding box.
        /// </summary>
        /// <param name="pData"></param>
        /// <returns></returns>
        public static Rectangle GetMeleeHitBox(IDataController pData)
        {
            if (pData.Conditions.IsFlipX)
                return new Rectangle(
                    pData.BoundingBox.Left - pData.BoundingBox.Width,
                    pData.BoundingBox.Top,
                    pData.BoundingBox.Width * 2,
                    pData.BoundingBox.Height);
            else
            {
                return new Rectangle(
                    pData.BoundingBox.Left,
                    pData.BoundingBox.Top,
                    pData.BoundingBox.Width * 2,
                    pData.BoundingBox.Height);
            }
        }
    }
}