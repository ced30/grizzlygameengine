﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base;
using Microsoft.Xna.Framework;

#endregion


namespace GrizzlyGameEngine.Core.Out.Entities.Prefabs
{
    /// <summary>
    /// An agent has a transform.
    /// </summary>
    public class Agent : DefaultEntity
    {
        public Agent(
            string pIdentifier,
            Rectangle pRect)
        {
            Id = pIdentifier;
            Data.Conditions.IsSound = true;
            Data.Conditions.IsVisible = true;
            Data.Position = new Vector2(pRect.X, pRect.Y);

            Data.Width = pRect.Width;
            Data.Height = pRect.Height;

            Systems.Replace(new SDefaultTransformController());
            Systems.Replace(new SDefaultPhysicsController());
            Systems.Replace(new SDefaultFamilyController());
            Systems.Replace(new SDefaultTransformController());
            Systems.Replace(new SDefaultRenderManager(Data));
            LstObservers = new List<IObserver>();
        }
    }
}