﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Base;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.SystemsManager;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Prefabs
{
    public class DefaultEntity : BaseEntity
    {
        public DefaultEntity()
        {
            Data = new DefaultDataController();
            Systems = new SDefaultSystemManager();

            Systems.Replace(new SDefaultAiController(Data.States));
            Systems.Replace(new SNoColliderController());
            Systems.Replace(new SNoFamilyController());
            Systems.Replace(new SNoInputController());
            Systems.Replace(new SNoPhysicsController());
            Systems.Replace(new SNoRenderManager());
            Systems.Replace(new SNoShaderController());
            Systems.Replace(new SNoTransformController());
        }
    }
}