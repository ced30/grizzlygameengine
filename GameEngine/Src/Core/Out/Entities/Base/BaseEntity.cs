﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Primitives2D;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Observer.Base;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Base
{
    public abstract class BaseEntity : IEntity
    {
        /// <inheritdoc />
        /// <summary>
        /// Used to pass spawned entities to the main list.
        /// </summary>
        public OnNewEntity OnNewEntityInstance { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Used to pass spawned particles to the particle manager.
        /// </summary>
        public OnNewParticle OnNewParticleInstance { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// List of subscribers. In real life, the list of subscribers can be
        /// stored more comprehensively (categorized by event type, etc.).
        /// </summary>
        public List<IObserver> LstObservers { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Contains all the common entity data..
        /// </summary>
        public IDataController Data { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Modular system manager, derive from systems to make your own.
        /// </summary>
        public ISystemManager Systems { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The faction of the entity.
        /// </summary>
        public string Faction { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The identifier of the entity.
        /// </summary>
        public string Id { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The type of the entity.
        /// </summary>
        public string Type { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Used to put entities in your family list.
        /// </summary>
        /// <param name="pEntity"></param>
        public void Adopt(IEntity pEntity)
        {
            Systems.Family.Adopt(pEntity, this);
        }

        /// <summary>
        /// Sets the parameter entity as a parent
        /// (does not add the target to the parent's group group)
        /// </summary>
        /// <param name="pParent"></param>
        public void BeAdoptedByAdopt(IEntity pParent)
        {
            Systems.Family.BeAdoptedBy(pParent);
            Faction = pParent.Faction;
        }


        #region Observer stuff

        public void Attach(IObserver pObserver)
        {
            LstObservers.Add(pObserver);
        }

        public void Detach(IObserver pObserver)
        {
            LstObservers.Remove(pObserver);
        }

        public void Notify()
        {
            if (LstObservers == null ||
               LstObservers.Count <= 0)
                return;

            for (var i = 0; i < LstObservers.Count; i++)
            {
                LstObservers[i].Update(this);
            }
        }

        #endregion


        #region Collisions stuff

        /// <inheritdoc />
        /// <summary>
        /// Adds types of entities we can collide with.
        /// </summary>
        /// <param name="pType"></param>
        public void CanCollideWith(string pType)
        {
            Data.LstCanCollideWithType.Add(pType);
        }

        /// <inheritdoc />
        /// <summary>
        /// passes the collision to the ia who handles the logic.
        /// </summary>
        /// <param name="pCollidedBy"></param>
        public void CollidedBy(IEntity pCollidedBy)
        {
            if (!pCollidedBy.Data.Conditions.IsCollideAbleByEntity)
                return;

            if (pCollidedBy.Data.Conditions.IsExpired)
                return;

            if (Data.LstCanCollideWithType.Count <= 0)
                return;

            if (Data.LstCanCollideWithType.Contains(pCollidedBy.Type))
            {
                Systems.Ai.Collide(this, pCollidedBy);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Tests entity collisions.
        /// </summary>
        /// <param name="pEntityA"></param>
        /// <param name="pEntityB"></param>
        public virtual void Collide(IEntity pEntityA, IEntity pEntityB)
        {
            if (pEntityA == pEntityB)
                return;

            if (!pEntityA.Data.Conditions.IsCollideAbleByEntity ||
                !pEntityB.Data.Conditions.IsCollideAbleByEntity)
                return;

            if (pEntityA.Data.Conditions.IsBounceAble)
            {
                if (pEntityB.Data.Conditions.IsSolid)
                {
                    if (Helpers.Dist(
                            pEntityA.Data.Center,
                            pEntityB.Data.Center)
                        <= pEntityB.Data.Attributes.TotalAttributes.CollisionRadius)
                    {
                        Systems.Transform.Bounce(pEntityA.Data, pEntityB.Data);
                    }
                }

            }
            if (pEntityB.Data.BoundingBox.Intersects(pEntityA.Data.BoundingBox))
            {
                pEntityA.CollidedBy(pEntityB);
            }
        }

        #endregion


        /// <inheritdoc />
        /// <summary>
        /// Plays a sound (sound observer must be attached and
        /// Data.IsSound must be true).
        /// </summary>
        /// <param name="pId"></param>
        public void PlaySound(string pId)
        {
            Data.SoundToPlay = new PackedSoundData(pId);
            Notify();

            Data.SoundToPlay = null;
        }

        /// <inheritdoc />
        /// <summary>
        /// Plays a sound (sound observer must be attached and
        /// Data.IsSound must be true).
        /// The closer the receiver is, the louder the sound.
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pEmitterPos"></param>
        /// <param name="pReceiverPos"></param>
        public void PlaySound(string pId, Vector2 pEmitterPos, Vector2 pReceiverPos)
        {
            Data.SoundToPlay = new PackedSoundData(pId, pEmitterPos, pReceiverPos);
            Notify();

            Data.SoundToPlay = null;
        }

        /// <inheritdoc />
        /// <summary>
        /// Main update.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pCollider"></param>
        /// <param name="pList"></param>
        public virtual void Update(GameTime pGameTime, TileMapCollider pCollider, List<IEntity> pList)
        {
            Systems.Update(pGameTime, this, pCollider, pList);
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the entity.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            Systems.Draw(pSpriteBatch, this);

            if (!World.Instance.WorldStatus.IsDebug) return;
            if (!Id.Equals("dropShadow"))
            {
                DrawDebug(pSpriteBatch);
            }
        }

        /// <summary>
        /// Draws debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        protected void DrawDebug(SpriteBatch pSpriteBatch)
        {
            Primitives2D.DrawRectangle(pSpriteBatch, Data.BoundingBox, Color.White, 1);
        }
    }
}