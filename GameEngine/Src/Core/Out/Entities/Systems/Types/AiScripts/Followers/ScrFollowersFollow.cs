﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Input;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Followers
{
    public class ScrFollowersFollow : SBaseAiScript
    {
        public FollowerStates CurrentFollowState { get; set; }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Input is SInputControllerPathFinding pF)
            {
                // distance between our entity and the target.
                var dist = Helpers.Dist(
                pOwner.Systems.Family.Parent.Data.Position,
                pOwner.Data.Position);

                if (CurrentFollowState == FollowerStates.None)
                {
                    if (dist > pOwner.Data.Attributes.TotalAttributes.FollowRadius)
                    {
                        if (pF.IsFinishedPath)
                            CurrentFollowState = FollowerStates.RequestPath;
                    }
                }
                else if (CurrentFollowState == FollowerStates.RequestPath)
                {
                    pF.StartPath(pOwner.Systems.Family.Parent.Data.Position);
                    CurrentFollowState = FollowerStates.FollowPath;
                }
                else if (CurrentFollowState == FollowerStates.FollowPath)
                {
                    if (pF.IsFinishedPath)
                    {
                        CurrentFollowState = FollowerStates.None;
                    }
                    else
                    {
                        pF.FollowPath(pGameTime, pOwner);
                    }
                }
            }
        }

        public enum FollowerStates
        {
            None,
            RequestPath,
            FollowPath
        }
    }
}