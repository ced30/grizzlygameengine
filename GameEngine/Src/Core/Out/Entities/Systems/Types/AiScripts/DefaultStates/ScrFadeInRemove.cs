﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates
{
    public class ScrFadeInRemove : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Systems.Renderer.IsFinishedAnimation)
            {
                if (pOwner.Data.Colors.Alpha <= 0)
                    pOwner.Data.Conditions.ToRemove = true;
                else pOwner.Data.Colors.Fade(Colors.FadeStates.In, Colors.OpacityStates.Transparent);
            }

            base.Update(pGameTime, pOwner);
        }
    }
}