﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.HitBox
{
    public class ScrHitBoxStateNormal : ScrCollideStateDefault
    {
        protected override void Collide(IEntity pOwner)
        {
            base.Collide(pOwner);
            
            if (!pOwner.Systems.Family.Parent.Systems.Ai.IsAttacking)
                pOwner.Data.Conditions.ToRemove = true;
        }
    }
}