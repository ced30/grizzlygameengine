﻿#region Usings

using System;
using System.Collections;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Followers
{
    public class ScrFollowOwnerTest : SBaseAiScript
    {
        #region Declarations

        private CoRoutine CoRoutine { get; }
        private IEntity Owner { get; set; }
        private int TargetIndex { get; set; }
        private Vector2 RallyPoint { get; set; }
        private Vector2[] Path { get; set; }
        public ScrFollowersFollow.FollowerStates CurrentFollowState { get; set; }
        private Vector2 Rounded(Vector2 pPos)
        {
            return new Vector2(
                (int)Math.Round(pPos.X),
                (int)Math.Round(pPos.Y));
        }

        public bool IsFinishedPath { get; set; }

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        public ScrFollowOwnerTest()
        {
            CoRoutine = new CoRoutine();
            IsFinishedPath = true;
        }

        protected IEnumerator FollowPath()
        {
            TargetIndex = 0;
            Vector2 currentWayPoint = Path[0];
            int tolerance = 1;

            while (true)
            {
                if (Helpers.Dist(Owner.Data.Position, currentWayPoint) <= tolerance)
                {
                    Owner.Systems.Transform.SetPosition(Owner.Data, Rounded(Owner.Data.Position));
                    TargetIndex++;

                    if (TargetIndex >= Path.Length)
                    {
                        IsFinishedPath = true;
                        yield break;
                    }
                    else
                    {
                        currentWayPoint = Path[TargetIndex];
                    }
                }
                else
                {
                    GetToWayPoint(currentWayPoint);
                    yield return null;
                }
            }
        }

        public virtual void FollowPath(GameTime pGameTime, IEntity pOwner)
        {
            if (Path != null)
            {
                if (Path.Length > 0)
                {
                    if (CoRoutine.Running)
                    {
                        CoRoutine.Update();
                    }
                }
            }
        }

        protected virtual void GetToWayPoint(Vector2 pWayPoint)
        {
            Owner.Systems.Transform.MoveWithAngles(Owner.Data, pWayPoint);
        }

        protected void OnPathFound(Vector2[] pNewPath, bool pSuccess)
        {
            if (pSuccess)
            {
                Path = pNewPath;
                TargetIndex = 0;

                if (Path != null && Path.Length > 0)
                {
                    CoRoutine.StopAll();
                    CoRoutine.Start(FollowPath());
                }
            }
        }

        public void StartPath(Vector2 pTargetPos, Vector2 pOwnerPos)
        {
            IsFinishedPath = false;
            RallyPoint = Rounded(pTargetPos);
            PathRequestManager.RequestPath(pOwnerPos, RallyPoint,
                OnPathFound);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (Owner == null)
                Owner = pOwner;

            // distance between our entity and the target.
            var dist = Helpers.Dist(
                Owner.Systems.Family.Parent.Data.Position,
                Owner.Data.Position);

            if (CurrentFollowState == ScrFollowersFollow.FollowerStates.None)
            {
                if (dist > Owner.Data.Attributes.TotalAttributes.FollowRadius)
                {
                    if (IsFinishedPath)
                        CurrentFollowState = ScrFollowersFollow.FollowerStates.RequestPath;
                }
            }
            else if (CurrentFollowState == ScrFollowersFollow.FollowerStates.RequestPath)
            {
                StartPath(Owner.Systems.Family.Parent.Data.Position, Owner.Data.Position);
                CurrentFollowState = ScrFollowersFollow.FollowerStates.FollowPath;
            }
            else if (CurrentFollowState == ScrFollowersFollow.FollowerStates.FollowPath)
            {
                if (IsFinishedPath)
                {
                    CurrentFollowState = ScrFollowersFollow.FollowerStates.None;
                }
                else
                {
                    FollowPath(pGameTime, Owner);
                }
            }

            // Sets the image horizontal flip according to the target's wayPoint position.
            Owner.Data.Conditions.IsFlipX = RallyPoint.X < Owner.Data.Center.X;
        }
    }
}