﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates
{
    public class ScrDeathStateTimer : SBaseAiScript
    {
        private Cooldown RemoveCd { get; }
        private const float RemoveTimer = 6f;

        public ScrDeathStateTimer()
        {
            RemoveCd = new Cooldown();
            RemoveCd.Start(RemoveTimer);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            pOwner.Data.Positional.SpeedCoef = 1;

            if (RemoveCd.IsFinished)
            {
                if (pOwner.Data.Colors.Alpha <= 0)
                    pOwner.Data.Conditions.ToRemove = true;
                else pOwner.Data.Colors.Fade(Colors.FadeStates.In, Colors.OpacityStates.Transparent);
            }

            RemoveCd.Update(pGameTime);
        }
    }
}