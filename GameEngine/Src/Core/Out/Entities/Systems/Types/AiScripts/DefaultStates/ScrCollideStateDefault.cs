﻿#region Usings

using System.Linq;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates
{
    public class ScrCollideStateDefault : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Ai.CurrentCollider == null)
            {
                pOwner.Data.States.SetFirstState();
                return;
            }

            if (pOwner.Systems.Ai.CurrentCollider.Data.Conditions.IsExpired)
                return;


            Collide(pOwner);
        }

        protected virtual void Collide(IEntity pOwner)
        {
            
        }
    }
}