﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates
{
    public class ScrDeathStateDefault : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Systems.Renderer.CurrentRenderer.IsAnimId("death"))
            {
                if (pOwner.Systems.Renderer.CurrentRenderer.IsFinished)
                    pOwner.Data.Conditions.IsExpired = true;
            }

            base.Update(pGameTime, pOwner);
        }
    }
}