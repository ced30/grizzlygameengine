﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Camera;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Projectiles
{
    /// <inheritdoc />
    public class ScrProjectileStateNormal : SBaseAiScript
    {
        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            base.Update(pGameTime, pOwner);

            if (pOwner.Systems.Collider.IsWorldCollisions)
            {
                if (Camera.Instance.Contains(pOwner.Data.BoundingBox))
                {
                    pOwner.PlaySound("arrowImpact", pOwner.Data.Position, pOwner.Systems.Family.Parent.Data.Position);
                }

                pOwner.Data.States.Set("death");
            }

            pOwner.Data.Positional.Velocity = new Vector2(
                pOwner.Data.Positional.Accel * (float)Dt * pOwner.Data.XFlip * pOwner.Data.Positional.SpeedCoef,
                pOwner.Data.Positional.Velocity.Y);
        }
    }
}