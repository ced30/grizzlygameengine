﻿#region Usings

using System;
using System.Collections;
using GrizzlyGameEngine.Core.Out.Components.PathFinding;
using GrizzlyGameEngine.Core.Out.Components.Primitives2D;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Input
{
    /// <inheritdoc />
    public class SInputControllerPathFinding : SDefaultInputController
    {
        #region Declarations

        private CoRoutine CoRoutine { get; }
        private IEntity Owner { get; set; }
        private int TargetIndex { get; set; }
        private Vector2 RallyPoint { get; set; }
        private Vector2[] Path { get; set; }
        private Vector2 Rounded(Vector2 pPos)
        {
            return new Vector2(
                (int)Math.Round(pPos.X),
                (int)Math.Round(pPos.Y));
        }

        public bool IsFinishedPath { get; set; }

        #endregion

        /// <summary>
        /// Constructor.
        /// </summary>
        public SInputControllerPathFinding()
        {
            CoRoutine = new CoRoutine();
            IsFinishedPath = true;
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            if (Path != null)
            {
                var tileSize = 16;
                for (int i = TargetIndex; i < Path.Length; i++)
                {
                    Primitives2D.DrawRectangleFilled(pSpriteBatch, new Rectangle(
                        (int)Path[i].X, (int)Path[i].Y,
                        tileSize, tileSize), Color.Black * 0.3f);
                }
            }
        }

        protected IEnumerator FollowPath()
        {
            TargetIndex = 0;
            Vector2 currentWayPoint = Path[0];
            int tolerance = 1;

            while (true)
            {
                if (Helpers.Dist(Owner.Data.Position, currentWayPoint) <= tolerance)
                {
                    Owner.Systems.Transform.SetPosition(Owner.Data, Rounded(Owner.Data.Position));
                    TargetIndex++;

                    if (TargetIndex >= Path.Length)
                    {
                        IsFinishedPath = true;
                        yield break;
                    }
                    else
                    {
                        currentWayPoint = Path[TargetIndex];
                    }
                }
                else
                {
                    GetToWayPoint(currentWayPoint);
                    yield return null;
                }
            }
        }

        public virtual void FollowPath(GameTime pGameTime, IEntity pOwner)
        {
            if (Path != null)
            {
                if (Path.Length > 0)
                {
                    if (CoRoutine.Running)
                    {
                        CoRoutine.Update();
                    }
                }
            }
        }

        protected virtual void GetToWayPoint(Vector2 pWayPoint)
        {
            Owner.Systems.Transform.MoveWithAngles(Owner.Data, pWayPoint);
        }

        protected void OnPathFound(Vector2[] pNewPath, bool pSuccess)
        {
            if (pSuccess)
            {
                Path = pNewPath;
                TargetIndex = 0;

                if (Path != null && Path.Length > 0)
                {
                    CoRoutine.StopAll();
                    CoRoutine.Start(FollowPath());
                }
            }
        }

        public void StartPath(Vector2 pPos)
        {
            IsFinishedPath = false;
            RallyPoint = Rounded(pPos);
            PathRequestManager.RequestPath(Owner.Data.Position, RallyPoint,
                OnPathFound);
        }

        public override void Update(GameTime pGameTime, IEntity pOwner)
        {
            if (Owner == null)
                Owner = pOwner;

            // Sets the image horizontal flip according to the target's wayPoint position.
            pOwner.Data.Conditions.IsFlipX = RallyPoint.X < pOwner.Data.Center.X;
        }
    }
}