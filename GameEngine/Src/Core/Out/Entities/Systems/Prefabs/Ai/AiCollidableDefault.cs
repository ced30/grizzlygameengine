﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai
{
    public class AiCollidableDefault : SDefaultAiController
    {
        public AiCollidableDefault(States pStates) : base(pStates)
        {
            pStates.Add("idle", new ScrFadeInRemove());
            pStates.Add("collide", new SAiStateNone());
            pStates.Set("idle");
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            pOwner.Data.States.Set("idle");
            base.OnCollision(pOwner, pCollidedBy);
        }
    }
}