﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.DefaultStates;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.HitBox;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Enums;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai
{
    public class AiHitBoxDefault : SDefaultAiController
    {
        public AiHitBoxDefault(States pStates) : base(pStates)
        {
          pStates.Add("idle", new ScrHitBoxStateNormal());
        }

        protected override void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {
            if (pCollidedBy == pOwner.Systems.Family.Parent ||
               pOwner.Id.Equals(pCollidedBy.Id))
                return;

            if (pCollidedBy.Systems.Family.Parent != null)
            {
                if (pCollidedBy.Systems.Family.Parent == pOwner.Systems.Family.Parent)
                    return;
            }

            var box = pOwner.Systems.Family.Parent.Data.BoundingBox;
            var xFlip = pOwner.Systems.Family.Parent.Data.XFlip;
            var rndX = World.Instance.R.Next(8);
            var rndY = World.Instance.R.Next(-8, 0);

            pOwner.OnNewParticleInstance = pOwner.Systems.Family.Parent.OnNewParticleInstance;

            var particleBox = new Rectangle(
                box.Center.X - box.Width / 2 + rndX + (box.Width * xFlip),
                pOwner.Data.BoundingBox.Y + rndY,
                box.Width,
                box.Height);

            // Make hit particle
            var particle = new Particle(ImporterState.StripAnim, "pickUp", particleBox);
            particle.Data.States.Replace("normal", new ScrFadeInRemove());
            pOwner.OnNewParticleInstance?.Invoke(EmitterState.Front, particle);
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            // We go to normal state.
            if (pOwner.Data.States.IsMatching("none"))
            {
                pOwner.Data.States.Set("idle");
            }

            base.UpdateGameLogic(pGameTime, pOwner);

            if (LstColliders.Count <= 0)
                pOwner.Data.Conditions.ToRemove = true;
        }
    }
}