﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai
{
    public class AiAttachedEntityDefault : SDefaultAiController
    {
        private Vector2 Offset { get; }

        public AiAttachedEntityDefault(States pStates, Vector2 pOffset) : base(pStates)
        {
            Offset = pOffset;
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            // We set the position under the parent entity and we apply the offset.
            Vector2 newPos = new Vector2(
                pOwner.Systems.Family.Parent.Data.BoundingBox.Center.X - pOwner.Data.Width / 2 + (0 - Offset.X * pOwner.Data.XFlip),
                pOwner.Systems.Family.Parent.Data.BoundingBox.Top + Offset.Y + 2);

            pOwner.Systems.Transform.SetPosition(pOwner.Data, newPos);

            // we turn the dropShadow in the same direction as the parent entity.
            pOwner.Data.Conditions.IsFlipX = !pOwner.Systems.Family.Parent.Data.Conditions.IsFlipX;

            // remove the attached entity if the parent is removed
            pOwner.Data.Conditions.ToRemove = pOwner.Systems.Family.Parent.Data.Conditions.ToRemove;
        }
    }
}