﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Types.AiScripts.Particles;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Ai
{
    public class AiParticleDefault : SDefaultAiController
    {
        public AiParticleDefault(States pStates) : base(pStates)
        {
            pStates.Add("idle", new ScrParticleStateNormal());
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            if (pOwner.Data.States.IsMatching("none"))
            {
                pOwner.Data.States.Set("idle");
            }

            base.UpdateGameLogic(pGameTime, pOwner);
        }
    }
}