﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Shader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Shader
{
    public class DefaultShaderItem : ABaseShaderItem
    {
        public DefaultShaderItem(Effect pEffect)
        {
            Effect = pEffect;
        }

        public override void Apply()
        {
            Effect.CurrentTechnique.Passes[0].Apply();
        }

        public override void Reset()
        {
            
        }

        public override void Update(GameTime pGameTime)
        {
            
        }
    }
}