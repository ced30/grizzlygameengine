﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Prefabs.Shader.Models
{
    /// <summary>
    /// This class is contains the logic for
    ///  a flashing colors shader.
    /// if you don't pass the color to the constructor,
    /// the flash color will be white by default.
    /// </summary>
    public class DefaultFlashShaderItem : DefaultShaderItem
    {
        #region Declarations

        private float _flashAmount;
        private Color FlashColor { get; }
        private const float FlashMax = 1f;
        private const float FlashStep = 0.16f;

        #endregion

        public float FlashAmount
        {
            get => _flashAmount;
            set => _flashAmount = MathHelper.Clamp(value, 0, 1);
        }

        /// <summary>
        /// With user defined color
        /// </summary>
        /// <param name="pEffect"></param>
        /// <param name="pColor"></param>
        public DefaultFlashShaderItem(Effect pEffect, Color pColor) : base(pEffect)
        {
            FlashAmount = FlashMax;
            FlashColor = pColor;
        }

        /// <summary>
        /// With default color
        /// </summary>
        /// <param name="pEffect"></param>
        public DefaultFlashShaderItem(Effect pEffect) : base(pEffect)
        {
            FlashAmount = FlashMax;
            FlashColor = Color.White;
        }

        public override void Reset()
        {
            FlashAmount = FlashMax;
        }

        public override void Update(GameTime pGameTime)
        {
            Effect.Parameters["pColor"].SetValue(new Vector4(
                FlashColor.R / 255f,
                FlashColor.G / 255f,
                FlashColor.B / 255f,
                FlashColor.A / 255f));

            Effect.Parameters["pAmount"].SetValue(FlashAmount);
            FlashAmount -= FlashStep;
        }
    }
}