﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseFamilyController : IFamilyController
    {
        public bool HasAParent => Parent != null;
        public bool HasMembers => Members.Count > 0;
        public double Dt { get; set; }
        public IEntity Parent { get; set; }
        public List<IEntity> Members { get; set; }

        /// <summary>
        /// Removes a child from the list if ToRemove is flagged.
        /// </summary>
        /// <param name="pEntity"></param>
        protected void Abandon(IEntity pEntity)
        {
            if(Members == null)
                return;

            if (pEntity.Data.Conditions.ToRemove)
                Members.Remove(pEntity);
        }

        /// <summary>
        /// Adds an entity to the list of children
        /// (children are destroyed when the parent entity is destroyed.)
        /// </summary>
        /// <param name="pChild"></param>
        /// <param name="pParent"></param>
        public void Adopt(IEntity pChild, IEntity pParent)
        {
            if (Members == null)
                Members = new List<IEntity>();

            if(pChild.Systems.Family == null)
                pChild.Systems.Family = new SDefaultFamilyController();

            pChild.Systems.Family.BeAdoptedBy(pParent);
            Members.Add(pChild);
        }

        /// <summary>
        /// Sets the parameter entity as a parent
        /// (does not add the target to the parent's group group)
        /// </summary>
        /// <param name="pParent"></param>
        public void BeAdoptedBy(IEntity pParent)
        {
            Parent = pParent;
        }

        /// <summary>
        /// Remove members if they are null
        /// </summary>
        /// <param name="pData"></param>
        public void Update(IDataController pData)
        {
            for (var i = 0; i < Members.Count; i++)
            {
                if (Members[i] == null)
                    Members.Remove(Members[i]);
            }
        }

        /// <summary>
        /// Returns a child entity with a given string identifier.
        /// </summary>
        /// <param name="pName"></param>
        /// <returns></returns>
        public IEntity GetChild(string pName)
        {
            if (Members.Count <= 0)
                return null;

            for (var i = Members.Count - 1; i >= 0; i--)
            {
                if (Members[i].Id == pName)
                    return Members[i];
            }

            return null;
        }
    }
}