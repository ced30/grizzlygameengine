﻿#region Usings

using Microsoft.Xna.Framework;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseTransformController : ITransformController
    {
        /// <inheritdoc />
        /// <summary>
        /// Delta-time, the time elapsed between 2 updates,
        /// used for movement calculations.
        /// </summary>
        public double Dt { get; set; }

        public void Bounce(IDataController pOwner, IDataController pCollider)
        {
            Vector2 cOfMass = (pOwner.Positional.Velocity + pCollider.Positional.Velocity) / 2;
            Vector2 normal1 = pCollider.Center - pOwner.Center;
            normal1.Normalize();

            pOwner.Positional.Velocity -= cOfMass;
            pOwner.Positional.Velocity = Vector2.Reflect(pOwner.Positional.Velocity, normal1);
            pOwner.Positional.Velocity += cOfMass;
        }

        /// <inheritdoc />
        /// <summary>
        /// Lerp at constant speed.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        public virtual void LerpAtConstantSpeed(IDataController pData, Vector2 pTargetPos)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Interpolates the camera position to reach the target's position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pParent"></param>
        public virtual void LerpToTarget(IDataController pData, Vector2 pParent)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// pWMove and pHMove are normals, they equals 1, 0 or -1.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pWMove"></param>
        /// <param name="pHMove"></param>
        public virtual void MoveWithNormals(IDataController pData, int pWMove, int pHMove)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Moves to the specified direction.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDirection"></param>
        public virtual void MoveWithDirection(IDataController pData, Direction pDirection)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Used to move particles around, pSpeed is the speed value.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pSpeed"></param>
        public virtual void MoveAtConstantSpeed(IDataController pData, float pSpeed)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Calculates the angle and apply forces to the initial position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        public virtual void MoveWithAngles(IDataController pData, Vector2 pTargetPos)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Adds param increment to "Rotation".
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pIncr"></param>
        public virtual void Rotate(IDataController pData, float pIncr)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the position given a param vector2
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pNewPos"></param>
        public virtual void SetPosition(IDataController pData, Vector2 pNewPos)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the angle (rotation).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pRotation"></param>
        public virtual void SetRotation(IDataController pData, float pRotation)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the scale (zoom).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pZoom"></param>
        public virtual void SetZoom(IDataController pData, float pZoom)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public virtual void Update(GameTime pGameTime, IDataController pData)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;
            var moveSpeed = pData.Positional.MovementSpeed(Dt);
            UpdatePosition(pData, moveSpeed.X, moveSpeed.Y);
        }

        /// <summary>
        /// pDx and pDy are velocity values.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        protected virtual void UpdatePosition(IDataController pData, float pDx, float pDy)
        {
            throw new System.NotImplementedException();
        }
    }
}