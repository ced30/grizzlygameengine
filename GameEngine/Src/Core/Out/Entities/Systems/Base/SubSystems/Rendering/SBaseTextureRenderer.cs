﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering
{
    public abstract class SBaseTextureRenderer : IAnimationController
    {
        public bool IsFinished => true;
        public double Dt { get; set; }
        public Texture2D Texture { get; set; }
        public abstract Vector2 OffsetPos { get; }

        public virtual void Update(GameTime pGameTime, IDataController pData)
        {

        }

        public bool IsAnimId(string pString)
        {
            return true;
        }

        public bool IsFrameGreater(float pNbFrame)
        {
            return true;
        }

        /// <inheritdoc />
        /// <summary>
        /// Offset vector used to draw the animations / textures.
        /// </summary>
        public Vector2 FrameOffset(int pFlipX)
        {
            return new Vector2(OffsetPos.X * (0 - pFlipX), OffsetPos.Y);
        }

        public Vector2 FrameCenter => new Vector2(Texture.Width / 2f, Texture.Height / 2f);

        public void PlayAnimation(string pName)
        {
            throw new System.NotImplementedException();
        }

        public void PlayFirst()
        {
            throw new System.NotImplementedException();
        }

        public void Restart()
        {
            
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the texture if the sprite has no animations.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {

        }
    }
}