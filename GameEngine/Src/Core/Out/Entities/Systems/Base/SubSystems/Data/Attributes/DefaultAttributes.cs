﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes
{
    public class DefaultAttributes : IAttributesController
    {
        /// <summary>
        /// The base attributes only change on level up and special conditions.
        /// </summary>
        public AttributesData BaseAttributes { get; set; }

        /// <summary>
        /// These are extra attributes gained form different sources (equipment, power-ups, spells, etc).
        /// </summary>
        public List<AttributesData> AttributeModifiers { get; set; }

        public AttributesData TotalAttributes
        {
            get => BaseAttributes + AttributesData.Sum(AttributeModifiers);
            set => throw new System.NotImplementedException();
        }

        public DefaultAttributes()
        {
            BaseAttributes = new AttributesData();
            AttributeModifiers = new List<AttributesData>();
        }
    }
}