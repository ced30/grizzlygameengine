﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes
{
    public class AttributesData
    {
        public int CollisionRadius
        {
            get => _collisionRadius;
            set => _collisionRadius = MathHelper.Clamp(value, 0, MaxCollisionRadius);
        }
        public int Damage
        {
            get => _damage;
            set => _damage = MathHelper.Clamp(value, 0, MaxDamage);
        }
        public int Defense
        {
            get => _defense;
            set => _defense = MathHelper.Clamp(value, 0, MaxDefense);
        }
        public int EnergyPoints
        {
            get => _energy;
            set => _energy = MathHelper.Clamp(value, 0, MaxEnergy);
        }
        public int FollowRadius
        {
            get => _followRadius;
            set => _followRadius = MathHelper.Clamp(value, 0, MaxFollowRadius);
        }
        public int HealthPoints
        {
            get => _health;
            set => _health = MathHelper.Clamp(value, 0, MaxHealth);
        }

        public int ManaPoints
        {
            get => _mana;
            set => _mana = value >= MaxMana ? MaxMana : value;
        }
        public int Speed
        {
            get => _speed;
            set => _speed = value >= MaxSpeed ? MaxSpeed : value;
        }
        public int StaminaPoints
        {
            get => _stamina;
            set => _stamina = value >= MaxStamina ? MaxStamina : value;
        }

        public int MaxCollisionRadius { get; set; }
        public int MaxDamage { get; set; }
        public int MaxDefense { get; set; }
        public int MaxEnergy { get; set; }
        public int MaxFollowRadius { get; set; }
        public int MaxHealth { get; set; }
        public int MaxMana { get; set; }
        public int MaxSpeed { get; set; }
        public int MaxStamina { get; set; }

        private int _collisionRadius;
        private int _damage;
        private int _defense;
        private int _energy;
        private int _followRadius;
        private int _health;
        private int _mana;
        private int _speed;
        private int _stamina;

        public static AttributesData operator +(AttributesData a, AttributesData b)
        {
            return new AttributesData()
            {
                CollisionRadius = a.CollisionRadius + b.CollisionRadius,
                Damage = a.Damage + b.Damage,
                Defense = a.Defense + b.Defense,
                EnergyPoints = a.EnergyPoints + b.EnergyPoints,
                FollowRadius = a.FollowRadius + b.FollowRadius,
                HealthPoints = a.HealthPoints + b.HealthPoints,
                ManaPoints = a.ManaPoints + b.ManaPoints,
                StaminaPoints = a.StaminaPoints + b.StaminaPoints,
                Speed = a.Speed + b.Speed,
            };
        }

        public static AttributesData operator -(AttributesData a, AttributesData b)
        {
            return new AttributesData()
            {
                CollisionRadius = a.CollisionRadius - b.CollisionRadius,
                Damage = a.Damage - b.Damage,
                Defense = a.Defense - b.Defense,
                EnergyPoints = a.EnergyPoints - b.EnergyPoints,
                FollowRadius = a.FollowRadius - b.FollowRadius,
                HealthPoints = a.HealthPoints - b.HealthPoints,
                ManaPoints = a.ManaPoints - b.ManaPoints,
                StaminaPoints = a.StaminaPoints - b.StaminaPoints,
                Speed = a.Speed - b.Speed,
            };
        }

        public AttributesData()
        {
            MaxCollisionRadius = 64;
            MaxDamage = 50;
            MaxDefense = 0;
            MaxEnergy = 100;
            MaxFollowRadius = 128;
            MaxHealth = 100;
            ManaPoints = 100;
            MaxSpeed = 1;
            MaxStamina = 100;

            CollisionRadius = 32;
            Damage = 50;
            Defense = 0;
            EnergyPoints = 100;
            FollowRadius = 48;
            HealthPoints = 100;
            ManaPoints = 100;
            Speed = 1;
            StaminaPoints = 100;
        }

        public static AttributesData Empty()
        {
            return new AttributesData()
            {
                CollisionRadius = 0,
                Damage = 0,
                Defense = 0,
                EnergyPoints = 0,
                FollowRadius = 0,
                HealthPoints = 0,
                ManaPoints = 0,
                Speed = 0,
                StaminaPoints = 0,
            };
        }

        /// <summary>
        /// Adds bonus attributes to the main attributes.
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns></returns>
        public static AttributesData Sum(List<AttributesData> attributes)
        {
            var finalAttributes = Empty();

            for (var i = attributes.Count - 1; i >= 0; i--)
            {
                finalAttributes += attributes[i];
            }

            return finalAttributes;
        }
    }
}