﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering
{
    public abstract class SBaseAtlasAnimator : IAnimationController
    {
        #region Declarations

        protected double Timer { get; set; }
        protected AtlasExtracted AnimationContainer { get; set; }
        protected AtlasAnimationItem CurrentAnimation { get; set; }

        public double Dt { get; set; }
        public int FrameHeight => CurrentAnimation.FrameHeight;
        public int FrameWidth => CurrentAnimation.FrameWidth;
        public Texture2D Texture { get; set; }
        public Vector2 FrameSize => new Vector2(FrameWidth, FrameHeight);
        public bool IsFinished =>
            CurrentAnimation.CurrentFrame >= CurrentAnimation.FrameCount - 1;

        public bool IsStop { get; set; }

        public Rectangle AnimBox
        {
            get
            {
                int col = CurrentAnimation.CurrentFrame % CurrentAnimation.NbXFrames + CurrentAnimation.FramePosX;
                int row = CurrentAnimation.CurrentFrame / CurrentAnimation.NbXFrames + CurrentAnimation.FramePosY;
                return new Rectangle(
                    col * FrameWidth,
                    row * FrameHeight,
                    FrameWidth,
                    FrameHeight);
            }
        }

        public Vector2 OffsetPos => CurrentAnimation.DrawOffset;

        #endregion


        /// <inheritdoc />
        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        public virtual void Update(GameTime pGameTime, IDataController pData)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;
        }

        public bool IsAnimId(string pString)
        {
            return CurrentAnimation.Identifier.Equals(pString);
        }

        public bool IsFrameGreater(float pNbFrame)
        {
            return CurrentAnimation.CurrentFrame > pNbFrame - 1;
        }

        public void PlayFirst()
        {
            CurrentAnimation = AnimationContainer.GetFirstAnimation();
        }

        public void Restart()
        {
            CurrentAnimation.CurrentFrame = 0;
            Timer = 0;
        }

        public virtual void Draw(SpriteBatch pSpriteBatch, IDataController pData) { }

        #region Helpers

        /// <inheritdoc />
        /// <summary>
        /// Offset vector used to draw the animations / textures.
        /// </summary>
        public Vector2 FrameOffset(int pFlipX)
        {
            return new Vector2(OffsetPos.X * (0 - pFlipX), OffsetPos.Y);
        }

        /// <inheritdoc />
        /// <summary>
        /// The size of the frame.
        /// </summary>
        public Vector2 FrameCenter => new Vector2(
            FrameSize.X / 2, FrameSize.Y / 2);

        /// <inheritdoc />
        /// <summary>
        /// Plays the animation which name corresponds to the string param.
        /// </summary>
        /// <param name="pName"></param>
        public void PlayAnimation(string pName)
        {
            if (CurrentAnimation.Identifier == pName)
                return;

            CurrentAnimation = AnimationContainer.GetAnimation(pName);
            CurrentAnimation.CurrentFrame = 0;
            Timer = 0;
        }


        #endregion
    }
}