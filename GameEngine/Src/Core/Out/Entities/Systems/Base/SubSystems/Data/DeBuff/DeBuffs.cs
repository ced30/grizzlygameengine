﻿using System.Collections.Generic;

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.DeBuff
{
    public class DeBuffs
    {
        private List<string> LstDeBuffs { get; set; }
        public DeBuffs()
        {
            LstDeBuffs = new List<string>()
            {
                "none"
            };
        }

        /// <summary>
        /// adds a deBuff to the list
        /// </summary>
        /// <param name="pId"></param>
        public void Add(string pId)
        {
            LstDeBuffs.Add(pId);
        }

        public bool Contains(string pId)
        {
            return LstDeBuffs.Contains(pId);
        }

        /// <summary>
        /// Removes a deBuff from the list
        /// </summary>
        /// <param name="pId"></param>
        public void Remove(string pId)
        {
            if (LstDeBuffs.Contains(pId))
                LstDeBuffs.Remove(pId);
        }

        public void Purge()
        {
            LstDeBuffs = new List<string>()
            {
                "none"
            };
        }
    }
}