﻿#region Usings

using System.Collections.Generic;
using System.Linq;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering
{
    /// <summary>
    /// Animator handling the strip animation, prefer atlas animations whenever possible.
    /// </summary>
    public abstract class SBaseStripAnimator : IAnimationController
    {
        #region Declarations

        /// <summary>
        /// The current Animation(private).
        /// </summary>
        protected StripAnimationItem _currentAnimation;

        /// <inheritdoc />
        public Texture2D Texture { get; set; }

        /// <summary>
        /// Timer is used to increment the currentFrame.
        /// </summary>
        protected double Timer { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Delta-time, the time elapsed between 2 updates.
        /// </summary>
        public double Dt { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Is the animation finished?
        /// </summary>
        public bool IsFinished =>
            _currentAnimation.CurrentFrame >= _currentAnimation.FrameCount - 1;

        /// <summary>
        /// Is the animation paused?
        /// </summary>
        public bool IsFrozen { get; set; }

        /// <summary>
        /// The dictionary containing the animations.
        /// </summary>
        public Dictionary<string, StripAnimationItem> AnimationsDictionary;

        /// <summary>
        /// The current Animation(public).
        /// </summary>
        public StripAnimationItem CurrentAnimation
        {
            get => _currentAnimation;
            set => _currentAnimation = value;
        }

        /// <summary>
        /// Represents the position and size of the frame on the atlas
        /// </summary>
        public Rectangle AnimBox
        {
            get
            {
                if (_currentAnimation.FrameCount < 2)
                {
                    return new Rectangle(
                        0,
                        0,
                        _currentAnimation.FrameWidth,
                        _currentAnimation.FrameHeight);
                }
                else
                {
                    int col = _currentAnimation.CurrentFrame % _currentAnimation.NbCol;
                    int row = _currentAnimation.CurrentFrame / _currentAnimation.NbCol;
                    return new Rectangle(
                        col * _currentAnimation.FrameWidth,
                        row * _currentAnimation.FrameHeight,
                        _currentAnimation.FrameWidth,
                        _currentAnimation.FrameHeight);
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Offset to center or offCenter the frame.
        /// </summary>
        public Vector2 OffsetPos => CurrentAnimation.DrawOffset;


        /// <summary>
        /// The size of the frame.
        /// </summary>
        public Vector2 FrameSize => new Vector2(CurrentAnimation.FrameWidth, CurrentAnimation.FrameHeight);


        #endregion


        /// <inheritdoc />
        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        public virtual void Update(GameTime pGameTime, IDataController pData)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <inheritdoc />
        /// <summary>
        /// Is the animation identifier equals to the param pIdentifier?
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public bool IsAnimId(string pId)
        {
            return CurrentAnimation.Id.Equals(pId);
        }

        /// <inheritdoc />
        /// <summary>
        /// Is the currentFrame &gt; than the parameter pNbFrame?
        /// </summary>
        /// <param name="pNbFrame"></param>
        /// <returns></returns>
        public bool IsFrameGreater(float pNbFrame)
        {
            return CurrentAnimation.CurrentFrame > pNbFrame - 1;
        }

        /// <inheritdoc />
        /// <summary>
        /// Offset vector used to draw the animations / textures.
        /// </summary>
        public Vector2 FrameOffset(int pFlipX)
        {
            return new Vector2(OffsetPos.X * (0 - pFlipX), OffsetPos.Y);
        }

        /// <inheritdoc />
        /// <summary>
        /// The size of the frame.
        /// </summary>
        public Vector2 FrameCenter => new Vector2(
            FrameSize.X / 2, FrameSize.Y / 2);

        /// <inheritdoc />
        /// <summary>
        /// Plays the animation corresponding to the pId parameter
        /// </summary>
        /// <param name="pId"></param>
        public virtual void PlayAnimation(string pId) { }

        public void PlayFirst()
        {
            _currentAnimation = AnimationsDictionary.First().Value;
        }

        public void Restart()
        {
            CurrentAnimation.CurrentFrame = 0;
            Timer = 0;
            IsFrozen = false;
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the animation.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch, IDataController pData) { }
    }
}