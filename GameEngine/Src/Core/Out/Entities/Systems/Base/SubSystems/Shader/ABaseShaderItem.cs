﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Shader
{
    public abstract class ABaseShaderItem : IShaderItem
    {
        public Effect Effect { get; set; }
        public string Id { get; set; }

        public virtual void Apply()
        {
            throw new System.NotImplementedException();
        }

        public virtual void Reset()
        {
            throw new System.NotImplementedException();
        }

        public virtual void Update(GameTime pGameTime)
        {
            throw new System.NotImplementedException();
        }
    }
}