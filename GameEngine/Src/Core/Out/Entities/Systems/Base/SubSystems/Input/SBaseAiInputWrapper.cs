﻿#region Usings

using GrizzlyGameEngine.Core.In.Inputs;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Input
{
    /// <summary>
    /// This class is used to wrap the global World.GeneralInput
    /// class to read the input in the Ai scripts.
    /// usage: open a reader, do your stuff, close the reader.
    /// </summary>
    public class SBaseAiInputWrapper
    {
        /// <summary>
        /// opens current input's reader depending on the input state.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        public void ReadInput(GameTime pGameTime, IEntity pEntity)
        {
            switch (World.Instance.Input.CurrentInputState)
            {
                case GeneralInput.InputStates.GamePad:
                    ReadGamePad(pGameTime, pEntity);
                    break;

                case GeneralInput.InputStates.Keyboard:
                    ReadKeyboard(pGameTime, pEntity);
                    break;
            }
        }

        /// <summary>
        /// Opens the keyboard input's reader.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        private void ReadKeyboard(GameTime pGameTime, IEntity pEntity)
        {
            World.Instance.Input.ReadKeyboard();
        }

        /// <summary>
        /// Opens the gamePad input's reader.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        private void ReadGamePad(GameTime pGameTime, IEntity pEntity)
        {
            World.Instance.Input.ReadGamePad();
        }

        /// <summary>
        /// Closes the current input's reader depending on the input state.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        public void CloseInput(GameTime pGameTime, IEntity pEntity)
        {
            switch (World.Instance.Input.CurrentInputState)
            {
                case GeneralInput.InputStates.GamePad:
                    CloseGamePad(pGameTime, pEntity);
                    break;

                case GeneralInput.InputStates.Keyboard:
                    CloseKeyboard(pGameTime, pEntity);
                    break;
            }
        }

        /// <summary>
        /// Closes the keyboard input reader.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        private void CloseKeyboard(GameTime pGameTime, IEntity pEntity)
        {
            World.Instance.Input.CloseKeyboard();
        }

        /// <summary>
        /// Closes the gamePad input reader.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pEntity"></param>
        private void CloseGamePad(GameTime pGameTime, IEntity pEntity)
        {
            World.Instance.Input.CloseGamePad();
        }
    }
}