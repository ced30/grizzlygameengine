﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States
{
    public class States
    {
        /// <summary>
        /// Dictionary containing the entities state scripts
        /// </summary>
        private Dictionary<string, IAiScript> DictionaryStates { get; }

        /// <summary>
        /// The current running script
        /// </summary>
        public IAiScript Current { get; set; }

        /// <summary>
        /// The previous running script.
        /// </summary>
        public IAiScript Previous { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public States()
        {
            DictionaryStates = new Dictionary<string, IAiScript>();
            Add("none", new SAiStateNone());
            Set("none");
        }

        /// <summary>
        /// Adds a state and the associated behavior.
        /// </summary>
        /// <param name="pStateId"></param>
        /// <param name="pScript"></param>
        public void Add(string pStateId, IAiScript pScript)
        {
            DictionaryStates.Add(pStateId, pScript);
            DictionaryStates[pStateId].Id = pStateId;
        }

        public IAiScript Get(string pId)
        {
            if (DictionaryStates.ContainsKey(pId))
                return DictionaryStates[pId];
            else
            {
                throw new NotImplementedException("!!! States class => The requested states is not present in the dictionary");
            }
        }

        /// <summary>
        /// Returns if the current state's Id is matching the parameter string.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public bool IsMatching(string pId)
        {
            return pId.Equals(Current.Id);
        }

        /// <summary>
        /// Replaces the state at given id with provided script
        /// </summary>
        /// <param name="pStateId"></param>
        /// <param name="pScript"></param>
        public void Replace(string pStateId, IAiScript pScript)
        {
            if (!DictionaryStates.ContainsKey(pStateId)) return;

            DictionaryStates.Remove(pStateId);
            DictionaryStates.Add(pStateId, pScript);
            DictionaryStates[pStateId].Id = pStateId;
        }

        /// <summary>
        /// updates the previous state and previous script
        /// then changes the current ones.
        /// </summary>
        /// <param name="pStateId"></param>
        public void Set(string pStateId)
        {
            Previous = Current ?? new SAiStateNone();
            Current = DictionaryStates[pStateId];
        }

        /// <summary>
        /// Sets the first state and first script.
        /// </summary>
        public void SetFirstState()
        {
            Previous = Previous == null ? new SAiStateNone() : Current;
            Current = DictionaryStates.First().Value;
        }
    }
}