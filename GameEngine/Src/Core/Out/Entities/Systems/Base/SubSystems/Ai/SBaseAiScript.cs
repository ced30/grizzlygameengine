﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Ai
{
    /// <inheritdoc />
    /// <summary>
    /// This is an AI script, containing the logic for 1 state,
    /// it contains a dictionary of coolDowns to time actions,
    /// they are automatically updated through the Update method.
    /// </summary>
    public class SBaseAiScript : IAiScript
    {
        /// <inheritdoc />
        /// <summary>
        /// Delta-time.
        /// </summary>
        public double Dt { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The name of the state.
        /// </summary>
        public string Id { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        public virtual void Update(GameTime pGameTime, IEntity pOwner)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;
        }
    }
}