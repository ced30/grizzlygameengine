﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseShaderController : IShaderController
    {
        protected Dictionary<string, IShaderItem> ShaderDictionary { get; set; }

        public IShaderItem CurrentShader { get; set; }
        protected IShaderItem PreviousShader { get; set; }

        public virtual void Add(string pName, IShaderItem pItem)
        {
            throw new System.NotImplementedException();
        }

        public virtual void Apply()
        {
            throw new System.NotImplementedException();
        }

        public virtual void Reset()
        {
            throw new System.NotImplementedException();
        }

        public virtual void SetFirstAsCurrent()
        {
            throw new System.NotImplementedException();
        }

        public virtual void Update(GameTime pGameTime)
        {
            throw new System.NotImplementedException();
        }

        public virtual void SetCurrent(string pShaderName)
        {
            throw new System.NotImplementedException();
        }
    }
}