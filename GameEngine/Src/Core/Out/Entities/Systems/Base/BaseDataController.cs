﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.DeBuff;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class BaseDataController : IDataController
    {
        /// <inheritdoc />
        /// <summary>
        /// The colors class handles colors / alpha
        /// FadeIn / FadeOut
        /// </summary>
        public Colors Colors { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Contains a list of currently applied deBuffs id
        /// </summary>
        public DeBuffs DeBuffs { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Contains the boolean properties
        /// </summary>
        public Conditions Conditions { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Contains the entity's attributes
        /// </summary>
        public DefaultAttributes Attributes { get;
            set; }

        /// <inheritdoc />
        /// <summary>
        /// Dictionary of imported Tmx properties
        /// </summary>
        public Dictionary<string, string> TmxProperties { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Dimensions
        /// </summary>
        public int Height { get; set; }
        public int Width { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Horizontal and vertical image flip
        /// </summary>
        public int XFlip
        {
            get
            {
                if (Conditions.IsFlipX)
                    return -1;
                else
                    return 1;
            }
        }
        public int YFlip
        {
            get
            {
                if (Conditions.IsFlipY)
                    return -1;
                else
                    return 1;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Contains the id or types of entities we can collide with
        /// </summary>
        public List<string> LstCanCollideWithType { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Contains the movement related data
        /// </summary>
        public PositionalData Positional { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The sound we are passing to the sound observer,
        /// null if no sound is to be played
        /// </summary>
        public PackedSoundData SoundToPlay { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Rectangle defining the entity position and dimensions
        /// </summary>
        public Rectangle BoundingBox => new Rectangle(
            (int)Position.X,
            (int)Position.Y,
            Width,
            Height);

        /// <inheritdoc />
        /// <summary>
        /// The current image flip effect
        /// </summary>
        public SpriteEffects CurrentFlipX
        {
            get
            {
                if (Conditions.IsFlipX)
                    return SpriteEffects.FlipHorizontally;
                else
                    return SpriteEffects.None;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// The entity's state machine
        /// </summary>
        public States States
        {
            get;
            set;
        }

        /// <inheritdoc />
        /// <summary>
        /// The center of the boundingBox
        /// </summary>
        public Vector2 Center => new Vector2(
            Position.X + BoundingBox.Width / 2f,
            Position.Y + BoundingBox.Height / 2f);

        /// <inheritdoc />
        /// <summary>
        /// The entity's position
        /// </summary>
        public Vector2 Position { get; set; }
    }
}