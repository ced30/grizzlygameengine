﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBasePhysicsController : IPhysicsController
    {
        public double Dt { get; set; }

        public virtual void ApplyFriction(IDataController pData)
        {

        }

        public virtual void Update(GameTime pGameTime, IDataController pData)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;

            ApplyFriction(pData);
        }
    }
}