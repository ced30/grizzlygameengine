﻿#region Usings

using System;
using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseAiController : IAiController
    {
        #region Declarations

        /// <inheritdoc />
        /// <summary>
        /// Is the player attacking at this instant?
        /// </summary>
        public bool IsAttacking { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Has a spell just been fired?
        /// </summary>
        public bool HasFired { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// The dictionary containing your coolDowns
        /// </summary>
        public Dictionary<string, Cooldown> DictionaryCoolDowns { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// the current collided entity
        /// </summary>
        public IEntity CurrentCollider { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// list of entities that the owner has collided till last update
        /// </summary>
        public List<IEntity> LstColliders { get; set; }

        public List<string> LstDeBuff { get; set; }

        #endregion


        #region Helper methods

        /// <inheritdoc />
        /// <summary>
        /// Adds a coolDown to the dictionary,
        /// takes a string and a coolDown as parameters.
        /// </summary>
        /// <param name="pCdName"></param>
        /// <param name="pCooldown"></param>
        public void AddCoolDown(string pCdName, Cooldown pCooldown)
        {
            if (DictionaryCoolDowns == null)
                DictionaryCoolDowns = new Dictionary<string, Cooldown>();

            DictionaryCoolDowns.Add(pCdName, pCooldown);
        }

        /// <inheritdoc />
        /// <summary>
        /// Returns the cooldown corresponding to the provided Id
        /// </summary>
        /// <param name="pCdId"></param>
        /// <returns></returns>
        public Cooldown GetCooldown(string pCdId)
        {
            if (DictionaryCoolDowns.ContainsKey(pCdId))
                return DictionaryCoolDowns[pCdId];
            else throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if an animation has just ended.
        /// </summary>
        /// <param name="pOwner"></param>
        protected void ProcessAnimations(IEntity pOwner)
        {
            if (pOwner.Systems.Renderer == null) return;

            if (pOwner.Systems.Renderer.IsFinishedAnimation)
            {
                OnAnimationEnd(pOwner);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Collisions logic.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollidedBy"></param>
        public virtual void Collide(IEntity pOwner, IEntity pCollidedBy)
        {
            if (LstColliders.Contains(pCollidedBy))
                return;

            CurrentCollider = pCollidedBy;
            LstColliders.Add(pCollidedBy);
            OnCollision(pOwner, pCollidedBy);
        }

        /// <inheritdoc />
        /// <summary>
        /// Collides without checking if the entity has already collided
        /// (used for damage over time effects)
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollider"></param>
        public void CollideWithoutCheck(IEntity pOwner, IEntity pCollider)
        {
            OnCollision(pOwner, pCollider);
        }

        /// <inheritdoc />
        /// <summary>
        /// Makes a HitBox.
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pRect"></param>
        public virtual void MakeHitBox(IEntity pOwner, Rectangle pRect)
        {
            throw new System.NotImplementedException();
        }

        protected void PurgeColliders()
        {
            // We check if the colliders are still alive
            for (var i = 0; i < LstColliders.Count; i++)
            {
                if (LstColliders[i] == null)
                {
                    LstColliders.Remove(LstColliders[i]);
                }
                else if (LstColliders[i].Data.Conditions.IsExpired ||
                         LstColliders[i].Data.Conditions.ToRemove)
                {
                    LstColliders.Remove(LstColliders[i]);
                }
            }

            if (LstColliders.Count <= 0)
            {
                CurrentCollider = null;
            }
        }

        #endregion


        #region Checks

        /// <summary>
        /// Checks if an animation has just started by comparing the current state and the previous state.
        /// </summary>
        /// <param name="pOwner"></param>
        protected virtual void ProcessStateChange(IEntity pOwner)
        {
            // if the previous state is not the currentState
            if (pOwner.Data.States.Previous.Equals(pOwner.Data.States.Current)) return;

            OnStateChange(pOwner);
            pOwner.Data.States.Previous = pOwner.Data.States.Current;
        }

        #endregion


        #region Actual user defined Ai functions

        /// <summary>
        /// logic executed when an animation has ended.
        /// </summary>
        /// <param name="pOwner"></param>
        protected virtual void OnAnimationEnd(IEntity pOwner) { }

        /// <summary>
        /// logic executed when an animation  Plays.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        protected virtual void OnAnimationPlay(GameTime pGameTime, IEntity pOwner) { }

        /// <summary>
        /// Goes through the list of entities who collided the owner and clears the list
        /// </summary>
        /// <param name="pOwner"></param>
        /// <param name="pCollidedBy"></param>
        protected virtual void OnCollision(IEntity pOwner, IEntity pCollidedBy)
        {

        }

        /// <summary>
        /// logic executed when an animation has begins.
        /// </summary>
        /// <param name="pOwner"></param>
        protected virtual void OnStateChange(IEntity pOwner) { }

        #endregion



        #region Updates

        /// <inheritdoc />
        /// <summary>
        /// animation logic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        public virtual void UpdateAnimations(GameTime pGameTime, IEntity pOwner)
        {
            ProcessStateChange(pOwner);

            OnAnimationPlay(pGameTime, pOwner);

            ProcessAnimations(pOwner);

            // Inherited animation logic goes after this.
        }

        /// <summary>
        /// Updates the coolDowns.
        /// </summary>
        /// <param name="pGameTime"></param>
        protected virtual void UpdateCoolDowns(GameTime pGameTime)
        {
            if (DictionaryCoolDowns == null)
                return;

            if (DictionaryCoolDowns.Count > 0)
            {
                foreach (Cooldown cD in DictionaryCoolDowns.Values)
                {
                    cD.Update(pGameTime);
                }
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Executes the Ai gameLogic.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pOwner"></param>
        public virtual void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            // Inherited gameLogic goes before this.
            pOwner.Data.States.Current.Update(pGameTime, pOwner);
            UpdateCoolDowns(pGameTime);
            PurgeColliders();
        }

        #endregion
    }
}