﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    /// <summary>
    /// This is a generic input wrapper to use with GeneralInput.
    /// </summary>
    public abstract class SBaseInputController : IInputController
    {
        public double Dt { get; set; }

        public virtual void Update(GameTime pGameTime, IEntity pOwner)
        {
            Dt = pGameTime.ElapsedGameTime.TotalSeconds;

            ReadInput(pGameTime, pOwner);
        }

        protected virtual void ReadInput(GameTime pGameTime, IEntity pOwner)
        {
        }

        protected virtual void ReadGamePad(GameTime pGameTime, IEntity pOwner)
        {

        }

        protected virtual void ReadKeyboard(GameTime pGameTime, IEntity pOwner)
        {

        }
    }
}