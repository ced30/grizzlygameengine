﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseColliderController : IColliderController
    {
        public int TileSize { get; set; }

        public bool IsWorldCollisions { get; set; }

        public virtual void AlignOnColumn(IDataController pData)
        {

        }

        public virtual void AlignOnLine(IDataController pData)
        {

        }

        public virtual void WorldCollisions(IDataController pData, TileMapCollider pCollider)
        {

        }

        public void Update(IDataController pData, TileMapCollider pCollider)
        {
            WorldCollisions(pData, pCollider);
        }
    }
}