﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Base
{
    public abstract class SBaseRenderManager : IRenderManager
    {
        public double Dt { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Is the animation finished?
        /// </summary>
        public bool IsFinishedAnimation => CurrentRenderer.IsFinished;
        public IAnimationController CurrentRenderer { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Updates the render classes depending on the state of the renderer.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        public virtual void Update(GameTime pGameTime, IDataController pData)
        {
            if (pData.Conditions.IsVisible)
            {
                pData.Colors.Update(pGameTime);
                CurrentRenderer?.Update(pGameTime, pData);
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the current render class.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {
            if (pData.Conditions.IsVisible)
                CurrentRenderer?.Draw(
                    pSpriteBatch, 
                    pData);
        }


        #region Helper Methods

        /// <inheritdoc />
        /// <summary>
        /// Play an animation depending on the current state.
        /// </summary>
        /// <param name="pName"></param>
        public void PlayAnimation(string pName)
        {
            if (CurrentRenderer == null ||
               CurrentRenderer is SDefaultTextureRenderer)
                return;

            CurrentRenderer.PlayAnimation(pName);
        }

        /// <inheritdoc />
        /// <summary>
        /// Initializes the atlas animator with an atlasAnimationContainer.
        /// </summary>
        /// <param name="pContainer"></param>
        public void Set(AtlasExtracted pContainer)
        {
            CurrentRenderer = new SDefaultAtlasAnimator(pContainer);
        }

        /// <inheritdoc />
        /// <summary>
        /// Initializes the simple Animator with a dictionary of string, simpleAnimations.
        /// </summary>
        /// <param name="pDictionary"></param>
        public void Set(Dictionary<string, StripAnimationItem> pDictionary)
        {
            CurrentRenderer = SDefaultStripAnimator.Make(pDictionary);
        }

        /// <inheritdoc />
        /// <summary>
        /// Initializes the texture.
        /// </summary>
        /// <param name="pTexturePack"></param>
        public void Set(TexturePack pTexturePack)
        {
            CurrentRenderer = new SDefaultTextureRenderer(pTexturePack);
        }

        #endregion
    }
}