﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    public class SDefaultPhysicsController : SBasePhysicsController
    {
        /// <summary>
        /// Apply physics.
        /// </summary>
        public override void ApplyFriction(IDataController pData)
        {
            // Horizontal
            if (pData.Positional.Velocity.X > 0)
            {
                pData.Positional.Velocity = new Vector2(
                    pData.Positional.Velocity.X -
                    pData.Positional.Friction * (float)Dt,
                    pData.Positional.Velocity.Y);

                if (pData.Positional.Velocity.X < 0)
                    pData.Positional.Velocity = new Vector2(0, pData.Positional.Velocity.Y);
            }
            if (pData.Positional.Velocity.X < 0)
            {
                pData.Positional.Velocity = new Vector2(
                    pData.Positional.Velocity.X +
                    pData.Positional.Friction * (float)Dt,
                    pData.Positional.Velocity.Y) ;

                if (pData.Positional.Velocity.X > 0)
                    pData.Positional.Velocity = new Vector2(0, pData.Positional.Velocity.Y);
            }

            // Vertical
            if (pData.Positional.Velocity.Y > 0)
            {
                pData.Positional.Velocity = new Vector2(
                    pData.Positional.Velocity.X,
                    pData.Positional.Velocity.Y -
                    pData.Positional.Friction * (float)Dt);
                
                if (pData.Positional.Velocity.Y < 0)
                    pData.Positional.Velocity = new Vector2(pData.Positional.Velocity.X, 0);
            }
            if (pData.Positional.Velocity.Y < 0)
            {
                pData.Positional.Velocity = new Vector2(
                    pData.Positional.Velocity.X,
                    pData.Positional.Velocity.Y +
                    pData.Positional.Friction * (float)Dt);

                if (pData.Positional.Velocity.Y > 0)
                    pData.Positional.Velocity = new Vector2(pData.Positional.Velocity.X, 0);
            }

            base.ApplyFriction(pData);
        }

        public override void Update(GameTime pGameTime, IDataController pData)
        {
            base.Update(pGameTime, pData);

            ApplyFriction(pData);
        }
    }
}