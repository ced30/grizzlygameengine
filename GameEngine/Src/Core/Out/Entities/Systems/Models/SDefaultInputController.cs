﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    /// <summary>
    /// This is a generic input wrapper to use with GeneralInput.
    /// </summary>
    public class SDefaultInputController : SBaseInputController
    {
        protected override void ReadInput(GameTime pGameTime, IEntity pOwner)
        {
            base.ReadInput(pGameTime, pOwner);

            ReadGamePad(pGameTime, pOwner);

            ReadKeyboard(pGameTime, pOwner);
        }
    }
}