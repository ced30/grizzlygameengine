﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems
{

    public class SDefaultFamilyController : SBaseFamilyController
    {
        public SDefaultFamilyController()
        {
            Members = new List<IEntity>();
        }
    }
}