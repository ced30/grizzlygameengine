﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems
{
    /// <inheritdoc />
    /// <summary>
    /// Animator handling the strip animation, prefer atlas animations whenever possible.
    /// </summary>
    public class SDefaultStripAnimator : SBaseStripAnimator
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pDictionary"></param>
        public SDefaultStripAnimator(Dictionary<string, StripAnimationItem> pDictionary)
        {
            AnimationsDictionary = pDictionary;
            _currentAnimation = GetFirst();
        }

        /// <inheritdoc />
        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        public override void Update(GameTime pGameTime, IDataController pData)
        {
            base.Update(pGameTime, pData);

            if (IsFrozen)
                return;

            if (IsFinished &&
                !CurrentAnimation.IsLooping)
                return;

            Timer += Dt;

            if (Timer > _currentAnimation.FrameSpeed * CurrentAnimation.SpeedCoef)
            {
                Timer = 0;
                _currentAnimation.CurrentFrame = (_currentAnimation.CurrentFrame + 1) % _currentAnimation.FrameCount;
            }

            if (_currentAnimation.CurrentFrame >= _currentAnimation.FrameCount)
            {
                Timer = 0;
                _currentAnimation.CurrentFrame = 0;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Draws the animation.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public override void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {
            //Draw the entity as usual.
            pSpriteBatch.Draw(_currentAnimation.Texture,
                new Vector2(
                    (int)(pData.Center.X + OffsetPos.X * pData.XFlip),
                    (int)(pData.Center.Y + OffsetPos.Y)),
                AnimBox,
                pData.Colors.Color,
                pData.Positional.Rotation,
                FrameCenter,
                pData.Positional.Zoom,
                pData.CurrentFlipX,
                0.0f
                );
        }


        #region Query Methods

        /// <summary>
        /// Returns a simpleAnimation if the dictionary contains pString.
        /// </summary>
        /// <param name="pString"></param>
        /// <returns></returns>
        public StripAnimationItem GetByName(string pString)
        {
            if (AnimationsDictionary.ContainsKey(pString))

                return AnimationsDictionary[pString];
            Debug.Print("Wrong animation name.");
            return null;
        }

        /// <summary>
        /// Returns the number of the current frame playing (int).
        /// </summary>
        /// <returns></returns>
        public int GetCurrentFrame()
        {
            return CurrentAnimation.CurrentFrame;
        }

        /// <summary>
        /// Returns true, if the current frame number is bigger or equal to pFrame1 and lower or equal to pFrame2.
        /// </summary>
        /// <param name="pFrame1"></param>
        /// <param name="pFrame2"></param>
        /// <returns></returns>
        public bool IsBetween(int pFrame1, int pFrame2)
        {
            return IsGreaterThan(pFrame1) &&
                   IsLowerThan(pFrame2);
        }

        /// <summary>
        /// Returns true if the current frame is equal to pFrame.
        /// </summary>
        /// <param name="pFrame"></param>
        /// <returns></returns>
        public bool IsFrameNb(int pFrame)
        {
            return _currentAnimation.CurrentFrame == pFrame;
        }

        /// <summary>
        /// Returns true if the current frame is greater or equal to pFrame.
        /// </summary>
        /// <param name="pFrame"></param>
        /// <returns></returns>
        public bool IsGreaterThan(int pFrame)
        {
            return CurrentAnimation.CurrentFrame >= pFrame;
        }

        /// <summary>
        /// Returns true if the current frame is lower or equal to pFrame.
        /// </summary>
        /// <param name="pFrame"></param>
        /// <returns></returns>
        public bool IsLowerThan(int pFrame)
        {
            return CurrentAnimation.CurrentFrame <= pFrame;
        }

        /// <summary>
        /// Returns true if the name of the current anim is equal to pString (string).
        /// </summary>
        /// <param name="pString"></param>
        /// <returns></returns>
        public bool IsMatching(string pString)
        {
            if (!AnimationsDictionary.ContainsKey(pString))
                return false;

            if (CurrentAnimation.Texture ==
                GetByName(pString).Texture)

                return true;
            return false;
        }

        /// <summary>
        /// Returns true if the current animation is equal to pAnim (simpleAnimation).
        /// </summary>
        /// <param name="pAnim"></param>
        /// <returns></returns>
        public bool IsMatching(StripAnimationItem pAnim)
        {
            return CurrentAnimation == pAnim;
        }

        /// <summary>
        /// Returns true if the current cycle of the animation is over.
        /// </summary>
        /// <returns></returns>
        public bool IsOver()
        {
            return IsFinished;
        }

        #endregion


        #region HelperMethods

        /// <summary>
        /// Returns the first animation of the dictionary.
        /// </summary>
        /// <returns></returns>
        public StripAnimationItem GetFirst()
        {
            return AnimationsDictionary.Values.First();
        }

        /// <summary>
        /// Plays the animation if it's not equal to the current animation.
        /// </summary>
        /// <param name="pAnimation"></param>
        public void Play(StripAnimationItem pAnimation)
        {
            if (_currentAnimation == pAnimation)
                return;

            _currentAnimation = pAnimation;

            Restart();
        }

        /// <summary>
        /// Plays the animation if it's not equal to the current animation.
        /// </summary>
        /// <param name="pAnimation"></param>
        public override void PlayAnimation(string pAnimation)
        {
            if (_currentAnimation == AnimationsDictionary[pAnimation])
                return;

            _currentAnimation = AnimationsDictionary[pAnimation];

            Restart();
        }

        /// <summary>
        /// sets the dictionary to pDico.
        /// </summary>
        /// <param name="pDico"></param>
        public void SetDictionary(Dictionary<string, StripAnimationItem> pDico)
        {
            AnimationsDictionary = pDico;
        }

        /// <summary>
        /// Sets the current frame to pFrame and freezes the animation.
        /// </summary>
        /// <param name="pFrame"></param>
        public void SetFrame(int pFrame)
        {
            CurrentAnimation.CurrentFrame = pFrame;
            Stop();
        }

        /// <summary>
        /// Sets the speed coef of the current animation.
        /// </summary>
        /// <param name="pCoef"></param>
        public void SetSpeedCoef(float pCoef)
        {
            CurrentAnimation.SetCoef(pCoef);
        }

        /// <summary>
        /// Freezes the animation.
        /// </summary>
        protected void Stop()
        {
            IsFrozen = true;
        }

        /// <summary>
        /// Returns a new AnimatorSimple, takes a dictionary string, SimpleAnimation as arguments.
        /// </summary>
        /// <param name="pAnims"></param>
        /// <returns></returns>
        public static SDefaultStripAnimator Make(Dictionary<string, StripAnimationItem> pAnims)
        {
            SDefaultStripAnimator aMan = new SDefaultStripAnimator(pAnims);
            return aMan;
        }

        #endregion
    }
}