﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems
{
    public class SDefaultTextureRenderer : SBaseTextureRenderer
    {
        public override Vector2 OffsetPos { get; }

        public SDefaultTextureRenderer(TexturePack pTexturePack)
        {
            OffsetPos = pTexturePack.OffsetPos;
            Texture = pTexturePack.Texture;
        }


        /// <inheritdoc />
        /// <summary>
        /// Draws the texture if the sprite has no animations.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public override void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {
            //Draw the entity as usual.
            if (Texture != null && pData.Conditions.IsVisible)
                pSpriteBatch.Draw(Texture,
                    new Vector2(
                        (int)(pData.Center.X + OffsetPos.X * pData.XFlip),
                        (int)(pData.Center.Y + OffsetPos.Y)),
                    null,
                    pData.Colors.Color,
                    MathHelper.ToRadians(pData.Positional.Rotation),
                    FrameCenter, 
                    pData.Positional.Zoom,
                    pData.CurrentFlipX,
                    0.0f);

            //            DrawDebug(pSpriteBatch, pData);
        }

        private void DrawDebug(SpriteBatch pSpriteBatch, IDataController pData)
        {
        }
    }
}