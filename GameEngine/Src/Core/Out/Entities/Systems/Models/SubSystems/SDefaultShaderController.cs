﻿#region Usings

using System.Collections.Generic;
using System.Linq;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems
{
    public class SDefaultShaderController : SBaseShaderController
    {
        public SDefaultShaderController()
        {
            // Loading shader into the dictionary
            // at class initialization.
            ShaderDictionary = new Dictionary<string, IShaderItem>();
        }

        public override void Add(string pId, IShaderItem pItem)
        {
            ShaderDictionary.Add(pId, pItem);
            ShaderDictionary[pId].Id = pId;
            SetFirstAsCurrent();
        }

        public override void Apply()
        {
            CurrentShader.Apply();
        }

        public override void Reset()
        {
            SetFirstAsCurrent();
        }

        public override void SetFirstAsCurrent()
        {
            if (ShaderDictionary.Count >= 0)
            {
                if (PreviousShader == null)
                {
                    PreviousShader = ShaderDictionary.First().Value;
                }
                else
                {
                    PreviousShader = CurrentShader;
                }

                CurrentShader = ShaderDictionary.First().Value;
                PreviousShader.Reset();
            }
        }

        public override void SetCurrent(string pShaderName)
        {
            if (ShaderDictionary.Count >= 0)
            {
                PreviousShader = CurrentShader;
                CurrentShader = ShaderDictionary[pShaderName];
                PreviousShader.Reset();
            }
        }

        public override void Update(GameTime pGameTime)
        {
            CurrentShader.Update(pGameTime);
        }
    }
}