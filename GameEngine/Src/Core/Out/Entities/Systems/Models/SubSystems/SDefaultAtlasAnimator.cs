﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Rendering;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.SubSystems
{
    /// <summary>
    /// This class handles atlas-animations.
    /// </summary>
    public class SDefaultAtlasAnimator : SBaseAtlasAnimator
    {
        /// <summary>
        /// Takes an atlasAnimationContainer as a parameter.
        /// </summary>
        /// <param name="pContainer"></param>
        public SDefaultAtlasAnimator(AtlasExtracted pContainer)
        {
            AnimationContainer = pContainer;
            CurrentAnimation = pContainer.GetFirstAnimation();
            PlayAnimation(CurrentAnimation.Identifier);
        }

        /// <inheritdoc />
        /// <summary>
        /// Main update method.
        /// </summary>
        /// <param name="pGameTime"></param>
        /// <param name="pData"></param>
        public override void Update(GameTime pGameTime, IDataController pData)
        {
            base.Update(pGameTime, pData);

            if (IsStop)
                return;

            if (IsFinished &&
                !CurrentAnimation.IsLooping)
                return;

            Timer += (float)pGameTime.ElapsedGameTime.TotalSeconds;

            if (Timer > CurrentAnimation.FrameDurations[CurrentAnimation.CurrentFrame] / 1000f * CurrentAnimation.SpeedCoef)
            {
                Timer = 0;
                CurrentAnimation.CurrentFrame = (CurrentAnimation.CurrentFrame + 1) % CurrentAnimation.FrameCount;
            }

            if (CurrentAnimation.CurrentFrame >= CurrentAnimation.FrameCount)
            {
                CurrentAnimation.CurrentFrame = 0;
            }
        }

        /// <summary>
        /// Draws the current frame.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        /// <param name="pData"></param>
        public override void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {
            //Draw the entity as usual.
            pSpriteBatch.Draw(
                AnimationContainer.AtlasTexture,
                new Vector2(
                    (int)(pData.Center.X + OffsetPos.X * pData.XFlip),
                    (int)(pData.Center.Y + OffsetPos.Y)),
                AnimBox,
                pData.Colors.Color,
                pData.Positional.Rotation,
                FrameCenter, 
                pData.Positional.Zoom,
                pData.CurrentFlipX,
                0.0f);
        }
    }
}