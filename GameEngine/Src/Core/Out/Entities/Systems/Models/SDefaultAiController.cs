﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    public class SDefaultAiController : SBaseAiController
    {
        public SDefaultAiController(States pStates)
        {
            LstColliders = new List<IEntity>();
            DictionaryCoolDowns = new Dictionary<string, Cooldown>();
        }

        public override void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            base.UpdateGameLogic(pGameTime, pOwner);

            if (pOwner.Data.Positional.Velocity.Y < 0)
                pOwner.Data.Conditions.IsFlipY = true;
            else if (pOwner.Data.Positional.Velocity.Y > 0)
                pOwner.Data.Conditions.IsFlipY = false;
        }
    }
}