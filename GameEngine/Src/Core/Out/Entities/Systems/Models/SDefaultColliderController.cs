﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    public class SDefaultColliderController : SBaseColliderController
    {
        public SDefaultColliderController()
        {
            TileSize = World.Instance.TileSize;
        }

        public override void AlignOnColumn(IDataController pData)
        {
            var col = Math.Floor((pData.Position.X + TileSize / 2f) / TileSize);
            pData.Position = new Vector2((float)(col * TileSize), pData.Position.Y);
        }

        public override void AlignOnLine(IDataController pData)
        {
            var lig = Math.Floor((pData.Position.Y + TileSize / 2f) / TileSize);
            pData.Position = new Vector2(pData.Position.X, (float)(lig * TileSize));
        }

        public override void WorldCollisions(IDataController pData, TileMapCollider pCollider)
        {
            if (!pData.Conditions.IsCollideAbleByWorld)
                return;

            IsWorldCollisions = false;

            var collider = pCollider;
            var collide = false;

            // Collision Right
            if (pData.Positional.Velocity.X > 0)
                collide = collider.Collide(pData, Direction.Right);

            // Stop
            if (collide)
            {
                IsWorldCollisions = true;

                pData.Positional.Velocity = new Vector2(0, pData.Positional.Velocity.Y);
                AlignOnColumn(pData);
            }

            collide = false;

            // Collision Left
            if (pData.Positional.Velocity.X < 0)
                collide = collider.Collide(pData, Direction.Left);

            // Stop
            if (collide)
            {
                IsWorldCollisions = true;

                pData.Positional.Velocity = new Vector2(0, pData.Positional.Velocity.Y);
                AlignOnColumn(pData);
            }

            collide = false;

            // Collision Above
            if (pData.Positional.Velocity.Y < 0)
            {
                collide = collider.Collide
                    (pData, Direction.Up);
            }

            if (collide)
            {
                IsWorldCollisions = true;

                pData.Positional.Velocity = new Vector2(pData.Positional.Velocity.X, 0);
                AlignOnLine(pData);
            }

            collide = false;


            // Collisions Below
            if (pData.Positional.Velocity.Y > 0)
            {
                collide = collider.Collide(pData, Direction.Down);
            }

            if (collide)
            {
                IsWorldCollisions = true;

                pData.Positional.Velocity = new Vector2(pData.Positional.Velocity.X, 0);
                AlignOnLine(pData);
            }
        }
    }
}