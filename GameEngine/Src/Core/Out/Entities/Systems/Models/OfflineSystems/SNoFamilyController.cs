﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoFamilyController : IFamilyController
    {
        public IEntity Parent { get; set; }
        public List<IEntity> Members { get; set; }

        public double Dt { get; set; }

        public void Adopt(IEntity pChild, IEntity pParent)
        {
            
        }

        public void BeAdoptedBy(IEntity pEntity)
        {
            
        }

        public void Update(IDataController pData)
        {
            
        }

        public void Draw(SpriteBatch pSpriteBatch, IEntity pEntity)
        {
            
        }

        public void Update(GameTime pGameTime, IEntity pEntity, TileMapCollider pCollider, List<IEntity> pList)
        {
            
        }

        public bool HasAParent { get; }
        public bool HasMembers { get; }

        public IEntity GetChild(string pName)
        {
            return null;
        }
    }
}