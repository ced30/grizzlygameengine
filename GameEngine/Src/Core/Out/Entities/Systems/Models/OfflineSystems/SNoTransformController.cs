﻿#region Usings

using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoTransformController : ITransformController
    {
        public double Dt { get; set; }
        public void Update(GameTime pGameTime, IDataController pData)
        {
            
        }

        public void Bounce(IDataController pOwner, IDataController pCollider)
        {
            throw new System.NotImplementedException();
        }

        public void LerpAtConstantSpeed(IDataController pData, Vector2 pTargetPos)
        {
            
        }

        public void LerpToTarget(IDataController pData, Vector2 pParent)
        {
            
        }

        public void MoveWithNormals(IDataController pData, int pWMove, int pHMove)
        {
            
        }

        public void Move(IEntity pEntity, float pDx, float pDy)
        {
            
        }

        public void MoveWithAngles(IDataController pData, Vector2 pTargetPos)
        {
            
        }

        public void MoveWithDirection(IDataController pData, Direction pDirection)
        {
            
        }

        public void SetPosition(IDataController pData, Vector2 pNewPos)
        {
            
        }

        public void Rotate(IDataController pData, float pIncr)
        {
            
        }

        public void SetRotation(IDataController pData, float pRotation)
        {
            
        }

        public void SetZoom(IDataController pData, float pZoom)
        {
            
        }

        public void MoveAtConstantSpeed(IDataController pData, float pSpeed)
        {
            
        }
    }
}