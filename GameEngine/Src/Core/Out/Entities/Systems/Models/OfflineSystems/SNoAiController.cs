﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoAiController : IAiController
    {
        public void AddScript(string pScriptName, IAiScript pScript)
        {
            throw new System.NotImplementedException();
        }

        public double Dt { get; set; }
        public void Update(GameTime pGameTime, IEntity pEntity)
        {
            throw new System.NotImplementedException();
        }

        public bool HasFired { get; set; }
        public Dictionary<string, Cooldown> DictionaryCoolDowns { get; set; }
        public Dictionary<string, IAiScript> DictionaryStates { get; set; }
        public IEntity CurrentCollider { get; set; }
        public List<IEntity> LstColliders { get; set; }
        public List<string> LstDeBuff { get; set; }

        public void AddCoolDown(string pCdName, Cooldown pCooldown)
        {
            
        }

        public void AddDeBuff(string pId)
        {
            throw new System.NotImplementedException();
        }

        public void AddState(string pStateId, IAiScript pBehavior)
        {
            throw new System.NotImplementedException();
        }

        public Cooldown GetCooldown(string pCdId)
        {
            throw new System.NotImplementedException();
        }

        public void Collide(IEntity pOwner, IEntity pCollider)
        {
            
        }

        public void CollideWithoutCheck(IEntity pOwner, IEntity pCollider)
        {
            throw new System.NotImplementedException();
        }

        public bool StateIdIsMatching(string pId)
        {
            throw new System.NotImplementedException();
        }

        public void MakeHitBox(IEntity pOwner, Rectangle pRect)
        {
            throw new System.NotImplementedException();
        }

        public void ReplaceState(string pStateId, IAiScript pScript)
        {
            throw new System.NotImplementedException();
        }

        public IAiScript CurrentState { get; set; }
        public IAiScript PreviousState { get; set; }
        public bool IsAttacking { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public void SetState(string pStateId)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateAnimations(GameTime pGameTime, IEntity pOwner)
        {
            
        }

        public void UpdateCoolDowns(GameTime pGameTime)
        {
            
        }

        public void UpdateGameLogic(GameTime pGameTime, IEntity pOwner)
        {
            
        }
    }
}