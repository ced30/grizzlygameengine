﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.TileMap.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoColliderController : IColliderController
    {
        public void Update(IDataController pData, TileMapCollider pCollider)
        {
            
        }

        public bool IsWorldCollisions { get; set; }

        public void AlignOnColumn(IDataController pData)
        {
            
        }

        public void AlignOnLine(IDataController pData)
        {
            
        }

        public void WorldCollisions(IDataController pData, TileMapCollider pCollider)
        {
            throw new System.NotImplementedException();
        }
    }
}