﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Components.SoundControl.Data;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.DeBuff;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public abstract class SNoDataController : IDataController
    {
        public Conditions Conditions { get; set; }
        public Colors Colors { get; set; }
        public DeBuffs DeBuffs { get; set; }
        public Dictionary<string, string> TmxProperties { get; set; }
        public float Accel { get; set; }
        public float BaseAccel { get; set; }
        public float BaseFriction { get; set; }
        public float BaseMaxSpeed { get; set; }
        public float Friction { get; set; }
        public float LerpValue { get; set; }
        public float MaxSpeed { get; set; }
        public float Rotation { get; set; }
        public float SpeedCoef { get; set; }
        public float Zoom { get; set; }
        public DefaultAttributes Attributes { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int HMove { get; set; }
        public int WMove { get; set; }
        public int XFlip { get; set; }
        public int YFlip { get; set; }
        public IShaderController Shader { get; set; }
        public List<string> LstCanCollideWithType { get; set; }
        public PositionalData Positional { get; set; }
        public Rectangle BoundingBox { get; set; }
        public SpriteEffects CurrentFlipX { get; set; }
        public States States { get; set; }
        public PackedSoundData SoundToPlay { get; set; }
        public Vector2 Center => Vector2.Zero;

        public Vector2 MovementSpeed(double pDt)
        {
            return Vector2.Zero;
        }

        public Vector2 Position { get; set; }

        public Vector2 Velocity { get; set; }
    }
}