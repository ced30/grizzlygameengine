﻿using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.PackedTypes;

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoRenderManager : IRenderManager
    {
        public double Dt { get; set; }
        public Vector2 DrawPos(Vector2 pPos, Vector2 pOriginImg, Vector2 pFrameCenter)
        {
            throw new System.NotImplementedException();
        }

        public Vector2 DrawOffset { get; set; }
        public Vector2 FrameSize { get; set; }
        public Vector2 FrameCenter { get; }

        public void Update(GameTime pGameTime, IDataController pData)
        {

        }

        public bool IsFinishedAnimation => true;
        public IAnimationController CurrentRenderer { get; set; }

        public Vector2 OriginImg(int pFlipX)
        {
            throw new System.NotImplementedException();
        }

        public void Draw(SpriteBatch pSpriteBatch, IDataController pData)
        {
            
        }

        public void PlayAnimation(string pName)
        {
            
        }

        public void Set(AtlasExtracted pContainer)
        {
            
        }

        public void Set(TexturePack pTexturePack)
        {
            
        }

        public void Set(Dictionary<string, StripAnimationItem> pDictionary)
        {
            
        }
    }
}