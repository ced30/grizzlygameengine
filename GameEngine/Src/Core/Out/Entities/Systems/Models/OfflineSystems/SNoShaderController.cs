﻿#region Usings

using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Types.Shader;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoShaderController : SBaseShaderController
    {
        public override void Add(string pName, IShaderItem pItem)
        {
            
        }

        public override void Apply()
        {

        }

        public override void Reset()
        {
            
        }

        public override void SetCurrent(string pShaderName)
        {
            
        }

        public override void SetFirstAsCurrent()
        {
            
        }

        public override void Update(GameTime pGameTime)
        {
            
        }
    }
}