﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems.SubSystems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SAiStateNone : IAiScript
    {
        public double Dt { get; set; }
        public string Id { get; set; }

        public void Update(GameTime pGameTime, IEntity pOwner)
        {
            
        }
    }
}