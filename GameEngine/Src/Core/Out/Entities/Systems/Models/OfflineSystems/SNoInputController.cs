﻿#region Usings

using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.OfflineSystems
{
    public class SNoInputController : IInputController
    {
        public double Dt { get; set; }
        public void Update(GameTime pGameTime, IEntity pEntity)
        {
            
        }
    }
}