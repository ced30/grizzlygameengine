﻿namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes
{
    public class Conditions
    {
        public bool IsBounceAble { get; set; }
        public bool IsBumpAble { get; set; }
        public bool IsCollideAbleByEntity { get; set; }
        public bool IsCollideAbleByWorld { get; set; }

        public bool IsExpired
        {
            get;
            set;
        }
        public bool IsFlipX { get; set; }
        public bool IsFlipY { get; set; }
        public bool IsSolid { get; set; }
        public bool IsSound { get; set; }
        public bool IsVisible { get; set; }

        public bool ToRemove
        {
            get;
            set;
        }
    }
}