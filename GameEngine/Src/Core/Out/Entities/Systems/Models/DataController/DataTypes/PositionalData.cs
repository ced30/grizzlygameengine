﻿#region Usings

using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes
{
    public class PositionalData
    {
        private float _friction;
        private float _rotation;
        private float _speedCoef;
        private float _zoom;
        private Vector2 _velocity;

        public float Accel { get; set; }
        public float BaseAccel { get; set; }
        public float BaseFriction { get; set; }
        public float BaseMaxSpeed { get; set; }

        public float Friction
        {
            get => _friction;
            set => _friction = MathHelper.Clamp(value, 0, 800);
        }

        public float LerpValue { get; set; }
        public float MaxSpeed { get; set; }

        public int HMove { get; set; }
        public int WMove { get; set; }

        public float Rotation
        {
            get => _rotation;
            set
            {
                var mod = value % 360;
                if (mod < 0)
                    mod += 180;
                _rotation = mod;
            }
        }

        public float SpeedCoef
        {
            get => _speedCoef;
            set => _speedCoef = MathHelper.Clamp(value, 0, 8);
        }

        public float Zoom
        {
            get => _zoom;
            set => _zoom = MathHelper.Clamp(value, 1, 3);
        }

        public Vector2 MovementSpeed(double pDt) => new Vector2(
            Velocity.X * (float)pDt,
            Velocity.Y * (float)pDt);

        public Vector2 Velocity
        {
            get => _velocity;
            set => _velocity = new Vector2(
                MathHelper.Clamp(value.X, 0 - MaxSpeed, MaxSpeed),
                MathHelper.Clamp(value.Y, 0 - MaxSpeed, MaxSpeed));
        }


    }
}