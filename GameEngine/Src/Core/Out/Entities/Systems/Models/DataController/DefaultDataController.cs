﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.Attributes;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.DeBuff;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base.SubSystems.Data.States;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController.DataTypes;

#endregion


namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models.DataController
{
    public class DefaultDataController : BaseDataController
    {
        public DefaultDataController()
        {
            Attributes = new DefaultAttributes();
            DeBuffs = new DeBuffs();
            LstCanCollideWithType = new List<string>();
            Positional = new PositionalData();
            Conditions = new Conditions();
            States = new States();

            Positional.Accel = 800;
            Positional.Friction = 150;
            Positional.LerpValue = 0.01f;
            Positional.MaxSpeed = 75;
            Positional.SpeedCoef = 1f;
            Positional.BaseAccel = Positional.Accel;
            Positional.BaseFriction = Positional.Friction;
            Positional.BaseMaxSpeed = Positional.MaxSpeed;
            Positional.Zoom = 1f;
        }
    }
}