﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.Colors;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    public class SDefaultRenderManager : SBaseRenderManager
    {
        public SDefaultRenderManager(IDataController pData)
        {
            pData.Colors = new Colors();
        }
    }
}