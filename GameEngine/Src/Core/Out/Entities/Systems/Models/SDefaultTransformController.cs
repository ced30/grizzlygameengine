﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Entities.Systems.Base;
using GrizzlyGameEngine.Core.Out.Enums;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities.Systems;
using GrizzlyGameEngine.Globals;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.Out.Entities.Systems.Models
{
    public class SDefaultTransformController : SBaseTransformController
    {
        /// <inheritdoc />
        /// <summary>
        /// Lerp and keeps a constant speed.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        public override void LerpAtConstantSpeed(IDataController pData, Vector2 pTargetPos)
        {
            var lerpVal = (float)(1 / Helpers.Dist(pData.Position, pTargetPos) * Dt) * 50;

            SetPosition(pData, new Vector2(MathHelper.Lerp(pData.Position.X, pTargetPos.X, lerpVal),
                MathHelper.Lerp(pData.Position.Y, pTargetPos.Y, lerpVal)));
        }

        /// <inheritdoc />
        /// <summary>
        /// Interpolates the camera position to reach the target's position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        public override void LerpToTarget(IDataController pData, Vector2 pTargetPos)
        {
            SetPosition(pData, new Vector2(MathHelper.Lerp(pData.Position.X, pTargetPos.X, pData.Positional.LerpValue),
                MathHelper.Lerp(pData.Position.Y, pTargetPos.Y, pData.Positional.LerpValue)));
        }

        /// <inheritdoc />
        /// <summary>
        /// pWMove and pHMove are normals, they equals 1, 0 or -1.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pWMove"></param>
        /// <param name="pHMove"></param>
        public override void MoveWithNormals(IDataController pData, int pWMove, int pHMove)
        {
            pData.Positional.Velocity = new Vector2(
                pData.Positional.Velocity.X + pData.Positional.Accel * pWMove * (float)Dt * pData.Positional.SpeedCoef,
                pData.Positional.Velocity.Y + pData.Positional.Accel * pHMove * (float)Dt * pData.Positional.SpeedCoef);
        }

        /// <inheritdoc />
        /// <summary>
        /// pDx and pDy are velocity values, is executed at each update.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDx"></param>
        /// <param name="pDy"></param>
        protected override void UpdatePosition(IDataController pData, float pDx, float pDy)
        {
            var direction = Vector2.Add(pData.Positional.MovementSpeed(Dt), pData.Position) - pData.Position;

            if (direction != Vector2.Zero)
                direction.Normalize();

            pData.Position = new Vector2(
                pData.Position.X + direction.X * pData.Positional.SpeedCoef, 
                pData.Position.Y + direction.Y * pData.Positional.SpeedCoef);
        }

        /// <inheritdoc />
        /// <summary>
        /// Moves to the specified direction.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pDirection"></param>
        public override void MoveWithDirection(IDataController pData, Direction pDirection)
        {
            switch (pDirection)
            {
                case Direction.Up:
                    pData.Positional.Velocity = new Vector2(
                        pData.Positional.Velocity.X,
                        pData.Positional.Velocity.Y - pData.Positional.Accel);
                    break;

                case Direction.Down:
                    pData.Positional.Velocity = new Vector2(
                        pData.Positional.Velocity.X,
                        pData.Positional.Velocity.Y + pData.Positional.Accel);
                    break;

                case Direction.Left:
                    pData.Positional.Velocity = new Vector2(
                        pData.Positional.Velocity.X - pData.Positional.Accel,
                        pData.Positional.Velocity.Y);
                    break;

                case Direction.Right:
                    pData.Positional.Velocity = new Vector2(
                        pData.Positional.Velocity.X + pData.Positional.Accel,
                        pData.Positional.Velocity.Y);
                    break;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Used to move particles around, pSpeed is the speed value.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pSpeed"></param>
        public override void MoveAtConstantSpeed(IDataController pData, float pSpeed)
        {
            var direction = new Vector2((float)Math.Cos(MathHelper.ToRadians(pData.Positional.Rotation)),
                (float)Math.Sin(MathHelper.ToRadians(pData.Positional.Rotation)));
            direction.Normalize();
            pData.Position += direction * pSpeed * pData.Positional.SpeedCoef;
        }

        /// <inheritdoc />
        /// <summary>
        /// Calculates the angle and apply forces to the initial position.
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pTargetPos"></param>
        public override void MoveWithAngles(IDataController pData, Vector2 pTargetPos)
        {
            float destinationAngle =
                (float)Helpers.Angle(
                pTargetPos,
                pData.Position);

            Vector2 direction = new Vector2(
                (float)(Math.Cos(destinationAngle) * pData.Positional.Accel * 0.3 * Dt * pData.Positional.SpeedCoef),
                (float)(Math.Sin(destinationAngle) * pData.Positional.Accel * 0.3 * Dt * pData.Positional.SpeedCoef));

            Vector2.Normalize(direction);
            pData.Positional.Velocity = Vector2.Negate(direction) * (pData.Positional.Accel * (float)Dt * pData.Positional.SpeedCoef);
        }

        /// <inheritdoc />
        /// <summary>
        /// Adds param increment to "Rotation".
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pIncr"></param>
        public override void Rotate(IDataController pData, float pIncr)
        {
            pData.Positional.Rotation += pIncr;
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the position given a param vector2
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pNewPos"></param>
        public override void SetPosition(IDataController pData, Vector2 pNewPos)
        {
            pData.Position = pNewPos;
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the angle (rotation).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pRotation"></param>
        public override void SetRotation(IDataController pData, float pRotation)
        {
            pData.Positional.Rotation = pRotation;
        }

        /// <inheritdoc />
        /// <summary>
        /// Sets the scale (zoom).
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pZoom"></param>
        public override void SetZoom(IDataController pData, float pZoom)
        {
            pData.Positional.Zoom = pZoom;
        }
    }
}