﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Components.Procedural.SubComponents;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;

#endregion

namespace GrizzlyGameEngine.Core.Out.Managers
{
    public class ProceduralManager
    {
        private SectorData CurrentSector { get; set; }
        private CellData[,] CurrentMap { get; set; }
        private ProceduralBuilder Builder { get; }
        private ProceduralDebugger Debugger { get; }
        private ProceduralPacker Packer { get; set; }
        private SpriteFont Font { get; }

        public ProceduralManager(
            Random pRandom,
            SpriteFont pFont)
        {
            Font = pFont;
            Builder = new ProceduralBuilder(pRandom);
            NewSector(4, 4, 106, 60, 16);
            Debugger = new ProceduralDebugger(CurrentSector, CurrentMap);
            Packer = new ProceduralPacker();
        }

        public void NewSector(
            int pNbQuadX, int pNbQuadY, 
            int pNbCellsX, int pNbCellsY, 
            int pTileSize)
        {
            CurrentSector = Builder.GetRawSector(pNbQuadX, pNbQuadY, pNbCellsX, pNbCellsY, pTileSize);
            CurrentMap = ProceduralBuilder.ConvertToMap(CurrentSector);
        }

        public static int GetInDoorGid(IndoorTileId pTye)
        {
            return (int)pTye;
        }

        public SectorData GetRawSector(int pNbCellX, int pNbCellY, int pTileSize)
        {
            return Builder.GetRawSector(4, 4, pNbCellX, pNbCellY, pTileSize);
        }

        public ProceduralMapData GetPackedMap(string pId, TmxTileSet pTileSet, Texture2D pTexture)
        {
            return Packer.Pack(pId, CurrentSector, CurrentMap, pTileSet, pTexture);
        }

        public void Reload()
        {
            CurrentSector = Builder.GetNew();
            CurrentMap = ProceduralBuilder.ConvertToMap(CurrentSector);
            Debugger.SetSector(CurrentSector);
        }

        public void SetCurrentQuad(Vector2 pPos)
        {
            Debugger.SetCurrentQuad(pPos);
        }

        public void SetNewSectorDimensions(int pNbQuadX, int pNbQuadY)
        {
            Builder.SetNewSectorDimensions(pNbQuadX, pNbQuadY);
        }

        public void ToggleState()
        {
            Debugger.ToggleState();
        }

        public void Draw(SpriteBatch pSpriteBatch)
        {
            Debugger.Draw(pSpriteBatch, Font);
        }
    }
}