﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Interfaces.Entities;
using GrizzlyGameEngine.Core.Out.Interfaces.Factory;
using TiledContentImporter.TiledSharp;

#endregion

namespace GrizzlyGameEngine.Core.Out.Managers
{
    /// <summary>
    /// Manages the type of factory required to produce the entity.
    /// </summary>
    public class EntityFactoryManager
    {
        private Dictionary<string, IEntityFactory> EntityFactories { get; }

        public EntityFactoryManager()
        {
            EntityFactories = new Dictionary<string, IEntityFactory>();
        }

        /// <summary>
        /// Adds a factory to the dictionary.
        /// </summary>
        public void AddFactory(string pName, IEntityFactory pFactory)
        {
            EntityFactories.Add(pName, pFactory);
        }

        /// <summary>
        /// Method 1 : pass a tmxObject containing an identifier and type
        ///            for the entity you want to create.
        /// </summary>
        /// <param name="pObj"></param>
        /// <returns></returns>
        public IEntity Get(TmxObject pObj)
        {
            return EntityFactories[pObj.Type].Make(pObj);
        }
    }
}