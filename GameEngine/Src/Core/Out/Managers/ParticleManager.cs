﻿#region Using

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components.ParticleEmitter;
using GrizzlyGameEngine.Core.Out.Entities.Prefabs;
using GrizzlyGameEngine.Core.Out.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace GrizzlyGameEngine.Core.Out.Managers
{
    public class ParticleManager
    {
        private const string CustomId = "custom";

        protected ParticleEmitterCustom BackEmitter { get; set; }
        protected List<ParticlesEmitter> FrontEmitters { get; set; }

        public ParticleManager()
        {
            BackEmitter = new ParticleEmitterCustom();
            FrontEmitters = new List<ParticlesEmitter>();
        }

        public void AddEmitter(ParticlesEmitter pEmitter)
        {
            FrontEmitters.Add(pEmitter);
        }

        public void AddParticle(EmitterState pState, Particle pParticle)
        {
            if (pState == EmitterState.Back)
            {
                BackEmitter.AddParticle(pParticle);
            }
            else if (pState == EmitterState.Front)
            {
                for (var i = FrontEmitters.Count - 1; i >= 0; i--)
                {
                    if (FrontEmitters[i].Id.Equals(CustomId))
                        FrontEmitters[i].AddParticle(pParticle);
                }
            }
        }

        public void Update(GameTime pGameTime)
        {
            // Back emitters
            BackEmitter.Update(pGameTime);

            // Front emitters
            if (FrontEmitters.Count <= 0)
                return;

            for (var i = FrontEmitters.Count - 1; i >= 0; i--)
            {
                FrontEmitters[i].Update(pGameTime);
            }
        }

        public void DrawBack(SpriteBatch pSpriteBatch)
        {
            BackEmitter.Draw(pSpriteBatch);
        }

        public void DrawFront(SpriteBatch pSpriteBatch)
        {
            if (FrontEmitters.Count <= 0)
                return;

            for (int i = 0; i < FrontEmitters.Count; i++)
            {
                FrontEmitters[i].Draw(pSpriteBatch);
            }
        }
    }
}