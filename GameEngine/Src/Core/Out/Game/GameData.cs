﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components.Procedural.Data;
using GrizzlyGameEngine.Core.Out.Components.Random;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

#endregion

namespace GrizzlyGameEngine.Core.Out.Game
{
    public class GameData
    {
        public GrizzlySeeder Seeder { get; set; }
        public Random Random { get; set; }
        public readonly GameWindow Window;
        public readonly GraphicsDeviceManager Graphics;
        public readonly ContentManager Content;
        public ProceduralMapData MapData { get; set; }

        public bool IsResizable { get; set; }
        public bool IsMouseVisible { get; set; }
        public string Title { get; set; }

        public GameData(
            ContentManager pContent,
            GameWindow window, 
            GraphicsDeviceManager graphicsDeviceManager)
        {
            Content = pContent;
            Window = window;
            Graphics = graphicsDeviceManager;

            IsResizable = true;
            IsMouseVisible = false;
            Title = "NewGame";
        }

        public void SetNewSeed(string pSeed)
        {
            if(Seeder == null)
                Seeder = new GrizzlySeeder(pSeed);
            else Seeder.SetNew(pSeed);

            MapData = new ProceduralMapData();
            Random = new Random(Seeder.Get());
        }
    }
}