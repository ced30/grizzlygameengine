﻿#region Usings

using GrizzlyGameEngine.Core.Out.Components.CoolDown;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.In.Inputs
{
    public class RumbleItem
    {
        protected Type CurrentType;

        public float LeftIntensity;
        public float RightIntensity;
        public float BothIntensity;
        public const float Dampening = 0.05f;

        public Cooldown Timer;

        public bool IsFinished;

        public RumbleItem(float pIntensity, Duration pDuration)
        {
            Timer = new Cooldown();
            BothIntensity = pIntensity;
            CurrentType = Type.SingleMotor;
            Timer.Start(GetValue(pDuration));
        }

        public RumbleItem(float pLIntensity, float pRIntensity, Duration pDuration)
        {
            Timer = new Cooldown();
            CurrentType = Type.DualMotors;
            LeftIntensity = pLIntensity;
            RightIntensity = pRIntensity;
            Timer.Start(GetValue(pDuration));
        }

        private float GetValue(Duration pDuration)
        {
            switch (pDuration)
            {
                case Duration.ExtremelyShort:
                    return 0.1f;

                case Duration.VeryShort:
                    return 0.16f;

                case Duration.Short:
                    return 0.26f;

                case Duration.Medium:
                    return 0.36f;

                case Duration.Long:
                    return 0.46f;

                default:
                    return 0f;
            }
        }

        public void Update(GameTime pGameTime)
        {
            if (Timer.IsFinished)
            {
                IsFinished = true;
            }

            Rumble();

            Timer.Update(pGameTime);

            Dampen();
        }

        private void StopVibrations()
        {
            Microsoft.Xna.Framework.Input.GamePad.SetVibration(PlayerIndex.One, 0, 0);
        }

        private void Rumble()
        {
            if (!IsFinished)
            {
                switch (CurrentType)
                {
                    case Type.SingleMotor:
                        Microsoft.Xna.Framework.Input.GamePad.SetVibration(PlayerIndex.One, BothIntensity, BothIntensity);
                        break;

                    case Type.DualMotors:
                        Microsoft.Xna.Framework.Input.GamePad.SetVibration(PlayerIndex.One, LeftIntensity, RightIntensity);
                        break;
                }
            }
            else
            {
                StopVibrations();
            }
        }

        private void Dampen()
        {
            switch (CurrentType)
            {
                case Type.SingleMotor:
                    BothIntensity -= Dampening;
                    if (BothIntensity < 0)
                        BothIntensity = 0;
                    break;

                case Type.DualMotors:
                    LeftIntensity -= Dampening;
                    if (LeftIntensity < 0)
                        LeftIntensity = 0;
                    RightIntensity -= Dampening;
                    if (RightIntensity < 0)
                        RightIntensity = 0;
                    break;
            }
        }

        public enum Type
        {
            SingleMotor,
            DualMotors,
        }
        public enum Duration
        {
            ExtremelyShort,
            VeryShort,
            Short,
            Medium,
            Long,
        }
    }
}