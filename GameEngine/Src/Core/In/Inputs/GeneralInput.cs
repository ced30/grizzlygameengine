﻿#region Usings

using System;
using GrizzlyGameEngine.Core.Out.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

#endregion

namespace GrizzlyGameEngine.Core.In.Inputs
{
    /// <inheritdoc />
    /// <summary>
    /// This class handles the keyboard inputs,
    /// gamePad inputs and gamePad vibrations.
    /// </summary>
    public class GeneralInput : GrizzlyComponent
    {
        /// <summary>
        /// GamePad buttons.
        /// </summary>
        public GpButton GamePad; // GamePad

        /// <summary>
        /// Keyboard keys.
        /// </summary>
        public KbKey Keyboard; // Keyboard

        /// <summary>
        /// Controller states
        /// </summary>
        public InputStates CurrentInputState;

        /// <summary>
        /// GamePad vibrations module.
        /// </summary>
        protected Rumbler Rumbler;

        /// <summary>
        /// Counter to increment state changes.
        /// </summary>
        private int _toggleCounter;

        /// <summary>
        /// Constructor.
        /// </summary>
        public GeneralInput()
        {
            GamePad = new GpButton();
            Keyboard = new KbKey();
            Rumbler = new Rumbler();

            LoadContent();
            SetInputState(InputStates.Keyboard);
        }



        /// <summary>
        /// Initialize GamePad and Keyboard.
        /// </summary>
        public new void LoadContent()
        {
            InitGamePad();
            InitKeyboard();

            base.LoadContent();
        }



        /// <summary>
        /// Initializes the gamePad and the buttons booleans.
        /// </summary>
        private void InitGamePad()
        {
            // Buttons
            GamePad.ButtonA = Buttons.A;
            GamePad.ButtonB = Buttons.B;
            GamePad.ButtonX = Buttons.X;
            GamePad.ButtonY = Buttons.Y;
            GamePad.ButtonStart = Buttons.Start;
            GamePad.ButtonSelect = Buttons.Back;
            GamePad.LeftShoulder = Buttons.LeftShoulder;
            GamePad.RightShoulder = Buttons.RightShoulder;

            // Pad initialization
            GamePad.GamePad1 = Microsoft.Xna.Framework.Input.GamePad.GetCapabilities(PlayerIndex.One);

            if (GamePad.GamePad1.IsConnected)
            {
                GamePad.OldGpState = Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex.One);

                // Init pressed state.
                GamePad.OldButtonAPressed = GamePad.OldGpState.Buttons.A == ButtonState.Pressed;
                GamePad.OldButtonBPressed = GamePad.OldGpState.Buttons.B == ButtonState.Pressed;
                GamePad.OldButtonSelectPressed = GamePad.OldGpState.Buttons.Back == ButtonState.Pressed;
                GamePad.OldButtonStartPressed = GamePad.OldGpState.Buttons.Start == ButtonState.Pressed;
                GamePad.OldButtonXPressed = GamePad.OldGpState.Buttons.X == ButtonState.Pressed;
                GamePad.OldButtonYPressed = GamePad.OldGpState.Buttons.Y == ButtonState.Pressed;
                GamePad.OldRightShoulderPressed = GamePad.OldGpState.Buttons.RightShoulder == ButtonState.Pressed;
                GamePad.OldLeftShoulderPressed = GamePad.OldGpState.Buttons.LeftShoulder == ButtonState.Pressed;

                // Init Released state.
                GamePad.OldButtonAReleased = GamePad.OldGpState.Buttons.A == ButtonState.Released;
                GamePad.OldButtonBReleased = GamePad.OldGpState.Buttons.B == ButtonState.Released;
                GamePad.OldButtonSelectReleased = GamePad.OldGpState.Buttons.Back == ButtonState.Released;
                GamePad.OldButtonStartReleased = GamePad.OldGpState.Buttons.Start == ButtonState.Released;
                GamePad.OldButtonXReleased = GamePad.OldGpState.Buttons.X == ButtonState.Released;
                GamePad.OldButtonYReleased = GamePad.OldGpState.Buttons.Y == ButtonState.Released;
                GamePad.OldRightShoulderReleased = GamePad.OldGpState.Buttons.RightShoulder == ButtonState.Released;
                GamePad.OldLeftShoulderReleased = GamePad.OldGpState.Buttons.LeftShoulder == ButtonState.Released;
            }
        }

        /// <summary>
        /// Initializes the keyboard and the keys booleans.
        /// </summary>
        private void InitKeyboard()
        {
            // Keys
            Keyboard.Escape = Keys.Escape;

            Keyboard.Up = Keys.Up;
            Keyboard.Down = Keys.Down;
            Keyboard.Left = Keys.Left;
            Keyboard.Right = Keys.Right;
            Keyboard.Action = Keys.D;
            Keyboard.Attack = Keys.E;
            Keyboard.Cancel = Keys.LeftShift;
            Keyboard.Jump = Keys.Space;
            Keyboard.Select = Keys.I;
            Keyboard.Start = Keys.Tab;
            Keyboard.LeftShoulder = Keys.Q;
            Keyboard.RightShoulder = Keys.A;
            Keyboard.RightTrigger = Keys.S;
            Keyboard.LeftTrigger = Keys.X;

            Keyboard.OldState = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            // Init pressed state.
            Keyboard.OldEscapePressed = Keyboard.OldState.IsKeyDown(Keyboard.Escape);

            Keyboard.OldUpPressed = Keyboard.OldState.IsKeyDown(Keyboard.Up);
            Keyboard.OldDownPressed = Keyboard.OldState.IsKeyDown(Keyboard.Down);
            Keyboard.OldLeftPressed = Keyboard.OldState.IsKeyDown(Keyboard.Left);
            Keyboard.OldRightPressed = Keyboard.OldState.IsKeyDown(Keyboard.Right);
            Keyboard.OldActionPressed = Keyboard.OldState.IsKeyDown(Keyboard.Action);
            Keyboard.OldAttackPressed = Keyboard.OldState.IsKeyDown(Keyboard.Attack);
            Keyboard.OldDashPressed = Keyboard.OldState.IsKeyDown(Keyboard.Cancel);
            Keyboard.OldJumpPressed = Keyboard.OldState.IsKeyDown(Keyboard.Jump);
            Keyboard.OldSelectPressed = Keyboard.OldState.IsKeyDown(Keyboard.Select);
            Keyboard.OldStartPressed = Keyboard.OldState.IsKeyDown(Keyboard.Start);
            Keyboard.OldLeftShoulderPressed = Keyboard.OldState.IsKeyDown(Keyboard.LeftShoulder);
            Keyboard.OldRightShoulderPressed = Keyboard.OldState.IsKeyDown(Keyboard.RightShoulder);
            Keyboard.OldLeftTriggerPressed = Keyboard.OldState.IsKeyDown(Keyboard.LeftTrigger);
            Keyboard.OldRightTriggerPressed = Keyboard.OldState.IsKeyDown(Keyboard.RightTrigger);

            // Init Released state.
            Keyboard.OldEscapePressed = Keyboard.OldState.IsKeyUp(Keyboard.Escape);

            Keyboard.OldUpReleased = Keyboard.OldState.IsKeyUp(Keyboard.Up);
            Keyboard.OldDownReleased = Keyboard.OldState.IsKeyUp(Keyboard.Down);
            Keyboard.OldLeftReleased = Keyboard.OldState.IsKeyUp(Keyboard.Left);
            Keyboard.OldRightReleased = Keyboard.OldState.IsKeyUp(Keyboard.Right);
            Keyboard.OldActionReleased = Keyboard.OldState.IsKeyUp(Keyboard.Action);
            Keyboard.OldAttackReleased = Keyboard.OldState.IsKeyUp(Keyboard.Attack);
            Keyboard.OldDashReleased = Keyboard.OldState.IsKeyUp(Keyboard.Cancel);
            Keyboard.OldJumpReleased = Keyboard.OldState.IsKeyUp(Keyboard.Jump);
            Keyboard.OldSelectReleased = Keyboard.OldState.IsKeyUp(Keyboard.Select);
            Keyboard.OldStartReleased = Keyboard.OldState.IsKeyUp(Keyboard.Start);
            Keyboard.OldLeftShoulderReleased = Keyboard.OldState.IsKeyUp(Keyboard.LeftShoulder);
            Keyboard.OldRightShoulderReleased = Keyboard.OldState.IsKeyUp(Keyboard.RightShoulder);
            Keyboard.OldLeftTriggerReleased = Keyboard.OldState.IsKeyUp(Keyboard.LeftTrigger);
            Keyboard.OldRightTriggerReleased = Keyboard.OldState.IsKeyUp(Keyboard.RightTrigger);
        }



        /// <inheritdoc />
        /// <summary>
        /// Updates the rumble module.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            if (GamePad.GamePad1.IsConnected)
            {
                if (GamePad.GamePad1.HasLeftVibrationMotor &&
                    GamePad.GamePad1.HasRightVibrationMotor)
                {
                    Rumbler.Update(pGameTime);
                }
            }
        }



        /// <summary>
        /// Read the GamePad for pressed and released buttons.
        /// </summary>
        public void ReadGamePad()
        {
            if (!GamePad.GamePad1.IsConnected) return;
            UpdateGamePadState();

            // Directions
            if (!GamePad.GamePad1.HasLeftXThumbStick) return;
            var leftAnalogX = GamePad.NewGpState.ThumbSticks.Left.X;
            var leftAnalogY = GamePad.NewGpState.ThumbSticks.Left.Y;

            // Left
            if (leftAnalogX < 0 - 0.5f)
            {
                GamePad.Left = true;
            }
            // Right
            else if (leftAnalogX > 0.5f)
            {
                GamePad.Right = true;
            }
            else
            {
                GamePad.Left = false;
                GamePad.Right = false;
            }

            // Down
            if (leftAnalogY < 0 - 0.5f)
            {
                GamePad.Down = true;
            }
            // Up
            else if (leftAnalogY > 0.5f)
            {
                GamePad.Up = true;
            }
            else
            {
                GamePad.Up = false;
                GamePad.Down = false;
            }
        }

        /// <summary>
        /// Read the Keyboard for pressed and released keys.
        /// </summary>
        public void ReadKeyboard()
        {
            UpdateKeyBoardState();
        }



        /// <summary>
        /// Reads GamePad's current state.
        /// </summary>
        private void UpdateGamePadState()
        {
            GamePad.NewGpState = Microsoft.Xna.Framework.Input.GamePad.GetState(
                PlayerIndex.One, GamePadDeadZone.IndependentAxes);
            GamePad.ButtonAPressed = GamePad.NewGpState.Buttons.A == ButtonState.Pressed;
            GamePad.ButtonBPressed = GamePad.NewGpState.Buttons.B == ButtonState.Pressed;
            GamePad.ButtonSelectPressed = GamePad.NewGpState.Buttons.Back == ButtonState.Pressed;
            GamePad.ButtonStartPressed = GamePad.NewGpState.Buttons.Start == ButtonState.Pressed;
            GamePad.ButtonXPressed = GamePad.NewGpState.Buttons.X == ButtonState.Pressed;
            GamePad.ButtonYPressed = GamePad.NewGpState.Buttons.Y == ButtonState.Pressed;
            GamePad.LeftShoulderPressed = GamePad.NewGpState.Buttons.LeftShoulder == ButtonState.Pressed;
            GamePad.RightShoulderPressed = GamePad.NewGpState.Buttons.RightShoulder == ButtonState.Pressed;

            GamePad.ButtonAReleased = GamePad.NewGpState.Buttons.A == ButtonState.Released;
            GamePad.ButtonBReleased = GamePad.NewGpState.Buttons.B == ButtonState.Released;
            GamePad.ButtonXReleased = GamePad.NewGpState.Buttons.X == ButtonState.Released;
            GamePad.ButtonYReleased = GamePad.NewGpState.Buttons.Y == ButtonState.Released;
            GamePad.ButtonStartReleased = GamePad.NewGpState.Buttons.Start == ButtonState.Released;
            GamePad.ButtonSelectReleased = GamePad.NewGpState.Buttons.Back == ButtonState.Released;
            GamePad.RightShoulderReleased = GamePad.NewGpState.Buttons.RightShoulder == ButtonState.Released;
            GamePad.LeftShoulderReleased = GamePad.NewGpState.Buttons.LeftShoulder == ButtonState.Released;
        }

        /// <summary>
        /// Reads Keyboard's current state.
        /// </summary>
        private void UpdateKeyBoardState()
        {
            Keyboard.NewKbState = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            // Init pressed state.
            Keyboard.EscapePressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Escape);

            Keyboard.UpPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Up);
            Keyboard.DownPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Down);
            Keyboard.LeftPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Left);
            Keyboard.RightPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Right);
            Keyboard.ActionPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Action);
            Keyboard.AttackPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Attack);
            Keyboard.DashPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Cancel);
            Keyboard.JumpPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Jump);
            Keyboard.SelectPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Select);
            Keyboard.StartPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.Start);
            Keyboard.LeftShoulderPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.LeftShoulder);
            Keyboard.RightShoulderPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.RightShoulder);
            Keyboard.LeftTriggerPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.LeftTrigger);
            Keyboard.RightTriggerPressed = Keyboard.NewKbState.IsKeyDown(Keyboard.RightTrigger);

            // Init Released state.
            Keyboard.EscapeReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Escape);

            Keyboard.UpReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Up);
            Keyboard.DownReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Down);
            Keyboard.LeftReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Left);
            Keyboard.RightReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Right);
            Keyboard.ActionReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Action);
            Keyboard.AttackReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Attack);
            Keyboard.DashReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Cancel);
            Keyboard.JumpReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Jump);
            Keyboard.SelectReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Select);
            Keyboard.StartReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.Start);
            Keyboard.LeftShoulderReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.LeftShoulder);
            Keyboard.RightShoulderReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.RightShoulder);
            Keyboard.LeftTriggerReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.LeftTrigger);
            Keyboard.RightTriggerReleased = Keyboard.NewKbState.IsKeyUp(Keyboard.RightTrigger);
        }



        /// <summary>
        /// The old gamePad input becomes the new.
        /// </summary>
        public void CloseGamePad()
        {
            // Directions.
            GamePad.OldUp = GamePad.Up;
            GamePad.OldDown = GamePad.Down;
            GamePad.OldLeft = GamePad.Left;
            GamePad.OldRight = GamePad.Right;

            // Pressed.
            GamePad.OldButtonAPressed = GamePad.ButtonAPressed;
            GamePad.OldButtonBPressed = GamePad.ButtonBPressed;
            GamePad.OldButtonSelectPressed = GamePad.ButtonSelectPressed;
            GamePad.OldButtonStartPressed = GamePad.ButtonStartPressed;
            GamePad.OldButtonXPressed = GamePad.ButtonXPressed;
            GamePad.OldButtonYPressed = GamePad.ButtonYPressed;
            GamePad.OldLeftShoulderPressed = GamePad.LeftShoulderPressed;
            GamePad.OldRightShoulderPressed = GamePad.RightShoulderPressed;

            // Released.
            GamePad.OldButtonAReleased = GamePad.ButtonAReleased;
            GamePad.OldButtonBReleased = GamePad.ButtonBReleased;
            GamePad.OldButtonSelectReleased = GamePad.ButtonSelectReleased;
            GamePad.OldButtonStartReleased = GamePad.ButtonStartReleased;
            GamePad.OldButtonXReleased = GamePad.ButtonXReleased;
            GamePad.OldButtonYReleased = GamePad.ButtonYReleased;
            GamePad.OldLeftShoulderReleased = GamePad.LeftShoulderReleased;
            GamePad.OldRightShoulderReleased = GamePad.RightShoulderReleased;

            GamePad.OldGpState = GamePad.NewGpState;
        }

        /// <summary>
        /// The old keyboard input becomes the new.
        /// </summary>
        public void CloseKeyboard()
        {
            // Init pressed state.
            Keyboard.OldEscapePressed = Keyboard.EscapePressed;

            Keyboard.OldUpPressed = Keyboard.UpPressed;
            Keyboard.OldDownPressed = Keyboard.DownPressed;
            Keyboard.OldLeftPressed = Keyboard.LeftPressed;
            Keyboard.OldRightPressed = Keyboard.RightPressed;
            Keyboard.OldActionPressed = Keyboard.ActionPressed;
            Keyboard.OldAttackPressed = Keyboard.AttackPressed;
            Keyboard.OldDashPressed = Keyboard.DashPressed;
            Keyboard.OldJumpPressed = Keyboard.JumpPressed;
            Keyboard.OldSelectPressed = Keyboard.SelectPressed;
            Keyboard.OldStartPressed = Keyboard.StartPressed;
            Keyboard.OldLeftShoulderPressed = Keyboard.LeftShoulderPressed;
            Keyboard.OldRightShoulderPressed = Keyboard.RightShoulderPressed;
            Keyboard.OldLeftTriggerPressed = Keyboard.LeftTriggerPressed;
            Keyboard.OldRightTriggerPressed = Keyboard.RightTriggerPressed;

            // Init Released state.
            Keyboard.OldEscapeReleased = Keyboard.EscapeReleased;

            Keyboard.OldUpReleased = Keyboard.UpReleased;
            Keyboard.OldDownReleased = Keyboard.DownReleased;
            Keyboard.OldLeftReleased = Keyboard.LeftReleased;
            Keyboard.OldRightReleased = Keyboard.RightReleased;
            Keyboard.OldActionReleased = Keyboard.ActionReleased;
            Keyboard.OldAttackReleased = Keyboard.AttackReleased;
            Keyboard.OldDashReleased = Keyboard.DashReleased;
            Keyboard.OldJumpReleased = Keyboard.JumpReleased;
            Keyboard.OldSelectReleased = Keyboard.SelectReleased;
            Keyboard.OldStartReleased = Keyboard.StartReleased;
            Keyboard.OldLeftShoulderReleased = Keyboard.LeftShoulderReleased;
            Keyboard.OldRightShoulderReleased = Keyboard.RightShoulderReleased;
            Keyboard.OldLeftTriggerReleased = Keyboard.LeftTriggerReleased;
            Keyboard.OldRightTriggerReleased = Keyboard.RightTriggerReleased;

            Keyboard.OldState = Keyboard.NewKbState;
        }



        /// <summary>
        /// Sets a state for the input.
        /// </summary>
        /// <param name="pScheme"></param>
        public void SetInputState(InputStates pScheme)
        {
            CurrentInputState = pScheme;
            _toggleCounter = (int) CurrentInputState;
        }

        /// <summary>
        /// Cycles through the input states.
        /// </summary>
        public void ToggleControls()
        {
            _toggleCounter++;
            if (_toggleCounter > Enum.GetValues(typeof(InputStates)).Length - 1)
            {
                _toggleCounter = 0;
            }

            SetInputState((InputStates)_toggleCounter);
        }

        #region Vibrations
        
        /// <summary>
        /// Sets the gamePad vibrations on, the 2 motors receive the same intensity.
        /// </summary>
        /// <param name="pIntensity"></param>
        /// <param name="pDuration"></param>
        public void Rumble(float pIntensity, RumbleItem.Duration pDuration)
        {
            if(CurrentInputState == InputStates.Keyboard)
                return;

            Rumbler.Add(pIntensity, pDuration);
        }

        /// <summary>
        /// Sets the gamePad vibrations on, the 2 motors receive different intensities.
        /// </summary>
        /// <param name="pRIntensity"></param>
        /// <param name="pDuration"></param>
        /// <param name="pLIntensity"></param>
        public void Rumble(float pLIntensity, float pRIntensity, RumbleItem.Duration pDuration)
        {
            if (CurrentInputState == InputStates.Keyboard)
                return;

            Rumbler.Add(pLIntensity, pRIntensity, pDuration);
        }

        #endregion

        /// <summary>
        /// The different input states.
        /// </summary>
        public enum InputStates
        {
            GamePad,
            Keyboard,
        }
    }
}