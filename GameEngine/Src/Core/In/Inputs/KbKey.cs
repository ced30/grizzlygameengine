﻿using Microsoft.Xna.Framework.Input;

namespace GrizzlyGameEngine.Core.In.Inputs
{
    /// <summary>
    /// This class holds the keyboard keys and their states as booleans.
    /// </summary>
    public struct KbKey
    {
        #region State

        public KeyboardState OldState;
        public KeyboardState NewKbState;

        #endregion


        #region Keys

        public Keys Escape { get; set; }

        public Keys Up { get; set; }
        public Keys Down { get; set; }
        public Keys Left { get; set; }
        public Keys Right { get; set; }

        public Keys Action { get; set; }
        public Keys Attack { get; set; }
        public Keys Cancel { get; set; }
        public Keys Jump { get; set; }
        public Keys Select { get; set; }
        public Keys Start { get; set; }
        public Keys LeftShoulder { get; set; }
        public Keys RightShoulder { get; set; }
        public Keys LeftTrigger { get; set; }
        public Keys RightTrigger { get; set; }

        #endregion


        #region Pressed

        // New
        
        public bool EscapePressed { get; set; }

        public bool UpPressed { get; set; }
        public bool DownPressed { get; set; }
        public bool LeftPressed { get; set; }
        public bool RightPressed { get; set; }

        public bool ActionPressed { get; set; }
        public bool AttackPressed { get; set; }
        public bool DashPressed { get; set; }
        public bool JumpPressed { get; set; }
        public bool SelectPressed { get; set; }
        public bool StartPressed { get; set; }
        public bool LeftShoulderPressed { get; set; }
        public bool RightShoulderPressed { get; set; }
        public bool LeftTriggerPressed { get; set; }
        public bool RightTriggerPressed { get; set; }

        // Old
        
        public bool OldEscapePressed { get; set; }

        public bool OldUpPressed { get; set; }
        public bool OldDownPressed { get; set; }
        public bool OldLeftPressed { get; set; }
        public bool OldRightPressed { get; set; }

        public bool OldActionPressed { get; set; }
        public bool OldAttackPressed { get; set; }
        public bool OldDashPressed { get; set; }
        public bool OldJumpPressed { get; set; }
        public bool OldSelectPressed { get; set; }
        public bool OldStartPressed { get; set; }
        public bool OldLeftShoulderPressed { get; set; }
        public bool OldRightShoulderPressed { get; set; }
        public bool OldLeftTriggerPressed { get; set; }
        public bool OldRightTriggerPressed { get; set; }

        #endregion


        #region Released

        // New
        public bool EscapeReleased { get; set; }

        public bool UpReleased { get; set; }
        public bool DownReleased { get; set; }
        public bool LeftReleased { get; set; }
        public bool RightReleased { get; set; }

        public bool ActionReleased { get; set; }
        public bool AttackReleased { get; set; }
        public bool DashReleased { get; set; }
        public bool JumpReleased { get; set; }
        public bool SelectReleased { get; set; }
        public bool StartReleased { get; set; }
        public bool LeftShoulderReleased { get; set; }
        public bool RightShoulderReleased { get; set; }
        public bool LeftTriggerReleased { get; set; }
        public bool RightTriggerReleased { get; set; }

        // Old
        public bool OldEscapeReleased { get; set; }

        public bool OldUpReleased { get; set; }
        public bool OldDownReleased { get; set; }
        public bool OldLeftReleased { get; set; }
        public bool OldRightReleased { get; set; }

        public bool OldActionReleased { get; set; }
        public bool OldAttackReleased { get; set; }
        public bool OldDashReleased { get; set; }
        public bool OldJumpReleased { get; set; }
        public bool OldSelectReleased { get; set; }
        public bool OldStartReleased { get; set; }
        public bool OldLeftShoulderReleased { get; set; }
        public bool OldRightShoulderReleased { get; set; }
        public bool OldLeftTriggerReleased { get; set; }
        public bool OldRightTriggerReleased { get; set; }

        #endregion
    }
}