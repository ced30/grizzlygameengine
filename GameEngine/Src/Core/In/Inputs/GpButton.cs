﻿#region Usings

using Microsoft.Xna.Framework.Input;

#endregion

namespace GrizzlyGameEngine.Core.In.Inputs
{
    /// <summary>
    /// This class holds all the buttons and their states as booleans.
    /// </summary>
    public struct GpButton
    {
        // Buttons
        public Buttons ButtonA { get; set; }
        public Buttons ButtonB { get; set; }
        public Buttons ButtonSelect { get; set; }
        public Buttons ButtonStart { get; set; }
        public Buttons ButtonX { get; set; }
        public Buttons ButtonY { get; set; }
        public Buttons LeftShoulder { get; set; }
        public Buttons RightShoulder { get; set; }


        #region Buttons Pressed

        // New
        public bool ButtonAPressed { get; set; }
        public bool ButtonBPressed { get; set; }
        public bool ButtonSelectPressed { get; set; }
        public bool ButtonStartPressed { get; set; }
        public bool ButtonXPressed { get; set; }
        public bool ButtonYPressed { get; set; }
        public bool RightShoulderPressed { get; set; }
        public bool LeftShoulderPressed { get; set; }

        // Old
        public bool OldButtonAPressed { get; set; }
        public bool OldButtonBPressed { get; set; }
        public bool OldButtonSelectPressed { get; set; }
        public bool OldButtonStartPressed { get; set; }
        public bool OldButtonXPressed { get; set; }
        public bool OldButtonYPressed { get; set; }
        public bool OldRightShoulderPressed { get; set; }
        public bool OldLeftShoulderPressed { get; set; }

        #endregion


        #region Buttons Released

        // New
        public bool ButtonAReleased { get; set; }
        public bool ButtonBReleased { get; set; }
        public bool ButtonXReleased { get; set; }
        public bool ButtonYReleased { get; set; }
        public bool ButtonSelectReleased { get; set; }
        public bool ButtonStartReleased { get; set; }
        public bool RightShoulderReleased { get; set; }
        public bool LeftShoulderReleased { get; set; }

        // Old
        public bool OldButtonAReleased { get; set; }
        public bool OldButtonBReleased { get; set; }
        public bool OldButtonXReleased { get; set; }
        public bool OldButtonYReleased { get; set; }
        public bool OldButtonSelectReleased { get; set; }
        public bool OldButtonStartReleased { get; set; }
        public bool OldRightShoulderReleased { get; set; }
        public bool OldLeftShoulderReleased { get; set; }

        #endregion


        #region Directions

        public bool Up { get; set; }
        public bool Down { get; set; }
        public bool Left { get; set; }
        public bool Right { get; set; }

        public bool OldUp { get; set; }
        public bool OldDown { get; set; }
        public bool OldLeft { get; set; }
        public bool OldRight { get; set; }


        #endregion


        public GamePadCapabilities GamePad1;
        public GamePadState NewGpState;
        public GamePadState OldGpState;
        public GamePadThumbSticks LeftTrigger { get; set; }
        public GamePadThumbSticks RightTrigger { get; set; }
    }
}