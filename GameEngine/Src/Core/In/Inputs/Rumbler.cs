﻿#region Usings

using System.Collections.Generic;
using GrizzlyGameEngine.Core.Out.Components;
using Microsoft.Xna.Framework;

#endregion

namespace GrizzlyGameEngine.Core.In.Inputs
{
    public class Rumbler : GrizzlyComponent
    {
        protected List<RumbleItem> LstRumble;

        public Rumbler()
        {
            LstRumble = new List<RumbleItem>();
        }

        public void Add(float pIntensity, RumbleItem.Duration pDuration)
        {
            LstRumble.Add(new RumbleItem(pIntensity, pDuration));
        }

        public void Add(float pLIntensity, float pRIntensity, RumbleItem.Duration pDuration)
        {
            LstRumble.Add(new RumbleItem(pLIntensity, pRIntensity, pDuration));
        }

        public override void Update(GameTime pGameTime)
        {
            if (LstRumble.Count > 0)
            {
                for (int i = 0; i < LstRumble.Count; i++)
                {
                    UpdateRumble(LstRumble[i], pGameTime);
                }
            }

            base.Update(pGameTime);
        }

        private void CleanUp(RumbleItem pRumble)
        {
            if (pRumble.IsFinished)
                LstRumble.Remove(pRumble);
        }

        public void UpdateRumble(RumbleItem pRumble, GameTime pGameTime)
        {
            pRumble.Update(pGameTime);
            CleanUp(pRumble);
        }
    }
}