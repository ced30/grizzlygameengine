﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace TiledContentImporter.Interfaces
{
    public interface IUpdatableComponent
    {
        /// <summary>
        /// Loads the content after initialization.
        /// </summary>
        void LoadContent();

        /// <summary>
        /// UnLoads the content before making the class null.
        /// </summary>
        void Unload();

        /// <summary>
        /// Main update method, takes a gameTime as argument.
        /// </summary>
        /// <param name="pGameTime"></param>
        void Update(GameTime pGameTime);

        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        void Draw(SpriteBatch pSpriteBatch);

        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        void DrawDebug(SpriteBatch pSpriteBatch);
    }
}