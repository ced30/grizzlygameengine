﻿namespace TiledContentImporter.Types.Enums
{
    public enum ImporterState
    {
        AtlasGroup,
        StripAnim,
        StripGroup,
        Texture,
        PngFont
    }
}