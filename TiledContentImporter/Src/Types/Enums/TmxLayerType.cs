﻿#region Usings


#endregion

namespace TiledContentImporter.Types.Enums
{
    public enum TmxLayerType
    {
        ForeGround = 2,
        Animations = 1,
        MidGround = 0,
        Unlisted = 3,
        Collisions = 4,
    }
}