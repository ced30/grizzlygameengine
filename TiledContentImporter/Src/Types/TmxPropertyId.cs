﻿namespace TiledContentImporter.Types
{
    public static class TmxPropertyId
    {
        public const string AnimId = "anim";
        public const string AtlasId = "atlas";
        public const string BounceId = "isBounce";
        public const string BumpId = "isBump";
        public const string DamageId = "damage";
        public const string DefenseId = "defense";
        public const string EnergyId = "energy";
        public const string FactionId = "faction";
        public const string HealthId = "health";
        public const string ManaId = "mana";
        public const string RendererId = "renderer";
        public const string SolidId = "isSolid";
        public const string SpeedId = "speed";
        public const string StripId = "strip";
        public const string TextureId = "texture";
    }
}