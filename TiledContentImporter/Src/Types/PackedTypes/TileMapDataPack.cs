﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Types.PackedTypes
{
    public class TileMapDataPack
    {
        /// <summary>
        /// The tileMap itself
        /// </summary>
        public TmxMap Map { get; set; }

        /// <summary>
        /// The layer for animated tiles, Depth = 2
        /// </summary>
        public List<MapLayer> AnimatedLayers { get; set; }

        /// <summary>
        /// List of mapLayer items.
        /// </summary>
        public MapLayer CurrentCollisionLayer { get; set; }

        /// <summary>
        /// List of mapLayer items.
        /// </summary>
        public List<MapLayer> CollisionLayers { get; set; }

        /// <summary>
        /// The front layer, is drawn last, Depth = 0
        /// </summary>
        public List<MapLayer> FrontLayers { get; set; }

        /// <summary>
        /// The middle layer, is drawn just before the entities, Depth = 1
        /// </summary>
        public List<MapLayer> MidLayers { get; set; }

        /// <summary>
        /// The map dimensions in col, lines
        /// </summary>
        public int MapHeight { get; set; }
        public int MapWidth { get; set; }

        /// <summary>
        /// The map dimensions in pixels
        /// </summary>
        public int TileHeight { get; set; }
        public int TileWidth { get; set; }
        public int MapHeightInPixels => MapHeight * TileHeight;
        public int MapWidthInPixels => MapWidth * TileWidth;

        /// <summary>
        /// The number or Columns
        /// </summary>
        public int TileSetColumns { get; set; }

        /// <summary>
        /// Rectangle representing the map dimensions
        /// </summary>
        public Rectangle MapRectangle { get; set; }

        /// <summary>
        /// The tileSet texture
        /// </summary>
        public Texture2D TileSetTexture2D { get; set; }

        /// <summary>
        /// The TmxTileSet from tiled editor
        /// </summary>
        public TmxTileSet TileSet { get; set; }

        public TileMapDataPack()
        {
            AnimatedLayers = new List<MapLayer>();
            CollisionLayers = new List<MapLayer>();
            FrontLayers = new List<MapLayer>();
            MidLayers = new List<MapLayer>();
        }
    }
}