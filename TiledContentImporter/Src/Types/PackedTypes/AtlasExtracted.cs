﻿#region Usings

using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;

#endregion

namespace TiledContentImporter.Types.PackedTypes
{
    /// <summary>
    /// This is the container contains the texture, dictionary of animations,
    /// </summary>
    public class AtlasExtracted
    {
        /// <summary>
        /// The texture of our atlas.
        /// </summary>
        public Texture2D AtlasTexture { get; set; }

        /// <summary>
        /// Dictionary containing animations data.
        /// </summary>
        public Dictionary<string, AtlasAnimationItem> DictionaryAnims { get; set; }

        /// <summary>
        /// Texture dimensions
        /// </summary>
        public int TextureHeight { get; set; }
        public int TextureWidth { get; set; }

        /// <summary>
        /// This is the package identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Constructor.
        /// and you're done.
        /// </summary>
        /// <param name="pExtractable"></param>
        /// <param name="pContent"></param>
        public AtlasExtracted(AtlasExtractable pExtractable, ContentManager pContent)
        {
            Id = pExtractable.Id;
            AtlasTexture = pContent.Load<Texture2D>(pExtractable.FilePath);
            TextureWidth = AtlasTexture.Width;
            TextureHeight = AtlasTexture.Height;

            DictionaryAnims = new Dictionary<string, AtlasAnimationItem>();
            foreach (var structAtlas in pExtractable.Dictionary.Values)
            {
                DictionaryAnims.Add(structAtlas.Id, new AtlasAnimationItem(structAtlas));
            }
        }

        /// <summary>
        /// Returns the first animation from the dictionary.
        /// </summary>
        /// <returns></returns>
        public AtlasAnimationItem GetFirstAnimation()
        {
            return DictionaryAnims.Values.First();
        }

        /// <summary>
        /// Returns an atlas animation,
        /// Pass an identifier as parameter
        /// </summary>
        /// <param name="pAnimId"></param>
        /// <returns></returns>
        public AtlasAnimationItem GetAnimation(string pAnimId)
        {
            return DictionaryAnims[pAnimId];
        }
    }
}