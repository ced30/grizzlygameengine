﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Types.PackedTypes
{
    /// <summary>
    /// This is used to regroup atlas animations containers and the atlas texture.
    /// </summary>
    public struct AtlasPack
    {
        /// <summary>
        /// Dictionary of TAnimStructAtlas (atlas animation data container).
        /// </summary>
        public Dictionary<string, AtlasData> Dico { get; set; }

        /// <summary>
        /// Identifier of the atlas animation set.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Texture of the atlas animation.
        /// </summary>
        public Texture2D Atlas { get; set; }
    }
}