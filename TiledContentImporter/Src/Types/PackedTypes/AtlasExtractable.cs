﻿#region Usings

using System.Collections.Generic;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Types.PackedTypes
{
    public class AtlasExtractable
    {
        /// <summary>
        /// The atlas identifier, ex: "Player".
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The path to the image file.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Dictionary containing animations data.
        /// </summary>
        public Dictionary<string, AtlasData> Dictionary { get; set; }

        /// <summary>
        /// Constructor.
        /// and you're done.
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pPath"></param>
        /// <param name="pDictionary"></param>
        public AtlasExtractable(string pId, string pPath, Dictionary<string, AtlasData> pDictionary)
        {
            Id = pId;
            FilePath = pPath;
            Dictionary = pDictionary;
        }
    }
}