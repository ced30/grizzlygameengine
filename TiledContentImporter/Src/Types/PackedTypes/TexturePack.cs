﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace TiledContentImporter.Types.PackedTypes
{
    /// <summary>
    /// This class is a container, composed of a texture2d, an identifier and a draw offset.
    /// </summary>
    public class TexturePack
    {
        /// <summary>
        /// The texture itself.
        /// </summary>
        public Texture2D Texture { get; set; }

        /// <summary>
        /// Used to offset the draw position.
        /// </summary>
        public Vector2 OffsetPos { get; set; }
    }
}