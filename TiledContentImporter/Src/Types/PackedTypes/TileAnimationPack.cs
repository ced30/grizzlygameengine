﻿using System.Collections.Generic;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

namespace TiledContentImporter.Types.PackedTypes
{
    public class TileAnimationPack
    {
        /// <summary>
        /// The animation frames
        /// </summary>
        public List<int> Frames { get; set; }

        /// <summary>
        /// the frame durations
        /// </summary>
        public List<int> FrameDurations { get; set; }

        /// <summary>
        /// the animation properties
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// The tile itself
        /// </summary>
        public TileItem Tile { get; set; }

        public TileAnimationPack(TileItem pTile, TmxMap pMap)
        {
            Tile = pTile;
            GetData(pTile, pMap);
        }

        /// <summary>
        /// Extracts data from the tile
        /// </summary>
        /// <param name="pTile"></param>
        /// <param name="pMap"></param>
        private void GetData(TileItem pTile, TmxMap pMap)
        {
            // get the tileSheet tiles to compare tiles.
            var tileSetTileList = pMap.Tilesets[0].Tiles;

            // Look for a tile with animation in the tileSet's tiles list.
            var isAnim = tileSetTileList.ContainsKey(pTile.Gid - 1);
            if (isAnim)
            {
                var animatedTile = tileSetTileList[pTile.Gid - 1];

                if (tileSetTileList[pTile.Gid - 1].AnimationFrames.Count > 0)
                {
                    if (animatedTile.Id > 0)
                    {
                        // Look for a Matching Gid.
                        if (pTile.Gid == animatedTile.Id + 1)
                        {
                            // Add the animated tiles to the list of animated tiles.
                            Frames = new List<int>();
                            FrameDurations = new List<int>();
                            Properties = animatedTile.Properties;

                            foreach (var frame in animatedTile.AnimationFrames)
                            {
                                Frames.Add(frame.Id);
                                FrameDurations.Add(frame.Duration);
                            }
                        }
                    }
                }
            }
        }
    }
}