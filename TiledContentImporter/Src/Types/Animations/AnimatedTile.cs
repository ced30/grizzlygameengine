﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using TiledContentImporter.Components;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Types.Animations
{
    public class AnimatedTile : TileItem
    {
        public TilesAnimator AnimationManager;

        public AnimatedTile(TileItem pTile, List<int> pList) : base(pTile)
        {
        }

        public AnimatedTile(TileAnimationPack pData) : base(pData)
        {
            SetAnimation(pData);
        }

        public void SetAnimation(TileAnimationPack pData)
        {
            AnimationManager = new TilesAnimator(
                new TileAnimationItem(pData));
        }

        public override void Update(GameTime pGameTime)
        {
            AnimationManager.Update(pGameTime);

            base.Update(pGameTime);
        }
    }
}