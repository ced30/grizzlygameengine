﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Types.Animations
{
    /// <summary>
    /// Data of an atlas animation, used to build TAnimContainerAtlas.
    /// </summary>
    public class AtlasAnimationItem
    {
        /// <summary>
        /// the number of frames per second.
        /// </summary>
        protected float Fps { get; set; }

        /// <summary>
        /// is the animation looping or does it stop when it ends?.
        /// </summary>
        public bool IsLooping { get; set; }

        /// <summary>
        /// The current animation speed.
        /// </summary>
        public float FrameSpeed =>
            FrameCount / Fps * SpeedCoef;

        /// <summary>
        /// Coefficient affecting the frameSpeed.
        /// </summary>
        public float SpeedCoef { get; set; }

        /// <summary>
        /// The number of the current frame being drawn.
        /// </summary>
        public int CurrentFrame { get; set; }

        /// <summary>
        /// The parameters contained in the animation.
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Total number of frames in the animation.
        /// </summary>
        public int FrameCount { get; set; }

        /// <summary>
        /// Frame dimensions
        /// </summary>
        public int FrameHeight { get; set; }
        public int FrameWidth { get; set; }

        /// <summary>
        /// The number of frames on the X axis.
        /// </summary>
        public int NbXFrames { get; set; }

        /// <summary>
        /// The number of frames on the Y axis.
        /// </summary>
        public int NbYFrames { get; set; }

        /// <summary>
        /// The position of the frame on the atlas in number of frames, starts at 0.
        /// </summary>
        public int FramePosX { get; set; }
        public int FramePosY { get; set; }

        /// <summary>
        /// The animation ID.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Each frame can have it's own duration
        /// </summary>
        public List<int> FrameDurations { get; set; }

        /// <summary>
        /// The position of the center of the frame.
        /// </summary>
        public Vector2 Center { get; set; }

        /// <summary>
        /// The offsets X and Y to center the texture.
        /// </summary>
        public Vector2 DrawOffset { get; set; } // The number of pixels we have to offset the texture when we draw.

        /// <summary>
        /// List of tmx objects.
        /// </summary>
        public TmxList<TmxObjectGroup> Objects { get; set; }

        public AtlasAnimationItem(AtlasData pStruct)
        {
            FrameDurations = pStruct.FrameDurations;
            FramePosX = pStruct.FirstFrameX;
            FramePosY = pStruct.FirstFrameY;
            FrameHeight = pStruct.FrameHeight;
            FrameWidth = pStruct.FrameWidth;
            Fps = 60;
            IsLooping = pStruct.IsLooping;
            Identifier = pStruct.Id;
            NbXFrames = pStruct.NbFrameX;
            NbYFrames = 1; // we usually don't use verticalIty in the atlas.
            Objects = pStruct.Objects;
            Properties = pStruct.Properties;
            FrameCount = NbXFrames * NbYFrames;
            DrawOffset = new Vector2(pStruct.DrawOffset.X, pStruct.DrawOffset.Y);
            SpeedCoef = pStruct.SpeedCoef;
            Center = new Vector2((int)(pStruct.FrameWidth / 2f),
                (int)(pStruct.FrameHeight / 2f));
        }
    }
}