﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Types.Animations
{
    public class StripAnimationItem
    {
        #region Public

        /// <summary>
        /// The frames per second.
        /// </summary>
        protected float Fps = 60;

        /// <summary>
        /// Is the animation looping?
        /// </summary>
        public bool IsLooping { get; set; }

        /// <summary>
        /// The current animation speed.
        /// </summary>
        public float FrameSpeed => FrameCount / Fps * SpeedCoef;

        /// <summary>
        /// Speed coefficient sor the animation speed.
        /// </summary>
        public float SpeedCoef { get; set; }

        /// <summary>
        /// The number of the current frame being drawn.
        /// </summary>
        public int CurrentFrame { get; set; }

        /// <summary>
        /// The parameters contained in the animation.
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Total number of frames in the animation.
        /// </summary>
        public int FrameCount { get; private set; }

        /// <summary>
        /// the number of columns / lines.
        /// </summary>
        public int NbCol, NbLig;

        /// <summary>
        /// Each frame can have it's own duration
        /// </summary>
        public List<int> FrameDurations { get; set; }

        /// <summary>
        /// The animation ID.
        /// </summary>
        public string Id;

        /// <summary>
        /// The texture strip.
        /// </summary>
        public Texture2D Texture { get; private set; }

        /// <summary>
        /// The position of the center of the frame.
        /// </summary>
        public Vector2 Center { get; set; }

        /// <summary>
        /// The position of the frame on the atlas in number of frames, starts at 0.
        /// </summary>
        public Vector2 DrawOffset { get; set; }

        /// <summary>
        /// The position of the center of the texture.
        /// </summary>
        public Vector2 TextureCenter { get; set; }

        /// <summary>
        /// List of tmx objects.
        /// </summary>
        public TmxList<TmxObjectGroup> Objects { get; set; }

        /// <summary>
        /// Frame dimensions
        /// </summary>
        public int FrameHeight
        {
            get
            {
                if (NbLig > 0)
                    return Texture.Height / NbLig;
                else
                {
                    return Texture.Height;
                }
            }
        }

        /// <summary>
        /// Frame dimensions
        /// </summary>
        public int FrameWidth
        {
            get
            {
                if (NbCol > 0)
                    return Texture.Width / NbCol;
                else
                {
                    return Texture.Width;
                }
            }
        }

        #endregion

        /// <summary>
        /// First rectangle is the frame size,
        /// 2nd rectangle is used to offset the image.
        /// </summary>
        /// <param name="pTexture"></param>
        /// <param name="pData"></param>
        public StripAnimationItem(Texture2D pTexture, StripData pData)
        {
            DrawOffset = pData.DrawOffset;
            FrameDurations = pData.FrameDurations;
            IsLooping = pData.IsLooping;
            Texture = pTexture;
            NbCol = Texture.Width / pData.FrameWidth;
            NbLig = Texture.Height / pData.FrameHeight;
            Objects = pData.Objects;
            Properties = pData.Properties;
            FrameCount = NbCol * NbLig;
            TextureCenter = new Vector2(Texture.Width / 2f, Texture.Height / 2f);
            SpeedCoef = pData.SpeedCoef;
            Id = pData.Id;
        }


        #region HelperMethods

        /// <summary>
        /// Sets the speed coefficient to modify the frame speed.
        /// </summary>
        /// <param name="pCoef"></param>
        public void SetCoef(float pCoef)
        {
            SpeedCoef = pCoef;
        }

        #endregion
    }
}