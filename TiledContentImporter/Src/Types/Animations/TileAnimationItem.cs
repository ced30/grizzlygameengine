﻿#region Usings

using System;
using System.Collections.Generic;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Types.Animations
{
    /// <summary>
    /// Tile animation data, used to build tile animations
    /// </summary>
    public class TileAnimationItem
    {
        /// <summary>
        /// The current frame being animated
        /// </summary>
        public int CurrentFrame => Frames[CurrentIndex];

        /// <summary>
        /// The index of the current animation
        /// </summary>
        public int CurrentIndex { get; set; }

        /// <summary>
        /// The speed of the current animation
        /// </summary>
        public float FrameSpeed => FrameCount / Fps * SpeedCoef;

        /// <summary>
        /// The frames per second
        /// </summary>
        protected float Fps { get; set; }

        /// <summary>
        /// Speed coefficient affecting the frame speed
        /// </summary>
        public float SpeedCoef { get; set; }

        /// <summary>
        /// The total number of frames
        /// </summary>
        public int FrameCount { get; set; }

        /// <summary>
        /// The list of frames
        /// </summary>
        public List<int> Frames { get; set; }

        /// <summary>
        /// The list of frame durations
        /// </summary>
        public List<int> FrameDurations { get; set; }

        /// <summary>
        /// The animation properties
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Constructor, takes a list of frames as argument
        /// </summary>
        public TileAnimationItem(TileAnimationPack pData)
        {
            var pSpeedCoef = "speedCoef";

            Frames = pData.Frames;
            FrameCount = Frames.Count;
            FrameDurations = pData.FrameDurations;
            Properties = pData.Properties;

            // randomize first frame
            CurrentIndex = new Random().Next(FrameCount - 1);

            Fps = 60;

            if (Properties.ContainsKey(pSpeedCoef))
            {
                SpeedCoef = float.Parse(Properties[pSpeedCoef]);
            }
            else SpeedCoef = 1;
        }
    }
}