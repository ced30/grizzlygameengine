﻿#region Usings

using System.Collections.Generic;
using TiledContentImporter.TiledSharp;

#endregion

namespace TiledContentImporter.Types.Data
{
    /// <summary>
    /// A type containing the useFull data of the tmxTileLayer,
    /// converted into a new type.
    /// </summary>
    public class MapLayer
    {
        /// <summary>
        /// Determines if the layer is a collision layer.
        /// </summary>
        public bool IsSolid { get; set; }

        /// <summary>
        /// Is the layer visible?
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// this dictionary contains the custom properties of the layer.
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// The actual depth of the layer, will determine the draw order.
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// The list of processed tmxTiles.
        /// </summary>
        public List<TileItem> Tiles { get; set; }

        /// <summary>
        /// The layer identifier.
        /// </summary>
        public string Identifier { get; set; }

        public MapLayer()
        {
            Tiles = new List<TileItem>();
            Properties = new Dictionary<string, string>();
        }
    }
}