﻿#region Usings

using Microsoft.Xna.Framework;
using TiledContentImporter.Components;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Types.Data
{
    /// <inheritdoc />
    /// <summary>
    /// TmxTiles converted into a new data format to allow more flexibility,
    /// used to store tile data.
    /// </summary>
    public class TileItem : UpdatableComponent
    {
        /// <summary>
        /// Handles the transparency.
        /// </summary>
        public Color Color { get; set; }

        public bool IsDebug { get; set; }

        /// <summary>
        /// The tile identifier.
        /// </summary>
        public int Gid { get; set; }
        public int Column { get; set; }
        public int Line { get; set; }
        public int X => Column * TileSize;
        public int Y => Line * TileSize;
        public Vector2 Position => new Vector2(X, Y);
        public Rectangle Rectangle
        {
            get
            {
                if (CurrentType == Type.JumpThrough)
                    return new Rectangle(X, Y, TileSize, TileSize / 2);
                else return new Rectangle(X, Y, TileSize, TileSize);
            }
        }
        public bool OldVisible { get; set; }
        public bool IsVisible { get; set; }
        public Type OldType { get; set; }
        public Type CurrentType { get; set; }

        public int TileSize { get; set; }

        public TileItem()
        {
            Color = Color.White;
            OldType = Type.None;
            CurrentType = Type.None;
            IsVisible = true;
            OldVisible = true;
        }

        public TileItem(TmxLayerTile pTile, TmxMap pMap)
        {
            Color = Color.White;
            OldType = Type.None;
            CurrentType = Type.None;
            IsVisible = true;
            OldVisible = true;
            Gid = pTile.Gid;
            Column = pTile.X;
            Line = pTile.Y;
            TileSize = pMap.TileWidth;
        }

        public TileItem(TileAnimationPack pData)
        {
            Color = pData.Tile.Color;
            OldType = pData.Tile.OldType;
            CurrentType = pData.Tile.CurrentType;
            IsVisible = pData.Tile.IsVisible;
            OldVisible = pData.Tile.OldVisible;
            Gid = pData.Tile.Gid;
            Column = pData.Tile.Column;
            Line = pData.Tile.Line;
            TileSize = pData.Tile.TileSize;
        }

        public TileItem(TileItem pTile)
        {
            Color = pTile.Color;
            OldType = pTile.OldType;
            CurrentType = pTile.CurrentType;
            IsVisible = pTile.IsVisible;
            OldVisible = pTile.OldVisible;
            Gid = pTile.Gid;
            Column = pTile.Column;
            Line = pTile.Line;
            TileSize = pTile.TileSize;
        }


        #region Helper Methods

        public void Break()
        {
            if (CurrentType != Type.Break)
                return;

            if (OldType != CurrentType)
            {
                OldType = CurrentType;
                CurrentType = Type.None;
            }
        }

        public void Repair()
        {
            var tempType = CurrentType;

            CurrentType = OldType;
            OldType = tempType;
        }

        public void SetVisible(bool pVisible)
        {
            IsVisible = pVisible;
        }

        public Type ParseType(TmxTileSet pTileSet)
        {
            if (Gid == 0)
                return Type.None;

            if (pTileSet.Tiles.ContainsKey(Gid - 1))
            {
                var tile = pTileSet.Tiles[Gid - 1];

                if (tile.Type == null)
                {
                    return Type.None;
                }

                switch (tile.Type.ToLower())
                {
                    case "":
                        return Type.None;
                    case "break":
                        return Type.Break;
                    case "climb":
                        return Type.Climb;
                    case "exit":
                        return Type.Exit;
                    case "hurt":
                        return Type.Hurt;
                    case "ladder":
                        return Type.Ladder;
                    case "jumpthrough":
                        return Type.JumpThrough;
                    case "none":
                        return Type.None;
                    case "move":
                        return Type.Move;
                    case "slide":
                        return Type.Slide;
                    case "slideright":
                        return Type.SlideRight;
                    case "slideleft":
                        return Type.SlideLeft;
                    case "solid":
                        return Type.Solid;
                    default:
                        return Type.None;
                }
            }
            else
            {

                return Type.None;
            }
        }
        
        #endregion

        /// <summary>
        /// Enum => types of tiles.
        /// </summary>
        public enum Type
        {
            Break,
            Climb,
            Exit,
            Hurt,
            Ladder,
            JumpThrough,
            None,
            Move,
            Slide,
            SlideRight,
            SlideLeft,
            Solid,
        }
    }
}