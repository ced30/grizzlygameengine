﻿#region Usings

using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Xna.Framework;
using TiledContentImporter.Managers;
using TiledContentImporter.TiledSharp;

#endregion

namespace TiledContentImporter.Types.Data
{
    /// <summary>
    /// Struct that holds data to build strip animations.
    /// </summary>
    public class StripData
    {
        /// <summary>
        /// Is the animation looping?
        /// </summary>
        public bool IsLooping = true;

        /// <summary>
        /// Speed coefficient sor the animation speed.
        /// </summary>
        public float SpeedCoef { get; set; }

        /// <summary>
        /// The width of the frame rectangle.
        /// </summary>
        public int FrameWidth { get; set; }

        /// <summary>
        /// The height of the frame rectangle.
        /// </summary>
        public int FrameHeight { get; set; }

        /// <summary>
        /// Id of the animation.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Path of the texture to load.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Offset to center the image in the box rectangle.
        /// </summary>
        public Vector2 DrawOffset { get; set; }

        /// <summary>
        /// The parameters contained in the animation.
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        /// <summary>
        /// Each frame can have it's own duration
        /// </summary>
        public List<int> FrameDurations { get; set; }

        /// <summary>
        /// List of tmx objects.
        /// </summary>
        public TmxList<TmxObjectGroup> Objects { get; set; }

        public StripData()
        {
            Properties = new Dictionary<string, string>();
            Objects = new TmxList<TmxObjectGroup>();
        }

        public virtual void InitProperties(TmxTileSet pSet, TmxTilesetTile pAnim)
        {
            Properties = pAnim.Properties;
            Objects = pAnim.ObjectGroups;

            // parameters id to look for
            const string pOffset = "offset";
            const string pFrameSize = "frameSize";
            const string pLooping = "isLooping";
            const string pSpeedCoef = "speedCoef";
            Path = TmxManager.TexturePath(pAnim.Image.Source);

            // we initialize with default values
            IsLooping = true;
            SpeedCoef = 1;
            FrameWidth = 32;
            FrameHeight = 32;
            Id = pAnim.Type;
            DrawOffset = Vector2.Zero;

            // we get the frame durations
            FrameDurations = new List<int>();
            for (int i = 0; i < pAnim.AnimationFrames.Count; i++)
            {
                FrameDurations.Add(pAnim.AnimationFrames[i].Duration);
            }

            string[] results;

            // We parse the properties for common parameters and if found,
            // we replace the default values by the value of the parameter
            if (Properties.ContainsKey(pOffset))
            {
                results = Properties[pOffset].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var offsetX = float.Parse(results[0], CultureInfo.InvariantCulture);
                var offsetY = float.Parse(results[1], CultureInfo.InvariantCulture);
                DrawOffset = new Vector2(offsetX, offsetY);
            }

            if (Properties.ContainsKey(pFrameSize))
            {
                results = Properties[pFrameSize].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                FrameWidth = int.Parse(results[0], CultureInfo.InvariantCulture);
                FrameHeight = int.Parse(results[1], CultureInfo.InvariantCulture);
            }

            if (Properties.ContainsKey(pLooping))
            {
                IsLooping = bool.Parse(Properties[pLooping]);
            }

            if (Properties.ContainsKey(pSpeedCoef))
            {
                SpeedCoef = float.Parse(Properties[pSpeedCoef], CultureInfo.InvariantCulture);
            }
        }
    }
}