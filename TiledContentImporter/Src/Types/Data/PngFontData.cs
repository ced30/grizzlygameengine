﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using TiledContentImporter.Managers;
using TiledContentImporter.TiledSharp;

#endregion

namespace TiledContentImporter.Types.Data
{
    public class PngFontData
    {
        public string Id { get; set; }

        public string Path { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        /// <summary>
        /// This dictionary keys are the characters and the values
        /// are the positions of the frames on the tileSheet
        /// </summary>
        public Dictionary<char, Rectangle> LocationRectangles { get; set; }

        public PngFontData(TmxTileSet pSet)
        {
            Id = pSet.Name;
            Path = TmxManager.TexturePath(pSet.Image.Source);
            Height = pSet.TileHeight;
            Width = pSet.TileWidth;
            LocationRectangles = new Dictionary<char, Rectangle>();

            for (int i = 0; i < pSet.Tiles.Count; i++)
            {
                var tile = pSet.Tiles[i];
                if (tile.Type.Equals(" "))
                    continue;
                
                char character = ' ';
                Debug.Assert(pSet.Columns != null, "pSet.Columns != null");
                int y = (int)(tile.Id / pSet.Columns);
                int x = (int)(tile.Id - y * pSet.Columns);

                if (tile.Type.Equals("space"))
                {
                    LocationRectangles.Add(character, new Rectangle(
                        x * Width,
                        y * Height,
                        Width,
                        Height));
                }
                else
                {
                    char[] aChar = tile.Type.ToCharArray();
                    if (aChar.Length > 1)
                        throw new NotImplementedException("String must contain only 1 char, except for space (noted: 'space')");

                    LocationRectangles.Add(aChar[0], new Rectangle(
                        x * pSet.TileWidth,
                        y * pSet.TileHeight,
                        pSet.TileWidth,
                        pSet.TileHeight));
                }
            }
        }
    }
}