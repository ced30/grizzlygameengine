﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Microsoft.Xna.Framework;
using TiledContentImporter.TiledSharp;

#endregion

namespace TiledContentImporter.Types.Data
{
    /// <inheritdoc />
    /// <summary>
    /// Struct that holds atlas data to build atlas animations.
    /// </summary>
    public class AtlasData : StripData
    {
        /// <summary>
        ///  First frame of the animation on the X axis.
        /// </summary>
        public int FirstFrameX { get; set; }

        /// <summary>
        ///  First frame of the animation on the Y axis.
        /// </summary>
        public int FirstFrameY { get; set; }

        /// <summary>
        ///  How many frames does the animation have on the X axis.
        /// </summary>
        public int NbFrameX { get; set; }

        /// <summary>
        /// Init the class by passing objects to fill the data. 
        /// </summary>
        /// <param name="pSet"></param>
        /// <param name="pAnim"></param>
        public override void InitProperties(TmxTileSet pSet, TmxTilesetTile pAnim)
        {
            // we declare the identifiers of the properties we're looking for.
            const string pLoop = "isLooping";
            const string pSpeedCoef = "speedCoef";

            // we get the animation properties in a dictionary<string, string>
            Properties = pAnim.Properties;

            // we get the frame durations.
            FrameDurations = new List<int>();
            for (int i = 0; i < pAnim.AnimationFrames.Count; i++)
            {
                FrameDurations.Add(pAnim.AnimationFrames[i].Duration);
            }

            // we initialize with default values.
            Debug.Assert(pSet.Columns != null, "pSet.Columns != null");
            int firstFrameY = pAnim.AnimationFrames.First().Id / pSet.Columns.Value;
            int firstFrameX = pAnim.AnimationFrames.First().Id - (firstFrameY * pSet.Columns.Value);
            bool loop = true;
            float speedCoef = 1;

            // We parse the properties for common parameters and if found,
            // we replace the default values by the value of the parameter.
            if (Properties.ContainsKey(pLoop))
            {
                loop = bool.Parse(Properties[pLoop]);
            }

            if (Properties.ContainsKey(pSpeedCoef))
            {
                speedCoef = float.Parse(Properties[pSpeedCoef], CultureInfo.InvariantCulture);
            }

            // then we distribute the values.
            DrawOffset = new Vector2(pSet.TileOffset.X, pSet.TileOffset.Y);
            FirstFrameX = firstFrameX;
            FirstFrameY = firstFrameY;
            FrameHeight = pSet.TileHeight;
            FrameWidth = pSet.TileWidth;
            Id = pAnim.Type;
            IsLooping = loop;
            NbFrameX = pAnim.AnimationFrames.Count;
            SpeedCoef = speedCoef;
        }
    }
}