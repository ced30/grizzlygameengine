﻿using Microsoft.Xna.Framework;

namespace TiledContentImporter.Types.Data
{
    /// <summary>
    /// This class is used to store texture data, such as id, path and draw offset.
    /// </summary>
    public class TextureData
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Identifier.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Used to offset the draw position.
        /// </summary>
        public Vector2 OffsetPos { get; set; }
    }
}