﻿#region Usings

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Enums;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Types.Data.tileMapDataConversion
{
    public class TmxLayerConverter
    {
        private const string SolidId = "isSolid";

        public static void Convert(TmxLayerCollection pCollection, TileMapDataPack pData)
        {
            // Set the depth value
            foreach (var key in pCollection.Data.Keys)
            {
                Process(pCollection.Data[key], pData, (int)key);
                
            }

            // Set the default collision layer
            if (pData.CollisionLayers.Count > 0)
                pData.CurrentCollisionLayer = pData.CollisionLayers[0];
        }

        public static void Process(Collection<TmxLayer> pTmxLayers, TileMapDataPack pData, int pKey)
        {
            for (int i = 0; i < pTmxLayers.Count; i++)
            {
                var oldTileLayer = pTmxLayers[i];
                // 1: create an empty new layer
                var newMapLayer = new MapLayer
                {
                    Identifier = oldTileLayer.Name,
                    IsVisible = oldTileLayer.Visible,
                    Properties = oldTileLayer.Properties,
                };

                // if the depth value is anything else than foreGround,
                // midGround or animated, we set it to midGround
                if (pKey >= 0 &&
                    pKey <= Enum.GetValues(typeof(TmxLayerType)).Length - 1)
                {
                    newMapLayer.Depth = pKey;
                }
                else newMapLayer.Depth = (int)TmxLayerType.MidGround;

                // 2 create tiles, transpose values and fill up the new layer.
                var newTileList = new List<TileItem>();
                for (int j = 0; j < oldTileLayer.Tiles.Count; j++)
                {
                    var oldTile = oldTileLayer.Tiles[j];
                    var newTile = new TileItem(oldTile, pData.Map);

                    // 3 get the Type from the tileSet.
                    if (pTmxLayers[i].Properties.ContainsKey(SolidId))
                    {
                        if (bool.TryParse(pTmxLayers[i].Properties[SolidId], out var solidOrNot))
                        {
                            if (solidOrNot)
                            {
                                newMapLayer.IsSolid = true;
                                newTile.CurrentType = newTile.ParseType(pData.TileSet);
                            }
                        }
                    }

                    if (GetTileAnimations(newTile, pData.Map) != null)
                    {
                        var data = new TileAnimationPack(newTile, pData.Map);
                        newTile = new AnimatedTile(data);
                    }

                    newTileList.Add(newTile);
                }

                newMapLayer.Tiles = newTileList;
                newMapLayer.Properties = pTmxLayers[i].Properties;

                // We sort the layers
                if (pKey == (int)TmxLayerType.ForeGround)
                    pData.FrontLayers.Add(newMapLayer);
                else if (pKey == (int)TmxLayerType.MidGround)
                    pData.MidLayers.Add(newMapLayer);
                else if (pKey == (int)TmxLayerType.Animations)
                    pData.AnimatedLayers.Add(newMapLayer);
                else if (pKey == (int) TmxLayerType.Collisions)
                {
                    // The collision layer gets the same depth as midGround.
                    newMapLayer.Depth = (int) TmxLayerType.MidGround;
                    pData.CollisionLayers.Add(newMapLayer);

                }
                else pData.MidLayers.Add(newMapLayer);
            }
        }

        private static List<int> GetTileAnimations(TileItem pTile, TmxMap pMap)
        {
            // get the tileSheet tiles to compare tiles.
            var tileSetTileList = pMap.Tilesets[0].Tiles;

            // Look for a tile with animation in the tileSet's tiles list.
            var isAnim = tileSetTileList.ContainsKey(pTile.Gid - 1);
            if (isAnim)
            {
                var animatedTile = tileSetTileList[pTile.Gid - 1];

                if (tileSetTileList[pTile.Gid - 1].AnimationFrames.Count > 0)
                {
                    if (animatedTile.Id > 0)
                    {
                        // Look for a Matching Gid.
                        if (pTile.Gid == animatedTile.Id + 1)
                        {
                            // Add the animated tiles to the list of animated tiles.
                            var frameList = new List<int>();
                            foreach (var animFrame in animatedTile.AnimationFrames)
                            {
                                frameList.Add(animFrame.Id);
                            }

                            return frameList;
                        }
                    }
                }
            }

            return null;
        }
    }
}
