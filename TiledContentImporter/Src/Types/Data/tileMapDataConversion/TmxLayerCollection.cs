﻿#region Usings

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Enums;

#endregion

namespace TiledContentImporter.Types.Data.tileMapDataConversion
{
    public class TmxLayerCollection
    {
        public TmxMap Map { get; set; }

        public Dictionary<TmxLayerType, Collection<TmxLayer>> Data { get; set; }

        public TmxLayerCollection(TmxMap pMap)
        {
            Map = pMap;
            Data = new Dictionary<TmxLayerType, Collection<TmxLayer>>
            {
                {TmxLayerType.ForeGround, new Collection<TmxLayer>()},
                {TmxLayerType.Animations, new Collection<TmxLayer>()},
                {TmxLayerType.MidGround, new Collection<TmxLayer>()},
                {TmxLayerType.Unlisted, new Collection<TmxLayer>()},
                {TmxLayerType.Collisions, new Collection<TmxLayer>()}
            };

            // First, we check if there are
            // some layer without groups
            LookForUnsortedLayers(Map);

            var depthStr = "depth";
            var groups = pMap.Groups;

            for (int i = 0; i < groups.Count; i++)
            {
                var group = groups[i];
                if (group.Properties.ContainsKey(depthStr))
                {
                    // We set the depth variable to 3 before checking for the parameter,
                    // so, if the parameter is not found of if the parameter is not an int,
                    // the layer goes directly in the unlisted list
                    var depth = 3;
                    if (int.TryParse(group.Properties[depthStr], out depth))
                    {
                        for (int j = 0; j < group.TileLayers.Count; j++)
                        {
                            var layer = group.TileLayers[j];

                            if (depth >= 0 &&
                                depth <= Enum.GetValues(typeof(TmxLayerType)).Length - 1)
                            {
                                Data[(TmxLayerType)depth].Add(layer);
                            }
                            else
                            {
                                Data[TmxLayerType.Unlisted].Add(layer);
                            }

                            // we check if it's solid
                            if (IsACollisionLayer(layer))
                                Data[TmxLayerType.Collisions].Add(layer);
                        }
                    }
                }
                else
                {
                    // if the group has no "depth" parameter, we put the layer in the unlisted list,
                    // we will check later if the layer itself has a depth parameter when sort and convert them.
                    for (var j = 0; j < group.TileLayers.Count; j++)
                    {
                        var layer = group.TileLayers[j];
                        Data[TmxLayerType.Unlisted].Add(layer);

                        // we check if it's solid
                        if (IsACollisionLayer(layer))
                            Data[TmxLayerType.Collisions].Add(layer);
                    }
                }
            }
        }

        private void LookForUnsortedLayers(TmxMap pMap)
        {
            for (int i = 0; i < pMap.TileLayers.Count; i++)
            {
                var layer = pMap.TileLayers[i];
                if (IsACollisionLayer(layer))
                {
                    Data[TmxLayerType.Collisions].Add(layer);
                }
                else Data[TmxLayerType.Unlisted].Add(pMap.TileLayers[i]);
            }
        }

        private bool IsACollisionLayer(ITmxLayer pLayer)
        {
            var solidStr = "isSolid";
            if (pLayer.Properties.ContainsKey(solidStr))
            {
                if (bool.TryParse(pLayer.Properties[solidStr], out var solidOrNot))
                {
                    if (solidOrNot)
                        return true;
                }
            }

            return false;
        }
    }
}