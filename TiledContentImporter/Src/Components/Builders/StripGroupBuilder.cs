﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class StripGroupBuilder
    {
        /// <summary>
        /// This dictionary contains all the necessary data to build the strips.
        /// </summary>
        private Dictionary<string, Dictionary<string, StripData>> StripDataPackage { get; set; }

        public Dictionary<string, StripAnimationItem> Get(string pId, ContentManager pContent)
        {
            Dictionary<string, StripAnimationItem> dictionaryToExport = new Dictionary<string, StripAnimationItem>();
            var unit = StripDataPackage[pId];
            foreach (StripData data in unit.Values)
            {
                StripAnimationItem item = new StripAnimationItem(
                    pContent.Load<Texture2D>(data.Path), data);

                dictionaryToExport.Add(data.Id, item);
            }

            return dictionaryToExport;
        }

        public void Set(Dictionary<string, Dictionary<string, StripData>> pStripsDataPackage)
        {
            StripDataPackage = pStripsDataPackage;
        }
    }
}