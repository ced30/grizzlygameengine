﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class AtlasGroupBuilder
    {
        /// <summary>
        /// This dictionary contains all the necessary data to build the AtlasExtracted.
        /// </summary>
        private Dictionary<string, AtlasExtractable> AtlasAnimationsPackage { get; set; }

        public AtlasExtracted Get(string pId, ContentManager pContent)
        {
            return new AtlasExtracted(AtlasAnimationsPackage[pId], pContent);
        }

        public void Set(Dictionary<string, AtlasExtractable> pExtractable)
        {
            AtlasAnimationsPackage = pExtractable;
        }
    }
}