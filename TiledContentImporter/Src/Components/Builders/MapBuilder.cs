﻿#region Usings

using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class MapBuilder
    {
        /// <summary>
        /// The tmx map we will be extracting data from.
        /// </summary>
        public TmxMap Map { get; set; }

        public TileMapDataPack Get(string pPath)
        {
            return null;
        }
    }
}