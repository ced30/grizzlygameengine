﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class TexturesBuilder
    {
        /// <summary>
        /// This dictionary contains all the necessary data to build the textures.
        /// </summary>
        public Dictionary<string, TextureData> TextureDataPackage { get; set; }

        public TexturePack Get(string pId, ContentManager pContent)
        {
            return new TexturePack()
            {
                Texture = pContent.Load<Texture2D>(TextureDataPackage[pId].Path),
                OffsetPos = TextureDataPackage[pId].OffsetPos
            };
        }

        public void Set(Dictionary<string, TextureData> pTexturePackage)
        {
            TextureDataPackage = pTexturePackage;
        }
    }
}