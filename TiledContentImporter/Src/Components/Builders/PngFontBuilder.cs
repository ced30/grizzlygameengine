﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class PngFontBuilder
    {
        private Dictionary<string, PngFontData> DataPackages { get; set; }

        public AtlasExtracted Get(string pId, ContentManager pContent)
        {
//            return new AtlasExtracted(AtlasAnimationsPackage[pId], pContent);

            return null;
        }

        public void Set(Dictionary<string, PngFontData> pData)
        {
            DataPackages = pData;
        }
    }
}