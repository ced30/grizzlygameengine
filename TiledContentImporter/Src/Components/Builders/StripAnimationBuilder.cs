﻿#region Usings

using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Builders
{
    public class StripAnimationBuilder
    {
        /// <summary>
        /// This dictionary contains all the necessary data to build the strips.
        /// </summary>
        private Dictionary<string, StripData> StripDataPackage { get; set; }

        public Dictionary<string, StripAnimationItem> Get(string pId, ContentManager pContent)
        {
            var dictionaryToExport = new Dictionary<string, StripAnimationItem>();
            var unit = StripDataPackage[pId];
            var item = new StripAnimationItem(
                pContent.Load<Texture2D>(unit.Path), unit);
            dictionaryToExport.Add(unit.Id, item);

            return dictionaryToExport;
        }

        public void Set(Dictionary<string, StripData> pStripsDataPackage)
        {
            StripDataPackage = pStripsDataPackage;
        }
    }
}