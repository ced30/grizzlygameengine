﻿#region Usings

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.Interfaces;

#endregion

namespace TiledContentImporter.Components
{
    /// <summary>
    /// This class is a base class, build on top of it.
    /// LoadContent is not called on class creation,
    /// load it manually after creation.
    /// </summary>
    public abstract class UpdatableComponent : IUpdatableComponent
    {
        /// <inheritdoc />
        /// <summary>
        /// Loads the content after initialization.
        /// </summary>
        public virtual void LoadContent() { }

        /// <inheritdoc />
        /// <summary>
        /// UnLoads the content before making the class null.
        /// </summary>
        public virtual void Unload() { }

        /// <inheritdoc />
        /// <summary>
        /// Main update method, takes a gameTime as argument.
        /// </summary>
        /// <param name="pGameTime"></param>
        public virtual void Update(GameTime pGameTime) { }

        /// <inheritdoc />
        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            
        }

        /// <inheritdoc />
        /// <summary>
        /// Draw debug infos.
        /// </summary>
        /// <param name="pSpriteBatch"></param>
        public virtual void DrawDebug(SpriteBatch pSpriteBatch) { }
    }
}