﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    public class StripAnimExtractor
    {
        public Dictionary<string, StripData> Extract(Dictionary<string, TmxTileSet> pTileSets)
        {
            Dictionary<string, StripData> strips = new Dictionary<string, StripData>();
            foreach (var set in pTileSets.Values)
            {
                foreach (var animation in set.Tiles.Values)
                {
                    StripData data = new StripData();
                    data.InitProperties(set, animation);
                    strips.Add(animation.Type, data);
                    Debug.Print("Successfully imported TileSet Strip animation: " + animation.Type);
                }
            }

            return strips;
        }
    }
}