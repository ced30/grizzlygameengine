﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using TiledContentImporter.Managers;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    /// <summary>
    /// This class imports atlas animations from .Tsx files.
    /// </summary>
    public class AtlasGroupsExtractor
    {
        /// <summary>
        /// Returns an AtlasExtractAble (atlas animation container).
        /// </summary>
        /// <param name="pTileSets"></param>
        /// <returns></returns>
        public Dictionary<string, AtlasExtractable> Extract(Dictionary<string, TmxTileSet> pTileSets)
        {
            var dictionaryAtlasExtractable = new Dictionary<string, AtlasExtractable>();

            // for each tileSet
            foreach (var set in pTileSets.Values)
            {
                var atlasDataDictionary = new Dictionary<string, AtlasData>();

                // for each tile in the tileSet
                foreach (var animation in set.Tiles.Values)
                {
                    if (animation.AnimationFrames.Count <= 0)
                        continue;

                    // we declare a new AtlasData and initialize it's values.
                    var data = new AtlasData();
                    data.InitProperties(set, animation);
                    atlasDataDictionary.Add(animation.Type, data);
                }

                // we retrieve the path
                var path = TmxManager.TexturePath(set.Image.Source);

                // and we add our data to the dictionary
                var ext = new AtlasExtractable(set.Name, path, atlasDataDictionary);
                dictionaryAtlasExtractable.Add(set.Name, ext);
                Debug.Print("Successfully imported TileSet Atlas: " + set.Name);
            }

            return dictionaryAtlasExtractable;
        }
    }
}