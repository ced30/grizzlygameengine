﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Xna.Framework;
using TiledContentImporter.Managers;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    public class TextureExtractor
    {
        /// <summary>
        /// The dictionary contains id + filePath.
        /// </summary>
        public Dictionary<string, TextureData> Extract(Dictionary<string, TmxTileSet> pTileSets)
        {
            Dictionary<string, TextureData> textures = new Dictionary<string, TextureData>();

            // For each tileSet
            foreach (var set in pTileSets.Values)
            {
                // For each tile contained in the tileSet.
                foreach (var tile in set.Tiles.Values)
                {
                    var properties = tile.Properties;
                    Vector2 offsetPos = Vector2.Zero;
                    const string pOffset = "offset";
                    if (properties.ContainsKey(pOffset))
                    {
                        var values = properties[pOffset].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        var offsetX = float.Parse(values[0], CultureInfo.InvariantCulture);
                        var offsetY = float.Parse(values[1], CultureInfo.InvariantCulture);
                        offsetPos = new Vector2(offsetX, offsetY);
                    }

                    var newData = new TextureData()
                    {
                        Id = tile.Type,
                        OffsetPos = offsetPos,
                        Path = TmxManager.TexturePath(tile.Image.Source)
                    };

                    textures.Add(tile.Type, newData);
                    Debug.Print("Successfully imported TileSet Texture: " + tile.Type);
                }
            }

            return textures;
        }
    }
}