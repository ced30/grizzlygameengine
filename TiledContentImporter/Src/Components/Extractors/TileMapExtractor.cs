﻿#region Usings

using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data.tileMapDataConversion;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    public class TileMapExtractor
    {
        public TileMapDataPack Extract(string pPath, ContentManager pContent)
        {
            var map = GetMap(pPath);
            var dataToReturn = new TileMapDataPack
            {
                Map = map,
            };

            InitializeData(dataToReturn, pContent);
            TmxLayerCollection collection = new TmxLayerCollection(map);
            TmxLayerConverter.Convert(collection, dataToReturn);
            Debug.Print("Successfully loaded tileMap: " + pPath);
            return dataToReturn;
        }

        /// <summary>
        /// Initializes TileMapData values
        /// </summary>
        /// <param name="pData"></param>
        /// <param name="pContent"></param>
        private void InitializeData(TileMapDataPack pData, ContentManager pContent)
        {
            pData.TileSet = pData.Map.Tilesets[0];
            pData.TileSetTexture2D = pContent.Load<Texture2D>("Levels/" + pData.TileSet.Name);
            pData.TileWidth = pData.TileSet.TileWidth;
            pData.TileHeight = pData.TileSet.TileHeight;
            pData.MapWidth = pData.Map.Width;
            pData.MapHeight = pData.Map.Height;
            pData.TileSetColumns = pData.TileSetTexture2D.Width / pData.TileWidth;
            pData.MapRectangle = new Rectangle(0, 0, pData.Map.Width * pData.Map.TileWidth,
                pData.Map.Height * pData.Map.TileHeight);
        }

        /// <summary>
        /// Returns the TmxMap.
        /// </summary>
        public static TmxMap GetMap(string pString)
        {
            return new TmxMap(pString);
        }
    }
}