﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    public class StripGroupsExtractor
    {
        public Dictionary<string, Dictionary<string, StripData>> Extract(Dictionary<string, TmxTileSet> pTileSets)
        {
            Dictionary<string, Dictionary<string, StripData>> stripsDictionary = new Dictionary<string, Dictionary<string, StripData>>();

            foreach (TmxTileSet set in pTileSets.Values)
            {
                Dictionary<string, StripData> strips = new Dictionary<string, StripData>();

                foreach (TmxTilesetTile animation in set.Tiles.Values)
                {
                    StripData data = new StripData();
                    data.InitProperties(set, animation);
                    strips.Add(animation.Type, data);
                }

                Debug.Print("Successfully imported TileSet Strip Group: " + set.Name);
                stripsDictionary.Add(set.Name, strips);
            }

            return stripsDictionary;
        }
    }
}