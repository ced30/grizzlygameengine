﻿#region Usings

using System.Collections.Generic;
using System.Diagnostics;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Data;

#endregion

namespace TiledContentImporter.Components.Extractors
{
    /// <summary>
    /// This class extracts png font from texture files contained on a TmxTileSet.
    /// </summary>
    public class PngFontExtractor
    {
        public Dictionary<string, PngFontData> Extract(Dictionary<string, TmxTileSet> pTileSets)
        {
            Dictionary<string, PngFontData> fontDataDictionary = new Dictionary<string, PngFontData>();

            foreach (TmxTileSet set in pTileSets.Values)
            {
                var data = new PngFontData(set);
                fontDataDictionary.Add(data.Id, data);
                Debug.Print("Successfully imported TileSet: " + set.Name);
            }

            return fontDataDictionary;
        }
    }
}