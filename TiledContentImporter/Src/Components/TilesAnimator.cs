﻿#region Usings

using Microsoft.Xna.Framework;
using TiledContentImporter.Types.Animations;

#endregion

namespace TiledContentImporter.Components
{
    /// <inheritdoc />
    /// <summary>
    /// This class animates tiles.
    /// </summary>
    public class TilesAnimator : UpdatableComponent
    {
        /// <summary>
        /// The current tile animation.
        /// </summary>
        public TileAnimationItem Animation { get; set; }

        /// <summary>
        /// The timer to increment frames.
        /// </summary>
        protected double Timer { get; set; }

        /// <summary>
        /// Constructor, takes a TAnimationTiles as argument and sets the current animation.
        /// </summary>
        /// <param name="currentAnimation"></param>
        public TilesAnimator(TileAnimationItem currentAnimation)
        {
            Animation = currentAnimation;
        }

        /// <summary>
        /// Updates the current frame of the current animation.
        /// </summary>
        /// <param name="pGameTime"></param>
        public override void Update(GameTime pGameTime)
        {
            base.Update(pGameTime);

            Timer += pGameTime.ElapsedGameTime.TotalSeconds;

            if (Timer > Animation.FrameDurations[Animation.CurrentIndex] / 1000f * Animation.SpeedCoef)
            {
                Timer = 0;
                Animation.CurrentIndex = (Animation.CurrentIndex + 1) % Animation.FrameCount;
            }

            if (Animation.CurrentIndex >= Animation.FrameCount)
            {
                Animation.CurrentIndex = 0;
            }
        }
    }
}