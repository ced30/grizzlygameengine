﻿#region Usings

using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using TiledContentImporter.Components.Builders;
using TiledContentImporter.Components.Extractors;
using TiledContentImporter.TiledSharp;
using TiledContentImporter.Types.Animations;
using TiledContentImporter.Types.Enums;
using TiledContentImporter.Types.PackedTypes;

#endregion

namespace TiledContentImporter.Managers
{
    /// <summary>
    /// This manager reads tmxTileSets to load content from them (animation, textures, etc).
    /// </summary>
    public class TmxManager
    {
        
        // The content manager
        private ContentManager Content { get; set; }

       // The extractors
        private AtlasGroupsExtractor AtlasGroupsExtractor { get; }
        private StripAnimExtractor StripAnimationExtractor { get; }
        private StripGroupsExtractor StripGroupsExtractor { get; }
        private TextureExtractor TextureExtractor { get; }
        private TileMapExtractor TileMapExtractor { get; }
        private PngFontExtractor FontExtractor { get; }

        // The builders
        private AtlasGroupBuilder AtlasGroupsBuilder { get; }
        private PngFontBuilder FontBuilder { get; }
        private StripAnimationBuilder StripAnimationBuilder { get; }
        private StripGroupBuilder StripGroupsBuilder { get; }
        private TexturesBuilder TexturesBuilder { get; }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="pContent"></param>
        public TmxManager(ContentManager pContent)
        {
            Content = pContent;

            AtlasGroupsExtractor = new AtlasGroupsExtractor();
            FontExtractor = new PngFontExtractor();
            StripAnimationExtractor = new StripAnimExtractor();
            StripGroupsExtractor = new StripGroupsExtractor();
            TextureExtractor = new TextureExtractor();
            TileMapExtractor = new TileMapExtractor();

            AtlasGroupsBuilder = new AtlasGroupBuilder();
            FontBuilder = new PngFontBuilder();
            StripAnimationBuilder = new StripAnimationBuilder();
            StripGroupsBuilder = new StripGroupBuilder();
            TexturesBuilder = new TexturesBuilder();
        }



        /// <summary>
        /// Populates the content dictionaries.
        /// </summary>
        /// <param name="pState"></param>
        /// <param name="pPath"></param>
        public void LoadContent(ImporterState pState, string pPath)
        {
            switch (pState)
            {
                case ImporterState.AtlasGroup:
                    AtlasGroupsBuilder.Set(AtlasGroupsExtractor.Extract(TileSet(pPath)));
                    break;

                case ImporterState.StripAnim:
                    StripAnimationBuilder.Set(StripAnimationExtractor.Extract(TileSet(pPath)));
                    break;

                case ImporterState.StripGroup:
                    StripGroupsBuilder.Set(StripGroupsExtractor.Extract(TileSet(pPath)));
                    break;

                case ImporterState.Texture:
                    TexturesBuilder.Set(TextureExtractor.Extract(TileSet(pPath)));
                    break;

                case ImporterState.PngFont:
                    FontBuilder.Set(FontExtractor.Extract(TileSet(pPath)));
                    break;
            }
        }

        /// <summary>
        /// Returns a new AtlasDataExtracted, which is an atlas animation package.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public AtlasExtracted GetAtlasGroup(string pId)
        {
            return AtlasGroupsBuilder.Get(pId, Content);
        }

        /// <summary>
        /// Returns a new AtlasDataExtracted, which is an atlas animation package.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public Dictionary<string, StripAnimationItem> GetStripGroup(string pId)
        {
            return StripGroupsBuilder.Get(pId, Content);
        }

        /// <summary>
        /// Returns a new AtlasDataExtracted, which is an atlas animation package.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public Dictionary<string, StripAnimationItem> GetStripAnim(string pId)
        {
            return StripAnimationBuilder.Get(pId, Content);
        }

        /// <summary>
        /// Returns a package wil all the tileMap data.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns></returns>
        public TileMapDataPack GetMapData(string pPath)
        {
            return TileMapExtractor.Extract(pPath, Content);
        }

        /// <summary>
        /// Returns a new AtlasDataExtracted, which is an atlas animation package.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public TexturePack GetTexture(string pId)
        {
            return TexturesBuilder.Get(pId, Content);
        }

        /// <summary>
        /// Takes a texture source from a tmxTileSet and returns the trimmed path
        /// </summary>
        /// <param name="pSource"></param>
        /// <returns></returns>
        public static string TexturePath(string pSource)
        {
            var source = pSource.Split('/');
            var sB = new StringBuilder();
            for (var i = 0; i < source.Length; i++)
            {
                if (i == 0)
                    continue;
                sB.Append(source[i]);
                if (i < source.Length - 1)
                    sB.Append("/");
            }

            var dotTrim = sB.ToString().Split('.');
            return dotTrim[0];
        }

        /// <summary>
        /// Returns a dictionary of tmxTileSets.
        /// </summary>
        /// <param name="pPath"></param>
        /// <returns></returns>
        private Dictionary<string, TmxTileSet> TileSet(string pPath)
        {
            var map = new TmxMap(pPath);
            Dictionary<string, TmxTileSet> tS = new Dictionary<string, TmxTileSet>();

            for (int i = 0; i < map.Tilesets.Count; i++)
            {
                tS.Add(map.Tilesets[i].Name, map.Tilesets[i]);
            }

            return tS;
        }
    }
}